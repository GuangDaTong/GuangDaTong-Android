package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.school

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListSchoolFavoriteAll
import com.guangdamiao.www.mew_android_debug.navigation.school.SchoolWebView
import es.dmoral.toasty.Toasty
import java.util.*

class School_ListAdpter(protected var context: Context, list_guidance: ArrayList<ListSchoolFavoriteAll>) : BaseAdapter() {

    private val mInflater:LayoutInflater
    private var mContext:Context?=null
    private var list_guidance:ArrayList<ListSchoolFavoriteAll>?=null
    private var onShowItemClickListener: School_ListAdpter.OnShowItemClickListener?=null


    init {
        this.mContext=context
        this.list_guidance = list_guidance
        this.mInflater= LayoutInflater.from(mContext)
    }

    override fun getCount(): Int {
        return list_guidance!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        var holder: School_ListAdpter.ViewHolder

        if (convertView == null) {
            holder= School_ListAdpter.ViewHolder()
            convertView = LayoutInflater.from(context).inflate(R.layout.school_list, parent, false)
            holder.school_list_status=convertView.findViewById(R.id.school_list_status) as ImageView
            holder.school_list_content=convertView.findViewById(R.id.school_list_content) as TextView
            holder.school_favorite_choose_cb=convertView.findViewById(R.id.school_favorite_choose_cb) as CheckBox
            holder.school_list_to=convertView.findViewById(R.id.school_list_to) as ImageView
            holder.school_favorite_rl=convertView.findViewById(R.id.school_favorite_rl) as RelativeLayout

            convertView.tag=holder
        }else{
            holder=convertView.tag as School_ListAdpter.ViewHolder
        }
        val listGuidance = list_guidance!!.get(position)//获取一个


        holder.school_list_status!!.visibility = View.GONE
        holder.school_list_content!!.setText(listGuidance.title)
        if(listGuidance.title!!.length>18){
            holder.school_list_content!!.setText(listGuidance!!.title!!.substring(0,18)+"...")
        }


        val link = listGuidance.link
        val webView_id = listGuidance.id
        val zan = listGuidance.zan_
        val isZan = listGuidance.isZan
        val readCount = listGuidance.readCount
        holder.school_favorite_rl!!.setOnClickListener {
            if(!listGuidance.isShow){
                val intent = Intent(context, SchoolWebView::class.java)//=====跳转到WebView
                val bundle = Bundle()
                bundle.putString("id", webView_id)
                if(webView_id.equals("")){
                    Toasty.info(context,"亲，该内容已被删除了哦~").show()
                }else{
                    bundle.putString("link", link)
                    bundle.putString("zan", zan)
                    bundle.putString("isZan", isZan)
                    bundle.putString("readCount", readCount)
                    bundle.putString("isFavorite", "1")
                    bundle.putString("urlEnd", listGuidance.urlEnd)
                    bundle.putString("title_first", listGuidance.title)
                    intent.putExtras(bundle)
                    context.startActivity(intent)
                }
            }
        }

        if(listGuidance.isShow!!){
            holder.school_favorite_choose_cb!!.visibility=View.VISIBLE
            holder.school_list_to!!.visibility=View.GONE
            holder.school_favorite_rl!!.setOnClickListener{
                if(!listGuidance.isChecked!!){
                    listGuidance.isChecked=true
                    holder.school_favorite_choose_cb!!.isChecked=true
                }else{
                    holder.school_favorite_choose_cb!!.isChecked=false
                    listGuidance.isChecked=false
                }
                onShowItemClickListener!!.onShowItemClick(listGuidance)
            }
        }else{
            holder.school_favorite_choose_cb!!.visibility=View.GONE
            holder.school_list_to!!.visibility=View.VISIBLE
        }


        holder.school_favorite_choose_cb!!.isChecked=listGuidance.isChecked!!


        return convertView!!
    }

    class ViewHolder{
        var school_list_status:ImageView?=null
        var school_list_content:TextView?=null
        var school_favorite_choose_cb:CheckBox?=null
        var school_list_to:ImageView?=null
        var school_favorite_rl:RelativeLayout?=null
    }

    interface OnShowItemClickListener {
        fun onShowItemClick(bean: ListSchoolFavoriteAll)
    }

    fun setOnShowItemClickListener(onShowItemClickListener: OnShowItemClickListener) {
        this.onShowItemClickListener = onShowItemClickListener
    }

}
