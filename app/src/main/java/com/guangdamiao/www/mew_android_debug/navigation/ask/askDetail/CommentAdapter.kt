package com.guangdamiao.www.mew_android_debug.navigation.ask.askDetail

import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.CommentConfig
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.CommentItem
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskMainRequest
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskReport
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconSelfInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.presenter.AskDetailPresenter
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.CustomPopWindow
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.nostra13.universalimageloader.core.ImageLoader
import es.dmoral.toasty.Toasty

class CommentAdapter{

    private var mContext: Context? = null
    private var mListview: CommentListView? = null
    var mDatas: ArrayList<CommentItem>? = null
    private var holder:ViewHolder?=ViewHolder()
    private var ask_publisher_id=""
    private var isAdopt=false
    private var isAdoptID=""
    private var mPresenter:AskDetailPresenter?=null
    private var popWindow_comment:CustomPopWindow?=null
    private var popWindow_report: CustomPopWindow?=null
    var ask_three_reply:TextView?=null
    var ask_three_report:TextView?=null
    var ask_three_cancle:TextView?=null
    var ask_report_five1:TextView?=null
    var ask_report_five2:TextView?=null
    var ask_report_five3:TextView?=null
    var ask_report_five4:TextView?=null
    var ask_report_cancle:TextView?=null

    constructor(context:Context){
        mContext=context
        mDatas= ArrayList<CommentItem>()
    }

    constructor(context:Context,datas:ArrayList<CommentItem>){
        mContext=context
        mDatas=datas
    }

    fun bindListView(listView: CommentListView?) {
        if (listView == null) {
            throw IllegalArgumentException("CommentListView is null....")
        }
        mListview = listView
    }

    fun set_Datas(datas: ArrayList<CommentItem>,ask_publisher_id:String,isAdopt:Boolean,isAdoptCommentID:String,mPresenter: AskDetailPresenter) {//数据转移，在AskDetail要用，AskDetail是一个活动
        
        if(datas!=null){
            this.mDatas = datas
            this.ask_publisher_id=ask_publisher_id
            this.mPresenter=mPresenter
            this.isAdopt=isAdopt
            this.isAdoptID=isAdoptCommentID
        }else{
            this.isAdopt=isAdopt
            this.isAdoptID=isAdoptCommentID
            this.ask_publisher_id=ask_publisher_id
            this.mDatas=ArrayList<CommentItem>()
            this.mPresenter=mPresenter
        }
    }

    fun get_Datas():ArrayList<CommentItem>{
        return mDatas!!
    }


    fun getCount(): Int {
        if(mDatas==null){
            return 0
        }
        return mDatas!!.size
    }

    fun getItemId(position: Int): Long {
        return position.toLong()
    }


    fun getItem(position: Int): CommentItem? {
        if (mDatas == null) {
            return null
        }
        if (position < mDatas!!.size) {
            return mDatas!![position]
        } else {
            throw ArrayIndexOutOfBoundsException()
        }
    }

    fun getView(position: Int): View? {

        val convertView = View.inflate(mContext, R.layout.ask_item_comment, null)
        //初始化
        holder!!.ask_list_icon=convertView.findViewById(R.id.ask_list_comment_icon) as ImageView
        holder!!.ask_list_comment_nick_tv=convertView.findViewById(R.id.ask_list_comment_nick_tv) as TextView
        holder!!.ask_list_comment_sex_tv=convertView.findViewById(R.id.ask_list_comment_sex_tv) as TextView
        holder!!.ask_list_comment_adopt_btn=convertView.findViewById(R.id.ask_list_comment_adopt_btn) as Button
        holder!!.ask_list_comment_delete_btn=convertView.findViewById(R.id.ask_list_comment_delete_btn) as Button
        holder!!.ask_list_commment_time=convertView.findViewById(R.id.ask_list_comment_time) as TextView
        holder!!.ask_list_comment_tv=convertView.findViewById(R.id.ask_list_comment_tv) as TextView
        holder!!.ask_list_comment_ll=convertView.findViewById(R.id.ask_list_comment_ll) as LinearLayout 


        val bean = mDatas!![position]
        //判断是否是自己的帖子
        //采纳
        val read =mContext!!.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val id = read.getString("id", "")
        val token=read.getString("token","")

        //头像
        getIcon(bean.icon!!,holder!!.ask_list_icon!!)

        //点击了头像
        holder!!.ask_list_icon!!.setOnClickListener{
            //自己的头像
            if(id!!.equals(bean.publisherID)){
                val intent:Intent=Intent(mContext, IconSelfInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",bean.publisherID)
                bundle.putString("icon",bean.icon)
                bundle.putString("sex",bean.gender)
                bundle.putString("nickname",bean.nickname)
                intent.putExtras(bundle)
                mContext!!.startActivity(intent)
            }else{
                val intent:Intent=Intent(mContext, IconInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",bean.publisherID)
                bundle.putString("icon",bean.icon)
                bundle.putString("sex",bean.gender)
                bundle.putString("nickname",bean.nickname)
                intent.putExtras(bundle)
                mContext!!.startActivity(intent)
            }
        }


        //昵称
        holder!!.ask_list_comment_nick_tv!!.text=bean!!.nickname
        var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
        holder!!.ask_list_comment_nick_tv!!.measure(spec,spec)
        var nickwidth= holder!!.ask_list_comment_nick_tv!!.measuredWidth
        LogUtils.d_debugprint(Constant.Ask_TAG,"\n\n现在nickname为：${bean!!.nickname} \n\n 长度为：${nickwidth} \n\n转换后的137.0f为：${DensityUtil.dip2px(mContext,137.0f)} ")
        var nicklength=bean!!.nickname!!.length
        while(nickwidth>DensityUtil.dip2px(mContext,137.0f)){
            nicklength-=2
            val name_display=bean!!.nickname!!.substring(0,nicklength)+"..."
            holder!!.ask_list_comment_nick_tv!!.text=name_display
            var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
            holder!!.ask_list_comment_nick_tv!!.measure(spec,spec)
            nickwidth= holder!!.ask_list_comment_nick_tv!!.measuredWidth
            LogUtils.d_debugprint(Constant.Ask_TAG,"\n\n现在nicklength为：$nicklength 宽度为：$nickwidth\n\n")
        }

        //性别
        if (!bean.gender.equals("")&&bean.gender!=null){
            if(bean.gender.equals("女")){
                holder!!.ask_list_comment_sex_tv!!.text="♀"
                holder!!.ask_list_comment_sex_tv!!.setTextColor(mContext!!.resources.getColor(R.color.ask_red))
            }else{
                holder!!.ask_list_comment_sex_tv!!.text="♂"
                holder!!.ask_list_comment_sex_tv!!.setTextColor(mContext!!.resources.getColor(R.color.headline))
            }
        }else{
            holder!!.ask_list_comment_sex_tv!!.visibility=View.GONE
        }

       
        if(id.equals(ask_publisher_id)&&!bean.publisherID!!.equals(id)&&!isAdopt){
            //如果是用户自己发的帖子，并且帖子评论人不是自己，并且没有被采纳，显示采纳按钮
            holder!!.ask_list_comment_adopt_btn!!.visibility=View.VISIBLE
            holder!!.ask_list_comment_delete_btn!!.visibility=View.GONE
        }else if(id.equals(ask_publisher_id)&&bean.publisherID!!.equals(id)){
            //如果是用户自己发的帖子，并且帖子评论人是自己，不需要是否采纳，显示删除，因为自己的评论无法被自己采纳的
            holder!!.ask_list_comment_adopt_btn!!.visibility=View.GONE
            holder!!.ask_list_comment_delete_btn!!.visibility=View.VISIBLE
        }else if(!id.equals(ask_publisher_id)&&bean.publisherID!!.equals(id)&&!bean.id!!.equals(isAdoptID)){
            //如果不是自己发的帖子，但是评论是自己的，并且这条评论没有被删除
            holder!!.ask_list_comment_adopt_btn!!.visibility=View.GONE
            holder!!.ask_list_comment_delete_btn!!.visibility=View.VISIBLE
        }else{
            //路人什么都不显示
            holder!!.ask_list_comment_adopt_btn!!.visibility=View.GONE
            holder!!.ask_list_comment_delete_btn!!.visibility=View.GONE
        }

        //时间
        val time = bean.createTime!!.substring(2, 16).replace("T", " ")
        holder!!.ask_list_commment_time!!.text=time

        //内容
        var list_content=bean.content!!
        if(!id.equals(bean.commentReceiverID)&&!ask_publisher_id.equals(bean.commentReceiverID)&&!bean.commentReceiverID.equals(ask_publisher_id)&&!bean.publisherID!!.equals(bean.commentReceiverID)){
            //如果被评论人不是发帖人，就会显示回复，而且不是回复自己

            list_content="@"+bean.commentReceiverNickname+":"+list_content
            val builder=SpannableStringBuilder(list_content)
            val buleSpan=ForegroundColorSpan(mContext!!.resources.getColor(R.color.headline))
            builder.setSpan(buleSpan,1,1+bean.commentReceiverNickname!!.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

            holder!!.ask_list_comment_tv!!.text=builder
        }else if(id.equals(bean.commentReceiverID)&&!ask_publisher_id.equals(bean.commentReceiverID)&&!bean.commentReceiverID.equals(ask_publisher_id)&&!bean.publisherID.equals(id)){
            //如果是被人回复自己的评论，不是回复楼主,也不是自己评论自己
            list_content="@我"+":"+list_content
            val builder=SpannableStringBuilder(list_content)
            val buleSpan=ForegroundColorSpan(mContext!!.resources.getColor(R.color.headline))
            builder.setSpan(buleSpan,1,2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            holder!!.ask_list_comment_tv!!.text=builder
        }else{
            holder!!.ask_list_comment_tv!!.text=list_content
        }

        holder!!.ask_list_comment_tv!!.setOnLongClickListener{
            val cmb=mContext!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            cmb.text= bean.content!!
            Toasty.info(mContext!!,"成功复制该评论内容！").show()
            true
        }

        //点击了评论空白处，弹出键盘
        holder!!.ask_list_comment_ll!!.setOnClickListener{
            showComentDialog(bean,position)
        }

        //点击了评论内容，也弹出键盘
        holder!!.ask_list_comment_tv!!.setOnClickListener{
            showComentDialog(bean,position)
        }

        //点击了删除按钮
        holder!!.ask_list_comment_delete_btn!!.setOnClickListener{
            showDeleteDialog(bean,position)
        }

        //点击了采纳按钮
        holder!!.ask_list_comment_adopt_btn!!.setOnClickListener{
            showAdoptDialog(bean,position)
        }
        
        return convertView
    }

    fun showAdoptDialog(bean:CommentItem,position: Int){
        SweetAlertDialog(mContext, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setCustomImage(R.mipmap.mew_icon)
                .setTitleText("提示")
                .setContentText("亲，采纳这条评论，采纳之后无法更改喔！")
                .setCancelText("再想想")
                .setConfirmText("我确定")
                .showCancelButton(true)
                .setCancelClickListener(object:SweetAlertDialog.OnSweetClickListener {
                    override fun onClick(sweetAlertDialog: SweetAlertDialog?) {
                        sweetAlertDialog!!.cancel()
                    }
                })
                .setConfirmClickListener(object:SweetAlertDialog.OnSweetClickListener{
                    override fun onClick(sweetAlertDialog: SweetAlertDialog?) {
                        sweetAlertDialog!!.cancel()
                        mPresenter!!.adoptComment(mContext,bean,position)
                    }
                })
                .show()
    }

    fun showDeleteDialog(bean:CommentItem,position: Int){

        SweetAlertDialog(mContext, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
            .setCustomImage(R.mipmap.mew_icon)
            .setTitleText("提示")
            .setContentText("删除这条评论？")
            .setCancelText("再想想")
            .setConfirmText("我确定")
            .showCancelButton(true)
            .setCancelClickListener(object:SweetAlertDialog.OnSweetClickListener {
                override fun onClick(sweetAlertDialog: SweetAlertDialog?) {
                    sweetAlertDialog!!.cancel()
                }
            })
            .setConfirmClickListener(object:SweetAlertDialog.OnSweetClickListener{
                override fun onClick(sweetAlertDialog: SweetAlertDialog?) {
                    sweetAlertDialog!!.cancel()
                    mPresenter!!.deleteComment(mContext,bean,position)
                }
            })
            .show()
    }


    fun showComentDialog(bean:CommentItem,position:Int){
        val contentView:View= LayoutInflater.from(mContext).inflate(R.layout.ask_detail_report_dialog,null)
        hanleDialog(contentView,bean,position)
        popWindow_comment= CustomPopWindow.PopupWindowBuilder(mContext)
                .setView(contentView)
                .enableBackgroundDark(true)
                .setBgDarkAlpha(0.7f)
                .setFocusable(true)
                .setOutsideTouchable(true)
                .setAnimationStyle(R.style.ask_three_anim)
                .create()
        popWindow_comment!!.showAtLocation( holder!!.ask_list_comment_ll, Gravity.CENTER_HORIZONTAL,0, DensityUtil.dip2px(mContext,260.0f))
    }

    private fun hanleDialog(contentView:View,bean:CommentItem,position: Int){
        ask_three_reply=contentView.findViewById(R.id.ask_three_reply) as TextView
        ask_three_report=contentView.findViewById(R.id.ask_three_report) as TextView
        ask_three_cancle=contentView.findViewById(R.id.ask_three_cancle) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow_comment!=null){
                    popWindow_comment!!.dissmiss()
                }
                when(v!!.id){
                    R.id.ask_three_reply -> make_reply(bean,position)
                    R.id.ask_three_report   -> make_report(bean.id!!,bean.publisherID!!,bean.askID!!)
                    R.id.ask_three_cancle   -> popWindow_comment!!.dissmiss()
                }
            }
        }
        ask_three_reply!!.setOnClickListener(listener)
        ask_three_report!!.setOnClickListener(listener)
        ask_three_cancle!!.setOnClickListener(listener)
    }

    private fun make_reply(bean:CommentItem,position:Int){
        val token=get_token()
        if(token.equals("")){
            val intent = Intent(mContext, LoginMain::class.java)
            mContext!!.startActivity(intent)
        }else if(!NetUtil.checkNetWork(mContext)){
            Toasty.info(mContext!!,Constant.NONETWORK).show()
        }else{
            val comment2sb= CommentConfig()
            comment2sb.askID=bean.askID!!
            comment2sb.commentReceiverID=bean.publisherID!!
            comment2sb.commentType= CommentConfig.Type.REPLY
            comment2sb.token=token
            comment2sb.hint="@"+bean.nickname+":"
            comment2sb.commentPosition=position
            mPresenter!!.showEditTextBody(comment2sb)
        }
    }

    private fun make_report(id:String,publisherID:String,askID:String){
        val token=get_token()
        val read = mContext!!.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val current_id = read.getString("id", "")
        if(token!=""&&!current_id.equals(publisherID)){
            val contentView:View=LayoutInflater.from(mContext).inflate(R.layout.ask_five_report_dialog,null)
            hanle_report_Dialog(contentView,id)
            popWindow_report= CustomPopWindow.PopupWindowBuilder(mContext)
                    .setView(contentView)
                    .enableBackgroundDark(true)
                    .setBgDarkAlpha(0.7f)
                    .setFocusable(true)
                    .setOutsideTouchable(true)
                    .setAnimationStyle(R.style.ask_three_anim)
                    .create()
            popWindow_report!!.showAtLocation(holder!!.ask_list_comment_ll,Gravity.CENTER_HORIZONTAL,0,DensityUtil.dip2px(mContext,215.0f))

        }else if(current_id.equals(publisherID)){
            Toasty.info(mContext!!,"亲，不能举报自己的评论喔~").show()
        }
    }

    private fun hanle_report_Dialog(contentView:View,id: String){
        ask_report_five1=contentView.findViewById(R.id.ask_five_1) as TextView
        ask_report_five2=contentView.findViewById(R.id.ask_five_2) as TextView
        ask_report_five3=contentView.findViewById(R.id.ask_five_3) as TextView
        ask_report_five4=contentView.findViewById(R.id.ask_five_4) as TextView
        ask_report_cancle=contentView.findViewById(R.id.ask_five_cancle) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow_report!=null){
                    popWindow_report!!.dissmiss()
                }
                when(v!!.id){
                    R.id.ask_five_1 -> make_report(id,"欺诈骗钱")
                    R.id.ask_five_2 -> make_report(id,"色情暴力")
                    R.id.ask_five_3 -> make_report(id,"广告骚扰")
                    R.id.ask_five_4 -> make_report_other(id,"")
                    R.id.ask_five_cancle  -> popWindow_report!!.dissmiss()
                }
            }
        }
        ask_report_five1!!.setOnClickListener(listener)
        ask_report_five2!!.setOnClickListener(listener)
        ask_report_five3!!.setOnClickListener(listener)
        ask_report_five4!!.setOnClickListener(listener)
        ask_report_cancle!!.setOnClickListener(listener)
    }

    private fun make_report(id:String,reason:String){
        val token=get_token()
        //开始举报=====请求服务器
        if(token!=""){

            val pDialog = SweetAlertDialog(mContext, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(true)
            pDialog.show()
            AskMainRequest.makeInformFromServer(mContext!!, Constant.Ask_TAG, Constant.Inform_CommentAsk, id, token, reason, pDialog)

        }
    }

    private fun make_report_other(id:String,reson:String){
        val token=get_token()
        //开始收藏=====请求服务器
        if(token!=""){
            val intent = Intent(mContext, AskReport::class.java)
            val bundle= Bundle()
            bundle.putString("id",id)
            bundle.putString("token",token)
            intent.putExtras(bundle)
            mContext!!.startActivity(intent)
        }
    }

    private fun get_token():String{
        val read = mContext!!.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token= read.getString("token", "")
        if(token.equals("")){
            val intent = Intent(mContext, LoginMain::class.java)
            mContext!!.startActivity(intent)
        }else{
            return token
        }
        return ""
    }

    inner class ViewHolder{
       var ask_list_icon:ImageView?=null
       var ask_list_comment_nick_tv:TextView?=null
       var ask_list_comment_sex_tv:TextView?=null
       var ask_list_comment_adopt_btn:Button?=null
       var ask_list_comment_delete_btn:Button?=null
       var ask_list_commment_time:TextView?=null
       var ask_list_comment_tv:TextView?=null
       var ask_list_comment_ll:LinearLayout?=null 
    }

    private fun getIcon(icon:String,image:ImageView) {
        if (icon !== "") {
            if (LogUtils.APP_IS_DEBUG) {
                ImageLoader.getInstance().displayImage( icon!!, image)
            } else {
                ImageLoader.getInstance().displayImage( icon!!, image)
            }
        }
    }

     fun notifyDataSetChanged() {
        if (mListview == null) {
            throw NullPointerException("listview is null, please bindListView first...")
        }
        mListview!!.removeAllViews()
        if (mDatas == null || mDatas!!.size == 0) {
            return
        }
        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        for (i in mDatas!!.indices) {
            val index = i
            val view = getView(index) ?: throw NullPointerException("listview item layout is null, please check getView()...")

            mListview!!.addView(view, index, layoutParams)
        }

    }



}
