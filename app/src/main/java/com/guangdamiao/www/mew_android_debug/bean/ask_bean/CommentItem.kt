package com.guangdamiao.www.mew_android_debug.bean.ask_bean

import java.io.Serializable


class CommentItem : Serializable {
    var id: String? = null
    var askID:String?=null
    var commentReceiverID: String? = null
    var commentReceiverNickname: String? = null
    var content: String? = null
    var publisherID:String?=null
    var createTime:String?=null
    var icon:String?=null
    var gender:String?=null
    var nickname:String?=null

    constructor():super()
    constructor(id:String,askID:String,commentReceiverID:String,commentReceiverNickname: String,nickname:String,content:String,publisherID:String,createTime:String,
                icon:String,gender:String){
        this.id=id
        this.askID=askID
        this.commentReceiverID=commentReceiverID
        this.commentReceiverNickname=commentReceiverNickname
        this.publisherID=publisherID
        this.createTime=createTime
        this.icon=icon
        this.nickname=nickname
        this.content=content
        this.gender=gender
    }
    constructor(id:String,askID:String,commentReceiverID:String,commentReceiverNickname: String,nickname:String,content:String,publisherID:String,createTime:String,
                icon:String){
        this.id=id
        this.askID=askID
        this.commentReceiverID=commentReceiverID
        this.commentReceiverNickname=commentReceiverNickname
        this.publisherID=publisherID
        this.createTime=createTime
        this.icon=icon
        this.nickname=nickname
        this.content=content
    }

    override fun toString(): String {
        return "CommentItem[id=$id,\naskID=$askID,\ncreateTime=$createTime,\nicon=$icon,\nnickname=$nickname,\ncontent=$content,\ngender=$gender]"
    }
}
