package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Black

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MyBlackFriends
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import es.dmoral.toasty.Toasty

class Mine_Black : BaseActivity(),Main_BlackBookAdapter.OnShowItemClickListener {

    val TAG=Constant.Me_TAG
    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_textview: TextView?=null
    private var title_right_imageview: ImageView?=null
    private var title_right_imageview_btn: Button?=null

    var pageNow = 0
    var handler = Handler()
    var mListView: ListView? = null

    var adapter: Main_BlackBookAdapter? = null
    var ptrClassicFrameLayout: PtrClassicFrameLayout? = null

    private var isShow=false
    private var selectList=ArrayList<MyBlackFriends>()
    private var dataList=ArrayList<MyBlackFriends>()
    private var lay: LinearLayout?=null
    private var no_data_rl: RelativeLayout?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mine_black)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        MainApplication.getInstance().addActivity(this)
        initView()
        adapter = Main_BlackBookAdapter(this@Mine_Black, dataList)
        mListView!!.setAdapter(adapter!!)//关键代码
        adapter!!.setOnShowItemClickListener(this)
        addSomeListener()
        initData()
    }
    

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Mine_Black,"我的黑名单")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Mine_Black,"我的黑名单")
        super.onPause()
    }

    private fun initData() {



        ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(true) }, 150)

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    pageNow = 0
                    RequestServerList(pageNow,adapter!!,ptrClassicFrameLayout!!)
                    if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                        ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                    }
                }, 200)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener{
            handler.postDelayed({
                RequestServerList(++pageNow,adapter!!,ptrClassicFrameLayout!!)
            }, 200)
        }
    }


    private fun RequestServerList(pageNow: Int, adapter: Main_BlackBookAdapter, ptrClassicFrameLayout: PtrClassicFrameLayout) {
        BlackRequest.getResultFromServer(this@Mine_Black,pageNow,dataList,adapter,ptrClassicFrameLayout,object: IDataRequestListener2String {
            override fun loadSuccess(response_string: String?) {
                title_right_textview!!.visibility= View.VISIBLE
                title_right_imageview_btn!!.visibility= View.VISIBLE
                if("noFriends".equals(response_string)){
                    title_right_textview!!.text="删除"
                    title_right_textview!!.visibility= View.GONE
                    no_data_rl!!.visibility= View.VISIBLE
                }else{
                    no_data_rl!!.visibility= View.GONE
                }
            }
        })
    }


    fun initView(){
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_textview=findViewById(R.id.title_right_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        no_data_rl=findViewById(R.id.no_data_rl) as RelativeLayout
        mListView=findViewById(R.id.mine_friends) as ListView
        title_center_textview!!.text="我的黑名单"
        title_right_textview!!.text="删除"
        title_right_imageview!!.visibility= View.GONE
        title_right_imageview_btn!!.visibility= View.GONE
        title_center_textview!!.visibility= View.VISIBLE
        title_right_textview!!.visibility= View.GONE
        ptrClassicFrameLayout=findViewById(R.id.list_Black_frame) as PtrClassicFrameLayout
        lay=findViewById(R.id.lay) as LinearLayout
    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f


            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f

            }
            false
        }

        title_right_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f


            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f

            }
            false
        }

        title_right_imageview_btn!!.setOnClickListener{
            title_right_textview!!.text=""
            //刷新列表，处理删除的情况
            if(isShow){
                isShow=false
            }else{
                isShow=true
                for(bean in dataList){
                    bean.isShow=true
                }
                adapter!!.notifyDataSetChanged()
                showOpervate()
                title_right_imageview_btn!!.isClickable=false
            }
        }


    }

    override fun onShowItemClick(bean: MyBlackFriends) {
        if (bean.isChecked!! && !selectList.contains(bean)) {
            selectList.add(bean)
        } else if (!bean.isChecked!! && selectList.contains(bean)) {
            selectList.remove(bean)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        if (isShow) {
            selectList.clear()
            for (bean in dataList) {
                bean.isChecked=false
                bean.isShow=false
            }
            isShow = false
            title_right_textview!!.text="删除"
            title_right_textview!!.visibility= View.VISIBLE
            title_right_imageview_btn!!.isClickable=true
            adapter!!.notifyDataSetChanged()
            dismissOperate()
        } else {
            super.onBackPressed()
        }
    }

    /**
     * 显示操作界面
     */
    private fun showOpervate() {
        lay!!.visibility= View.VISIBLE
        val anim = AnimationUtils.loadAnimation(this, R.anim.operate_in)
        lay!!.animation=anim
        // 返回、删除、全选和反选按钮初始化及点击监听
        val tvBack = findViewById(R.id.operate_back) as TextView
        val tvDelete = findViewById(R.id.operate_delete) as TextView
        val tvSelect = findViewById(R.id.operate_select) as TextView
        val tvInvertSelect = findViewById(R.id.invert_select) as TextView

        tvBack.setOnClickListener {
            if (isShow) {
                selectList.clear()
                for (bean in dataList) {
                    bean.isChecked=false
                    bean.isShow=false
                }
                isShow = false
                title_right_textview!!.text="删除"
                title_right_textview!!.visibility= View.VISIBLE
                title_right_imageview_btn!!.isClickable=true
                adapter!!.notifyDataSetChanged()
                dismissOperate()
            }
        }
        tvSelect.setOnClickListener {
            for (bean in dataList) {
                if (!bean.isChecked!!) {
                    bean.isChecked=true
                    if (!selectList.contains(bean)) {
                        selectList.add(bean)
                    }
                }
            }
            adapter!!.notifyDataSetChanged()
        }
        tvInvertSelect.setOnClickListener {
            for (bean in dataList) {
                if (!bean.isChecked!!) {
                    bean.isChecked=true
                    if (!selectList.contains(bean)) {
                        selectList.add(bean)
                    }
                } else {
                    bean.isChecked=false
                    if (selectList.contains(bean)) {
                        selectList.remove(bean)
                    }
                }
            }
            adapter!!.notifyDataSetChanged()
        }
        tvDelete.setOnClickListener {
            if(lay!=null) dismissOperate()
            if (selectList != null && selectList.size > 0) {
                val IDs=ArrayList<String>()
                for(i in selectList.indices){
                    IDs.add(selectList[i].id!!)
                }
                val token=get_token()
                if(!token.equals("")){
                    //这里进行数据库的删除
                    val pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                    pDialog.progressHelper.barColor = Color.parseColor("#A5DC86")
                    pDialog.titleText = "Loading"
                    pDialog.setCancelable(false)
                    pDialog.show()

                    BlackRequest.deleteFriends(this@Mine_Black,IDs,token,object:IDataRequestListener2String{
                        override fun loadSuccess(response_string: String?) {
                            if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                                if(pDialog!=null) pDialog.cancel()
                                if (isShow) {
                                    selectList.clear()
                                    for (bean in dataList) {
                                        bean.isChecked=false
                                        bean.isShow=false
                                    }
                                    isShow = false
                                    title_right_textview!!.text="删除"
                                    title_right_textview!!.visibility= View.VISIBLE
                                    title_right_imageview_btn!!.isClickable=true
                                    adapter!!.notifyDataSetChanged()
                                    dismissOperate()
                                }
                                val pDialog2= SweetAlertDialog(this@Mine_Black, SweetAlertDialog.SUCCESS_TYPE)
                                pDialog2.setTitleText("删除成功")
                                pDialog2.setContentText("亲，您已经取消拉黑TA了")
                                pDialog2.show()
                                val handler= Handler()
                                handler.postDelayed({
                                    pDialog2.cancel()
                                },618)
                            }else if(!JsonUtil.get_key_string("msg",response_string).equals("")){
                                if(pDialog!=null) pDialog.cancel()
                                if (isShow) {
                                    selectList.clear()
                                    for (bean in dataList) {
                                        bean.isChecked=false
                                        bean.isShow=false
                                    }
                                    isShow = false
                                    title_right_textview!!.text="删除"
                                    title_right_textview!!.visibility= View.VISIBLE
                                    title_right_imageview_btn!!.isClickable=true
                                    adapter!!.notifyDataSetChanged()
                                    dismissOperate()
                                }
                                val pDialog2= SweetAlertDialog(this@Mine_Black, SweetAlertDialog.ERROR_TYPE)
                                pDialog2.setTitleText("删除失败")
                                pDialog2.setContentText("亲，${JsonUtil.get_key_string("msg",response_string)}")
                                pDialog2.show()
                                val handler= Handler()
                                handler.postDelayed({
                                    pDialog2.cancel()
                                },618)
                            }
                        }
                    })
                    dataList.removeAll(selectList)
                    adapter!!.notifyDataSetChanged()
                    selectList.clear()
                }
            } else {
                Toasty.info(this@Mine_Black,"亲，您还没有选择要删除的拉黑用户喔").show()
                if (isShow) {
                    selectList.clear()
                    for (bean in dataList) {
                        bean.isChecked=false
                        bean.isShow=false
                    }
                    isShow = false
                    title_right_textview!!.text="删除"
                    title_right_textview!!.visibility= View.VISIBLE
                    title_right_imageview_btn!!.isClickable=true
                    adapter!!.notifyDataSetChanged()
                    dismissOperate()
                }
            }
        }
    }

    private fun get_token():String{
        val read = this@Mine_Black.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token= read.getString("token", "")
        if(token.equals("")){
            val intent = Intent(this@Mine_Black, LoginMain::class.java)
            this@Mine_Black.startActivity(intent)
        }else{
            return token
        }
        return ""
    }

    /**
     * 隐藏操作界面
     */
    private fun dismissOperate() {
        val anim = AnimationUtils.loadAnimation(this@Mine_Black, R.anim.operate_out)
        lay!!.visibility= View.GONE
        lay!!.animation=anim
    }
}
