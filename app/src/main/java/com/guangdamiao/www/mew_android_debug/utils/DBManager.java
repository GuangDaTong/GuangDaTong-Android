package com.guangdamiao.www.mew_android_debug.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

/**
 * Created by Jason_Jan on 2017/12/22.
 */

public class DBManager {
    private static DBManager dbManager;
    private Context context;

    private DBManager(Context context) {
        this.context = context;
    }

    public static DBManager getInstance(Context context) {
        if (null == dbManager) {
            dbManager = new DBManager(context);
        }
        return dbManager;
    }

    /**
     * @param
     * @description 通过执行sql语句来操作数据库
     * @author jason
     */
    public void execSql(SQLiteDatabase db, String sql) {
        if (null != db && !TextUtils.isEmpty(sql)) {
            db.execSQL(sql);
        }
        db.close();
    }
}
