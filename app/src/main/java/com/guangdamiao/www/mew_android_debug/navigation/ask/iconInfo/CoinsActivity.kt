package com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.JEventUtils
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import es.dmoral.toasty.Toasty

class CoinsActivity : BaseActivity()
{
    

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_textview: TextView? = null
    private var title_right_imageview: ImageView?=null
    private var title_right_imageview_btn:Button?=null

    private var coins_message_et: EditText?=null
    private var coins_word_num_tv: TextView?=null
    private var coins_moneyNum_et:EditText?=null
    private var coins_remin_num= Constant.EDIT_MAX_WORDS
    private var getBundle:Bundle?=null

    private var receiverID=""
    private var token=""
    private var centerWords=""

    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coins)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        getBundle=intent.extras
        initView()
        addSomeListener()
    }

    override fun onResume(){
        if(centerWords.equals(Constant.Give_Coins)){
            JAnalyticsInterface.onPageStart(this@CoinsActivity,"金币赠送")
        }else if(centerWords.equals(Constant.Want_Coins)){
            JAnalyticsInterface.onPageStart(this@CoinsActivity,"金币赠送")
        }
        super.onResume()
    }

    override fun onPause(){
        if(centerWords.equals(Constant.Give_Coins)){
            JAnalyticsInterface.onPageEnd(this@CoinsActivity,"金币索要")
        }else if(centerWords.equals(Constant.Want_Coins)){
            JAnalyticsInterface.onPageEnd(this@CoinsActivity,"金币索要")
        }
        super.onPause()
    }

    fun initView() {
        if(getBundle!=null){
            receiverID=getBundle!!.getString("receiverID")
            token=getBundle!!.getString("token")
            centerWords=getBundle!!.getString("centerWords")
        }
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_textview = findViewById(R.id.title_right_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        coins_moneyNum_et=findViewById(R.id.coins_moneyNum_et) as EditText
        title_right_imageview!!.visibility= View.GONE
        title_center_textview!!.text = centerWords
        title_right_textview!!.text = "完成"
        title_center_textview!!.visibility = View.VISIBLE
        title_right_textview!!.visibility = View.VISIBLE
        title_right_imageview_btn!!.visibility=View.VISIBLE
        coins_message_et=findViewById(R.id.coins_message_et) as EditText
        coins_word_num_tv=findViewById(R.id.coins_word_num_tv) as TextView

    }

    fun addSomeListener() {
        title_left_imageview_btn!!.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f
            }
            false
        }

        val editChangedListener=EditChangedListener()
        coins_message_et!!.addTextChangedListener(editChangedListener)

        title_right_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_imageview_btn!!.alpha=0.618f
                title_right_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))


            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_imageview_btn!!.alpha=1.0f
                title_right_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }

        //点击右侧的完成，请求服务器
        title_right_imageview_btn!!.setOnClickListener{


            var coins_num_edit=coins_moneyNum_et!!.text.toString()
            if(coins_num_edit.equals("")){
                Toasty.info(this@CoinsActivity,"亲，您还没有输入金币数量喔~").show()
            }else if(coins_num_edit.length>1&&coins_num_edit[0].equals('0')&&coins_num_edit[1].equals(coins_num_edit[1])){
                Toasty.info(this@CoinsActivity,"您输入的金币数量不合法！").show()
            }else if(coins_num_edit.equals("0")) {
                Toasty.info(this@CoinsActivity, "亲，金币数量不能为0喔~").show()
            }else{
                if(!receiverID.equals("")&&!token.equals("")){
                    //赠送
                    if(centerWords.equals(Constant.Give_Coins)){
                        JEventUtils.onCountEvent(this@CoinsActivity,Constant.Event_GiveCoins)//赠送金币次数统计
                        val pDialog1 =SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog1.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                        pDialog1.setTitleText("Loading")
                        pDialog1.setCancelable(true)
                        pDialog1.show()
                        val amount=coins_num_edit
                        val words=coins_message_et!!.text.toString()
                        IconInfoRequest.requestGiveCoins(this@CoinsActivity,receiverID,amount,words,token,object:IDataRequestListener2String{
                            override fun loadSuccess(response_string: String?) {
                                if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                                    pDialog1.cancel()
                                    LogUtils.d_debugprint(Constant.Icon_Info_TAG,"个人信息中：赠送金币成功！！！\n")
                                    val pDialog= SweetAlertDialog(this@CoinsActivity, SweetAlertDialog.SUCCESS_TYPE)
                                    pDialog.setTitleText("提示")
                                    pDialog.setContentText("亲，您已经成功赠送TA金币")
                                    pDialog.show()
                                    val handler= Handler()
                                    handler.postDelayed({
                                        pDialog.cancel()
                                        finish()
                                        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
                                    },1236)
                                }else{
                                    if(JsonUtil.get_key_string("msg",response_string)!=null){
                                        pDialog1.cancel()
                                        val pDialog=SweetAlertDialog(this@CoinsActivity, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        pDialog.setCustomImage(R.mipmap.mew_icon)
                                        pDialog.setTitleText("提示")
                                        pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                                        pDialog.show()
                                        val handler= Handler()
                                        handler.postDelayed({
                                            pDialog.cancel()
                                        },1236)
                                    }
                                }
                            }
                        })
                    }
                    //索要
                    if(centerWords.equals(Constant.Want_Coins)){
                        JEventUtils.onCountEvent(this@CoinsActivity,Constant.Event_WantCoins)//索要金币次数统计
                        val pDialog1 =SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                        pDialog1.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"))
                        pDialog1.setTitleText("Loading")
                        pDialog1.setCancelable(true)
                        pDialog1.show()
                        val amount=coins_num_edit
                        val words=coins_message_et!!.text.toString()
                        IconInfoRequest.requestWantCoins(this@CoinsActivity,receiverID,amount,words,token,object:IDataRequestListener2String{
                            override fun loadSuccess(response_string: String?) {
                                if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                                    LogUtils.d_debugprint(Constant.Icon_Info_TAG,"个人信息中：索要金币成功！！！\n")
                                    pDialog1.cancel()
                                    val pDialog= SweetAlertDialog(this@CoinsActivity, SweetAlertDialog.SUCCESS_TYPE)
                                    pDialog.setTitleText("提示")
                                    pDialog.setContentText("亲，您已经成功向TA索要金币")
                                    pDialog.show()
                                    val handler= Handler()
                                    handler.postDelayed({
                                        pDialog.cancel()
                                        finish()
                                        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
                                    },1236)
                                }else{
                                    if(JsonUtil.get_key_string("msg",response_string)!=null){
                                        pDialog1.cancel()
                                        val pDialog=SweetAlertDialog(this@CoinsActivity, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        pDialog.setCustomImage(R.mipmap.mew_icon)
                                        pDialog.setTitleText("提示")
                                        pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                                        pDialog.show()
                                        val handler= Handler()
                                        handler.postDelayed({
                                            pDialog.cancel()
                                        },1236)
                                    }
                                }
                            }
                        })
                    }
                }
            }
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish()
            overridePendingTransition(0, R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    //点击空白隐藏键盘
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@CoinsActivity.currentFocus != null) {
                if (this@CoinsActivity.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@CoinsActivity.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }

    internal inner class EditChangedListener : TextWatcher {
        private var temp: CharSequence = ""//监听前的文本
        private val editStart: Int = 0//光标开始位置
        private val editEnd: Int = 0//光标结束位置
        private val charMaxNum = coins_remin_num

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            temp = s
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if(temp.length<charMaxNum){
                coins_word_num_tv!!.text=temp.length.toString()+"/"+coins_remin_num
            }
        }

        override fun afterTextChanged(s: Editable) {
            if(temp.length>charMaxNum){
                Toasty.info(this@CoinsActivity,"您输入的字数已经超过了限制").show()
            }
        }
    }
    
    
}
