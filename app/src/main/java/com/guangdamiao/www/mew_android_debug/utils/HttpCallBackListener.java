package com.guangdamiao.www.mew_android_debug.utils;

/**
 * Created by Jason_Jan on 2017/12/20.
 */

public interface HttpCallBackListener {
    void onSuccess(String respose);
    void onError();
}
