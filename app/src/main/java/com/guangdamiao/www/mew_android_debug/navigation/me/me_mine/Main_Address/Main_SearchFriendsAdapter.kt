package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Address

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MySearchFriends
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconSelfInfo
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil
import com.nostra13.universalimageloader.core.ImageLoader
import java.util.*

class Main_SearchFriendsAdapter(context: Context, mySearchFriends: ArrayList<MySearchFriends>) : BaseAdapter(){
    private val mInflater: LayoutInflater//从别人得到 自己用
    private var mContext: Context? = null//从别人得到 自己用
    private var mySearchFriends: ArrayList<MySearchFriends>?=null

    init {
        this.mContext = context
        this.mySearchFriends=mySearchFriends
        this.mInflater = LayoutInflater.from(mContext)
    }
    override fun getItem(arg0: Int): Any {

        return mySearchFriends!![arg0]
    }
    override fun getItemId(position: Int): Long {

        return position.toLong()
    }
    override fun getCount(): Int {

        return mySearchFriends!!.size
    }
    override fun getView(position: Int, convertView: View?, parent: android.view.ViewGroup): View {
        var convertView = convertView
        var holder:ViewHolder

        if (convertView == null) {
            holder= ViewHolder()
            convertView = mInflater.inflate(R.layout.mine_item_search_friends, null)
            holder.indexText = convertView!!.findViewById(R.id.mine_item_searchfriend) as TextView
            holder.mine_searchfriend_icon=convertView!!.findViewById(R.id.mine_searchfriend_icon) as ImageView
            holder.mine_searchfriend_next=convertView!!.findViewById(R.id.mine_searchfriend_next) as ImageView
            holder.mine_searchfriends_rl=convertView!!.findViewById(R.id.mine_searchfriends_rl) as RelativeLayout
            holder.mine_item_searchfriend_sex=convertView!!.findViewById(R.id.mine_item_searchfriend_sex) as TextView 
            holder.mine_item_searchfriend_school=convertView!!.findViewById(R.id.mine_item_searchfriend_school) as TextView 
            holder.mine_item_searchfriend_college=convertView!!.findViewById(R.id.mine_item_searchfriend_college) as TextView 
            convertView.tag=holder
        }else{
            holder=convertView.tag as ViewHolder
        }

        val bean=mySearchFriends!!.get(position)

        holder.indexText!!.text = mySearchFriends!![position].nickname
        holder.indexText!!.setTextColor(mContext!!.resources.getColor(R.color.black))
        getMyIcon(mySearchFriends!![position].icon!!,holder.mine_searchfriend_icon!!)


        //昵称
        holder!!.indexText!!.text=mySearchFriends!![position]!!.nickname
        var spec= View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        holder!!.indexText!!.measure(spec,spec)
        var nickwidth= holder!!.indexText!!.measuredWidth
        //LogUtils.d_debugprint(Constant.Message_TAG,"\n\n现在nickname为：${mySearchFriends!![position]!!.nickname} \n\n 长度为：${nickwidth} \n\n转换后的167.0f为：${DensityUtil.dip2px(context,187.0f)} ")
        var nicklength=mySearchFriends!![position]!!.nickname!!.length
        while(nickwidth> DensityUtil.dip2px(mContext, 250.0f)){
            nicklength-=2
            val name_display=mySearchFriends!![position]!!.nickname!!.substring(0,nicklength)+"..."
            holder!!.indexText!!.text=name_display
            var spec= View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
            holder!!.indexText!!.measure(spec,spec)
            nickwidth= holder!!.indexText!!.measuredWidth
            // LogUtils.d_debugprint(Constant.Message_TAG,"\n\n现在nicklength为：$nicklength 宽度为：$nickwidth\n\n")
        }
        
        //性别
        if(mySearchFriends!![position].gender.equals("男")){
            holder.mine_item_searchfriend_sex!!.text="♂"
            holder.mine_item_searchfriend_sex!!.setTextColor(mContext!!.resources.getColor(R.color.headline))
            holder.mine_item_searchfriend_sex!!.visibility=View.VISIBLE
        }else if(mySearchFriends!![position].gender.equals("女")){
            holder.mine_item_searchfriend_sex!!.text="♀"
            holder.mine_item_searchfriend_sex!!.setTextColor(mContext!!.resources.getColor(R.color.ask_red))
            holder.mine_item_searchfriend_sex!!.visibility=View.VISIBLE
        }else{
            holder.mine_item_searchfriend_sex!!.visibility=View.GONE
        }
        
        //显示学校
        if(!mySearchFriends!![position].school.equals("")){
            holder.mine_item_searchfriend_school!!.text=mySearchFriends!![position].school
            holder.mine_item_searchfriend_school!!.visibility=View.VISIBLE
        }else{
            holder.mine_item_searchfriend_school!!.visibility=View.GONE
        }

        //显示学院
        if(!mySearchFriends!![position].college.equals("")){
            holder.mine_item_searchfriend_college!!.text=mySearchFriends!![position].college
            holder.mine_item_searchfriend_college!!.visibility=View.VISIBLE
        }else{
            holder.mine_item_searchfriend_college!!.visibility=View.GONE
        }

        holder.mine_searchfriends_rl!!.setOnClickListener{
            val read = mContext!!.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val current_userID = read.getString("id", "")

            if(current_userID!!.equals(bean.id)){
                val intent:Intent=Intent(mContext, IconSelfInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",bean.id)
                bundle.putString("icon",bean.icon)
                bundle.putString("sex",bean.gender)
                bundle.putString("nickname",bean.nickname)
                intent.putExtras(bundle)
                mContext!!.startActivity(intent)
            }else{
                val intent:Intent=Intent(mContext,IconInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",bean.id)
                bundle.putString("icon",bean.icon)
                bundle.putString("sex",bean.gender)
                bundle.putString("nickname",bean.nickname)
                intent.putExtras(bundle)
                mContext!!.startActivity(intent)
            }
        }

        return convertView
    }

    private fun getMyIcon(icon_url:String,icon_object: ImageView) {
        if (icon_url !== "") {
            if (LogUtils.APP_IS_DEBUG) {
                ImageLoader.getInstance().displayImage(icon_url, icon_object)
            } else {
                ImageLoader.getInstance().displayImage(icon_url, icon_object)
            }
        }
    }

   class ViewHolder{
       var indexText: TextView?=null
       var mine_searchfriend_icon: ImageView?=null
       var mine_searchfriend_next: ImageView?=null
       var mine_searchfriends_rl: RelativeLayout?=null
       var mine_item_searchfriend_sex:TextView?=null
       var mine_item_searchfriend_school:TextView?=null
       var mine_item_searchfriend_college:TextView?=null
       
   }
}