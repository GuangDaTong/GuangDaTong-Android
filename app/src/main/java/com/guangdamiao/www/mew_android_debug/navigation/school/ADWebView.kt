package com.guangdamiao.www.mew_android_debug.navigation.school

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.*
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import cn.jiguang.share.android.api.JShareInterface
import cn.jiguang.share.android.api.PlatActionListener
import cn.jiguang.share.android.api.Platform
import cn.jiguang.share.android.api.ShareParams
import cn.jiguang.share.qqmodel.QQ
import cn.jiguang.share.qqmodel.QZone
import cn.jiguang.share.wechat.Wechat
import cn.jiguang.share.wechat.WechatMoments
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.CustomPopWindow
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil
import com.guangdamiao.www.mew_android_debug.utils.Loading_view
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import com.guangdamiao.www.mew_android_debug.webviewPicture.MJavascriptInterface
import com.guangdamiao.www.mew_android_debug.webviewPicture.MyWebViewClient
import es.dmoral.toasty.Toasty
import java.util.*

class ADWebView : Activity() {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_imageview_btn:Button?=null
    private var title_right_imageview: ImageView?=null
    private var urlFirst:String?=null
    private var urlPath:String?=null
    private var webView_content: WebView?=null
    private var popWindow:CustomPopWindow?=null  //分享
    private var popWindow_share:CustomPopWindow?=null

    private var link=""
    private var title=""
    private var imageUrls=ArrayList<String>()

   /* private var handler: Handler? = object : Handler() {
        override fun handleMessage(msg: Message) {

            val SuccessString = msg.obj as ArrayList<String>
            val shareWay=SuccessString[0]
            val token=SuccessString[1]
            LogUtils.d_debugprint(Constant.School_TAG,"首页广告分享成功！！！即将向服务器发送消息~~")

            AskMainRequest.makeShareToServer(Constant.School_TAG,Constant.URL_Share_Guidance,this@ADWebView,"",shareWay,token)

        }
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        setContentView(R.layout.me_webview)
        initView()
        addSomeListener()
        getUrl()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@ADWebView,"广告页")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@ADWebView,"广告页")
        super.onPause()
    }

    private fun getUrl(){
        if(LogUtils.APP_IS_DEBUG){
            urlFirst= Constant.BASEURLFORTEST
        }else{
            urlFirst= Constant.BASEURLFORRELEASE
        }
        urlPath=link
        if(urlPath!!.indexOf("?")==-1){
            urlPath=urlPath+"?OSType=android"
        }else{
            urlPath=urlPath+"&OSType=android"
        }
        val loading_view= Loading_view(this@ADWebView,R.style.CustomDialog)
        loading_view.show()
        if(urlPath!=null&&!urlPath.equals("")){
            val wvcc: WebChromeClient =object: WebChromeClient(){
                override fun onReceivedTitle(view: WebView?, title: String?) {
                    if(loading_view!=null) loading_view.cancel()
                    super.onReceivedTitle(view, title)
                }
            }


            webView_content!!.addJavascriptInterface(MJavascriptInterface(this,imageUrls),"imagelistener")
            webView_content!!.setWebViewClient(MyWebViewClient())
            webView_content!!.setWebChromeClient(wvcc)
            webView_content!!.loadUrl(urlPath)
           /* webView_content!!.setWebViewClient(object: WebViewClient(){
                override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                    if(loading_view!=null) loading_view.cancel()
                    super.onReceivedError(view, request, error)

                }
            })*/
        }
    }

    fun initView() {
        if(intent!!.extras!=null){
            title=intent!!.extras.getString("title")
            link=intent!!.extras.getString("link")
        }

        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_center_textview!!.text = title
        title_center_textview!!.visibility = View.VISIBLE
        title_right_imageview!!.visibility=View.GONE
        title_right_imageview_btn!!.visibility=View.GONE
        webView_content = findViewById(R.id.webView_content) as WebView
        val webSettings: WebSettings =webView_content!!.settings
        webSettings.javaScriptEnabled=true
        webSettings.blockNetworkImage=false
        webSettings.useWideViewPort=true
        webSettings.loadWithOverviewMode=true
        webSettings.builtInZoomControls=true
        webSettings.setSupportZoom(true)
        webSettings.displayZoomControls=false

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP)
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE)
    }


    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            finish()
            overridePendingTransition(0, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.alpha=0.618f
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.alpha=1.0f
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }

        title_right_imageview_btn!!.setOnClickListener{
               showOneDialog()
        }

        title_right_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_imageview_btn!!.alpha=0.618f
                title_right_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))

            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_imageview_btn!!.alpha=1.0f
                title_right_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }
    }


    private fun showOneDialog(){

        val contentView:View= LayoutInflater.from(this).inflate(R.layout.pop_one_share_dialog,null)
        hanleDialog(contentView)
        popWindow= CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .enableBackgroundDark(true)
                .setBgDarkAlpha(0.7f)
                .setFocusable(true)
                .setOutsideTouchable(true)
                .setAnimationStyle(R.style.ask_three_anim)
                .create()
        popWindow!!.showAtLocation(title_right_imageview_btn, Gravity.CENTER_HORIZONTAL,0, DensityUtil.dip2px(this,960.0f))

    }

    private fun hanleDialog(contentView:View){
        val me_one_photo=contentView.findViewById(R.id.me_one_share) as TextView
        val me_one_cancle=contentView.findViewById(R.id.me_one_cancle) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow!=null){
                    popWindow!!.dissmiss()
                }
                when(v!!.id){
                    R.id.me_one_share    -> makeShare()
                    R.id.me_one_cancle   -> popWindow!!.dissmiss()
                }
            }
        }
        me_one_photo!!.setOnClickListener(listener)
        me_one_cancle!!.setOnClickListener(listener)
    }
    
    private fun makeShare(){
        if(!link.equals("")){
            val contentView:View=LayoutInflater.from(this).inflate(R.layout.ask_share,null)
            hanleShareDialog(contentView,link)//这个函数还没有写
            popWindow_share= CustomPopWindow.PopupWindowBuilder(this)
                    .setView(contentView)
                    .enableBackgroundDark(true)
                    .setBgDarkAlpha(0.7f)
                    .setFocusable(true)
                    .setOutsideTouchable(true)
                    .setAnimationStyle(R.style.ask_share_anim)
                    .create()
            popWindow_share!!.showAtLocation(title_right_imageview_btn,Gravity.CENTER,0,0)
        }
    }

    private fun hanleShareDialog(contentView: View,link:String){
        val ask_share_qq=contentView.findViewById(R.id.ask_share_qq) as RelativeLayout
        val ask_share_wechat=contentView.findViewById(R.id.ask_share_wechat) as RelativeLayout
        val ask_share_qq_zone=contentView.findViewById(R.id.ask_share_qq_zone) as RelativeLayout
        val ask_share_wechat_friends=contentView.findViewById(R.id.ask_share_wechat_friends) as RelativeLayout
        val ask_share_copy=contentView.findViewById(R.id.ask_share_copy) as RelativeLayout


        ask_share_qq!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAD(title, QQ.Name,"QQFriend")
        }

        ask_share_qq_zone!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAD(title,QZone.Name,"QQZone")
        }

        ask_share_wechat!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAD(title,Wechat.Name,"weChatFriend")
        }

        ask_share_wechat_friends!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAD(title, WechatMoments.Name,"weChatCircle")
        }

        ask_share_copy!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            val cm=this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val mClipData= ClipData.newPlainText("Label",link)
            cm.primaryClip=mClipData
            Toasty.info(this,"亲，复制成功，赶快分享一下吧~").show()
        }

    }

    private fun shareAD(title:String,shareType:String,shareWay:String){
        //JEventUtils.onCountEvent(this,Constant.Event_AskShare)//分享帖子内容统计

        val shareParams= ShareParams()
        shareParams.shareType= Platform.SHARE_WEBPAGE
        shareParams.text=title
        shareParams.title="广大通"
        if(shareType.equals(WechatMoments.Name)){
            shareParams.title=title
        }

        shareParams.url=urlPath
        shareParams.imagePath=MainApplication.ImagePath

        LogUtils.d_debugprint(Constant.School_TAG,"分享的参数：$shareType,${shareParams.shareType},${shareParams.text},${shareParams.title},${shareParams.url}")

        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token = read.getString("token", "")


        JShareInterface.share(shareType, shareParams, object : PlatActionListener {

            override fun onComplete(p0: Platform?, p1: Int, p2: HashMap<String, Any>?) {
               /* if (handler != null) {
                    val message = handler!!.obtainMessage()
                    val SuccessString=ArrayList<String>()
                    SuccessString.add(shareWay)
                    SuccessString.add(token)
                    message.obj = SuccessString
                    handler!!.sendMessage(message)
                }*/
            }

            override fun onError(platform: Platform, i: Int, i1: Int, throwable: Throwable) {
                LogUtils.d_debugprint(Constant.School_TAG,"首页广告内容分享失败！！！"+throwable.toString())
            }

            override fun onCancel(platform: Platform, i: Int) {
                LogUtils.d_debugprint(Constant.School_TAG,"首页广告内容分享取消！！！")
            }
        })
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(0,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }
}
