package com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.ImageSingleActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.*
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import es.dmoral.toasty.Toasty
import org.json.JSONArray

class IconInfo : BaseActivity() {

    private var title_left_imageview_btn: Button?=null
    private var title_center_textview: TextView?=null
    private var title_right_imageview:ImageView?=null
    private var iconInfo_nickname_tv:TextView?=null
    private var iconInfo_sex_tv:TextView?=null
    private var iconInfo_icon_iv:ImageView?=null
    private var iconInfo_zan_iv:ImageView?=null
    private var iconInfo_zan_num_tv:TextView?=null
    private var iconInfo_money_num_tv:TextView?=null
    private var iconInfo_level_btn:Button?=null
    private var iconInfo_college_tv:TextView?=null
    private var iconInfo_grade_tv:TextView?=null
    private var iconInfo_contact_rl:RelativeLayout?=null
    private var iconInfo_give_rl:RelativeLayout?=null
    private var iconInfo_demand_rl:RelativeLayout?=null
    private var iconInfo_message_rl:RelativeLayout?=null
    private var iconInfo_report_rl:RelativeLayout?=null
    private var iconInfo_myBlack_rl:RelativeLayout?=null
    private var iconInfo_zan_btn:Button?=null
    //举报页面的选项
    private var popWindow_report: CustomPopWindow?=null
    var ask_report_five1:TextView?=null
    var ask_report_five2:TextView?=null
    var ask_report_five3:TextView?=null
    var ask_report_five4:TextView?=null
    var ask_report_cancle:TextView?=null


    var nickname=""
    var sex=""
    var icon=""
    var id=""
    var zanIs=""
    var zan=""
    var loading_first:Loading_view?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
        setContentView(R.layout.activity_icon_info)
        initView()
        configImageLoader()
        addSomeListener()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@IconInfo,"他人资料页")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@IconInfo,"他人资料页")
        super.onPause()
    }

    override fun onStart() {
        if(!id.equals("")){
            loading_first= Loading_view(this@IconInfo,R.style.CustomDialog)
            loading_first!!.show()
            IconInfoRequest.requestForInfo(this@IconInfo,id,object: IDataRequestListener2String {
                override fun loadSuccess(response_string: String) {
                    if(loading_first!=null) loading_first!!.cancel()

                    LogUtils.d_debugprint(Constant.Icon_Info_TAG,"从服务器接收到的数据为：\n"+response_string)
                    val result = JsonUtil.get_key_string("result", response_string)
                        zanIs=JsonUtil.get_key_string("isZan",result)
                        zan=JsonUtil.get_key_string("zan",result)
                    val level=JsonUtil.get_key_string("level",result)
                    val college=JsonUtil.get_key_string("college",result)
                    val grade=JsonUtil.get_key_string("grade",result)
                    val coins=JsonUtil.get_key_string("coins",result)
                        sex=JsonUtil.get_key_string("gender",result)

                    if(sex.equals("男")){
                        iconInfo_sex_tv!!.text="♂"
                        iconInfo_sex_tv!!.setTextColor(this@IconInfo.resources.getColor(R.color.headline))
                        iconInfo_sex_tv!!.visibility=View.VISIBLE
                    }else if(sex.equals("女")){
                        iconInfo_sex_tv!!.text="♀"
                        iconInfo_sex_tv!!.setTextColor(this@IconInfo.resources.getColor(R.color.ask_red))
                        iconInfo_sex_tv!!.visibility=View.VISIBLE
                    }else{
                        iconInfo_sex_tv!!.visibility=View.GONE
                    }
                    if(zanIs.equals("1")){
                        iconInfo_zan_iv!!.setImageResource(R.mipmap.zan_blue)
                        iconInfo_zan_iv!!.setColorFilter(R.color.red)
                    }else{
                        iconInfo_zan_iv!!.setImageResource(R.mipmap.zan_gray)
                    }
                    iconInfo_zan_num_tv!!.text=zan
                    iconInfo_level_btn!!.text="LV"+level
                    iconInfo_money_num_tv!!.text=coins
                    iconInfo_college_tv!!.text=college+"•"
                    if(grade.equals("")){
                        iconInfo_grade_tv!!.text="其他"
                    }else{
                        iconInfo_grade_tv!!.text=grade
                    }
                }
            })
        }
        super.onStart()
    }

    fun initView(){
        title_left_imageview_btn=findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview=findViewById(R.id.title_center_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        iconInfo_nickname_tv=findViewById(R.id.iconInfo_nickname_tv) as TextView
        iconInfo_sex_tv=findViewById(R.id.iconInfo_sex_tv) as TextView
        iconInfo_icon_iv=findViewById(R.id.iconInfo_icon_iv) as ImageView
        iconInfo_zan_iv=findViewById(R.id.iconInfo_zan_iv) as ImageView
        iconInfo_zan_num_tv=findViewById(R.id.iconInfo_zan_num_tv) as TextView
        iconInfo_money_num_tv=findViewById(R.id.iconInfo_money_num_tv) as TextView
        iconInfo_level_btn=findViewById(R.id.iconInfo_level_btn) as Button
        iconInfo_college_tv=findViewById(R.id.iconInfo_college_tv) as TextView
        iconInfo_grade_tv=findViewById(R.id.iconInfo_grade_tv) as TextView
        iconInfo_contact_rl=findViewById(R.id.iconInfo_contact_rl) as RelativeLayout
        iconInfo_give_rl=findViewById(R.id.iconInfo_give_rl) as RelativeLayout
        iconInfo_demand_rl=findViewById(R.id.iconInfo_demand_rl) as RelativeLayout
        iconInfo_message_rl=findViewById(R.id.iconInfo_message_rl) as RelativeLayout
        iconInfo_report_rl=findViewById(R.id.iconInfo_report_rl) as RelativeLayout
        iconInfo_myBlack_rl=findViewById(R.id.iconInfo_myBlack_rl) as RelativeLayout
        iconInfo_zan_btn=findViewById(R.id.iconInfo_zan_btn) as Button

        title_right_imageview!!.visibility= View.GONE
        title_center_textview!!.visibility= View.VISIBLE

        if(intent.extras!=null){
            val bundle=intent.extras//这里接受前面传过来的姓名，头像icon地址，性别
            nickname=bundle.getString("nickname")
            sex=bundle.getString("sex")
            icon=bundle.getString("icon")
            id=bundle.getString("id")
        }
        LogUtils.d_debugprint(Constant.Icon_Info_TAG,"昵称："+nickname+"\n性别："+sex+"\n头像icon"+icon)


        title_center_textview!!.text=nickname+" 的资料"
        var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
        title_center_textview!!.measure(spec,spec)
        var nickwidth= title_center_textview!!.measuredWidth
        var nicklength=nickname!!.length
        while(nickwidth> DensityUtil.dip2px(this@IconInfo,250.0f)){
            nicklength-=2
            val name_display=nickname!!.substring(0,nicklength)+"..."
            title_center_textview!!.text=name_display+" 的资料"
            var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
            title_center_textview!!.measure(spec,spec)
            nickwidth= title_center_textview!!.measuredWidth
        }

        iconInfo_nickname_tv!!.text=nickname

        //性别
        if(sex.equals("男")){
            iconInfo_sex_tv!!.text="♂"
            iconInfo_sex_tv!!.setTextColor(this@IconInfo.resources.getColor(R.color.headline))
            iconInfo_sex_tv!!.visibility=View.VISIBLE
        }else if(sex.equals("女")){
            iconInfo_sex_tv!!.text="♀"
            iconInfo_sex_tv!!.setTextColor(this@IconInfo.resources.getColor(R.color.ask_red))
            iconInfo_sex_tv!!.visibility=View.VISIBLE
        }else{
            iconInfo_sex_tv!!.visibility=View.GONE
        }
        if (icon !== "") {
            if (LogUtils.APP_IS_DEBUG) {
                ImageLoader.getInstance().displayImage( icon!!, iconInfo_icon_iv)
            } else {
                ImageLoader.getInstance().displayImage( icon!!, iconInfo_icon_iv)
            }
        }
        iconInfo_icon_iv!!.setOnClickListener{
            if(icon!=null) ImageSingleActivity.startImageSingleActivity(this@IconInfo,icon!!)
        }
    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            this.finish()
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f
            }
            false
        }

        iconInfo_zan_btn!!.setOnClickListener{
            //判断是否登录
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                //他人资料页点赞，首先view点赞，再请求服务器
                if(!id.equals("")){
                    if(!zanIs.equals("")&&!zan.equals("")){
                        if(zanIs.equals("0")){
                            zanIs="1"
                            iconInfo_zan_iv!!.setImageResource(R.mipmap.zan_blue)
                            iconInfo_zan_iv!!.setColorFilter(R.color.red)
                            iconInfo_zan_num_tv!!.text=(zan.toInt()+1).toString()
                            //再去请求服务器吧
                            IconInfoRequest.requestAddZan(this@IconInfo,id,token)
                        }else{
                            Toasty.info(this@IconInfo,"亲，您已经赞过TA了喔~").show()
                        }
                    }
                }
            }else{
                val intent: Intent =Intent(this@IconInfo,LoginMain::class.java)
                startActivity(intent)
            }
        }

        iconInfo_contact_rl!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")

            SweetAlertDialog(this@IconInfo, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("提示")
                    .setContentText("亲，您要添加TA为好友？")
                    .setCustomImage(R.mipmap.mew_icon)
                    .setConfirmText("确定")
                    .setConfirmClickListener { sDialog ->
                        sDialog.dismissWithAnimation()

                        if(!id.equals("")&&!token.equals("")){
                            //先判断是否在黑名单里面
                            val read = this.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
                            val blacklist= read.getString("blacklist", "[]")
                            val blacklist_array=ArrayList<String>()
                            val jsonArray=JSONArray(blacklist)
                            if(jsonArray.length()>0) {
                                for (i in 0..jsonArray.length()-1) {
                                    blacklist_array.add(jsonArray.getString(i))
                                }
                            }
                            var isExistBlack=false
                            for(j in blacklist_array.indices){
                                if(id.equals(blacklist_array[j])){
                                    isExistBlack=true
                                }
                            }
                            if(isExistBlack){
                                val pDialog= SweetAlertDialog(this@IconInfo, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                pDialog.setCustomImage(R.mipmap.mew_icon)
                                pDialog.setTitleText("提示")
                                pDialog.setContentText("亲，TA已经被您拉黑了~")
                                pDialog.show()
                                val handler= Handler()
                                handler.postDelayed({
                                    pDialog.cancel()
                                },1236)
                            }else{
                                val loading_view:Loading_view= Loading_view(this@IconInfo,R.style.CustomDialog)
                                loading_view.show()
                                IconInfoRequest.requestAddFriend(this@IconInfo,id,token,loading_view)
                            }

                        }else{
                            val intent = Intent(this@IconInfo, LoginMain::class.java)
                            this@IconInfo.startActivity(intent)
                            overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
                        }
                    }
                    .setCancelText("取消")
                    .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                    .show()

        }

        iconInfo_give_rl!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!id.equals("")&&!token.equals("")){
                val intent:Intent=Intent(this@IconInfo,CoinsActivity::class.java)
                val bundle=Bundle()
                bundle.putString("receiverID",id)
                bundle.putString("token",token)
                bundle.putString("centerWords",Constant.Give_Coins)
                intent.putExtras(bundle)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent = Intent(this@IconInfo, LoginMain::class.java)
                this@IconInfo.startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }
        iconInfo_demand_rl!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!id.equals("")&&!token.equals("")){
                val intent:Intent=Intent(this@IconInfo,CoinsActivity::class.java)
                val bundle=Bundle()
                bundle.putString("receiverID",id)
                bundle.putString("token",token)
                bundle.putString("centerWords",Constant.Want_Coins)
                intent.putExtras(bundle)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent = Intent(this@IconInfo, LoginMain::class.java)
                this@IconInfo.startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }
        iconInfo_message_rl!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!id.equals("")&&!token.equals("")){
                val intent:Intent=Intent(this@IconInfo,WordsActivity::class.java)
                val bundle=Bundle()
                bundle.putString("ID",id)
                bundle.putString("nickname",nickname)
                bundle.putString("token",token)
                bundle.putString("centerWords",Constant.Leave_Words)
                bundle.putString("rightWords","发送")
                intent.putExtras(bundle)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent = Intent(this@IconInfo, LoginMain::class.java)
                this@IconInfo.startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }
        iconInfo_report_rl!!.setOnClickListener{
            if(!id.equals("")){
                make_report(id)
            }
        }

        iconInfo_myBlack_rl!!.setOnClickListener{

            //Common.showLongToast(getContext(),"这里要提示登录的提示框，确定即登录，取消即返回");
            SweetAlertDialog(this@IconInfo, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("提示")
                    .setContentText("将TA加入黑名单后，您将不会再收到TA发送的留言以及帖子。是否继续？")
                    .setCustomImage(R.mipmap.mew_icon)
                    .setConfirmText("我确定")
                    .setConfirmClickListener { sDialog ->
                        sDialog.dismissWithAnimation()
                        if(!id.equals("")){
                            make_black(id)
                        }
                    }
                    .setCancelText("再想想")
                    .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                    .show()

        }
    }

    private fun make_black(id:String){
        val token=get_token()
        if(!token.equals("")){
            val loading_view:Loading_view= Loading_view(this@IconInfo,R.style.CustomDialog)
            loading_view.show()
            IconInfoRequest.requestBlackUsers(this@IconInfo,id,token,object:IDataRequestListener2String{
                override fun loadSuccess(response_string: String?) {

                    LogUtils.d_debugprint(Constant.Icon_Info_TAG,"拉黑TA=====服务器返回的数据："+response_string)
                    loading_view.cancel()
                    if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                        LogUtils.d_debugprint(Constant.Icon_Info_TAG,"个人信息中：拉黑TA 成功！！！\n")
                        //添加到黑名单全局文件
                        val read = this@IconInfo.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
                        val blacklist = read.getString("blacklist", "[]")

                        val blacklist_array=ArrayList<String>()
                        val jsonArray=JSONArray(blacklist)
                        if(jsonArray.length()>0){
                            for(i in 0..jsonArray.length()-1){
                                blacklist_array.add(jsonArray.getString(i))
                            }
                        }
                        blacklist_array.add(id)
                        val jsonArray_new= JSONArray()
                        for(i in blacklist_array.indices){
                            jsonArray_new.put(blacklist_array[i])
                        }

                        val editor: SharedPreferences.Editor=getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE).edit()
                        editor.putString("blacklist",jsonArray_new.toString())
                        editor.commit()

                        LogUtils.d_debugprint(Constant.Icon_Info_TAG,"\n黑名单：原来的json为：$blacklist\n现在的json为：${jsonArray_new.toString()}")

                        val pDialog= SweetAlertDialog(this@IconInfo, SweetAlertDialog.SUCCESS_TYPE)
                        pDialog.setTitleText("提示")
                        pDialog.setContentText("亲，您已经拉黑TA了~")
                        pDialog.show()
                        val handler= Handler()
                        handler.postDelayed({
                            pDialog.cancel()
                        },1236)
                    }else{
                        loading_view.cancel()
                        if(JsonUtil.get_key_string("msg",response_string)!=null){
                            val pDialog= SweetAlertDialog(this@IconInfo, SweetAlertDialog.WARNING_TYPE)
                            pDialog.setTitleText("提示")
                            pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                            pDialog.show()
                            val handler= Handler()
                            handler.postDelayed({
                                pDialog.cancel()
                            },1236)
                        }
                    }
                }
            })
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.push_right_in,R.anim.push_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun make_report(id:String){
        val token=get_token()
        if(token!=""){
            val contentView:View= LayoutInflater.from(this@IconInfo).inflate(R.layout.ask_five_report_dialog,null)
            hanle_report_Dialog(contentView,id)
            popWindow_report= CustomPopWindow.PopupWindowBuilder(this@IconInfo)
                    .setView(contentView)
                    .enableBackgroundDark(true)
                    .setBgDarkAlpha(0.7f)
                    .setFocusable(true)
                    .setOutsideTouchable(true)
                    .setAnimationStyle(R.style.ask_three_anim)
                    .create()
            popWindow_report!!.showAtLocation(iconInfo_report_rl, Gravity.CENTER_HORIZONTAL,0, DensityUtil.dip2px(this@IconInfo,215.0f))

        }
    }

    private fun hanle_report_Dialog(contentView:View,id: String){
        ask_report_five1=contentView.findViewById(R.id.ask_five_1) as TextView
        ask_report_five2=contentView.findViewById(R.id.ask_five_2) as TextView
        ask_report_five3=contentView.findViewById(R.id.ask_five_3) as TextView
        ask_report_five4=contentView.findViewById(R.id.ask_five_4) as TextView
        ask_report_cancle=contentView.findViewById(R.id.ask_five_cancle) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow_report!=null){
                    popWindow_report!!.dissmiss()
                }
                when(v!!.id){
                    R.id.ask_five_1 -> make_report(id,"欺诈骗钱")
                    R.id.ask_five_2 -> make_report(id,"色情暴力")
                    R.id.ask_five_3 -> make_report(id,"广告骚扰")
                    R.id.ask_five_4 -> make_report_other(id,"")
                    R.id.ask_five_cancle  -> popWindow_report!!.dissmiss()
                }
            }
        }
        ask_report_five1!!.setOnClickListener(listener)
        ask_report_five2!!.setOnClickListener(listener)
        ask_report_five3!!.setOnClickListener(listener)
        ask_report_five4!!.setOnClickListener(listener)
        ask_report_cancle!!.setOnClickListener(listener)
    }

    private fun make_report(id:String,reason:String){
        val token=get_token()
        //开始举报=====请求服务器
        if(token!=""){
            val loading_view:Loading_view= Loading_view(this@IconInfo,R.style.CustomDialog)
            loading_view.show()
            IconInfoRequest.requestInformUsers(this@IconInfo,id,reason,token,object:IDataRequestListener2String{
                override fun loadSuccess(response_string: String?) {

                    LogUtils.d_debugprint(Constant.Icon_Info_TAG,"举报TA=====服务器返回的数据："+response_string)
                    loading_view.cancel()
                    if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){
                        LogUtils.d_debugprint(Constant.Icon_Info_TAG,"个人信息中：举报TA 成功！！！\n")
                        val pDialog= SweetAlertDialog(this@IconInfo, SweetAlertDialog.SUCCESS_TYPE)
                        pDialog.setTitleText("提示")
                        pDialog.setContentText("亲，您已经成功举报TA~")
                        pDialog.show()
                        val handler= Handler()
                        handler.postDelayed({
                            pDialog.cancel()
                        },1236)
                    }else{
                        loading_view.cancel()
                        if(JsonUtil.get_key_string("msg",response_string)!=null){
                            val pDialog= SweetAlertDialog(this@IconInfo, SweetAlertDialog.WARNING_TYPE)
                            pDialog.setTitleText("提示")
                            pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                            pDialog.show()
                            val handler= Handler()
                            handler.postDelayed({
                                pDialog.cancel()
                            },1236)
                        }
                    }
                }
            })
        }
    }

    private fun make_report_other(id:String,reson:String){
        val token=get_token()
        //开始收藏=====请求服务器
        if(token!=""){
            val intent = Intent(this@IconInfo, WordsActivity::class.java)
            val bundle= Bundle()
            bundle.putString("ID",id)
            bundle.putString("centerWords",Constant.Report_Words)
            bundle.putString("rightWords","完成")
            bundle.putString("token",token)
            intent.putExtras(bundle)
            this@IconInfo.startActivity(intent)
        }
    }

    private fun get_token():String{
        val read = this@IconInfo.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token= read.getString("token", "")
        if(token.equals("")){
            val intent = Intent(this@IconInfo, LoginMain::class.java)
            this@IconInfo.startActivity(intent)
        }else{
            return token
        }
        return ""
    }

    /**
     * 配置ImageLoder
     */
    private fun configImageLoader() {
        // 初始化ImageLoader
        val options = DisplayImageOptions.Builder().showImageOnLoading(R.mipmap.icon_empty) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.icon_empty) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.icon_error) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true) // 设置下载的图片是否缓存在SD卡中
                // .displayer(new RoundedBitmapDisplayer(20)) // 设置成圆角图片
                .build() // 创建配置过得DisplayImageOption对象

        val config = ImageLoaderConfiguration.Builder(this@IconInfo).defaultDisplayImageOptions(options)
                .threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(Md5FileNameGenerator()).tasksProcessingOrder(QueueProcessingType.LIFO).build()

        ImageLoader.getInstance().init(config)
    }

}
