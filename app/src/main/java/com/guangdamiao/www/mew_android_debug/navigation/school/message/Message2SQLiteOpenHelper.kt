package com.guangdamiao.www.mew_android_debug.navigation.school.message

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2017/12/10.
 */

class Message2SQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
          val name = "message2.db"
          val version = 1
          val M2_Name="message2"
          val M2_Id="id"
          val M2_Sid="s_id"
          val M2_AskID="askID"
          val M2_Icon="icon"
          val M2_Gender="gender"
          val M2_NickName="nickname"
          val M2_SenderID="senderID"
          val M2_CreateTime="createTime"
          val M2_Detail="detail"
          val M2_Type="type"
          val M2_IsRead="isRead"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table message2(id integer primary key autoincrement,s_id varchar(20),askID varchar(20),icon varchar(50), gender varchar(4),"
                + "nickname varchar(20),senderID varchar(20),createTime varchar(50),detail varchar(2048),type varchar(20),isRead varchar(2))")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    //打开数据库时调用
    override fun onOpen(db: SQLiteDatabase) {
        //如果数据库已经存在，则再次运行不执行onCreate()方法，而是执行onOpen()打开数据库
        super.onOpen(db)
    }

}
