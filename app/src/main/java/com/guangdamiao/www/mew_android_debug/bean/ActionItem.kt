package com.guangdamiao.www.mew_android_debug.bean

import android.content.Context
import android.graphics.drawable.Drawable

/**

 * @ClassName: ActionItem
 * *
 * @Description: 弹窗内部子类项（绘制标题和图标）
 * *
 * @author yiw
 * *
 */
class ActionItem {
    // 定义图片对象
    var mDrawable: Drawable? = null
    // 定义文本对象
    var mTitle: CharSequence

    constructor(title: CharSequence) {
        this.mDrawable = null
        this.mTitle = title
    }

    constructor(drawable: Drawable, title: CharSequence) {
        this.mDrawable = drawable
        this.mTitle = title
    }

    constructor(context: Context, titleId: Int, drawableId: Int) {
        this.mTitle = context.resources.getText(titleId)
        this.mDrawable = context.resources.getDrawable(drawableId)
    }

    constructor(context: Context, title: CharSequence, drawableId: Int) {
        this.mTitle = title
        this.mDrawable = context.resources.getDrawable(drawableId)
    }

    fun setItemTv(tv: CharSequence) {
        mTitle = tv
    }
}
