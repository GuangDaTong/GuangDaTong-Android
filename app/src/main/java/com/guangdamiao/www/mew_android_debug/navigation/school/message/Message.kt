package com.guangdamiao.www.mew_android_debug.navigation.school.message

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.ViewPager
import android.util.DisplayMetrics
import android.view.*
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.HTab
import com.guangdamiao.www.mew_android_debug.bean.HTabDb
import com.guangdamiao.www.mew_android_debug.bean.message_bean.MessageUserData
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import java.io.File

class Message : FragmentActivity(),ViewPager.OnPageChangeListener{


    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_textview: TextView? = null
    private var title_right_imageview_btn:Button?=null
    private var title_right_imageview_btn1:Button?=null
    private var title_right_imageview_btn2:Button?=null
    private var bt1:Button?=null
    private var bt2:Button?=null
    private var bt3:Button?=null

    private var hTabs:List<HTab>?=null
    private val newsList = ArrayList<Fragment>()
    private var adapter: MessageAdapter? = null
    private var vp_: ViewPager? = null
    private var rg_: RadioGroup?=null
    private var hv_: HorizontalScrollView? = null
    private val parent: ViewGroup?=null
    private val TAG= Constant.Message_TAG

    companion object {
       var message_userData=HashMap<String, MessageUserData>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)


        initView()
        initImageLoader()
        addSomeListener()
        initTab()
        initViewPager()
        JumpJpushMessage()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Message,"消息中心主页面")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Message,"消息中心主页面")
        super.onPause()
    }

    fun JumpJpushMessage(){
        val choose=intent.getIntExtra("choose",0)
        if(choose!=0){
            LogUtils.d_debugprint(Constant.Message_TAG,"现在在Message中，choose目前为：$choose,即将执行跳转")
            val handler= Handler()
            handler.postDelayed({
               vp_!!.currentItem=choose
            },618)
        }
    }

    /** 初始化imageLoader  */
    private fun initImageLoader() {
        val options = DisplayImageOptions.Builder().showImageForEmptyUri(R.mipmap.icon_empty)
                .showImageOnFail(R.mipmap.icon_empty).showImageOnLoading(R.mipmap.icon_empty).cacheInMemory(true)
                .cacheOnDisc(true).build()

        val cacheDir = File(Constant.DEFAULT_SAVE_IMAGE_PATH)
        val imageconfig = ImageLoaderConfiguration.Builder(this@Message)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(200)
                .discCache(UnlimitedDiscCache(cacheDir))
                .discCacheFileNameGenerator(Md5FileNameGenerator())
                .defaultDisplayImageOptions(options).build()

        ImageLoader.getInstance().init(imageconfig)

    }

    private fun initTab(){
            val hTabs = HTabDb.getMessageSelected()
            for (i in hTabs.indices) {
                //设置头部项布局初始化数据
                val rbButton = layoutInflater.inflate(R.layout.tab_top1, null) as RadioButton//=====直接用首页导航的tab布局
                rbButton.id = i
                rbButton.text = hTabs[i].name
                val dm = DisplayMetrics()
                val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
                wm.defaultDisplay.getMetrics(dm)
                val screenWidth = dm.widthPixels
                rbButton.width = screenWidth / 3

                val params = RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT,
                        RadioGroup.LayoutParams.WRAP_CONTENT)
                //加入RadioGroup
                rg_!!.addView(rbButton, params)
            }


         rg_!!.check(0)
    }

    private fun initViewPager(){
        hTabs = HTabDb.getMessageSelected()
        FirstMessageTab()
        SecondMessageTab()
        ThirdMessageTab()
        //设置viewpager适配器
        adapter = MessageAdapter(supportFragmentManager, newsList)
        vp_!!.adapter = adapter
        //两个viewpager切换不重新加载
        vp_!!.offscreenPageLimit = 2
        //设置默认
        vp_!!.currentItem = 0
        //设置viewpager监听事件
        vp_!!.setOnPageChangeListener(this)
    }

    private fun FirstMessageTab(){
        val fm1 = MessageFm1()
        val bundle1 = Bundle()
        bundle1.putString("name", hTabs!![0].name)
        fm1.arguments = bundle1
        newsList.add(fm1)
    }

    private fun SecondMessageTab(){
        val fm2 = MessageFm2()
        val bundle1 = Bundle()
        bundle1.putString("name", hTabs!![1].name)
        fm2.arguments = bundle1
        newsList.add(fm2)
    }

    private fun ThirdMessageTab(){
        val fm3 = MessageFm3()
        val bundle1 = Bundle()
        bundle1.putString("name", hTabs!![2].name)
        fm3.arguments = bundle1
        newsList.add(fm3)
    }

    private fun initView() {
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_textview = findViewById(R.id.title_right_textview) as TextView
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_right_imageview_btn1=findViewById(R.id.title_right_imageview_btn1) as Button
        title_right_imageview_btn2=findViewById(R.id.title_right_imageview_btn2) as Button

        vp_=findViewById(R.id.message_view) as ViewPager
        rg_=findViewById(R.id.message_rg) as RadioGroup
        hv_=findViewById(R.id.message_hv) as HorizontalScrollView
        rg_!!.setOnCheckedChangeListener { group, id ->vp_!!.currentItem=id  }
        setTitleVisible()
        bt1=findViewById(R.id.bt1) as Button
        bt2=findViewById(R.id.bt2) as Button
        bt3=findViewById(R.id.bt3) as Button
        
    }

    private fun setTitleVisible() {
        title_center_textview!!.text = "消息中心"
        title_right_textview!!.text = "清空"
        title_center_textview!!.visibility = View.VISIBLE
        title_right_textview!!.visibility = View.VISIBLE
    }

    override fun onPageScrollStateChanged(arg0: Int) {}

    override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

    override fun onPageSelected(id: Int) {
        if(id==0){
            title_right_imageview_btn!!.visibility=View.VISIBLE
            title_right_imageview_btn1!!.visibility=View.GONE
            title_right_imageview_btn2!!.visibility=View.GONE
        }else if(id==1){
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_imageview_btn1!!.visibility=View.VISIBLE
            title_right_imageview_btn2!!.visibility=View.GONE
        }else if(id==2){
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_imageview_btn1!!.visibility=View.GONE
            title_right_imageview_btn2!!.visibility=View.VISIBLE
        }
        setTab(id)
    }

    /***
     * 页面跳转切换头部偏移设置
     * @param id
     */
    private fun setTab(id: Int) {
        val rbButton = rg_!!.getChildAt(id) as RadioButton
        rbButton.isChecked = true
        val left = rbButton.left
        val width = rbButton.measuredWidth
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        val screenWidth = metrics.widthPixels//屏幕宽度
        //移动距离= 左边的位置 + button宽度的一半 - 屏幕宽度的一半
        val len = left + width / 2 - screenWidth / 2
        hv_!!.smoothScrollTo(len, 0)
    }

    private fun addSomeListener(){
       title_left_imageview_btn!!.setOnClickListener{
           finish()
           overridePendingTransition(0,R.anim.slide_right_out)
       }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.alpha=0.618f
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.alpha=1.0f
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)

            }
            false
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(0,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun query1NowAll():Int{
        val sql="select * from "+ Message1SQLiteOpenHelper.M1_Name+" where isRead='0'"
        val helper1=Message1SQLiteOpenHelper(this@Message)
        val cursor=helper1!!.readableDatabase.rawQuery(sql,null)
        var message1_unread=0
        while(cursor.moveToNext()){
            message1_unread++
        }
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message1有多少条未读的：$message1_unread\n\n")
        return message1_unread

    }

    private fun query2NowAll():Int{
        val sql="select * from "+ Message2SQLiteOpenHelper.M2_Name+" where isRead='0'"
        val helper2=Message2SQLiteOpenHelper(this@Message)
        val cursor= helper2!!.readableDatabase.rawQuery(sql,null)
        var message2_unread=0
        while(cursor.moveToNext()){
            message2_unread++
        }

        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message2有多少条未读的：$message2_unread\n\n")
        return message2_unread
    }

    private fun query3NowAll():Int{
        val sql="select * from "+ Message3SQLiteOpenHelper.M3_Name+" where isRead='0'"
        val helper3=Message3SQLiteOpenHelper(this@Message)
        val cursor= helper3!!.readableDatabase.rawQuery(sql,null)
        var message3_unread=0
        while(cursor.moveToNext()){
            message3_unread++
        }

        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message3有多少条未读的：$message3_unread\n\n")
        return message3_unread
    }
}
