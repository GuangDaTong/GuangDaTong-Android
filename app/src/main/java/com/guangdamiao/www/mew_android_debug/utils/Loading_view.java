package com.guangdamiao.www.mew_android_debug.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;

import com.guangdamiao.www.mew_android_debug.R;


/**
 * Created by Jason_Jan on 2017/12/14.
 */

public class Loading_view extends ProgressDialog {
   public Loading_view(Context context){
       super(context);
   }

   public Loading_view(Context context,int theme){
       super(context,theme);
   }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(getContext());
    }

    private void init(Context context){
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.loading);
        WindowManager.LayoutParams params=getWindow().getAttributes();
        params.width=WindowManager.LayoutParams.WRAP_CONTENT;
        params.height=WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);
    }

    @Override
    public void show(){
        super.show();
    }

    @Override
    public void dismiss(){
        super.dismiss();
    }
}
