package com.guangdamiao.www.mew_android_debug.bean.ask_bean

import java.io.Serializable


class ZanItem : Serializable {
    var id: String? = null
    var user: User? = null
    companion object {
        private const val serialVersionUID = 1L
    }
}
