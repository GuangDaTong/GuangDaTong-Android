package com.guangdamiao.www.mew_android_debug.global;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Environment;

import com.bulong.rudeness.RudenessScreenHelper;
import com.guangdamiao.www.mew_android_debug.R;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.tencent.bugly.crashreport.CrashReport;

import org.xutils.x;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cn.jiguang.analytics.android.api.JAnalyticsInterface;
import cn.jiguang.share.android.api.JShareInterface;
import cn.jpush.android.api.JPushInterface;


/**
 * Created by Jason_Jan on 2017/12/13.
 */

public class MainApplication extends Application {

    private List<Activity> activities = new ArrayList<Activity>();
    private static MainApplication instance;
    public static String ImagePath;
    private static Context mContext;

    public void onCreate() {
        super.onCreate();
        instance = this;

        //xutil3开源库的初始化
        x.Ext.init(this);

        //屏幕适应
       new RudenessScreenHelper(this,750).activate();

        //Bugly错误上报功能初始化
        CrashReport.initCrashReport(getApplicationContext(), "d6d42f104b", false);


        //极光推送
        JPushInterface.setDebugMode(false);
        JPushInterface.init(this);
        JPushInterface.stopCrashHandler(this);//关闭CrashLog上报

        //极光统计
        JAnalyticsInterface.init(this);
        JAnalyticsInterface.setDebugMode(false);
        JAnalyticsInterface.initCrashHandler(this);

        //极光分享
        JShareInterface.init(this);
        JShareInterface.setDebugMode(false);


        //日志统计
        LogUtils.INSTANCE.init(this);
        LogUtils.INSTANCE.d_debugprint("入口","入口");

        //上下文，方便以后调用
        mContext=getApplicationContext();//得到上下文

        //极光分享要用的
        new Thread(){
            @Override
            public void run() {
                File imageFile =  copyResurces( "mew_app_icon.png", "mew_share.png", 0);
                if(imageFile != null){
                    ImagePath = imageFile.getAbsolutePath();
                }
                super.run();
            }
        }.start();


    }


    private  File copyResurces(String src, String dest, int flag){
        File filesDir = null;
        try {
            if(flag == 0) {//copy to sdcard
                filesDir = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/mew/" + dest);
                File parentDir = filesDir.getParentFile();
                if(!parentDir.exists()){
                    parentDir.mkdirs();
                }
            }else{//copy to data
                filesDir = new File(getFilesDir(), dest);
            }
            if(!filesDir.exists()) {
                filesDir.createNewFile();
                InputStream open = getAssets().open(src);
                FileOutputStream fileOutputStream = new FileOutputStream(filesDir);
                byte[] buffer = new byte[4 * 1024];
                int len = 0;
                while ((len = open.read(buffer)) != -1) {
                    fileOutputStream.write(buffer, 0, len);
                }
                open.close();
                fileOutputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            if(flag == 0){
                filesDir = copyResurces( src,  dest, 1);
            }
        }
        return filesDir;
    }

    public MainApplication() {
    }


    public static Context getContext(){
        return mContext;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        initImageLoader();
    }


    public static MainApplication getInstance(){
        return instance;
    }

    public void addActivity(Activity activity){
        activities.add(activity);
    }

    public void finishActivity(Activity activity){
        if (activity!=null) {
            this.activities.remove(activity);
            activity.finish();
            activity = null;
        }
    }

    public void exit(){
        for (Activity activity : activities) {
            if (activity!=null) {
                this.activities.remove(activity);
                activity.finish();
            }
        }
        System.exit(0);
    }

    public void finishActivity(){
        for (Activity activity : activities) {
            if (null != activity) {
                activity.finish();
            }
        }
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    /**
     * 判断activity是否存在
     */
    public  boolean isExistActivityByClass(Class<?> cls) {

        for (Activity activity : activities) {
            LogUtils.INSTANCE.d_debugprint("消息中心--推送测试","\n\n\n现在在MainApplication中，参数cls为："+cls.toString()+"\n当前activity为："+activity.getClass().toString());
            if (activity.getClass().equals(cls)) {
                LogUtils.INSTANCE.d_debugprint("消息中心--推送测试","\n\n\n现在在MainApplication中，并且Navigation还存在");
                return true;
            }
        }
        LogUtils.INSTANCE.d_debugprint("消息中心--推送测试","\n\n\n现在在MainApplication中，并且Navigation不存在啦！！！");
        return false;
    }

    /** 初始化imageLoader */
    private void initImageLoader() {
        DisplayImageOptions options = new DisplayImageOptions.Builder().showImageForEmptyUri(R.mipmap.icon_empty)
                .showImageOnFail(R.mipmap.icon_empty).showImageOnLoading(R.mipmap.icon_empty).cacheInMemory(true)
                .cacheOnDisc(true).build();

        File cacheDir = new File(Constant.Companion.getDEFAULT_SAVE_IMAGE_PATH());
        ImageLoaderConfiguration imageconfig = new ImageLoaderConfiguration.Builder(this)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(200)
                .discCache(new UnlimitedDiscCache(cacheDir))
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .defaultDisplayImageOptions(options).build();

        ImageLoader.getInstance().init(imageconfig);

    }
}