package com.guangdamiao.www.mew_android_debug.loginActivity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.view.ViewCompat
import android.text.TextUtils
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import cn.smssdk.EventHandler
import cn.smssdk.SMSSDK
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.find.ServiceActivity
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.mob.MobSDK
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

class RegisterMain : Activity(),View.OnClickListener{

    private val TAG=Constant.Register_Tag
    private var eventHandler: EventHandler? = null//为了注册监听
    private var phone: EditText? = null//手机号
    private var iphone:String?=null
    private var but1: Button?=null//下一步
    private var code: EditText?=null//验证码
    private var icode: String=""
    private var termOfService: TextView?=null//服务条款
    private var register_main_link:TextView?=null
    private var register_main_delete_btn: Button?=null//删除图标
    private var sendCode: TextView?=null//发送验证码
    private var txt_leftTime: TextView?=null//发送验证码后的显示
    private var time = Constant.Register_SendCode_MaxTime
    private var flag_but1_click=false//发送按钮是否被点击


    protected fun a(): String? {
        return null
    }

    protected fun b(): String? {
        return null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        LoginCacheActivity.addActivity(this)
        fullscreen()
       // SMSSDK.setAskPermisionOnReadContact(true)
        init()
        addSomeListener()
        MobSDK.init(this, this.a(), this.b())

    }

    private fun addSomeListener(){
        register_main_delete_btn!!.setOnClickListener{
            val intent:Intent=Intent(this, LoginMain::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        sendCode!!.setOnClickListener(this)//设置发送验证监听器
        but1!!.setOnClickListener(this)//设置注册按钮监听器

        register_main_link!!.setOnClickListener{
            val intent:Intent=Intent(this@RegisterMain, ServiceActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }
    }

    private fun fullscreen(){
        setContentView(R.layout.register_main)
        val activity: Activity = this
        val statusColor: Int = Color.parseColor(Constant.FULLCOLOR) as Int
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = activity.window
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = statusColor
            val mContentView: ViewGroup = activity.findViewById(Window.ID_ANDROID_CONTENT) as ViewGroup
            val mChildView: View = mContentView.getChildAt(0)
            if (mChildView != null) {
                ViewCompat.setFitsSystemWindows(mChildView, false)
            }
        }
    }
    
    private fun init() {
        termOfService = findViewById(R.id.register_main_txt3) as TextView
        register_main_delete_btn=findViewById(R.id.register_main_delete_btn) as Button
        phone = findViewById(R.id.register_main_phone) as EditText
        code = findViewById(R.id.register_main_code) as EditText
        txt_leftTime = findViewById(R.id.register_main_txt_leftTime) as TextView
        sendCode = findViewById(R.id.register_main_send) as TextView
        but1 = findViewById(R.id.register_main_but1) as Button
        register_main_link=findViewById(R.id.register_main_link) as TextView
    }
    
    override fun onClick(v: View) {
        when (v.id) {
            R.id.register_main_send ->
                                        {
                                            flag_but1_click = true
                                            if (phonehasvalue()) {
                                                val pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                                                pDialog.progressHelper.barColor = Color.parseColor("#A5DC86")
                                                pDialog.titleText = "Loading"
                                                pDialog.setCancelable(true)
                                                pDialog.show()
                                                myserverCheckPhoneIsNew(this@RegisterMain,pDialog)
                                            }
                                        }
            R.id.register_main_but1 ->
                                         if(phonehasvalue() && codehasvalue()&&send_code_click())
                                          {
                                              val pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                                              pDialog.progressHelper.barColor = Color.parseColor("#A5DC86")
                                              pDialog.titleText = "Loading"
                                              pDialog.setCancelable(true)
                                              pDialog.show()
                                              MyServerCheck(this@RegisterMain,pDialog)
                                          }

                                       /* if(phonehasvalue()&&codehasvalue()){
                                            var dataFromServer:Bundle=Bundle()
                                            dataFromServer.putString("phone",iphone!!)
                                            dataFromServer.putString("result","123")
                                            val intent:Intent=Intent(this@RegisterMain,OrganizingData::class.java)
                                            intent.putExtras(dataFromServer)
                                            startActivity(intent)//------------------------活动跳转处
                                            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
                                        }*/

            else -> {}
        }
    }

    private fun send_code_click():Boolean{
        if(flag_but1_click){
            return true
        }else{
            Toasty.warning(this@RegisterMain,Constant.Register_No_SendCode).show()
            return false
        }
    }

    private fun phonehasvalue():Boolean{
        if (!TextUtils.isEmpty(phone!!.text.toString().trim { it <= ' ' })) {
            if (phone!!.text.toString().trim { it <= ' ' }.length == 11) {
                iphone = phone!!.text.toString().trim { it <= ' ' }
                code!!.requestFocus()
                return true
            } else {
                Toasty.warning(this@RegisterMain,"手机号输入有误&_&").show()
                phone!!.requestFocus()
                return false
            }
        } else {
            Toasty.warning(this@RegisterMain,"请输入您的手机号").show()
            phone!!.requestFocus()
            return false
        }
    }

    private fun codehasvalue():Boolean{
        if (!TextUtils.isEmpty(code!!.text.toString().trim { it <= ' ' })) {
            if (code!!.text.toString().trim { it <= ' ' }.length == 4) {
                icode = code!!.text.toString().trim { it <= ' ' }
                return true
            } else {
                Toasty.warning(this@RegisterMain, Constant.Register_Error_Code).show()
                code!!.requestFocus()
                return false
            }
        } else {
            Toasty.warning(this@RegisterMain, Constant.Register_No_Code).show()
            code!!.requestFocus()
            return false
        }
        return false
    }

    //验证码发送成功后提示文字
    private fun reminderText() {
        var handlerText: Handler = object : Handler() {
            override fun handleMessage(msg: Message) {
                if (msg.what == 1) {
                    if (time > 0) {
                        txt_leftTime!!.text = "验证码已发送" + time + "秒"
                        time--
                        this.sendEmptyMessageDelayed(1, 1000)
                    } else {
                        txt_leftTime!!.text = ""
                        time = 60
                        txt_leftTime!!.visibility = View.GONE
                        sendCode!!.visibility = View.VISIBLE
                    }
                } else {
                    code!!.setText("")
                    txt_leftTime!!.text = ""
                    time = 60
                    txt_leftTime!!.visibility = View.GONE
                    sendCode!!.visibility = View.VISIBLE
                }
            }
        }
        txt_leftTime!!.visibility = View.VISIBLE
        sendCode!!.visibility=View.GONE
        handlerText.sendEmptyMessageDelayed(1, 1000)
    }

    fun  MyServerCheck(context:Context,pDialog: SweetAlertDialog){
        if(NetUtil.checkNetWork(this)){
            val salt_phone:String
            val urlPath:String
            if(LogUtils.APP_IS_DEBUG){
                salt_phone= Constant.SALTFORTEST+iphone
                urlPath=Constant.BASEURLFORTEST + Constant.OrganizingData_Check
            }else{
                salt_phone= Constant.SALTFORRELEASE+iphone
                urlPath=Constant.BASEURLFORRELEASE + Constant.OrganizingData_Check
            }
            val sign:String= MD5Util.md5(salt_phone)
            val jsonObject=JSONObject()
            jsonObject.put("code",icode)
            jsonObject.put("sign",sign)
            jsonObject.put("phone",iphone)
            val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")

            LogUtils.d_debugprint(Constant.LoginMain_Tag, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())

            val client = AsyncHttpClient(true, 80, 443)
            client.post(context, urlPath, stringEntity, Constant.ContentType, object : AsyncHttpResponseHandler() {

                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                    if (responseBody != null) {
                        if(pDialog!=null) pDialog.cancel()
                        val resultDate = String(responseBody!!, charset("utf-8"))
                        val response = JSONObject(resultDate)
                        if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                            val response_string = response.toString()
                            if (JsonUtil.get_key_string("code", response_string).equals(Constant.RIGHTCODE)) {
                                var result:String=""
                                result= JsonUtil.get_key_string(Constant.Server_Result,response_string!!)
                                //用Bundle记录服务器返回的数据
                                var dataFromServer:Bundle=Bundle()
                                dataFromServer.putString("phone",iphone!!)
                                dataFromServer.putString("result",result!!)
                                LogUtils.d_debugprint(Constant.LoginMain_Tag, "给完善资料传的参数为：phone=$iphone,result=$result \n")
                                val intent:Intent=Intent(this@RegisterMain,OrganizingData::class.java)
                                intent.putExtras(dataFromServer)
                                startActivity(intent)//------------------------活动跳转处
                                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
                            }else{
                                Toasty.error(this@RegisterMain,"亲，您输入的验证码有误！").show()
                            }
                        }
                    }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                    if (pDialog != null) pDialog.cancel()
                    LogUtils.d_debugprint(Constant.LoginMain_Tag, "网络异常！！！\n")
                    Toasty.info(context, Constant.REQUESTFAIL).show()
                }
            })
        }else{
            Toasty.info(this,Constant.NONETWORK).show()
        }

    }

    fun myserverCheckPhoneIsNew(context:Context,pDialog: SweetAlertDialog){
        if (NetUtil.checkNetWork(context)) {
            val sign:String
            val urlPath:String
            val account:String=iphone!!
            if(LogUtils.APP_IS_DEBUG){
                sign=MD5Util.md5(Constant.SALTFORTEST+account)
                urlPath=Constant.BASEURLFORTEST + Constant.Register_IsAccountExist
            }else{
                sign=MD5Util.md5(Constant.SALTFORRELEASE+account)
                urlPath=Constant.BASEURLFORRELEASE + Constant.Register_IsAccountExist
            }
            val jsonObject = JSONObject()
            jsonObject.put("account", account)
            jsonObject.put("sign",sign)
            val stringEntity = StringEntity(jsonObject.toString(), "UTF-8")
            LogUtils.d_debugprint(Constant.LoginMain_Tag, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())

            val client = AsyncHttpClient(true, 80, 443)
            client.post(context, urlPath, stringEntity, Constant.ContentType, object : AsyncHttpResponseHandler() {

                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                    if(responseBody!=null){
                        val resultDate = String(responseBody!!, charset("utf-8"))
                        val response= JSONObject(resultDate)
                        if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                            val response_string = response.toString()
                            if (JsonUtil.get_key_string("code", response_string).equals(Constant.RIGHTCODE)) {
                                LogUtils.d_debugprint(Constant.LoginMain_Tag, "服务器返回的数据为：$response_string\n")

                                var getCode:String=""
                                var result:String=""
                                var isExist:Boolean
                                getCode=JsonUtil.get_key_string(Constant.Server_Code,response_string!!)
                                result=JsonUtil.get_key_string(Constant.Server_Result,response_string!!)
                                isExist=JsonUtil.get_key_boolean("isExist",result!!)
                                LogUtils.d_debugprint(Constant.LoginMain_Tag,isExist.toString())
                                if(pDialog!=null) pDialog.cancel()
                                if(Constant.RIGHTCODE.equals(getCode)&&!isExist) {

                                        SMSSDK.getVerificationCode("86", iphone)//=====发送验证码
                                        reminderText()

                                } else if(isExist){
                                       Toasty.error(context,"亲，账号已经被注册过了~").show()
                                }else{
                                    Toasty.error(context,"亲，无法发送验证码！").show()
                                }

                            }
                        }
                    }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                    if(pDialog!=null) pDialog.cancel()
                    Toasty.info(context,Constant.REQUESTFAIL).show()
                }

            })
        } else {
            Toasty.info(context, Constant.NONETWORK).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        SMSSDK.unregisterEventHandler(eventHandler)
    }

    /**
     * 点击空白区域隐藏键盘.
     */
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@RegisterMain.currentFocus != null) {
                if (this@RegisterMain.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@RegisterMain.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
       if(keyCode==KeyEvent.KEYCODE_BACK){
           val intent:Intent=Intent(this, LoginMain::class.java)
           startActivity(intent)
           finish()
           overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
       }
        return super.onKeyDown(keyCode, event)
    }

}
