package com.guangdamiao.www.mew_android_debug.navigation.find

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListFindAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.navigation.ImageSingleActivity
import com.guangdamiao.www.mew_android_debug.navigation.ViewHolder
import com.nostra13.universalimageloader.core.ImageLoader
import java.util.*

/**

 */
class FindPartTimeAdpter(protected var context: Context, list_partTimeJob: ArrayList<ListFindAll>) : BaseAdapter() {
    private var list_partTimeJob = ArrayList<ListFindAll>()


    init {
        this.list_partTimeJob = list_partTimeJob

    }

    override fun getCount(): Int {
        return list_partTimeJob.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.find_list, parent, false)
        }

        val listpartTimeJob = list_partTimeJob.get(position)//获取一个
        val list_icon = ViewHolder.get<ImageView>(convertView, R.id.find_list_icon)
        val list_title = ViewHolder.get<TextView>(convertView, R.id.find_list_title)
        val list_time = ViewHolder.get<TextView>(convertView, R.id.find_list_time1)
        val list_position = ViewHolder.get<TextView>(convertView, R.id.find_list_position1)
        val list_fee = ViewHolder.get<TextView>(convertView, R.id.find_list_fee1)



        list_title.text=listpartTimeJob.title
        if(listpartTimeJob.title!!.length>20){
            list_title.text=listpartTimeJob.title!!.substring(0,18)+"..."
        }

        val time = listpartTimeJob.createTime!!.substring(0, 19).replace("T", " ")
        list_time.text=time
        list_position.text=listpartTimeJob.position
        list_fee.text=listpartTimeJob.fee


        ImageLoader.getInstance().displayImage( listpartTimeJob.icon!!, list_icon)
        val link = listpartTimeJob.link
        val webView_id = listpartTimeJob.id
        val zan = listpartTimeJob.zan_
        val isZan = listpartTimeJob.isZan
        val readCount = listpartTimeJob.readCount
        val isFavorite = listpartTimeJob.isFavorite

        list_icon!!.setOnClickListener{
            if(listpartTimeJob.icon!=null) ImageSingleActivity.startImageSingleActivity(context,listpartTimeJob.icon!!)
        }

        convertView!!.setOnClickListener {
            val intent = Intent(context, FindWebView::class.java)
            val bundle = Bundle()
            bundle.putInt("position",position)
            bundle.putString("id", webView_id)
            bundle.putString("link", link)
            bundle.putString("zan", zan)
            bundle.putString("isZan", isZan)
            bundle.putString("readCount", readCount)
            bundle.putString("isFavorite", isFavorite)
            bundle.putString("urlEnd", Constant.Find_PartTimeJob)
            bundle.putString("title_first", list_title.text.toString())
            intent.putExtras(bundle)
            context.startActivity(intent)
            //这里处理点击事件
        }



        return convertView
    }


}
