package com.guangdamiao.www.mew_android_debug.navigation.find.findcache

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2017/12/10.
 */

class Find1SQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        private val name = "find1.db"
        private val version = 1
         val F1_Name="find1"
         val F1_Id="id"
         val F1_Sid="s_id"
         val F1_Icon="icon"
         val F1_Title="title"
         val F1_CreateTime="createTime"
         val F1_Position="position"
         val F1_Fee="fee"
         val F1_Description="description"
         val F1_Link="link"
         val F1_ReadCount="readCount"
         val F1_Zan="zan"
         val F1_IsZan="isZan"
         val F1_IsFavorite="isFavorite"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table find1(id integer primary key autoincrement," +
                "${Find1SQLiteOpenHelper.F1_Sid} varchar(20)," +
                "${Find1SQLiteOpenHelper.F1_Icon} varchar(100)," +
                "${Find1SQLiteOpenHelper.F1_Title} varchar(20),"+
                "${Find1SQLiteOpenHelper.F1_CreateTime} varchar(50)," +
                "${Find1SQLiteOpenHelper.F1_Position} varchar(20)," +
                "${Find1SQLiteOpenHelper.F1_Fee} varchar(20)," +
                "${Find1SQLiteOpenHelper.F1_Description} varchar(382)," +
                "${Find1SQLiteOpenHelper.F1_Link} varchar(100)," +
                "${Find1SQLiteOpenHelper.F1_ReadCount} varchar(5)," +
                "${Find1SQLiteOpenHelper.F1_Zan} varchar(5)," +
                "${Find1SQLiteOpenHelper.F1_IsZan} varchar(2)," +
                "${Find1SQLiteOpenHelper.F1_IsFavorite} varchar(2))")

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    //打开数据库时调用
    override fun onOpen(db: SQLiteDatabase) {
        //如果数据库已经存在，则再次运行不执行onCreate()方法，而是执行onOpen()打开数据库
        super.onOpen(db)
    }

}
