package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Black

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MyBlackFriends
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.JSONFormatUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/20.
 */


class BlackRequest {

    companion object {

        fun getResultFromServer(context: Context, pageIndexNow: Int, listAll: ArrayList<MyBlackFriends>, adapter: Main_BlackBookAdapter, ptrClassicFrameLayout: PtrClassicFrameLayout, listener: IDataRequestListener2String) {

            if (NetUtil.checkNetWork(context)) {
                val pageSize= Constant.PAGESIZE
                val sign:String
                val urlPath:String
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if(LogUtils.APP_IS_DEBUG){
                    sign= MD5Util.md5(Constant.SALTFORTEST+token)
                    urlPath= Constant.BASEURLFORTEST + Constant.URL_List_Blacklist
                }else{
                    sign= MD5Util.md5(Constant.SALTFORRELEASE+token)
                    urlPath= Constant.BASEURLFORRELEASE + Constant.URL_List_Blacklist
                }

                val jsonObject=JSONObject()
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")

                LogUtils.d_debugprint(Constant.Me_TAG, Constant.TOSERVER + urlPath + "\n提交的参数为：\n" + jsonObject.toString())

                val client=AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                      if(responseBody!=null){
                          val resultDate = String(responseBody!!, charset("utf-8"))
                          val response=JSONObject(resultDate)
                          LogUtils.d_debugprint(Constant.Me_TAG, response!!.toString())
                          val resultFromServer=response!!.toString()
                          LogUtils.d_debugprint(Constant.Me_TAG, Constant.GETDATAFROMSERVER + JSONFormatUtil.formatJson(resultFromServer!!)+"\n\n")
                          var getCode: String = ""
                          getCode = JsonUtil.get_key_string(Constant.Server_Code, resultFromServer!!)
                          if (Constant.RIGHTCODE.equals(getCode)) {
                              var result: String = ""
                              var getData: List<Map<String, Any?>>
                              result = JsonUtil.get_key_string(Constant.Server_Result, resultFromServer!!)
                              getData = JsonUtil.getListMap("data", result)
                              LogUtils.d_debugprint(Constant.Me_TAG, "json解析出来的对象是=" + getData.toString())

                              if (getData != null && getData.size>0) {
                                  if(pageIndexNow==0){
                                      listAll.clear()
                                  }
                                  for (i in getData.indices) {
                                      val myFriend = MyBlackFriends()
                                      if (getData[i].getValue("createTime")!= null && getData[i].getValue("icon")!= null && getData[i].getValue("nickname")!= null && getData[i].getValue("id")!= null) {
                                          myFriend.createTime = getData[i].getValue("createTime").toString()
                                          myFriend.icon = getData[i].getValue("icon").toString()
                                          myFriend.nickname = getData[i].getValue("nickname").toString()
                                          myFriend.id = getData[i].getValue("id").toString()
                                          listAll.add(myFriend)
                                      }
                                  }
                                  adapter!!.notifyDataSetChanged()
                                  ptrClassicFrameLayout!!.refreshComplete()
                                  ptrClassicFrameLayout!!.loadMoreComplete(true)
                                  listener.loadSuccess(result)
                              } else if(pageIndexNow==0){
                                  ptrClassicFrameLayout!!.refreshComplete()
                                  ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                  ptrClassicFrameLayout!!.loadMoreComplete(false)
                                  listener.loadSuccess("noFriends")
                              }else{
                                  ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                  ptrClassicFrameLayout!!.loadMoreComplete(false)
                                  listener.loadSuccess(result)
                              }
                          }else if(Constant.LOGINFAILURECODE.equals(getCode)){
                              val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                              val account=read.getString("account","")
                              val password=read.getString("password","")
                              val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                              val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.SERVERBROKEN, Context.MODE_APPEND).edit()
                              editor2.putString("account",account)
                              editor2.putString("password",password)
                              editor2.commit()
                              editor.clear()
                              editor.commit()
                              SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                      .setTitleText(Constant.LOGINFAILURETITLE)
                                      .setContentText(Constant.LOGINFAILURE)
                                      .setCustomImage(R.mipmap.mew_icon)
                                      .setConfirmText("确定")
                                      .setConfirmClickListener { sDialog ->
                                          sDialog.dismissWithAnimation()
                                          //跳转到登录，不用传东西过去
                                          val intent = Intent(context, LoginMain::class.java)
                                          context.startActivity(intent)
                                      }
                                      .setCancelText("取消")
                                      .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                      .show()
                          }else{
                              Toasty.info(context, Constant.SERVERBROKEN).show()
                          }
                       }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                        ptrClassicFrameLayout!!.refreshComplete()
                         Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            } else {
                Toasty.info(context, Constant.NONETWORK).show()
            }
        }

        fun deleteFriends(context: Context,IDs:ArrayList<String>,token:String,listener:IDataRequestListener2String){
            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""

                if(LogUtils.APP_IS_DEBUG){
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                    urlPath=Constant.BASEURLFORTEST+Constant.URL_Delete_Blacklist
                }else{
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Delete_Blacklist
                }

                val jsonObject=JSONObject()
                val jsonArray= JSONArray()
                for(i in IDs.indices){
                    jsonArray.put(i,IDs[i])
                }
                jsonObject.put("IDs",jsonArray)
                jsonObject.put("sign",sign)

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Icon_Info_TAG,Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client=AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if(statusCode.toString().equals(Constant.RIGHTCODE)){
                                val response_string=response.toString()
                                //开始解析
                                LogUtils.d_debugprint(Constant.Icon_Info_TAG,"服务器返回删除黑名单是："+response_string)
                                //移除黑名单全局文件
                                val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
                                val blacklist = read.getString("blacklist", "[]")

                                val blacklist_array=ArrayList<String>()
                                val jsonArray=JSONArray(blacklist)
                                if(jsonArray.length()>0){
                                    for(i in 0..jsonArray.length()-1){
                                        blacklist_array.add(jsonArray.getString(i))
                                    }
                                    for(j in IDs.indices){
                                        blacklist_array.remove(IDs[j])
                                    }
                                }

                                val jsonArray_new= JSONArray()
                                for(i in blacklist_array.indices){
                                    jsonArray_new.put(blacklist_array[i])
                                }

                                val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE).edit()
                                editor.putString("blacklist",jsonArray_new.toString())
                                editor.commit()

                                LogUtils.d_debugprint(Constant.Icon_Info_TAG,"\n黑名单：原来的json为：$blacklist\n现在的json为：${jsonArray_new.toString()}")

                                listener.loadSuccess(response_string)
                            }else{
                                Toasty.info(context,Constant.NONETWORK).show()
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }
    }
}
