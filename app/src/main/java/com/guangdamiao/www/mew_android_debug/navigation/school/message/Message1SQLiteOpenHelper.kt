package com.guangdamiao.www.mew_android_debug.navigation.school.message

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2017/12/10.
 */

class Message1SQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        private val name = "message1.db"
        private val version = 1
         val M1_Name="message1"
         val M1_Id="id"
         val M1_Sid="s_id"
         val M1_Icon="icon"
         val M1_Gender="gender"
         val M1_NickName="nickname"
         val M1_SenderID="senderID"
         val M1_CreateTime="createTime"
         val M1_Words="words"
         val M1_IsRead="isRead"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table message1(id integer primary key autoincrement,s_id varchar(20),icon varchar(50), gender varchar(4)," + "nickname varchar(20),senderID varchar(20),createTime varchar(50),words varchar(2048),isRead varchar(2))")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    //打开数据库时调用
    override fun onOpen(db: SQLiteDatabase) {
        //如果数据库已经存在，则再次运行不执行onCreate()方法，而是执行onOpen()打开数据库
        super.onOpen(db)
    }

}
