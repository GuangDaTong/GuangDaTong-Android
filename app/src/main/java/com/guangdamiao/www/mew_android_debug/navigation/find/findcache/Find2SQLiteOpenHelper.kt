package com.guangdamiao.www.mew_android_debug.navigation.find.findcache

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2017/12/10.
 */

class Find2SQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        private val name = "find2.db"
        private val version = 1
         val F2_Name="find2"
         val F2_Id="id"
         val F2_Sid="s_id"
         val F2_Icon="icon"
         val F2_Title="title"
         val F2_CreateTime="createTime"
         val F2_Position="position"
         val F2_Fee="fee"
         val F2_Description="description"
         val F2_Link="link"
         val F2_ReadCount="readCount"
         val F2_Zan="zan"
         val F2_IsZan="isZan"
         val F2_IsFavorite="isFavorite"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table find2(id integer primary key autoincrement," +
                "${Find2SQLiteOpenHelper.F2_Sid} varchar(20)," +
                "${Find2SQLiteOpenHelper.F2_Icon} varchar(100)," +
                "${Find2SQLiteOpenHelper.F2_Title} varchar(20),"+
                "${Find2SQLiteOpenHelper.F2_CreateTime} varchar(50)," +
                "${Find2SQLiteOpenHelper.F2_Position} varchar(20)," +
                "${Find2SQLiteOpenHelper.F2_Fee} varchar(20)," +
                "${Find2SQLiteOpenHelper.F2_Description} varchar(382)," +
                "${Find2SQLiteOpenHelper.F2_Link} varchar(100)," +
                "${Find2SQLiteOpenHelper.F2_ReadCount} varchar(5)," +
                "${Find2SQLiteOpenHelper.F2_Zan} varchar(5)," +
                "${Find2SQLiteOpenHelper.F2_IsZan} varchar(2)," +
                "${Find2SQLiteOpenHelper.F2_IsFavorite} varchar(2))")

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    //打开数据库时调用
    override fun onOpen(db: SQLiteDatabase) {
        //如果数据库已经存在，则再次运行不执行onCreate()方法，而是执行onOpen()打开数据库
        super.onOpen(db)
    }

}
