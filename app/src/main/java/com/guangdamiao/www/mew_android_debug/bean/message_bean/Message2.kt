package com.guangdamiao.www.mew_android_debug.bean.message_bean

import java.io.Serializable


class Message2 : Serializable {
    var id: String? = null
    var askID:String?=null
    var senderID: String? = null
    var type: String? = null
    var detail: String? = null
    var createTime:String?=null
    var icon:String?=null
    var gender:String?=null
    var nickname:String?=null
    var isRead:String?=""

    constructor():super()
    constructor(isRead:String,id:String,askID:String,senderID:String,type: String,nickname:String,detail:String,createTime:String, icon:String,gender:String){
        this.id=id
        this.askID=askID
        this.senderID=senderID
        this.type=type
        this.createTime=createTime
        this.icon=icon
        this.nickname=nickname
        this.detail=detail
        this.gender=gender
        this.isRead=isRead
    }


    override fun toString(): String {
        return "CommentItem[id=$id,\naskID=$askID,\ncreateTime=$createTime,\nicon=$icon,\nnickname=$nickname,\ndetail=$detail,\ngender=$gender]"
    }
}
