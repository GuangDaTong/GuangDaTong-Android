package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.find

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListFindFavoriteAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ImageSingleActivity
import com.guangdamiao.www.mew_android_debug.navigation.ViewHolder
import com.guangdamiao.www.mew_android_debug.navigation.find.FindWebView
import com.nostra13.universalimageloader.core.ImageLoader
import es.dmoral.toasty.Toasty
import java.util.*

class Find_Favorite2Adpter(protected var context: Context, list_train: ArrayList<ListFindFavoriteAll>) : BaseAdapter() {
    private var list_train:ArrayList<ListFindFavoriteAll>?=null
    private var icon: String? = null
    private var onShowItemClickListener: Find_Favorite2Adpter.OnShowItemClickListener?=null

    init {
        this.list_train = list_train

    }

    override fun getCount(): Int {
        return list_train!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.find_list2, parent, false)
        }

        val listTrain = list_train!!.get(position)//获取一个

        val list_icon = ViewHolder.get<ImageView>(convertView, R.id.train_list_icon)
        val list_title = ViewHolder.get<TextView>(convertView, R.id.train_list_title)
        val list_time = ViewHolder.get<TextView>(convertView, R.id.train_list_time1)
        val list_position = ViewHolder.get<TextView>(convertView, R.id.train_list_position1)
        val list_fee = ViewHolder.get<TextView>(convertView, R.id.train_list_fee1)
        val list_description = ViewHolder.get<TextView>(convertView, R.id.train_list_description)
        val train_favorite_checkBox=ViewHolder.get<CheckBox>(convertView,R.id.train_favorite_checkBox)
        val train_list_to=ViewHolder.get<ImageView>(convertView,R.id.train_list_to)
        val train_favorite_ll=ViewHolder.get<LinearLayout>(convertView,R.id.train_favorite_ll)

        val time = listTrain.createTime!!.substring(0, 19).replace("T", " ")
        LogUtils.d_debugprint(Constant.Me_Favorite_TAG, "培训发表时间：$time")

        icon = listTrain.icon
        list_title.setText(listTrain.title)
        if(listTrain.title!!.length>18){
            list_title!!.setText(listTrain!!.title!!.substring(0,18)+"...")
        }
        list_time.setText(time)
        list_position.setText(listTrain.position)
        list_fee.setText(listTrain.fee)
        list_description.text = "摘要：" + listTrain.description!!

        ImageLoader.getInstance().displayImage( icon!!, list_icon)

        list_icon!!.setOnClickListener{
            if(listTrain.icon!=null) ImageSingleActivity.startImageSingleActivity(context,listTrain.icon!!)
        }

        val link = listTrain.link
        val webView_id = listTrain.id
        val zan = listTrain.zan_
        val isZan = listTrain.isZan
        val readCount = listTrain.readCount

        train_favorite_ll!!.setOnClickListener {
            val intent = Intent(context, FindWebView::class.java)
            val bundle = Bundle()

            bundle.putString("id", webView_id)
            if(webView_id.equals("")){
                Toasty.info(context,"亲，该内容已被删除了哦~").show()
            }else{
                bundle.putString("link", link)
                bundle.putString("zan", zan)
                bundle.putString("isZan", isZan)
                bundle.putString("readCount", readCount)
                bundle.putString("isFavorite", "1")
                bundle.putString("urlEnd", Constant.Find_Training)
                bundle.putString("title_first", listTrain.title)

                intent.putExtras(bundle)
                context.startActivity(intent)
            }
        }

        if(listTrain.isShow!!){
            train_favorite_checkBox!!.visibility=View.VISIBLE
            train_list_to!!.visibility=View.GONE
            train_favorite_ll!!.setOnClickListener{
                if(!listTrain.isChecked!!){
                    listTrain.isChecked=true
                    train_favorite_checkBox!!.isChecked=true
                }else{
                    train_favorite_checkBox!!.isChecked=false
                    listTrain.isChecked=false
                }
                onShowItemClickListener!!.onShowItemClick(listTrain)
            }
        }else{
            train_favorite_checkBox!!.visibility=View.GONE
            train_list_to!!.visibility=View.VISIBLE
        }


        train_favorite_checkBox!!.isChecked=listTrain.isChecked!!


        return convertView!!
    }

    interface OnShowItemClickListener {
        fun onShowItemClick(bean: ListFindFavoriteAll)
    }

    fun setOnShowItemClickListener(onShowItemClickListener: OnShowItemClickListener) {
        this.onShowItemClickListener = onShowItemClickListener
    }


}
