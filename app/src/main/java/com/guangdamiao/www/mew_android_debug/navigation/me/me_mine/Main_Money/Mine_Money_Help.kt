package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Money

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.*
import android.widget.Button
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.Loading_view
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils

class Mine_Money_Help : Activity() {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var webView_content:WebView?=null
    private var loading_first: Loading_view?=null
    private var urlPath:String?=null
    private var urlFirst:String?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        setContentView(R.layout.me_webview)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        //加载
        loading_first= Loading_view(this@Mine_Money_Help,R.style.CustomDialog)
        loading_first!!.show()

        initView()
        addSomeListener()
        getUrl()

    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Mine_Money_Help,"金币收益记录帮助")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Mine_Money_Help,"金币收益记录帮助")
        super.onPause()
    }

    private fun getUrl(){
        if(LogUtils.APP_IS_DEBUG){
            urlFirst= Constant.BASEURLFORTEST
        }else{
            urlFirst= Constant.BASEURLFORRELEASE
        }

        urlPath=urlFirst!!+"/help/coin"
        if(urlPath!=null){
            getWebViewTitle()
        }
    }

    private fun getWebViewTitle(){
        val wvcc: WebChromeClient =object: WebChromeClient(){
            override fun onReceivedTitle(view: WebView?, title: String?) {
                super.onReceivedTitle(view, title)
                if(loading_first!=null){
                    loading_first!!.dismiss()
                }
            }
        }
        webView_content!!.setWebChromeClient(wvcc)
        webView_content!!.loadUrl(urlPath)
        webView_content!!.setWebViewClient(object: WebViewClient(){
            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                super.onReceivedError(view, request, error)
                if(loading_first!=null){
                    loading_first!!.dismiss()
                }

            }
        })
    }

    fun initView(){
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_center_textview!!.text="关于金币收益"
        title_center_textview!!.visibility= View.VISIBLE
        webView_content=findViewById(R.id.webView_content) as WebView
        val webSettings:WebSettings=webView_content!!.settings
        webSettings.javaScriptEnabled=true
        webSettings.blockNetworkImage=false
        webSettings.useWideViewPort=true
        webSettings.loadWithOverviewMode=true
        webSettings.builtInZoomControls=true
        webSettings.setSupportZoom(true)
        webSettings.displayZoomControls=false

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP)
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE)
    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            if(webView_content!!.canGoBack()){
                webView_content!!.goBack()
            }else{
                finish()
                overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            if(webView_content!!.canGoBack()){
                webView_content!!.goBack()
            }else{
                finish()
                overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
            }
        }
        return super.onKeyDown(keyCode, event)
    }
}
