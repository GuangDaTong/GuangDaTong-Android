package com.guangdamiao.www.mew_android_debug.navigation.me.me_about

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.utils.Common
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils

class MeAbout : BaseActivity() {

    private var title_left_imageview: ImageView?=null
    private var title_left_imageview_btn: Button?=null
    private var title_center_textview: TextView?=null
    private var title_right_imageview:ImageView?=null
    private var me_about_join_rl:RelativeLayout?=null
    private var me_about_terms_rl:RelativeLayout?=null
    private var me_about_version_tv:TextView?=null
    private var me_about_mew_rl:RelativeLayout?=null

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@MeAbout,"我-关于")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@MeAbout,"我-关于")
        super.onPause()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_me_about)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        initView()
        addSomeListener()

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), 0)
            }
        }
    }

    private fun initView(){
        title_left_imageview=findViewById(R.id.title_left_imageview) as ImageView
        title_left_imageview_btn=findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview=findViewById(R.id.title_center_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        me_about_join_rl=findViewById(R.id.me_about_join_rl) as RelativeLayout
        me_about_terms_rl=findViewById(R.id.me_about_terms_rl) as RelativeLayout
        me_about_mew_rl=findViewById(R.id.me_about_mew_rl) as RelativeLayout
        me_about_version_tv=findViewById(R.id.me_about_version_tv) as TextView

        title_center_textview!!.text="关于"
        title_center_textview!!.visibility= View.VISIBLE
        title_right_imageview!!.visibility=View.GONE
        me_about_version_tv!!.text="广大通 "+Common.getVersion(this@MeAbout)
    }

    private fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
                 if(event.action== MotionEvent.ACTION_DOWN){
                     title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                     title_left_imageview_btn!!.alpha=0.618f


                 }else if(event.action== MotionEvent.ACTION_UP){
                     title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                     title_left_imageview_btn!!.alpha=1.0f

                 }
                 false
             }


        me_about_mew_rl!!.setOnClickListener{
            val intent: Intent = Intent(this@MeAbout, Me_About_Mew::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }

        me_about_join_rl!!.setOnClickListener{
            val intent: Intent = Intent(this@MeAbout, Me_About_JoinUS::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }

        me_about_terms_rl!!.setOnClickListener{
            val intent: Intent = Intent(this@MeAbout, Me_About_Terms::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }

        me_about_mew_rl!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                me_about_mew_rl!!.alpha=0.618f
                me_about_mew_rl!!.setBackgroundColor(resources.getColor(R.color.bgcolor2))

            }else{
                me_about_mew_rl!!.alpha=1.0f
                me_about_mew_rl!!.setBackgroundColor(Color.WHITE)

            }
            false
        }

        me_about_join_rl!!.setOnTouchListener { v, event ->
                 if(event.action== MotionEvent.ACTION_DOWN){
                     me_about_join_rl!!.alpha=0.618f
                     me_about_join_rl!!.setBackgroundColor(resources.getColor(R.color.bgcolor2))

                 }else{
                     me_about_join_rl!!.alpha=1.0f
                     me_about_join_rl!!.setBackgroundColor(Color.WHITE)

                 }
                 false
             }

        me_about_terms_rl!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                me_about_terms_rl!!.alpha=0.618f
                me_about_terms_rl!!.setBackgroundColor(resources.getColor(R.color.bgcolor2))

            }else{
                me_about_terms_rl!!.alpha=1.0f
                me_about_terms_rl!!.setBackgroundColor(Color.WHITE)

            }
            false
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }
}
