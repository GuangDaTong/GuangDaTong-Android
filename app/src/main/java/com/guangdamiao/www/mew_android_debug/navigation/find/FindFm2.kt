package com.guangdamiao.www.mew_android_debug.navigation.find


import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListFindAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.find.findcache.Find2SQLiteOpenHelper
import java.util.*

class FindFm2 : Fragment() {


    private var name: String? = null
    //栏目列表
    private val listItems = ArrayList<ListFindAll>()
    private var mlistView: ListView? = null
    private var adapter: FindTrainAdpter? = null
    private val TAG = Constant.Find_TAG
    private var pageNow = 0
    private val orderBy = "createTime"
    private val order = "desc"
    private var ptrClassicFrameLayout: PtrClassicFrameLayout? = null
    var handler = Handler()

    var flag=0

    private var helper2: Find2SQLiteOpenHelper?=null


    override fun setArguments(args: Bundle) {
        name = args.getString("name")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_find_all, container, false)
        helper2= Find2SQLiteOpenHelper(context)
        
        mlistView = view.findViewById(R.id.list_find_new) as ListView
        ptrClassicFrameLayout = view!!.findViewById(R.id.list_find_frame) as PtrClassicFrameLayout


        adapter = FindTrainAdpter(context, listItems)
        mlistView!!.setAdapter(adapter!!)//关键代码
        mlistView!!.setSelector(R.color.bgcolor)

        initData()

        return view
    }

    override fun onStart() {
        if(flag==0){
            flag++
            queryNowAll(Find2SQLiteOpenHelper.F2_Name,listItems)
            adapter!!.notifyDataSetChanged()
            ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(false) }, 150)
        }
        val read = context.getSharedPreferences("update_find2", Context.MODE_PRIVATE)
        val update_position = read.getInt("position", 0)
        val update_id=read.getString("id","")
        val update_isZan=read.getString("isZan","")
        val update_readCount=read.getString("readCount","")
        val update_zan=read.getString("zan","")
        if(listItems.size>0&&listItems.size-1>=update_position){
            if(listItems[update_position]!=null&&listItems[update_position].id!!.equals(update_id)){
                listItems[update_position].zan_=update_zan
                listItems[update_position].isZan=update_isZan
                listItems[update_position].readCount=update_readCount

                adapter!!.notifyDataSetChanged()
            }
        }

        super.onStart()
    }

    private fun initData() {

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    pageNow = 0
                    RequestServerList(pageNow,orderBy,order,adapter!!,ptrClassicFrameLayout!!)
                    adapter!!.notifyDataSetChanged()
                    if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                        ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                    }
                }, 100)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener( {
            handler.postDelayed({
                //请求更多页
                RequestServerList(++pageNow,orderBy,order,adapter!!,ptrClassicFrameLayout!!)
                //ptrClassicFrameLayout!!.loadMoreComplete(true)
                if (pageNow === 1) {
                    //set load more disable
                    ptrClassicFrameLayout!!.setLoadMoreEnable(true);
                }
            }, 100)
        })
    }


    private fun RequestServerList(pageNow: Int, orderBy: String, order: String,adapter:FindTrainAdpter,ptrClassicFrameLayout: PtrClassicFrameLayout) {
        SameListRequest.getResultFromServer(context, TAG, Constant.Find_Training, pageNow, orderBy, order, listItems,adapter,ptrClassicFrameLayout)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser){
            JAnalyticsInterface.onPageStart(context,"栏目-培训")
        }else{
            JAnalyticsInterface.onPageEnd(context,"栏目-培训")
        }
    }

    //数据库查询
    private fun queryNowAll(table:String,listItems:ArrayList<ListFindAll>){


        val sql="select * from "+ Find2SQLiteOpenHelper.F2_Name+" order by id asc"
        listItems.clear()
        val cursor= helper2!!.readableDatabase.rawQuery(sql,null)

        while(cursor.moveToNext()){
            val find= ListFindAll()
            find.id=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_Sid))
            find.icon=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_Icon))
            find.title=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_Title))
            find.createTime=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_CreateTime))
            find.position=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_Position))
            find.fee=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_Fee))
            find.description=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_Description))
            find.link=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_Link))
            find.readCount=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_ReadCount))
            find.zan_=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_Zan))
            find.isZan=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_IsZan))
            find.isFavorite=cursor.getString(cursor.getColumnIndex(Find2SQLiteOpenHelper.F2_IsFavorite))

            listItems.add(find)
        }
        LogUtils.d_debugprint(Constant.Find_TAG,"本地数据库查找到的$table ：\n\n"+listItems.toString())
    }


}
