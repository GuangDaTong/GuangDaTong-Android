package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.ViewPager
import android.util.DisplayMetrics
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.HTab
import com.guangdamiao.www.mew_android_debug.bean.HTabDb
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.ask.AskFavoriteFm
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.find.ActivityFavoriteFm
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.find.PartTimeJobFm
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.find.TrainFavoriteFm
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.school.ClubFavoriteFm
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.school.GuidanceFavoriteFm
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.school.LookFavoriteFm
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.school.SummaryFavoriteFm
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import es.dmoral.toasty.Toasty

class Mine_Favorite : FragmentActivity(), ViewPager.OnPageChangeListener {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_textview: TextView?=null
    private var token:String?=null
    private var hTabs:List<HTab>?=null
    private val newsList = ArrayList<Fragment>()
    private var adapter: FavoriteAdapter? = null
    private var vp_: ViewPager? = null
    private var rg_: RadioGroup?=null
    private var hv_: HorizontalScrollView? = null
    private val TAG= Constant.Me_TAG
    private var lay:LinearLayout?=null
    private var title_right_imageview_btn: Button?=null
    private var title_right_imageview_btn1: Button?=null
    private var title_right_imageview_btn2: Button?=null
    private var title_right_imageview_btn3: Button?=null
    private var title_right_imageview_btn4: Button?=null
    private var title_right_imageview_btn5: Button?=null
    private var title_right_imageview_btn6: Button?=null
    private var title_right_imageview_btn7: Button?=null

    private var flag=0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mine_favorite)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)

        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        token= read.getString("token", "")
        initView()
        addSomeListener()
        initTab()//=====最上方的单选按钮组
        listMineFavorite()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Mine_Favorite,"我的收藏主页面")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Mine_Favorite,"我的收藏主页面")
        super.onPause()
    }

    fun initView(){
        lay=findViewById(R.id.lay) as LinearLayout
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_right_imageview_btn1=findViewById(R.id.title_right_imageview_btn1) as Button
        title_right_imageview_btn2=findViewById(R.id.title_right_imageview_btn2) as Button
        title_right_imageview_btn3=findViewById(R.id.title_right_imageview_btn3) as Button
        title_right_imageview_btn4=findViewById(R.id.title_right_imageview_btn4) as Button
        title_right_imageview_btn5=findViewById(R.id.title_right_imageview_btn5) as Button
        title_right_imageview_btn6=findViewById(R.id.title_right_imageview_btn6) as Button
        title_right_imageview_btn7=findViewById(R.id.title_right_imageview_btn7) as Button

        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_textview=findViewById(R.id.title_right_textview) as TextView
        title_center_textview!!.text="我的收藏"
        title_right_textview!!.text="删除"
        title_center_textview!!.visibility= View.VISIBLE
        title_right_textview!!.visibility=View.VISIBLE
        vp_=findViewById(R.id.message_view) as ViewPager
        rg_=findViewById(R.id.message_rg) as RadioGroup
        hv_=findViewById(R.id.message_hv) as HorizontalScrollView
        rg_!!.setOnCheckedChangeListener { group, id ->vp_!!.currentItem=id  }

    }

    private fun dismissOperate() {
        val anim = AnimationUtils.loadAnimation(this@Mine_Favorite, R.anim.operate_out)
        lay!!.visibility=View.GONE
        lay!!.animation=anim
    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    fun listMineFavorite(){
        if(NetUtil.checkNetWork(this)){
            if(token!=null&&token!=""){
                //这里来请求服务器
                initViewPager()//=====每一个分页初始化
            }else{
                Toasty.info(this, Constant.NOTLOGIN).show()
            }
        }else{
            Toasty.info(this, Constant.NONETWORK).show()
        }
    }

    private fun initTab(){
        val hTabs = HTabDb.getFavoriteSelected()
        for (i in hTabs.indices) {
            val rbButton = layoutInflater.inflate(R.layout.tab_top2, null) as RadioButton//=====直接用首页导航的tab布局
            rbButton.id = i
            rbButton.text = hTabs[i].name
            val dm = DisplayMetrics()
            val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
            wm.defaultDisplay.getMetrics(dm)
            val screenWidth = dm.widthPixels
            rbButton.width = screenWidth / 4
            val params = RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT,
                    RadioGroup.LayoutParams.WRAP_CONTENT)
            rg_!!.addView(rbButton, params)
        }
        rg_!!.check(0)
    }

    private fun initViewPager(){
        hTabs = HTabDb.getFavoriteSelected()
        AskFavoriteTab()
        GuidanceFavoriteTab()
        SummaryFavoriteTab()
        ClubFavoriteTab()
        LookFavoriteTab()
        PartTimeJobFavoriteTab()
        TrainFavoriteTab()
        ActivityFavoriteTab()
        adapter = FavoriteAdapter(supportFragmentManager, newsList)//整理碎片融为一体，大体显示，后面还需要一个细节的Adapter
        vp_!!.adapter = adapter
        vp_!!.offscreenPageLimit = 7
        vp_!!.currentItem = 0
        vp_!!.setOnPageChangeListener(this)
    }

    private fun AskFavoriteTab(){
        val ask_f= AskFavoriteFm()
        val bundle_=Bundle()
        bundle_.putString("name", hTabs!![0].name)
        ask_f.arguments = bundle_
        newsList.add(ask_f)
    }

    private fun GuidanceFavoriteTab(){
        val gui_f= GuidanceFavoriteFm()
        val bundle_=Bundle()
        bundle_.putString("name", hTabs!![1].name)
        gui_f.arguments = bundle_
        newsList.add(gui_f)
    }

    private fun SummaryFavoriteTab(){
        val sum_f= SummaryFavoriteFm()
        val bundle_=Bundle()
        bundle_.putString("name", hTabs!![2].name)
        sum_f.arguments = bundle_
        newsList.add(sum_f)
    }

    private fun ClubFavoriteTab(){
        val clu_f= ClubFavoriteFm()
        val bundle_=Bundle()
        bundle_.putString("name", hTabs!![3].name)
        clu_f.arguments = bundle_
        newsList.add(clu_f)
    }

    private fun LookFavoriteTab(){
        val loo_f= LookFavoriteFm()
        val bundle_=Bundle()
        bundle_.putString("name", hTabs!![4].name)
        loo_f.arguments = bundle_
        newsList.add(loo_f)
    }

    private fun PartTimeJobFavoriteTab(){
        val par_f= PartTimeJobFm()
        val bundle_=Bundle()
        bundle_.putString("name", hTabs!![5].name)
        par_f.arguments = bundle_
        newsList.add(par_f)
    }

    private fun TrainFavoriteTab(){
        val tra_f= TrainFavoriteFm()
        val bundle_=Bundle()
        bundle_.putString("name", hTabs!![6].name)
        tra_f.arguments = bundle_
        newsList.add(tra_f)
    }

    private fun ActivityFavoriteTab(){
        val act_f= ActivityFavoriteFm()
        val bundle_=Bundle()
        bundle_.putString("name", hTabs!![7].name)
        act_f.arguments = bundle_
        newsList.add(act_f)
    }

    override fun onPageScrollStateChanged(arg0: Int) {}

    override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

    override fun onPageSelected(id: Int) {

        if(id==0){
            title_right_imageview_btn1!!.visibility=View.GONE
            title_right_imageview_btn2!!.visibility=View.GONE
            title_right_imageview_btn3!!.visibility=View.GONE
            title_right_imageview_btn4!!.visibility=View.GONE
            title_right_imageview_btn5!!.visibility=View.GONE
            title_right_imageview_btn6!!.visibility=View.GONE
            title_right_imageview_btn7!!.visibility=View.GONE
            title_right_imageview_btn!!.visibility=View.VISIBLE
            title_right_textview!!.visibility=View.VISIBLE
        }else if(id==1){
            title_right_imageview_btn1!!.visibility=View.VISIBLE
            title_right_imageview_btn2!!.visibility=View.GONE
            title_right_imageview_btn3!!.visibility=View.GONE
            title_right_imageview_btn4!!.visibility=View.GONE
            title_right_imageview_btn5!!.visibility=View.GONE
            title_right_imageview_btn6!!.visibility=View.GONE
            title_right_imageview_btn7!!.visibility=View.GONE
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_textview!!.visibility=View.VISIBLE
        }else if(id==2){
            title_right_imageview_btn1!!.visibility=View.GONE
            title_right_imageview_btn2!!.visibility=View.VISIBLE
            title_right_imageview_btn3!!.visibility=View.GONE
            title_right_imageview_btn4!!.visibility=View.GONE
            title_right_imageview_btn5!!.visibility=View.GONE
            title_right_imageview_btn6!!.visibility=View.GONE
            title_right_imageview_btn7!!.visibility=View.GONE
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_textview!!.visibility=View.VISIBLE
        }else if(id==3){
            title_right_imageview_btn1!!.visibility=View.GONE
            title_right_imageview_btn2!!.visibility=View.GONE
            title_right_imageview_btn3!!.visibility=View.VISIBLE
            title_right_imageview_btn4!!.visibility=View.GONE
            title_right_imageview_btn5!!.visibility=View.GONE
            title_right_imageview_btn6!!.visibility=View.GONE
            title_right_imageview_btn7!!.visibility=View.GONE
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_textview!!.visibility=View.VISIBLE
        }else if(id==4){
            title_right_imageview_btn1!!.visibility=View.GONE
            title_right_imageview_btn2!!.visibility=View.GONE
            title_right_imageview_btn3!!.visibility=View.GONE
            title_right_imageview_btn4!!.visibility=View.VISIBLE
            title_right_imageview_btn5!!.visibility=View.GONE
            title_right_imageview_btn6!!.visibility=View.GONE
            title_right_imageview_btn7!!.visibility=View.GONE
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_textview!!.visibility=View.VISIBLE
        }else if(id==5){
            title_right_imageview_btn1!!.visibility=View.GONE
            title_right_imageview_btn2!!.visibility=View.GONE
            title_right_imageview_btn3!!.visibility=View.GONE
            title_right_imageview_btn4!!.visibility=View.GONE
            title_right_imageview_btn5!!.visibility=View.VISIBLE
            title_right_imageview_btn6!!.visibility=View.GONE
            title_right_imageview_btn7!!.visibility=View.GONE
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_textview!!.visibility=View.VISIBLE
        }else if(id==6){
            title_right_imageview_btn1!!.visibility=View.GONE
            title_right_imageview_btn2!!.visibility=View.GONE
            title_right_imageview_btn3!!.visibility=View.GONE
            title_right_imageview_btn4!!.visibility=View.GONE
            title_right_imageview_btn5!!.visibility=View.GONE
            title_right_imageview_btn6!!.visibility=View.VISIBLE
            title_right_imageview_btn7!!.visibility=View.GONE
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_textview!!.visibility=View.VISIBLE
        }else if(id==7){
            title_right_imageview_btn1!!.visibility=View.GONE
            title_right_imageview_btn2!!.visibility=View.GONE
            title_right_imageview_btn3!!.visibility=View.GONE
            title_right_imageview_btn4!!.visibility=View.GONE
            title_right_imageview_btn5!!.visibility=View.GONE
            title_right_imageview_btn6!!.visibility=View.GONE
            title_right_imageview_btn7!!.visibility=View.VISIBLE
            title_right_imageview_btn!!.visibility=View.GONE
            title_right_textview!!.visibility=View.VISIBLE
        }
        setTab(id)
    }

    private fun setTab(id: Int) {
        val rbButton = rg_!!.getChildAt(id) as RadioButton
        rbButton.isChecked = true
        val left = rbButton.left
        val width = rbButton.measuredWidth
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        val screenWidth = metrics.widthPixels
        val len = left + width / 2 - screenWidth / 2
        hv_!!.smoothScrollTo(len, 0)
    }



}

