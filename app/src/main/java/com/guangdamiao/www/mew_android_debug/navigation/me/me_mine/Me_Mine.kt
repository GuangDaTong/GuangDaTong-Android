package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Address.Mine_AddressBook
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Ask.Mine_Ask
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Black.Mine_Black
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Money.Mine_Money
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.Mine_Favorite
import com.guangdamiao.www.mew_android_debug.navigation.school.message.Message
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils

class Me_Mine : BaseActivity() {

    private var title_left_imageview_btn: Button?=null
    private var title_center_textview: TextView? = null
    private var title_right_imageview: ImageView?=null
    private var mine_addressBook:TextView?=null
    private var mine_money:TextView?=null
    private var mine_ask:TextView?=null
    private var mine_favorite:TextView?=null
    private var mine_message:TextView?=null
    private var mine_black:TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_me_mine)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        MainApplication.getInstance().addActivity(this)
        initView()
        addSomeListener()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Me_Mine,"我-我的")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Me_Mine,"我-我的")
        super.onPause()
    }

    fun initView(){
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        title_center_textview!!.text="我的"
        title_right_imageview!!.visibility=View.GONE
        title_center_textview!!.visibility= View.VISIBLE
        mine_addressBook=findViewById(R.id.mine_addressBook) as TextView
        mine_money=findViewById(R.id.mine_money) as TextView
        mine_ask=findViewById(R.id.mine_ask) as TextView
        mine_favorite=findViewById(R.id.mine_favorite) as TextView
        mine_message=findViewById(R.id.mine_message) as TextView
        mine_black=findViewById(R.id.mine_black) as TextView
    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f


            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f

            }
            false
        }

        mine_addressBook!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent: Intent =Intent(this@Me_Mine, Mine_AddressBook::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@Me_Mine,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }
        mine_money!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent: Intent =Intent(this@Me_Mine, Mine_Money::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@Me_Mine,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }
        mine_ask!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent:Intent=Intent(this@Me_Mine, Mine_Ask::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@Me_Mine,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }
        mine_favorite!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent:Intent=Intent(this@Me_Mine, Mine_Favorite::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@Me_Mine,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }
        mine_message!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent:Intent=Intent(this@Me_Mine,Message::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@Me_Mine,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }

        }

        mine_black!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent: Intent =Intent(this@Me_Mine, Mine_Black::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@Me_Mine,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }
}
