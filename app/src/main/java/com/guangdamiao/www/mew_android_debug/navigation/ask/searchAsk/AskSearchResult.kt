package com.guangdamiao.www.mew_android_debug.navigation.ask.searchAsk

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.MyAsk
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskMainAdpter
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskMainRequest
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener
import com.guangdamiao.www.mew_android_debug.navigation.ask.presenter.AskMainPresenter
import com.guangdamiao.www.mew_android_debug.navigation.ask.view.AskMainView
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import es.dmoral.toasty.Toasty
import java.util.*

class AskSearchResult : BaseActivity(), AskMainView {

    private var title_left_imageview_btn: Button?=null
    private var title_center_textview: TextView? = null
    private var title_right_imageview: ImageView?=null
    private val listItems = ArrayList<MyAsk>()
    private var mlistView: ListView? = null
    private var adapter: AskMainAdpter? = null
    private var ptrClassicFrameLayout: PtrClassicFrameLayout? = null
    private var mPresenter: AskMainPresenter?=null
    var handler = Handler()
    var pageNow=0
    val orderBy=Constant.OrderBy_Default
    val order=Constant.Order_Default
    val TAG=Constant.Ask_TAG
    var range:String?=null
    var college:String?=null
    var ask_type_choose:String?=null
    var key=""
    var type=""
    var label=""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
        setContentView(R.layout.activity_ask_search_result)

        initView()
        addSomeListener()

        mlistView = findViewById(R.id.list_ask_lv) as ListView
        ptrClassicFrameLayout = findViewById(R.id.list_ask_frame) as PtrClassicFrameLayout
        //设置一个Presenter,本身继承了一个接口
        mPresenter= AskMainPresenter(this)

        adapter = AskMainAdpter(this@AskSearchResult, listItems)
        adapter!!.setAskPresenter(mPresenter!!)//将数据源绑定了Presenter,非常关键
        mlistView!!.setAdapter(adapter)//关键代码
        initData()
    }

    override fun onResume(){
        super.onResume()
        JAnalyticsInterface.onPageStart(this@AskSearchResult,"搜索结果")
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@AskSearchResult,"搜索结果")
        super.onPause()
    }

    override fun update2AddZan(position:Int,listener: IDataRequestListener) {
        adapter!!.listAllAsk.get(position).zanIs="1"
        adapter!!.listAllAsk.get(position).zan=(adapter!!.listAllAsk.get(position).zan!!.toInt()+1).toString()
        adapter!!.notifyDataSetChanged()
        listener.loadSuccess(position)

    }

    override fun update2DeleteAsk(position: Int) {
        if(adapter!!.listAllAsk[position]!=null){
            adapter!!.listAllAsk.removeAt(position)
            adapter!!.notifyDataSetChanged()
            val pDialog= SweetAlertDialog(this@AskSearchResult, SweetAlertDialog.SUCCESS_TYPE)
            pDialog.setTitleText("提示")
            pDialog.setContentText("亲，您已经成功删除该帖子")
            pDialog.show()
            val handler= Handler()
            handler.postDelayed({
                pDialog.cancel()
            },1236)
        }
    }

    override fun update2ShieldingAsk(position: Int) {
        if(adapter!!.listAllAsk[position]!=null){
            adapter!!.listAllAsk.removeAt(position)
            adapter!!.notifyDataSetChanged()
        }
    }


    fun initView(){
        title_left_imageview_btn=findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview=findViewById(R.id.title_center_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        title_right_imageview!!.visibility=View.GONE
        title_center_textview!!.text="搜索结果"
        title_center_textview!!.visibility= View.VISIBLE

        val bundle=intent.extras
        key=bundle.getString("key")
        type=bundle.getString("type")
        label=bundle.getString("label")

    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            this.finish()
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.push_right_in,R.anim.push_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onStart() {
        val read = this@AskSearchResult.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val ask_type_choose = read.getString("ask_type_choose", "")
        LogUtils.d_debugprint(Constant.ASK_SEARCH_TAG,"/n/n这里是ThreeFm中的onStart方法"+"此时的ask_choose_type为"+ask_type_choose)

        initData()
        super.onStart()
    }

    private fun initData() {

        val read = this@AskSearchResult.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        college= read.getString("college", "")
        ask_type_choose=read.getString("ask_type_choose","")

        if(!college.equals("")&&ask_type_choose!!.equals(Constant.Ask_Type_College)){
            range=Constant.Ask_Type_College
        }else if(college.equals("")&&ask_type_choose!!.equals(Constant.Ask_Type_College)){
            Toasty.warning(this@AskSearchResult,"亲，您还未填写学院信息").show()
            range=Constant.Ask_Type_School
            val editor: SharedPreferences.Editor=this@AskSearchResult.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
            editor.putString("ask_type_choose",Constant.Ask_All_School)
            editor.commit()
        }else if(ask_type_choose!!.equals(Constant.Ask_All_School)){
            range=Constant.Ask_Type_School
        }else{//第一次帖子类型没选，为空，默认是全校
            range=Constant.Ask_Type_School
        }

        ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(false) }, 150)

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    pageNow = 0
                    RequestServerList(pageNow,orderBy,order,adapter!!,ptrClassicFrameLayout!!)
                    adapter!!.notifyDataSetChanged()
                    if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                        ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                    }
                }, 100)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener( {
            handler.postDelayed({
                //请求更多页
                //ptrClassicFrameLayout!!.loadMoreComplete(true)
                RequestServerList(++pageNow,orderBy,order,adapter!!,ptrClassicFrameLayout!!)
                /*if (pageNow === 1) {
                    //set load more disable
                    ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                }*/
            }, 100)
        })
    }

    private fun RequestServerList(pageNow: Int, orderBy: String, order: String, adapter: AskMainAdpter, ptrClassicFrameLayout: PtrClassicFrameLayout) {
        AskMainRequest.getSearchResultFromServer(this@AskSearchResult, TAG, Constant.Search_ask, pageNow, orderBy, order, range!!,college!!,key,type,label,listItems,adapter,ptrClassicFrameLayout)
    }

}
