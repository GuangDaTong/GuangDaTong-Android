package com.guangdamiao.www.mew_android_debug.navigation.ask.view;


import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener;

/**
 * 
* @ClassName: AskMainView
* @Description: view,服务器响应后更新界面 
* @author JasonJan
*
 */
public interface AskMainView {

	 void update2AddZan( final int postion,final IDataRequestListener listener);
	 void update2DeleteAsk(final int position);
	 void update2ShieldingAsk(final int position);
}
