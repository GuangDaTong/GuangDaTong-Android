package com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.ImageSingleActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.navigation.me.me_info.InfoRequest
import com.guangdamiao.www.mew_android_debug.navigation.me.me_info.Me_Info
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Address.Mine_AddressBook
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Ask.Mine_Ask
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Black.Mine_Black
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Money.Mine_Money
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.Mine_Favorite
import com.guangdamiao.www.mew_android_debug.navigation.school.message.Message
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.Loading_view
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import es.dmoral.toasty.Toasty

class IconSelfInfo : BaseActivity() {

    private var title_left_imageview_btn: Button?=null
    private var title_center_textview: TextView?=null
    private var title_right_imageview: ImageView?=null
    private var title_right_textview:  TextView?=null
    private var title_right_imageview_btn:Button?=null
    private var iconSelfInfo_nickname_tv: TextView?=null
    private var iconSelfInfo_sex_tv: TextView?=null
    private var iconSelfInfo_icon_iv: ImageView?=null
    private var iconSelfInfo_zan_iv: ImageView?=null
    private var iconSelfInfo_zan_num_tv: TextView?=null
    private var iconSelfInfo_money_num_tv: TextView?=null
    private var iconSelfInfo_level_btn: Button?=null
    private var iconSelfInfo_college_tv: TextView?=null
    private var iconSelfInfo_grade_tv: TextView?=null
    private var iconSelfInfo_bookAddress_rl: RelativeLayout?=null
    private var iconSelfInfo_moneyRecord_rl: RelativeLayout?=null
    private var iconSelfInfo_myAsk_rl: RelativeLayout?=null
    private var iconSelfInfo_myFavorite_rl: RelativeLayout?=null
    private var iconSelfInfo_myMessage_rl: RelativeLayout?=null
    private var iconSelfInfo_myBlack_rl:RelativeLayout?=null
    private var iconSelfInfo_zan_btn: Button?=null


    var nickname=""
    var sex=""
    var icon=""
    var id=""
    var zanIs=""
    var zan=""
    var loading_first:Loading_view?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
        setContentView(R.layout.activity_icon_self_info)
        if(intent.extras!=null){
            val bundle=intent.extras//这里接受前面传过来的姓名，头像icon地址，性别
            nickname=bundle.getString("nickname")
            sex=bundle.getString("sex")
            icon=bundle.getString("icon")
            id=bundle.getString("id")
        }
        initView()
        configImageLoader()
        addSomeListener()
        
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@IconSelfInfo,"个人资料页")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@IconSelfInfo,"个人资料页")
        super.onPause()
    }


    override fun onStart() {

        if(!id.equals("")){
            loading_first= Loading_view(this@IconSelfInfo,R.style.CustomDialog)
            loading_first!!.show()
            IconInfoRequest.requestForInfo(this@IconSelfInfo,id,object: IDataRequestListener2String {
                override fun loadSuccess(response_string: String) {
                    LogUtils.d_debugprint(Constant.Me_TAG,"从服务器接收到的数据为：\n"+response_string)
                    val result = JsonUtil.get_key_string("result", response_string)
                    zanIs= JsonUtil.get_key_string("isZan",result)
                    zan= JsonUtil.get_key_string("zan",result)
                    val level= JsonUtil.get_key_string("level",result)
                    val college= JsonUtil.get_key_string("college",result)
                    val grade= JsonUtil.get_key_string("grade",result)
                    val coins= JsonUtil.get_key_string("coins",result)
                    val nickname=JsonUtil.get_key_string("nickname",result)
                    val gender=JsonUtil.get_key_string("gender",result)

                    if(zanIs.equals("1")){
                        iconSelfInfo_zan_iv!!.setImageResource(R.mipmap.zan_blue)
                        iconSelfInfo_zan_iv!!.setColorFilter(R.color.red)
                    }else{
                        iconSelfInfo_zan_iv!!.setImageResource(R.mipmap.zan_gray)
                    }
                    iconSelfInfo_nickname_tv!!.text=nickname

                    if(gender.equals("男")){
                        iconSelfInfo_sex_tv!!.text="♂"
                        iconSelfInfo_sex_tv!!.setTextColor(this@IconSelfInfo.resources.getColor(R.color.headline))
                        iconSelfInfo_sex_tv!!.visibility=View.VISIBLE
                    }else if(sex.equals("女")){
                        iconSelfInfo_sex_tv!!.text="♀"
                        iconSelfInfo_sex_tv!!.setTextColor(this@IconSelfInfo.resources.getColor(R.color.ask_red))
                        iconSelfInfo_sex_tv!!.visibility=View.VISIBLE
                    }else{
                        iconSelfInfo_sex_tv!!.visibility=View.GONE
                    }

                    iconSelfInfo_zan_num_tv!!.text=zan
                    iconSelfInfo_level_btn!!.text="LV"+level
                    iconSelfInfo_money_num_tv!!.text=coins
                    iconSelfInfo_college_tv!!.text=college+"•"
                    if(grade.equals("")){
                        iconSelfInfo_grade_tv!!.text="其他"
                    }else{
                        iconSelfInfo_grade_tv!!.text=grade
                    }
                }
            })
        }

        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token = read.getString("token", "")
        
        if(NetUtil.checkNetWork(this)&&!token.equals("")){

            InfoRequest.updataInfo(this@IconSelfInfo,token!!,object:IDataRequestListener2String{
                override fun loadSuccess(response_string: String?) {
                    if(!response_string.equals("")&&JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){

                        loading_first!!.cancel()

                        val result=JsonUtil.get_key_string("result",response_string)
                        val account=JsonUtil.get_key_string("account",result)
                        val nickname_=JsonUtil.get_key_string("nickname",result)
                        val icon_=JsonUtil.get_key_string("icon",result)
                        val school=JsonUtil.get_key_string("school",result)
                        val college=JsonUtil.get_key_string("college",result)
                        val grade=JsonUtil.get_key_string("grade",result)

                        val editor: SharedPreferences.Editor=getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                        editor.putString("account",account)
                        editor.putString("nickname",nickname_)
                        editor.putString("icon",icon_)
                        editor.putString("school",school)
                        editor.putString("college",college)
                        editor.putString("grade",grade)

                        editor.commit()

                        val read = this@IconSelfInfo.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                        val gender = read.getString("gender", "")

                        nickname=nickname_
                        sex=gender
                        icon=icon_
                        //iconSelfInfo_icon_iv!!.setImageResource(R.mipmap.user_default_icon)
                        initView()

                    }else if(JsonUtil.get_key_string("code",response_string!!).equals(Constant.LOGINFAILURECODE)){
                        loading_first!!.cancel()

                        val read = this@IconSelfInfo.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                        val account=read.getString("account","")
                        val password=read.getString("password","")
                        val editor: SharedPreferences.Editor=this@IconSelfInfo.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                        editor.clear()
                        editor.commit()
                        val editor2: SharedPreferences.Editor=this@IconSelfInfo.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                        editor2.putString("account",account)
                        editor2.putString("password",password)
                        editor2.commit()

                        SweetAlertDialog(this@IconSelfInfo, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                .setTitleText(Constant.LOGINFAILURETITLE)
                                .setContentText(Constant.LOGINFAILURE)
                                .setCustomImage(R.mipmap.mew_icon)
                                .setConfirmText("确定")
                                .setConfirmClickListener { sDialog ->
                                    sDialog.dismissWithAnimation()
                                    //跳转到登录，不用传东西过去
                                    val intent = Intent(this@IconSelfInfo, LoginMain::class.java)
                                    this@IconSelfInfo.startActivity(intent)
                                }
                                .setCancelText("取消")
                                .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                .show()

                    }else{
                        loading_first!!.cancel()

                        Toasty.info(this@IconSelfInfo,"个人信息刷新失败！").show()
                    }
                }
            })
        }else{
            if(loading_first!=null){
                loading_first!!.cancel()
            }
            Toasty.info(this,Constant.NONETWORK).show()
        }
        
        
        super.onStart()
    }

    fun initView(){
        title_left_imageview_btn=findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview=findViewById(R.id.title_center_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        title_right_textview=findViewById(R.id.title_right_textview) as TextView
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        iconSelfInfo_nickname_tv=findViewById(R.id.iconSelfInfo_nickname_tv) as TextView
        iconSelfInfo_sex_tv=findViewById(R.id.iconSelfInfo_sex_tv) as TextView
        iconSelfInfo_icon_iv=findViewById(R.id.iconSelfInfo_icon_iv) as ImageView
        iconSelfInfo_zan_iv=findViewById(R.id.iconSelfInfo_zan_iv) as ImageView
        iconSelfInfo_zan_num_tv=findViewById(R.id.iconSelfInfo_zan_num_tv) as TextView
        iconSelfInfo_money_num_tv=findViewById(R.id.iconSelfInfo_money_num_tv) as TextView
        iconSelfInfo_level_btn=findViewById(R.id.iconSelfInfo_level_btn) as Button
        iconSelfInfo_college_tv=findViewById(R.id.iconSelfInfo_college_tv) as TextView
        iconSelfInfo_grade_tv=findViewById(R.id.iconSelfInfo_grade_tv) as TextView
        iconSelfInfo_bookAddress_rl=findViewById(R.id.iconSelfInfo_bookAddress_rl) as RelativeLayout
        iconSelfInfo_moneyRecord_rl=findViewById(R.id.iconSelfInfo_moneyRecord_rl) as RelativeLayout
        iconSelfInfo_myAsk_rl=findViewById(R.id.iconSelfInfo_myAsk_rl) as RelativeLayout
        iconSelfInfo_myFavorite_rl=findViewById(R.id.iconSelfInfo_myFavorite_rl) as RelativeLayout
        iconSelfInfo_myMessage_rl=findViewById(R.id.iconSelfInfo_myMessage_rl) as RelativeLayout
        iconSelfInfo_myBlack_rl=findViewById(R.id.iconSelfInfo_myBlack_rl) as RelativeLayout
        iconSelfInfo_zan_btn=findViewById(R.id.iconSelfInfo_zan_btn) as Button

        title_right_textview!!.text="编辑"
        title_right_textview!!.visibility=View.VISIBLE
        title_right_imageview!!.visibility= View.GONE
        title_center_textview!!.visibility= View.VISIBLE
        title_right_imageview_btn!!.visibility=View.VISIBLE


        LogUtils.d_debugprint(Constant.Me_TAG,"昵称："+nickname+"\n性别："+sex+"\n头像icon"+icon)
        title_center_textview!!.text="个人信息"
        iconSelfInfo_nickname_tv!!.text=nickname
        //性别
        if(sex.equals("男")){
            iconSelfInfo_sex_tv!!.text="♂"
            iconSelfInfo_sex_tv!!.setTextColor(this@IconSelfInfo.resources.getColor(R.color.headline))
            iconSelfInfo_sex_tv!!.visibility= View.VISIBLE
        }else if(sex.equals("女")){
            iconSelfInfo_sex_tv!!.text="♀"
            iconSelfInfo_sex_tv!!.setTextColor(this@IconSelfInfo.resources.getColor(R.color.ask_red))
            iconSelfInfo_sex_tv!!.visibility= View.VISIBLE
        }else{
            iconSelfInfo_sex_tv!!.visibility= View.GONE
        }
        if (icon !== "") {

            if (LogUtils.APP_IS_DEBUG) {
                ImageLoader.getInstance().displayImage( icon!!, iconSelfInfo_icon_iv)
            } else {
                ImageLoader.getInstance().displayImage( icon!!, iconSelfInfo_icon_iv)
            }
        }

        iconSelfInfo_icon_iv!!.setOnClickListener{
            if(icon!=null) ImageSingleActivity.startImageSingleActivity(this@IconSelfInfo,icon!!)
        }
    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            this.finish()
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f
            }
            false
        }

        title_right_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_imageview_btn!!.alpha=0.618f
                title_right_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))


            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_imageview_btn!!.alpha=1.0f
                title_right_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }

        title_right_imageview_btn!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if (token.equals("")) {//=====token过期，或者未登录

                //跳转到登录，不用传东西过去
                val intent = Intent(this@IconSelfInfo, LoginMain::class.java)
                this@IconSelfInfo.startActivity(intent)

            } else {
                //跳转到个人详细资料的页面
                //跳转到登录，不用传东西过去
                val intent = Intent(this@IconSelfInfo, Me_Info::class.java)
                this@IconSelfInfo.startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out)
            }
        }
        iconSelfInfo_zan_btn!!.setOnClickListener{
            //判断是否登录
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                //他人资料页点赞，首先view点赞，再请求服务器
                if(!id.equals("")){
                    if(!zanIs.equals("")&&!zan.equals("")){
                        if(zanIs.equals("0")){
                            zanIs="1"
                            iconSelfInfo_zan_iv!!.setImageResource(R.mipmap.zan_blue)
                            iconSelfInfo_zan_iv!!.setColorFilter(R.color.red)
                            iconSelfInfo_zan_num_tv!!.text=(zan.toInt()+1).toString()
                            //再去请求服务器吧
                            IconInfoRequest.requestAddZan(this@IconSelfInfo,id,token)
                        }else{
                            Toasty.info(this@IconSelfInfo,"亲，您已经赞过了喔~").show()
                        }
                    }
                }
            }else{
                val intent: Intent = Intent(this@IconSelfInfo, LoginMain::class.java)
                startActivity(intent)
            }
        }

        iconSelfInfo_bookAddress_rl!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent: Intent =Intent(this@IconSelfInfo, Mine_AddressBook::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@IconSelfInfo,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }

        }
        iconSelfInfo_moneyRecord_rl!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent: Intent =Intent(this@IconSelfInfo, Mine_Money::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@IconSelfInfo,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }

        }
        iconSelfInfo_myAsk_rl!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent:Intent=Intent(this@IconSelfInfo, Mine_Ask::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@IconSelfInfo,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }
        iconSelfInfo_myFavorite_rl!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent:Intent=Intent(this@IconSelfInfo, Mine_Favorite::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@IconSelfInfo,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }

        iconSelfInfo_myMessage_rl!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent:Intent=Intent(this@IconSelfInfo, Message::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@IconSelfInfo,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }

        }

        iconSelfInfo_myBlack_rl!!.setOnClickListener{
            val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent: Intent =Intent(this@IconSelfInfo, Mine_Black::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent:Intent=Intent(this@IconSelfInfo,LoginMain::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out)
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.push_right_in,R.anim.push_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    /**
     * 配置ImageLoder
     */
    private fun configImageLoader() {
        // 初始化ImageLoader
        val options = DisplayImageOptions.Builder().showImageOnLoading(R.mipmap.icon_empty) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.icon_empty) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.icon_error) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true) // 设置下载的图片是否缓存在SD卡中
                // .displayer(new RoundedBitmapDisplayer(20)) // 设置成圆角图片
                .build() // 创建配置过得DisplayImageOption对象

        val config = ImageLoaderConfiguration.Builder(this@IconSelfInfo).defaultDisplayImageOptions(options)
                .threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(Md5FileNameGenerator()).tasksProcessingOrder(QueueProcessingType.LIFO).build()

        ImageLoader.getInstance().init(config)
    }


}
