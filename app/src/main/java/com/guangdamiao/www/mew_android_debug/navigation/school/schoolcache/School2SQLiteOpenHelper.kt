package com.guangdamiao.www.mew_android_debug.navigation.school.schoolcache

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2017/12/10.
 */

class School2SQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        private val name = "school2.db"
        private val version = 1
        val S2_Name="school2"
        val S2_Id="id"
        val S2_Sid="s_id"
        val S2_Title="title"
        val S2_CreateTime="createTime"
        val S2_Type="type"
        val S2_Link="link"
        val S2_ReadCount="readCount"
        val S2_Zan="zan"
        val S2_IsZan="isZan"
        val S2_IsFavorite="isFavorite"
        val S2_UrlEnd="urlEnd"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table school2(id integer primary key autoincrement," +
                "${School2SQLiteOpenHelper.S2_Sid} varchar(20)," +
                "${School2SQLiteOpenHelper.S2_Title} varchar(20),"+
                "${School2SQLiteOpenHelper.S2_CreateTime} varchar(100)," +
                "${School2SQLiteOpenHelper.S2_Type} varchar(5)," +
                "${School2SQLiteOpenHelper.S2_Link} varchar(100)," +
                "${School2SQLiteOpenHelper.S2_ReadCount} varchar(4)," +
                "${School2SQLiteOpenHelper.S2_Zan} varchar(2)," +
                "${School2SQLiteOpenHelper.S2_IsZan} varchar(2)," +
                "${School2SQLiteOpenHelper.S2_IsFavorite} varchar(2)," +
                "${School2SQLiteOpenHelper.S2_UrlEnd} varchar(50))")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    //打开数据库时调用
    override fun onOpen(db: SQLiteDatabase) {
        //如果数据库已经存在，则再次运行不执行onCreate()方法，而是执行onOpen()打开数据库
        super.onOpen(db)
    }


}
