package com.guangdamiao.www.mew_android_debug.global

import android.os.Environment
import java.io.File

/**
 * Created by Jason_Jan on 2017/12/11.
 */

class Constant {
    companion object {
        val BASEURLFORTEST="https://debug.collegemew.com"         //测试url根部
        //val BASEURLFORTEST="http://172.22.27.12"
        //val BASEURLFORTEST="192.168.1.118:8000"
        val BASEURLFORRELEASE="https://www.collegemew.com"        //发布url根部
        val SALTFORTEST="mewmewstudio"                             //md5测试字段
        val SALTFORRELEASE=""      //md5发布字段
        val CONTENTTYPE="application/json"                         //解析格式
        val SCHOOLDEFULT="广州大学"                                 //默认学校
        val PAGESIZE=20                                            //一页显示个数
        val RIGHTCODE="200"                                        //服务器返回正确的code
        val FULLCOLOR="#008000"                                    //全屏默认颜色
        var ENCODE="UTF-8"                                         //默认向服务器传递数据的编码
        val TAGDEBUG="debug_test"                                  //Log标签
        val Server_Result="result"                                 //服务器返回字段result
        val Server_Msg="msg"                                       //服务器返回字段msg
        val Server_Code="code"                                     //服务器返回字段code
        val Server_DebugMsg="debugMsg"                             //服务器返回字段debugMsg
        val ContentType="application/json"                         //默认的请求类型
        val ShareFile_UserInfo="userinfo"                          //用户全局共享文件
        val ShareFile_BlackList="blacklist"                        //黑名单全局共享文件
        val ShareFile_Location="record_location"                   //记录位置的全局共享文件
        val ShareFile_BlackAsk="blacklist_askid"                   //记录屏蔽帖子的id
        val ShareFile_isFirstInstall="isFirstInstall"              //记录是否是第一个安装
        val Chartset=";charset=UTF-8"
        val NONETWORK="亲，网络不给力，请检查网络"                    //网络没有连接提示
        val REQUESTFAIL="亲，网络超时，请稍候重试！"
        val UNKNOWNERROR="亲，未知的错误发生了，请及时反馈哦"
        val TOSERVER="向服务器请求的数据"
        val GETDATAFROMSERVER="服务器返回的数据"
        val SERVERBROKEN="服务器异常，请稍后再试"
        val LOGINFAILURETITLE="登录失效"
        val LOGINFAILURECODE="201"
        val LOGINFAILURE="登录已失效，请重新登录"
        val NOTLOGIN="亲，您还没有登录"
        val OrderBy_Default="createTime"
        val Order_Default="desc"
        val DEFAULT_SAVE_IMAGE_PATH= Environment.getExternalStorageDirectory().toString() + File.separator + "mew" + File.separator + "Images"+File.separator
        val ThumImage_Path="/sdcard/mew/temp/"

        val Check_Version="/checkVersion"                          //检查版本
        val LogOut="/logout"                                       //退出登录
        val URL_PushInfo="/pushInfo"                                   //上传推送ID,位置信息

        val Register_Tag="注册中"                                   //注册页面要用
        val Register_No_SendCode="您还没有发送验证码"
        val Register_Error_Phone="请输入正确的手机号"
        val Register_No_Phone="请输入您的手机号"
        val Register_Error_Code="请输入完整的验证码"
        val Register_No_Code="请输入验证码"
        val Register_AccountExist="账号已存在"
        val Register_AccountNotExist="账号不存在"
        val Reset_Password="/resetPassword"                         //终止密码
        val Register_SendCode_MaxTime=60
        val Register_IsAccountExist="/isAccountExist"               //账号是否存在的接口
        val Register_LoginMain_Jump=1                               //活动跳转



        val OrganizingData_Tag="完善资料中"                          //完善资料页面要用到
        val OrganizingData_Register="/register"
        val OrganizingData_NoAcademy="请选择学院"
        val OrganizingData_Password_Error="亲，密码不匹配"
        val OrganizingData_Check = "/checkVerifyCode"
        val OrganizingData_Academy="/list/college"
        val OrginizingData_No_Academy="未找到学院数据"
        val Choose_Academy_TAG="选择学院"

        val LoginMain_Tag="登录页面"                                 //登录页面要用到
        val LoginMain_Login="/login"

        val FourFm_TAG="我的模块"                                   //我的模块标签


        val School_TAG="首页模块"                                   //首页模块
        val School_Report="反馈问题"                                //首页右上角反馈信息有误的页面的title
        val URL_Report_Guidance="/report/guidance"
        val URL_Report_Summary="/report/summary"
        val URL_Report_Club="/report/club"
        val URL_Report_Look="/report/look"
        val School_AD="/list/advertisement"
        val School_ListGuidance="/list/guidance"
        val School_ListSummary="/list/summary"
        val School_ListClub="/list/club"
        val School_ListLook="/list/look"
        val School_DataGuidance="/data/guidance"
        val School_DataSummary="/data/summary"
        val School_DataClub="/data/club"
        val School_DataLook="/data/look"
        val School_Favorite_Guidance="/favorite/guidance"
        val School_Favorite_Summary="/favorite/summary"
        val School_Favorite_Club="/favorite/club"
        val School_Favorite_Look="/favorite/look"
        val School_List_FGUIdance="/list/favorite/guidance"
        val School_List_FSummary="/list/favorite/summary"
        val School_List_FClub="/list/favorite/club"
        val School_List_FLook="/list/favorite/look"
        val School_Delete_FGuidance="/delete/favorite/guidance"
        val School_Delete_FSummary="/delete/favorite/summary"
        val School_Delete_FClub="/delete/favorite/club"
        val School_Delete_FLook="/delete/favorite/look"

        val Message_TAG="消息模块"                                  //首页右上角的信封
        val Message_List_LeaveWords="/list/leaveWords"
        val Message_List_AskMsg="/list/askMsg"
        val Message_List_Notices="/list/notices"
        val Message_List_UserData="/list/userData"               //获取消息中心更新的用户资料


        val Find_TAG="栏目模块"                                     //栏目模块
        val Find_PartTimeJob="/list/partTimeJob"
        val Find_Activity="/list/activity"
        val Find_Training="/list/training"
        val Find_PartTimeJob_Detail="/data/partTimeJob"
        val Find_Activity_Detail="/data/activity"
        val Find_Training_Detail="/data/training"
        val Find_Favorite_PartTimeJob="/favorite/partTimeJob"
        val Find_Favorite_Training="/favorite/training"
        val Find_Favorite_Activity="/favorite/activity"
        val URL_ContactService_PartTimeJob="/contactService/partTimeJob"
        val URL_ContactService_Activity="/contactService/activity"  //联系客服
        val URL_ContactService_Training="/contactService/training"
        val URL_SignUp_Activity="/signUp/activity"                  //我要报名
        val URL_SignUp_Training="/signUp/training"
        val URL_SignUp_PartTimeJob="/signUp/partTimeJob"
        val Find_List_FActivity="/list/favorite/activity"
        val Find_List_FTraining="/list/favorite/training"
        val Find_List_FPartTimeJob="/list/favorite/partTimeJob"
        val Find_Delete_FActivity="/delete/favorite/activity"
        val Find_Delete_FTraining="/delete/favorite/training"
        val Find_Delete_FPartTimeJob="/delete/favorite/partTimeJob"


        val Me_TAG="我的模块"                                      //我的模块
        val Me_Favorite_TAG="我收藏"                               //我的收藏模块
        val PHONEPICTURE=101                                      //手机相册的一个代号
        val Me_ChooseAcademy=102                                  //我的个人信息选择学院
        val Me_ChooseSex=103                                      //我的个人信息选择性别
        val Me_ChooseGrade=104                                    //我的个人信息选择年级
        val Me_ChooseName=105                                     //我的个人信息选择姓名

        val Me_AlterPassword="/alterPassword"                     //设置中修改密码
        val Me_AlterData="/alterData"                             //修改个人资料

        val Mine_List_Friends="/list/friends"                     //通讯录好友列表
        val URL_Delete_Friends="/delete/friends"                  //删除通讯录好友

        val URL_Mine_Account_Book="/accountBook"                      //收支详情
        val URL_Me_SetPush="/setPush"                                 //设置推送开关

        val JG_PUSH_TAG="极光推送"
        val TAG_JG_Analytics="极光统计"

        val Ask_Same_College="同个学院的帖子"                     //帖子模块
        val Ask_All_School="全校的帖子"
        val Ask_List="/list/ask"                                  //帖子类表
        val Ask_TAG="帖子模块标签"
        val Ask_Type_College="college"
        val Ask_Type_School="school"
        val Ask_Type_All_College="all college"
        val Ask_Type_All_School="all school"
        val Ask_Favorite="/favorite/ask"                          //帖子收藏
        val Ask_Detail="/data/ask"                                //帖子详情
        val Ask_List_Comment="/list/comment/ask"                  //帖子评论列表
        val Comment_Ask="/comment/ask"                            //评论帖子
        val Adopt_Comment="/adopt/ask"
        val Delete_Comment="/delete/commentAsk"                   //删除评论
        val AskWrite_ChooseType=201                               //帖子选择类型
        val AskWrite_ChooseLabel=202                              //帖子添加标签
        val AskWrite_ChooseSee=203                                //帖子谁可以看
        val Ask_Delete="/delete/ask"                              //删除帖子
        val CoinsAndRules="/getCoinsAndRules"                     //获取当前金币和金币扣费规则
        val URL_Data_Person="/data/person"                        //他人资料页
        val URL_Data_Self="/data/self"                            //个人信息
        val URL_List_MyAsk="/list/myAsk"                          //我的帖子列表
        val URL_Favorite_Ask="/list/favorite/ask"                 //帖子收藏列表
        val URL_Delete_Favorite_Ask="/delete/favorite/ask"        //删除收藏的帖子


        //点赞的url
        val Zan_Ask="/zan/ask"                                    //帖子点赞
        val Zan_Guidance="/zan/guidance"                          //首页导航
        val Zan_Summary="/zan/summary"                            //四年概要
        val Zan_Club="/zan/club"                                  //首页社团
        val Zan_Look="/zan/look"                                  //看看
        val Zan_Activity="/zan/activity"                          //活动
        val Zan_PartTimeJob="/zan/partTimeJob"                     //兼职
        val Zan_Training="/zan/training"                           //培训

        val URL_Zan_Users="/zan/users"                            //他人资料页
        val URL_Add_Friend="/addFriend"                           //添加好友
        val URL_Give_Coins="/giveCoins"                           //赠送金币
        val URL_Want_Coins="/wantCoins"                           //索要金币
        val URL_Leave_Words="/leaveWords"                         //给他留言

        //分享的url
        val URL_Share_Guidance="/share/guidance"
        val URL_Share_Summary="/share/summary"
        val URL_Share_Club="/share/club"
        val URL_Share_Look="/share/look"
        val URL_Share_Activity="/share/activity"
        val URL_Share_PartTimeJob="/share/partTimeJob"
        val URL_Share_Training="/share/training"
        val URL_Share_Ask="/share/ask"
        val URL_Share_APP="/share/App"


        //举报的url
        val Inform_Ask="/inform/ask"
        val URL_Inform_Users="/inform/users"
        val Inform_CommentAsk="/inform/commentAsk"

        val EDIT_MAX_WORDS=2048
        val ASK_WRITE_TAG="发表帖子"                                     //发帖子标签
        val ASK_SEARCH_TAG="帖子搜索"                                    //搜索帖子标签
        val Icon_Info_TAG="资料页"                                       //点击了头像后的资料页
        val Search_ask="/list/search/ask"                               //搜索帖子
        val SEND_ASK="/send/ask"                                        //发布帖子
        val IMG_ADD_TAG="mew/image"

        val Give_Coins="赠送TA金币"                                     //共享标题中用到
        val Want_Coins="向TA索要金币"
        val Leave_Words="给TA留言"
        val Report_Words="举报TA"
        val ContactService_Words="我要咨询"


        //极光统计=====自定义事件
        val Event_AskSwitchList="askSwitchList"
        val Event_DiscoveryCollect="discoveryCollect"
        val Event_AskCollect="askCollect"
        val Event_SchoolCollect="schoolCollect"
        val Event_AskSearchBegin="askSearchBegin"
        val Event_AskSearch="askSearch"
        val Event_SendAsk="sendAsk"
        val Event_WriteAsk="writeAsk"
        val Event_SchollShare="schoolShare"
        val Event_DiscoveryShare="discoveryShare"
        val Event_AskShare="askShare"
        val Event_SchoolReport="schoolReport"
        val Event_AskReport="askReport"
        val Event_ReportNum="ReportNum"
        val Event_LeaveMessage="LeaveMessage"
        val Event_DiscovetyApply="DiscoveryApply"
        val Event_ContractService="ContractService"
        val Event_WantCoins="WantCoins"
        val Event_GiveCoins="GiveCoins"

        //msgPush，消息中心的系统消息的类型

        val Message_ZanAsk="zanAsk"
        val Message_AdoptComment="adoptComment"
        val Message_FavoriteAsk="favoriteAsk"
        val Message_CommentAsk="commentAsk"
        val Message_CommentUser="commentUser"
        val Message_ZanUser="zanUser"
        val Message_GiveCoins="giveCoins"
        val Message_WantCoins="wantCoins"
        val Message_ContactAdded="contactAdded"
        val Message_AskInformed="askInformed"
        val Message_UserInformed="userInformed"
        val Message_CommentAskInformed="commentAskInformed"
        val Message_ReportAdopted="reportAdopted"
        val Message_ReportRejected="reportRejected"
        val Message_AdviseAdopted="adviseAdopted"
        val Message_AdviseRejected="adviseRejected"
        val Message_AskDeleted="askDeleted"
        val Message_CommentAskDeleted="commentAskDeleted"
        val Message_InformForbidden="informForbidden"
        val Message_ChatForbidden="chatForbidden"
        val Message_RewardForInform="rewardForInform"
        val Message_InformRejected="informRejected"
        val Message_MuchInformed_ToUser="如果存在恶意举报行为，您的举报功能可能会被系统禁用！"
        val Message_ForbiddenInformed_ToUser="在未来的72小时内，您将不能再使用举报功能！"
        val Message_ForbiddenChat_ToUser="在未来的72小时内，您将不能再使用留言功能！"
        val Message_ForbiddenSendAsk_ToUser="在未来的72小时内，您将不能再发送帖子！"
        val Message_ForbiddenSendAskComment_ToUser="在未来的72小时内，您将不能再发送帖子评论！"
        val Message_AskInformed_ToUser="若举报属实，请您尽快自行删除帖子！"
        val Message_UserInformed_ToUser="请您立刻停止这种行为，否则您的留言功能可能会被禁用！"
        val Message_CommentAskInformed_ToUser="若举报属实，请您尽快删除评论！"

        //极光推送回调选择的页面
        val Jpush_LeaveWords="leaveWords"
        val Jpush_Ask="ask"
        val JPush_Notices="notice"
        val JPush_UrgentAsk="urgentAsk"

        //拉黑机制url
        val URL_Blacklist="/blacklist"                             //拉黑用户
        val URL_Delete_Blacklist="/delete/blacklist"               //删除拉黑
        val URL_List_Blacklist="/list/blacklist"                   //黑名单列表

        val SearchFriends_TAG="搜索好友"                           //搜索好友tag
        val URL_Search_Users="/list/search/users"

        //QQ授权
        val URL_IsQQRegister="/isQQRegister"
        val URL_Login_QQ="/login/qq"

    }

}
