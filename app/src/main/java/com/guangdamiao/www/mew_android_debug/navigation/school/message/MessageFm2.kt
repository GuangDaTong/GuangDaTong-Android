package com.guangdamiao.www.mew_android_debug.navigation.school.message

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView
import android.widget.RelativeLayout
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.message_bean.Message2
import com.guangdamiao.www.mew_android_debug.bean.message_bean.MessageUserData
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.BadgeView
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class MessageFm2 : Fragment() {


    private var name: String? = null
    private val listItems = ArrayList<Message2>()
    private var adapter: Message2_Adpter? = null
    private val TAG = Constant.Message_TAG
    private var mlistView: ListView? = null

    private var pageNow = 0
    private var ptrClassicFrameLayout: PtrClassicFrameLayout? = null
    internal var handler = Handler()
    private var loading_first: SweetAlertDialog?=null
    private var flag=0

    private var helper2:Message2SQLiteOpenHelper?=null
    public var badge2: BadgeView?=null
    var bt2:Button?=null

    var title_right_imageview_btn1: Button?=null
    var title_right_textview: TextView?=null

    private var no_data_rl: RelativeLayout?=null


    override fun setArguments(args: Bundle) {
        name = args.getString("name")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view_ = inflater!!.inflate(R.layout.fragment_message, container, false)
        title_right_imageview_btn1=activity.findViewById(R.id.title_right_imageview_btn1) as Button
        title_right_textview=activity.findViewById(R.id.title_right_textview) as TextView
        bt2=activity.findViewById(R.id.bt2) as Button 
        no_data_rl=view_.findViewById(R.id.no_data_rl) as RelativeLayout


        helper2= Message2SQLiteOpenHelper(context)
        mlistView = view_!!.findViewById(R.id.message_lv) as ListView
        ptrClassicFrameLayout = view_!!.findViewById(R.id.message_frame) as PtrClassicFrameLayout


        adapter = Message2_Adpter(context, listItems,badge2)
        mlistView!!.adapter = adapter//关键代码

        initData()
        addSomeListener()
        query2NowAll(listItems,0)
        if(listItems.size>0){
            no_data_rl!!.visibility= View.GONE
        }else{
            no_data_rl!!.visibility= View.VISIBLE
        }

        return view_
    }

    private fun addSomeListener(){
        title_right_imageview_btn1!!.setOnClickListener {
            if (listItems.size > 0) {
                SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText("确认清空")
                        .setContentText("亲，您要清空您的帖子消息？")
                        .setCustomImage(R.mipmap.mew_icon)
                        .setConfirmText("确定")
                        .setConfirmClickListener { sDialog ->
                            sDialog.dismissWithAnimation()
                            delete2NowAll()
                            adapter!!.notifyDataSetChanged()
                            RequestServerList(0)
                        }
                        .setCancelText("取消")
                        .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                        .show()
            }else{
                Toasty.info(context,"亲，帖子消息已经全部清空").show()
            }
        }

        title_right_imageview_btn1!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_textview!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_textview!!.alpha=1.0f

            }
            false
        }
    }

    private fun initData() {
       /* if(flag2==2){
            *//*loading_first= SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
            loading_first!!.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"))
            loading_first!!.setTitleText("Loading")
            loading_first!!.setCancelable(false)
            loading_first!!.show()*//*
            flag++
            ptrClassicFrameLayout!!.postDelayed({ ptrClassicFrameLayout!!.autoRefresh(true) }, 150)
        }*/

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                if(!activity.isFinishing){
                    handler.postDelayed({
                        pageNow = 0
                        //query2NowAll(listItems,pageNow)
                        RequestServerList(pageNow)
                        if (!ptrClassicFrameLayout!!.isLoadMoreEnable) {
                            ptrClassicFrameLayout!!.isLoadMoreEnable = true
                        }

                    }, 100)
                }
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener {

            if(!activity.isFinishing){
                ++pageNow
                val old_size=listItems.size

                //请求消息中心更新用户资料
                val IDs= query2IDs(pageNow)
                if(IDs.size<=0){
                    query2NowAll(listItems, pageNow)
                    adapter!!.notifyDataSetChanged()
                    val new_size=listItems.size
                    if(new_size==old_size){
                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                    }else{
                        ptrClassicFrameLayout!!.refreshComplete()
                        ptrClassicFrameLayout!!.loadMoreComplete(true)
                    }
                }else{
                    Message_Request.listUserData(context,IDs,ptrClassicFrameLayout!!,object: IDataRequestListener2String {
                        override fun loadSuccess(response_string: String?) {

                            val getData = JsonUtil.getListMap("list", response_string!!)//=====这里可能发生异常
                            LogUtils.d_debugprint(Constant.Message_TAG, "json解析出来的对象是=" + getData.toString())
                            if (getData != null && getData.size > 0) {

                                for (i in getData.indices) {

                                    val id=getData[i].getValue("id").toString()
                                    val icon=getData[i].getValue("icon").toString()
                                    val nickname=getData[i].getValue("nickname").toString()
                                    val gender=getData[i].getValue("gender").toString()

                                    val messageUserData= MessageUserData()
                                    messageUserData.icon=icon
                                    messageUserData.nickname=nickname
                                    messageUserData.gender=gender

                                    if(Message.message_userData[id]==null&&messageUserData!=null){
                                        Message.message_userData.put(id,messageUserData)
                                    }
                                    //更新数据库
                                    update2IDs(id, icon, nickname, gender)
                                }

                                LogUtils.d_debugprint(Constant.Message_TAG,"现在，Message.message_userData的长度为：${Message.message_userData.size} \n\n\n")
                                //查找数据库
                                query2NowAll(listItems, pageNow)
                                adapter!!.notifyDataSetChanged()
                                val new_size=listItems.size
                                if(new_size==old_size){
                                    ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                    ptrClassicFrameLayout!!.loadMoreComplete(false)
                                }else{
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                }

                            }else{
                                //查找数据库
                                query2NowAll(listItems, pageNow)
                                adapter!!.notifyDataSetChanged()
                                val new_size=listItems.size
                                if(new_size==old_size){
                                    ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                    ptrClassicFrameLayout!!.loadMoreComplete(false)
                                }else{
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                }
                            }
                        }
                    })
                }

            }
        }
    }

    private fun RequestServerList(pageNow: Int) {
        if(!activity.isFinishing){
            Message_Request.getResultFromServerAskMsg(activity,context, TAG, Constant.Message_List_AskMsg, pageNow, listItems, adapter!!, ptrClassicFrameLayout!!,loading_first,no_data_rl!!)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        if(isVisibleToUser&&flag==0){
            flag++
            if(!activity.isFinishing){
                ptrClassicFrameLayout!!.postDelayed({ ptrClassicFrameLayout!!.autoRefresh(true) }, 150)
            }
            JAnalyticsInterface.onPageStart(context,"消息中心-帖子")
        }else if(isVisibleToUser){
            JAnalyticsInterface.onPageStart(context,"消息中心-帖子")
        }else{
            JAnalyticsInterface.onPageEnd(context,"消息中心-帖子")
        }
    }

    private fun query2NowAll(listItems:ArrayList<Message2>,pageNow:Int){
        val offset_page=pageNow*Constant.PAGESIZE
        val limit_page=Constant.PAGESIZE
        val sql="select * from "+Message2SQLiteOpenHelper.M2_Name+" order by createTime desc limit $limit_page offset $offset_page"

        if(pageNow==0) listItems.clear()
        val cursor= helper2!!.readableDatabase.rawQuery(sql,null)
        while(cursor.moveToNext()){
            val message2=Message2()
            message2.id=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_Sid))
            message2.askID=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_AskID))
            message2.icon=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_Icon))
            message2.gender=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_Gender))
            message2.nickname=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_NickName))
            message2.senderID=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_SenderID))
            message2.createTime=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_CreateTime))
            message2.detail=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_Detail))
            message2.type=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_Type))
            message2.isRead=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_IsRead))

            val read = context.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE)
            val blacklist= read.getString("blacklist", "[]")
            val blacklist_array=ArrayList<String>()
            val jsonArray= JSONArray(blacklist)
            if(jsonArray.length()>0) {
                for (i in 0..jsonArray.length()-1) {
                    blacklist_array.add(jsonArray.getString(i))
                }
            }
            var isExistBlack=false
            for(j in blacklist_array.indices){
                if(message2.senderID.equals(blacklist_array[j])){
                    isExistBlack=true
                }
            }
            if(isExistBlack){

            }else{
                listItems.add(message2)
            }
        }
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message2为：\n\n"+listItems.toString())
    }

    private fun delete2NowAll(){
        val sql="delete from "+Message2SQLiteOpenHelper.M2_Name
        listItems.clear()
        val db = helper2!!.writableDatabase
        db!!.execSQL(sql)
        db!!.close()
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库删除了Message2所有数据！！！")
    }

    private fun update2NowAll(){
        val sql="update message2 set isRead='1'"
        val db=helper2!!.writableDatabase
        db!!.execSQL(sql)
        db!!.close()
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库更新了Message2所有数据=====全部设为已读！！！\n\n")
    }

    private fun query2IDs(pageNow:Int):ArrayList<String>{

        val offset_page=pageNow*Constant.PAGESIZE
        val limit_page=Constant.PAGESIZE
        val sql="select senderID from "+Message2SQLiteOpenHelper.M2_Name+" order by createTime desc limit $limit_page offset $offset_page"
        val IDs=ArrayList<String>()

        val cursor=helper2!!.readableDatabase.rawQuery(sql,null)
        while(cursor.moveToNext()){
            val item=cursor.getString(cursor.getColumnIndex(Message2SQLiteOpenHelper.M2_SenderID))
            if(IDs.size==0) IDs.add(item)
            for(i in IDs.indices){
                if(!IDs[i].equals(item)){
                    if(i==IDs.size-1) IDs.add(item)
                }else{
                    break
                }
            }
        }

        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库查找到的Message2的IDs为：\n\n"+IDs.toString())

        return IDs
    }

    private fun update2IDs(senderID:String,icon:String,nickname: String,gender:String){
        val sql="update message2 set icon='$icon',nickname='$nickname',gender='$gender' where senderID='$senderID';"
        val db= helper2!!.writableDatabase
        db!!.execSQL(sql)
        db!!.close()
        LogUtils.d_debugprint(Constant.Message_TAG,"本地数据库更新了Message2所有数据=====更新了刚请求过来的20个新的资料！！！\n\n")
    }

}
