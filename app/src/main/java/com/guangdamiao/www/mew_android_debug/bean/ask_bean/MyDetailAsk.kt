package com.guangdamiao.www.mew_android_debug.bean.ask_bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class MyDetailAsk {

    var link:String?=null
    var commentCount:String?=null
    var id: String? = null
    var createTime:String?=null
    var publisherID:String?=null
    var nickname:String?=null
    var icon:String?=null
    var gender:String?=null
    var reward:String?=null
    var zan:String?=null
    var type:String?=null
    var label:String?=null
    var content:String?=null
    var image:ArrayList<String>?=null
    var thumbnailImage:ArrayList<String>?=null
    var zanIs:String?=null
    var favoriteIs:String?=null
    var title:String?=null
    var adviseComment:Comment?=null

    constructor():super()
    constructor(id:String,link:String,createTime:String,publisherID:String,nickname:String,icon:String,reward:String,zan:String,type:String,label:String,content:String,isZan:String,isFavorite:String,commentCount:String,title:String){
        this.id=id
        this.link=link
        this.title=title
        this.commentCount=commentCount
        this.createTime=createTime
        this.publisherID=publisherID
        this.nickname=nickname
        this.icon=icon
        this.reward=reward
        this.zan=zan
        this.type=type
        this.label=label
        this.content=content
        this.zanIs=isZan
        this.favoriteIs=isFavorite
    }
    constructor(title:String,link:String,id:String,createTime:String,publisherID:String,nickname:String,icon:String,gender:String,reward:String,zan:String,type:String,label:String,content:String,image:ArrayList<String>,thumbnailImage:ArrayList<String>,isZan:String,isFavorite:String,commentCount:String,adviseComment:Comment){
        this.id=id
        this.link=link
        this.title=title
        this.createTime=createTime
        this.publisherID=publisherID
        this.nickname=nickname
        this.icon=icon
        this.reward=reward
        this.zan=zan
        this.type=type
        this.label=label
        this.content=content
        this.zanIs=isZan
        this.favoriteIs=isFavorite
        this.gender=gender
        this.image=image
        this.thumbnailImage=thumbnailImage
        this.commentCount=commentCount
        this.adviseComment=adviseComment

    }
    constructor(title:String,id:String,createTime:String,publisherID:String,nickname:String,icon:String,gender:String,reward:String,zan:String,type:String,label:String,content:String,link:String,image:ArrayList<String>,thumbnailImage:ArrayList<String>,isZan:String,isFavorite:String,commentCount:String){
        this.id=id
        this.link=link
        this.title=title
        this.createTime=createTime
        this.publisherID=publisherID
        this.nickname=nickname
        this.icon=icon
        this.reward=reward
        this.zan=zan
        this.type=type
        this.label=label
        this.content=content
        this.zanIs=isZan
        this.favoriteIs=isFavorite
        this.gender=gender
        this.image=image
        this.thumbnailImage=thumbnailImage
        this.commentCount=commentCount


    }

    override fun toString(): String {
        return "MyAsk[id=$id,\nlink=$link,\ntitle=$title,\ncreateTime=$createTime,\npublisherID=$publisherID,nickname=$nickname,icon=$icon,reward=$reward,zan=$zan,type=$type,label=$label,content=$content,isZan=$zanIs,isFavorite=$favoriteIs,commentCount=$commentCount]"
    }
}
