package com.guangdamiao.www.mew_android_debug.navigation.ask.askMain

import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.*
import cn.jiguang.share.android.api.JShareInterface
import cn.jiguang.share.android.api.PlatActionListener
import cn.jiguang.share.android.api.Platform
import cn.jiguang.share.android.api.ShareParams
import cn.jiguang.share.qqmodel.QQ
import cn.jiguang.share.qqmodel.QZone
import cn.jiguang.share.wechat.Wechat
import cn.jiguang.share.wechat.WechatMoments
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.MyAsk
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ViewHolder
import com.guangdamiao.www.mew_android_debug.navigation.ask.askDetail.AskDetail
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconSelfInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.presenter.AskMainPresenter
import com.guangdamiao.www.mew_android_debug.navigation.ask.searchAsk.AskSearchResult
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.*
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.ImageSize
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import java.util.*

/**

 */
class AskMainAdpter(protected var context: Context, listAllAsk: ArrayList<MyAsk>) : BaseAdapter() {
        var listAllAsk = ArrayList<MyAsk>()
            set(listAllAsk){//填充输入给listAllAsk
                if(listAllAsk!=null){
                    field=listAllAsk
                }
            }
    private var mPresenter: AskMainPresenter?=null //处理服务器交互和view更新的类
    private var icon: String? = null
    var main_icon_iv: ImageView?=null
    private var popWindow: CustomPopWindow?=null
    private var popWindow_report: CustomPopWindow?=null
    private var popWindow_share: CustomPopWindow?=null
    var ask_main_right_btn:Button?=null
    var ask_three_favorite:TextView?=null
    var ask_three_report:TextView?=null
    var ask_three_cancle:TextView?=null
    var ask_report_five1:TextView?=null
    var ask_report_five2:TextView?=null
    var ask_report_five3:TextView?=null
    var ask_report_five4:TextView?=null
    var ask_report_cancle:TextView?=null
    var ask_share_qq:RelativeLayout?=null
    var ask_share_wechat:RelativeLayout?=null
    var ask_share_qq_zone:RelativeLayout?=null
    var ask_share_wechat_friends:RelativeLayout?=null
    var ask_share_copy:RelativeLayout?=null

    var share_id=""
    var share_shareWay=""
    var share_token=""

    init {
        this.listAllAsk = listAllAsk
    }

    fun setAskPresenter(presenter: AskMainPresenter){//实现了这个适配器可以处理一切的关键！！
        mPresenter=presenter
    }

    override fun getCount(): Int {
        return listAllAsk.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.ask_main_list, parent, false)
        }

        configImageLoader()
        val listItemAsk = listAllAsk.get(position)//获取一个


        main_icon_iv = ViewHolder.get<ImageView>(convertView, R.id.ask_main_icon)
        val ask_left_type:Button = ViewHolder.get<Button>(convertView, R.id.ask_left_type)
        val ask_main_nick_tv = ViewHolder.get<TextView>(convertView, R.id.ask_main_nick_tv)
        val ask_main_sex_tv=ViewHolder.get<TextView>(convertView,R.id.ask_main_sex_tv)
        val ask_main_more_iv=ViewHolder.get<ImageView>(convertView,R.id.ask_main_more_iv)
        val ask_main_delete_tv=ViewHolder.get<TextView>(convertView,R.id.ask_main_delete_tv)
        ask_main_right_btn=ViewHolder.get<Button>(convertView,R.id.ask_main_right_btn)
        val ask_main_time=ViewHolder.get<TextView>(convertView,R.id.ask_main_time)
        val ask_main_reward_tv=ViewHolder.get<TextView>(convertView,R.id.ask_main_reward_tv)
        val ask_main_reward_num_tv=ViewHolder.get<TextView>(convertView,R.id.ask_main_reward_num_tv)
        val ask_main_reward_tv2=ViewHolder.get<TextView>(convertView,R.id.ask_main_reward_tv2)
        val ask_main_urgent_iv=ViewHolder.get<ImageView>(convertView,R.id.ask_main_urgent_iv)
        val ask_main_urgent_tv=ViewHolder.get<TextView>(convertView,R.id.ask_main_urgent_tv)
        val ask_content=ViewHolder.get<TextView>(convertView,R.id.ask_content)
        val ask_main_share=ViewHolder.get<ImageView>(convertView,R.id.ask_main_share)
        val ask_main_share_btn=ViewHolder.get<Button>(convertView,R.id.ask_main_share_btn)
        val ask_main_zan=ViewHolder.get<ImageView>(convertView,R.id.ask_main_zan)
        val ask_main_zan_btn=ViewHolder.get<Button>(convertView,R.id.ask_main_zan_btn)
        val ask_main_zan_num=ViewHolder.get<TextView>(convertView,R.id.ask_main_zan_num_tv)
        val ask_main_comment=ViewHolder.get<ImageView>(convertView,R.id.ask_main_comment)
        val ask_main_comment_btn=ViewHolder.get<Button>(convertView,R.id.ask_main_comment_btn)
        val ask_main_comment_num=ViewHolder.get<TextView>(convertView,R.id.ask_main_comment_num_tv)
        val ask_main_deal_tv=ViewHolder.get<TextView>(convertView,R.id.ask_main_deal_tv)//是否解决
        val ask_main_title_tv=ViewHolder.get<TextView>(convertView,R.id.ask_main_title_tv)
        val multiImageView=convertView!!.findViewById(R.id.ask_main_multi_iv) as MultiImageView
        val ask_main_layout=convertView!!.findViewById(R.id.ask_main_layout) as LinearLayout
        val ask_main_first_btn=convertView!!.findViewById(R.id.ask_main_first_btn) as Button



        //判断是否是自己的帖子
        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token=read.getString("token","")
        val current_userID = read.getString("id", "")

        if(current_userID.equals(listItemAsk.publisherID)){
            ask_main_more_iv.visibility=View.GONE
            ask_main_delete_tv.visibility=View.VISIBLE
            //监听删除按钮
            ask_main_right_btn!!.setOnTouchListener { v, event ->
                if(event.action== MotionEvent.ACTION_DOWN){
                    ask_main_delete_tv!!.alpha=0.618f
                }else{
                    ask_main_delete_tv!!.alpha=1.0f
                }
                false
            }

            ask_main_right_btn!!.setOnClickListener {
                if(mPresenter!=null){
                    val IDs=ArrayList<String>()
                    IDs.add(listItemAsk.id!!)
                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setCustomImage(R.mipmap.mew_icon)
                            .setTitleText("提示")
                            .setContentText("删除这条帖子？")
                            .setCancelText("再想想")
                            .setConfirmText("我确定")
                            .showCancelButton(true)
                            .setCancelClickListener(object:SweetAlertDialog.OnSweetClickListener {
                                override fun onClick(sweetAlertDialog: SweetAlertDialog?) {
                                    sweetAlertDialog!!.cancel()
                                }
                            })
                            .setConfirmClickListener(object:SweetAlertDialog.OnSweetClickListener{
                                override fun onClick(sweetAlertDialog: SweetAlertDialog?) {
                                    sweetAlertDialog!!.cancel()
                                    if(NetUtil.checkNetWork(context)){
                                        mPresenter!!.deleteAsk(context,token,IDs,position)
                                    }
                                }
                            })
                            .show()
                }
            }

        }else{
            ask_main_more_iv.visibility=View.VISIBLE
            ask_main_delete_tv.visibility=View.GONE
            //监听三个点
            ask_main_right_btn!!.setOnClickListener {
                if(!get_token().equals("")){
                    showDialog(listItemAsk.id!!,position)
                }
            }

            ask_main_right_btn!!.setOnTouchListener { v, event ->
                if(event.action== MotionEvent.ACTION_DOWN){
                    ask_main_more_iv!!.alpha=0.618f
                }else{
                    ask_main_more_iv!!.alpha=1.0f
                }
                false
            }
        }



        //判断帖子是否被采纳
        if(null!=listItemAsk.commentID&&!listItemAsk.commentID.equals("")){
            ask_main_deal_tv.visibility=View.VISIBLE
        }else{
            ask_main_deal_tv.visibility=View.GONE
        }

        //帖子标题
        if(null!=listItemAsk.title&&!listItemAsk.title.equals("")){
            ask_main_title_tv.text="\"${listItemAsk.title}\""
        }else{
            ask_main_title_tv.text="无题"
        }

        ask_main_title_tv.setOnLongClickListener{
            val cmb=context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            cmb.text=ask_main_title_tv.text
            Toasty.info(context,"成功复制该帖子标题！").show()
            true
        }

        //时间
        val time = listItemAsk.createTime!!.substring(0, 16).replace("T", " ")
        val display_date=DateUtil.string2Date(time,"yyyy-MM-dd HH:mm")
        val display_string=DateUtil.getTimestampString(display_date)
        ask_main_time.setText(display_string)

        //昵称
        if(listItemAsk.nickname!!.length>12){
            val name_display=listItemAsk.nickname!!.substring(0,12)+"..."
            ask_main_nick_tv.setText(name_display)
        }else{
            ask_main_nick_tv.setText(listItemAsk.nickname)
        }

        //性别
        if(listItemAsk.gender.equals("男")){
            ask_main_sex_tv.text="♂"
            ask_main_sex_tv.setTextColor(context.resources.getColor(R.color.headline))
            ask_main_sex_tv.visibility=View.VISIBLE
        }else if(listItemAsk.gender.equals("女")){
            ask_main_sex_tv.text="♀"
            ask_main_sex_tv.setTextColor(context.resources.getColor(R.color.ask_red))

            ask_main_sex_tv.visibility=View.VISIBLE
        }else{
            ask_main_sex_tv.visibility=View.GONE
        }

        //左侧标签
        if(!listItemAsk.label.equals("")){
            ask_left_type.setText("#"+listItemAsk.label)
            if(listItemAsk.label.equals("寻物启事")){
                ask_left_type.setTextColor(context.resources.getColor(R.color.ask_red))
            }else if(listItemAsk.label.equals("失物招领")){
                ask_left_type.setTextColor(context.resources.getColor(R.color.ask_green))
            }else{
                ask_left_type.setTextColor(context.resources.getColor(R.color.ask_yellow))
            }
            ask_left_type.setOnClickListener{
                val intent: Intent =Intent(context, AskSearchResult::class.java)
                val bundle=Bundle()
                bundle.putString("key","")
                bundle.putString("type","")
                bundle.putString("label",listItemAsk.label)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }
        }

        ask_left_type!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                ask_left_type!!.alpha=0.618f
                ask_left_type.setBackgroundColor(context.resources.getColor(R.color.bgcolor))
            }else{
                ask_left_type!!.alpha=1.0f
                ask_left_type.setBackgroundColor(Color.WHITE)
            }
            false
        }

        //内容
        if(listItemAsk.content!!.length>200){
            val content_display=listItemAsk.content!!.substring(0,200)+"..."
            ask_content.setText(content_display)
        }else{
            ask_content.setText(listItemAsk.content)
        }

        ask_content.setOnLongClickListener{
            val cmb=context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            cmb.text=ask_content.text
            Toasty.info(context,"成功复制该帖子内容！").show()
            true
        }

        //是否有悬赏
        if(!listItemAsk.reward.equals("0")){
            ask_main_reward_tv.visibility=View.VISIBLE
            ask_main_reward_num_tv.text=listItemAsk.reward
            ask_main_reward_num_tv.visibility=View.VISIBLE
            ask_main_reward_tv2.visibility=View.VISIBLE
        }else{
            ask_main_reward_tv.visibility=View.GONE
            ask_main_reward_num_tv.visibility=View.GONE
            ask_main_reward_tv2.visibility=View.GONE
        }

        //是否是急问
        if(listItemAsk.type.equals("急问")){
            ask_main_urgent_iv.visibility=View.VISIBLE
            ask_main_urgent_tv.visibility=View.VISIBLE
        }else{
            ask_main_urgent_iv.visibility=View.GONE
            ask_main_urgent_tv.visibility=View.GONE
        }

        //是否赞过
        if(listItemAsk.zanIs.equals("1")){
            ask_main_zan.setImageResource(R.mipmap.zan_blue)
        }else{
            ask_main_zan.setImageResource(R.mipmap.zan_gray)
        }

        //赞的数量
        ask_main_zan_num.text=listItemAsk.zan

        //评论的数量
        ask_main_comment_num.text=listItemAsk.commentCount

        //左侧用户头像
        icon = listItemAsk.icon
        getIcon()

        //点击了头像
        main_icon_iv!!.setOnClickListener{
            //自己的头像
            if(current_userID!!.equals(listItemAsk.publisherID)){
                val intent:Intent=Intent(context, IconSelfInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",listItemAsk.publisherID)
                bundle.putString("icon",listItemAsk.icon)
                bundle.putString("sex",listItemAsk.gender)
                bundle.putString("nickname",listItemAsk.nickname)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }else{
                val intent:Intent=Intent(context,IconInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",listItemAsk.publisherID)
                bundle.putString("icon",listItemAsk.icon)
                bundle.putString("sex",listItemAsk.gender)
                bundle.putString("nickname",listItemAsk.nickname)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }
        }

        ask_main_first_btn!!.setOnClickListener{
            //自己的头像
            if(current_userID!!.equals(listItemAsk.publisherID)){
                val intent:Intent=Intent(context, IconSelfInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",listItemAsk.publisherID)
                bundle.putString("icon",listItemAsk.icon)
                bundle.putString("sex",listItemAsk.gender)
                bundle.putString("nickname",listItemAsk.nickname)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }else{
                val intent:Intent=Intent(context,IconInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",listItemAsk.publisherID)
                bundle.putString("icon",listItemAsk.icon)
                bundle.putString("sex",listItemAsk.gender)
                bundle.putString("nickname",listItemAsk.nickname)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }
        }

        //下方图片 1~N张布局显示
        var photos:ArrayList<String>?=ArrayList<String>()
        var thumbnailImage:ArrayList<String>?=ArrayList<String>()
        photos=listItemAsk.image//将服务器传过来的网址给到photos
        thumbnailImage=listItemAsk.thumbnailImage
        if(photos!=null&&photos.size>0){
            multiImageView!!.visibility=View.VISIBLE
            multiImageView!!.setList(thumbnailImage)
            multiImageView!!.setOnItemClickListener { view, position ->
                // 因为单张图片时，图片实际大小是自适应的，imageLoader缓存时是按测量尺寸缓存的
                ImagePagerActivity.imageSize = ImageSize(view.measuredWidth, view.measuredHeight)
                ImagePagerActivity.startImagePagerActivity(context, photos!!,position)
            }

        }else{
            multiImageView!!.visibility=View.GONE
        }

        //点赞监听
        ask_main_zan_btn!!.setOnClickListener{
            if(mPresenter!=null){
                if(token.equals("")&&NetUtil.checkNetWork(context)){
                    //提示登录
                    val intent = Intent(context, LoginMain::class.java)
                    context.startActivity(intent)
                }else if(!NetUtil.checkNetWork(context)){
                    Toasty.info(context,Constant.NONETWORK).show()
                }else if(listItemAsk.zanIs.equals("1")){
                    Toasty.info(context,"亲，您已经赞过了").show()
                }else{
                    ask_main_zan.setImageResource(R.mipmap.zan_blue)
                    mPresenter!!.addZan(position,listItemAsk.id,token,context)
                }
            }
        }

        //分享点击事件处理
        ask_main_share_btn!!.setOnClickListener{
            showShareDialog(listItemAsk.title!!,listItemAsk.content!!,listItemAsk.link!!,listItemAsk.id!!)
        }

        //空白点击事件
        ask_main_layout!!.setOnClickListener{
            val intent:Intent=Intent(context, AskDetail::class.java)
            if(NetUtil.checkNetWork(context)){
                val bundle=Bundle()
                bundle.putString("id",listItemAsk.id)
                bundle.putString("link",listItemAsk.link)
                bundle.putString("publisherID",listItemAsk.publisherID)
                bundle.putString("isZan",listItemAsk.zanIs)
                bundle.putInt("position",position)
                intent.putExtras(bundle)

                //记录当前帖子的id,返回时，更新这条帖子的评论和是否点赞
                val editor: SharedPreferences.Editor=context.getSharedPreferences("update_ask", Context.MODE_APPEND).edit();
                editor.putString("id",listItemAsk.id)
                editor.putString("isZan",listItemAsk.zanIs)
                editor.putString("commentCount",listItemAsk.commentCount)
                editor.putString("zan",listItemAsk.zan)
                editor.putInt("position",position)
                if(null!=listItemAsk.commentID&&!listItemAsk.commentID.equals("")){
                    editor.putString("isDeal","1")
                }else{
                    editor.putString("isDeal","0")
                }
                editor.commit()

                context.startActivity(intent)
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        //评论
        ask_main_comment_btn!!.setOnClickListener{
            val intent:Intent=Intent(context, AskDetail::class.java)
            if(NetUtil.checkNetWork(context)){
                val bundle=Bundle()
                bundle.putString("id",listItemAsk.id)
                bundle.putString("link",listItemAsk.link)
                bundle.putString("publisherID",listItemAsk.publisherID)
                bundle.putString("isZan",listItemAsk.zanIs)
                bundle.putInt("position",position)
                intent.putExtras(bundle)

                //记录当前帖子的id,返回时，更新这条帖子的评论和是否点赞
                val editor: SharedPreferences.Editor=context.getSharedPreferences("update_ask", Context.MODE_APPEND).edit()
                editor.putString("id",listItemAsk.id)
                editor.putString("isZan",listItemAsk.zanIs)
                editor.putString("commentCount",listItemAsk.commentCount)
                editor.putString("zan",listItemAsk.zan)
                editor.putInt("position",position)
                if(null!=listItemAsk.commentID&&!listItemAsk.commentID.equals("")){
                    editor.putString("isDeal","1")
                }else{
                    editor.putString("isDeal","0")
                }
                editor.commit()

                context.startActivity(intent)
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        //评论
        ask_content!!.setOnClickListener{
            val intent:Intent=Intent(context, AskDetail::class.java)
            if(NetUtil.checkNetWork(context)){
                val bundle=Bundle()
                bundle.putString("id",listItemAsk.id)
                bundle.putString("link",listItemAsk.link)
                bundle.putString("publisherID",listItemAsk.publisherID)
                bundle.putString("isZan",listItemAsk.zanIs)
                bundle.putInt("position",position)
                intent.putExtras(bundle)

                //记录当前帖子的id,返回时，更新这条帖子的评论和是否点赞
                val editor: SharedPreferences.Editor=context.getSharedPreferences("update_ask", Context.MODE_APPEND).edit()
                editor.putString("id",listItemAsk.id)
                editor.putString("isZan",listItemAsk.zanIs)
                editor.putString("commentCount",listItemAsk.commentCount)
                editor.putString("zan",listItemAsk.zan)
                editor.putInt("position",position)
                if(null!=listItemAsk.commentID&&!listItemAsk.commentID.equals("")){
                    editor.putString("isDeal","1")
                }else{
                    editor.putString("isDeal","0")
                }
                editor.commit()

                context.startActivity(intent)
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }




        return convertView!!
    }


    /*internal inner class ViewHolder{
        var main_icon_iv:ImageView?=null
        var ask_left_type:Button?=null
        var ask_main_nick_tv:TextView?=null
        var ask_main_sex_tv:TextView?=null
        var ask_main_more_iv:ImageView?=null
        var ask_main_delete_tv:TextView?=null
        var ask_main_right_btn:Button?=null
        var ask_main_time:TextView?=null
        var ask_main_reward_tv:TextView?=null
        var ask_main_reward_num_tv:TextView?=null
        var ask_main_reward_tv2:TextView?=null
        var ask_main_urgent_iv:ImageView?=null
        var ask_main_urgent_tv:TextView?=null
        var ask_content:TextView?=null
        var ask_main_share:ImageView?=null
        var ask_main_zan:ImageView?=null
        var ask_main_zan_num:TextView?=null
        var ask_main_comment_num:TextView?=null
        var ask_main_comment:ImageView?=null
        var multiImageView:MultiImageView?=null
    }
*/

    fun showShareDialog(title:String,content:String,link:String,id:String){
        val contentView:View=LayoutInflater.from(context).inflate(R.layout.ask_share,null)
        hanleShareDialog(contentView,title,content,link,id)//这个函数还没有写
        popWindow_share= CustomPopWindow.PopupWindowBuilder(context)
                .setView(contentView)
                .enableBackgroundDark(true)
                .setBgDarkAlpha(0.7f)
                .setFocusable(true)
                .setOutsideTouchable(true)
                .setAnimationStyle(R.style.ask_share_anim)
                .create()
        popWindow_share!!.showAtLocation(ask_main_right_btn,Gravity.CENTER,0,0)

    }

    private fun hanleShareDialog(contentView: View,title: String,content: String,link:String,id:String){
        ask_share_qq=contentView.findViewById(R.id.ask_share_qq) as RelativeLayout
        ask_share_wechat=contentView.findViewById(R.id.ask_share_wechat) as RelativeLayout
        ask_share_qq_zone=contentView.findViewById(R.id.ask_share_qq_zone) as RelativeLayout
        ask_share_wechat_friends=contentView.findViewById(R.id.ask_share_wechat_friends) as RelativeLayout
        ask_share_copy=contentView.findViewById(R.id.ask_share_copy) as RelativeLayout


        ask_share_qq!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,content,link,id,QQ.Name,"QQFriend")
        }

        ask_share_qq_zone!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,content,link,id,QZone.Name,"QQZone")
        }

        ask_share_wechat!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,content,link,id, Wechat.Name,"weChatFriend")
        }

        ask_share_wechat_friends!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(title,content,link,id,WechatMoments.Name,"weChatCircle")
        }

        ask_share_copy!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            val cm=context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val mClipData= ClipData.newPlainText("Label",link+"?id="+id)
            cm.primaryClip=mClipData
            Toasty.info(context,"亲，复制成功，赶快分享一下吧~").show()
        }

    }

    private fun shareAsk(title:String,content:String,link:String,id:String,shareType:String,shareWay:String){
        JEventUtils.onCountEvent(context,Constant.Event_AskShare)//分享帖子内容统计

        val shareParams=ShareParams()
        shareParams.shareType=Platform.SHARE_WEBPAGE
        var display_content=content
        if(content.length>30){
            display_content=content.substring(0,30)+"..."
        }
        shareParams.text=display_content
        shareParams.title=title
        if(shareType.equals(WechatMoments.Name)){
            shareParams.title=title
        }
        shareParams.url=link+"?id=$id"
        shareParams.imagePath=MainApplication.ImagePath

        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        share_token = read.getString("token", "")
        share_id=id
        share_shareWay=shareWay


        JShareInterface.share(shareType, shareParams, mPlatActionListener)
    }

    private val mPlatActionListener = object : PlatActionListener {
        override fun onComplete(platform: Platform, action: Int, data: HashMap<String, Any>) {
            LogUtils.d_debugprint(Constant.Ask_TAG,"帖子分享成功！！！")
            Toasty.info(context,"亲，分享成功！").show()
            AskMainRequest.makeShareToServer(Constant.Ask_TAG,Constant.URL_Share_Ask,context,share_id,share_shareWay,share_token)
        }

        override fun onError(platform: Platform, action: Int, errorCode: Int, error: Throwable) {
            LogUtils.d_debugprint(Constant.Ask_TAG,"帖子分享失败！！！")
            Toasty.info(context,"亲，帖子分享失败！").show()
        }

        override fun onCancel(platform: Platform, action: Int) {
            LogUtils.d_debugprint(Constant.Ask_TAG,"帖子分享取消！！！")
            Toasty.info(context,"亲，您已取消分享！").show()
        }
    }

    fun showDialog(id:String,position:Int){
        val contentView:View=LayoutInflater.from(context).inflate(R.layout.ask_three_point_dialog,null)
        hanleDialog(contentView,id,position)
        popWindow= CustomPopWindow.PopupWindowBuilder(context)
                .setView(contentView)
                .enableBackgroundDark(true)
                .setBgDarkAlpha(0.7f)
                .setFocusable(true)
                .setOutsideTouchable(true)
                .setAnimationStyle(R.style.ask_three_anim)
                .create()
        popWindow!!.showAtLocation(ask_main_right_btn,Gravity.CENTER_HORIZONTAL,0,DensityUtil.dip2px(context,260.0f))
    }


    private fun hanleDialog(contentView:View,id: String,position:Int){
        ask_three_favorite=contentView.findViewById(R.id.ask_three_favorite) as TextView
        ask_three_report=contentView.findViewById(R.id.ask_three_report) as TextView
        ask_three_cancle=contentView.findViewById(R.id.ask_three_cancle) as TextView
        val ask_three_shielding=contentView.findViewById(R.id.ask_three_shielding) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow!=null){
                    popWindow!!.dissmiss()
                }
                when(v!!.id){
                    R.id.ask_three_favorite -> make_favorite(id)
                    R.id.ask_three_report   -> make_report(id)
                    R.id.ask_three_shielding-> make_shielding(id,position)
                    R.id.ask_three_cancle   -> popWindow!!.dissmiss()
                }
            }
        }
        ask_three_shielding!!.setOnClickListener(listener)
        ask_three_favorite!!.setOnClickListener(listener)
        ask_three_report!!.setOnClickListener(listener)
        ask_three_cancle!!.setOnClickListener(listener)
    }

    private fun make_shielding(id:String,position:Int){
        //添加到屏蔽的帖子全局文件
        val read = context.getSharedPreferences(Constant.ShareFile_BlackAsk, Context.MODE_PRIVATE)
        val blacklist = read.getString("blackask", "[]")

        val blacklist_array=ArrayList<String>()
        val jsonArray= JSONArray(blacklist)
        if(jsonArray.length()>0){
            for(i in 0..jsonArray.length()-1){
                blacklist_array.add(jsonArray.getString(i))
            }
        }
        blacklist_array.add(id)
        val jsonArray_new= JSONArray()
        for(i in blacklist_array.indices){
            jsonArray_new.put(blacklist_array[i])
        }

        val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_BlackAsk, Context.MODE_PRIVATE).edit()
        editor.putString("blackask",jsonArray_new.toString())
        val read2 = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val userID= read2.getString("id", "")
        editor.putString("userID",userID)
        editor.commit()

        LogUtils.d_debugprint(Constant.Ask_TAG,"\n黑名单帖子id：原来的json为：$blacklist\n现在的json为：${jsonArray_new.toString()} \n当前用户id为：$userID")

        //视图上删去这条帖子
        mPresenter!!.shieldingAsk(context,position)

    }

    private fun make_favorite(id:String){
        JEventUtils.onCountEvent(context,Constant.Event_AskCollect)//收藏帖子内容统计

        val token=get_token()
        //开始收藏=====请求服务器

        if(!token.equals("")&&NetUtil.checkNetWork(context)){

           val pDialog = SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
           pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
           pDialog.setTitleText("Loading");
           pDialog.setCancelable(true)
           pDialog.show()

            AskMainRequest.makeFavoriteFromServer(context, Constant.Ask_TAG, Constant.Ask_Favorite, id, token, pDialog)
        }
    }

    private fun make_report(id:String){

        val token=get_token()
        if(!token.equals("")&&NetUtil.checkNetWork(context)){
            val contentView:View=LayoutInflater.from(context).inflate(R.layout.ask_five_report_dialog,null)
            hanle_report_Dialog(contentView,id)
            popWindow_report= CustomPopWindow.PopupWindowBuilder(context)
                    .setView(contentView)
                    .enableBackgroundDark(true)
                    .setBgDarkAlpha(0.7f)
                    .setFocusable(true)
                    .setOutsideTouchable(true)
                    .setAnimationStyle(R.style.ask_three_anim)
                    .create()
            popWindow_report!!.showAtLocation(ask_main_right_btn,Gravity.CENTER_HORIZONTAL,0,DensityUtil.dip2px(context,215.0f))

        }
    }

    private fun hanle_report_Dialog(contentView:View,id: String){
        ask_report_five1=contentView.findViewById(R.id.ask_five_1) as TextView
        ask_report_five2=contentView.findViewById(R.id.ask_five_2) as TextView
        ask_report_five3=contentView.findViewById(R.id.ask_five_3) as TextView
        ask_report_five4=contentView.findViewById(R.id.ask_five_4) as TextView
        ask_report_cancle=contentView.findViewById(R.id.ask_five_cancle) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow_report!=null){
                    popWindow_report!!.dissmiss()
                }
                when(v!!.id){
                    R.id.ask_five_1 -> make_report(id,"欺诈骗钱")
                    R.id.ask_five_2 -> make_report(id,"色情暴力")
                    R.id.ask_five_3 -> make_report(id,"广告骚扰")
                    R.id.ask_five_4 -> make_report_other(id,"")
                    R.id.ask_five_cancle  -> popWindow_report!!.dissmiss()
                }
            }
        }
        ask_report_five1!!.setOnClickListener(listener)
        ask_report_five2!!.setOnClickListener(listener)
        ask_report_five3!!.setOnClickListener(listener)
        ask_report_five4!!.setOnClickListener(listener)
        ask_report_cancle!!.setOnClickListener(listener)
    }

    private fun make_report(id:String,reason:String){
        JEventUtils.onCountEvent(context,Constant.Event_AskReport)//举报帖子内容统计

        val token=get_token()
        //开始举报=====请求服务器
        if(!token.equals("")&&NetUtil.checkNetWork(context)){

             val pDialog = SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                  pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                  pDialog.setTitleText("Loading");
                  pDialog.setCancelable(true)
                  pDialog.show()
            AskMainRequest.makeInformFromServer(context, Constant.Ask_TAG, Constant.Inform_Ask, id, token, reason, pDialog)

        }
    }

    private fun make_report_other(id:String,reson:String){
        JEventUtils.onCountEvent(context,Constant.Event_AskReport)//举报帖子内容统计

        val token=get_token()
        //开始收藏=====请求服务器
        if(!token.equals("")&&NetUtil.checkNetWork(context)){
            val intent = Intent(context, AskReport::class.java)
            val bundle= Bundle()
            bundle.putString("id",id)
            bundle.putString("token",token)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }

    private fun get_token():String{
        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token= read.getString("token", "")
        if(token.equals("")){
            val intent = Intent(context, LoginMain::class.java)
            context.startActivity(intent)
        }else{
            return token
        }
        return ""
    }

    private fun getIcon() {
        if (icon !== "") {
            if (LogUtils.APP_IS_DEBUG) {
                ImageLoader.getInstance().displayImage( icon!!, main_icon_iv)
            } else {
                ImageLoader.getInstance().displayImage( icon!!, main_icon_iv)
            }
        }
    }

    /**
     * 配置ImageLoder
     */
    private fun configImageLoader() {
        // 初始化ImageLoader
        val options = DisplayImageOptions.Builder().showImageOnLoading(R.mipmap.icon_empty) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.icon_empty) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.icon_error) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true) // 设置下载的图片是否缓存在SD卡中
                // .displayer(new RoundedBitmapDisplayer(20)) // 设置成圆角图片
                .build() // 创建配置过得DisplayImageOption对象

        val config = ImageLoaderConfiguration.Builder(context).defaultDisplayImageOptions(options)
                .threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(Md5FileNameGenerator()).tasksProcessingOrder(QueueProcessingType.LIFO).build()

        ImageLoader.getInstance().init(config)
    }

}
