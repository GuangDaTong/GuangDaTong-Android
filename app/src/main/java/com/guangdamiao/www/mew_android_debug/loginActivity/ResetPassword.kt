package com.guangdamiao.www.mew_android_debug.loginActivity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.text.TextUtils
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import cn.smssdk.SMSSDK
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.mob.MobSDK
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

class ResetPassword : BaseActivity() ,View.OnClickListener{


    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_imageview:ImageView?=null
    private val TAG= Constant.Register_Tag
    private var phone: EditText? = null//手机号
    private var iphone:String?=null
    private var but1: Button?=null//下一步
    private var code: EditText?=null//验证码
    private var icode: String=""
    private var sendCode: TextView?=null//发送验证码
    private var txt_leftTime: TextView?=null//发送验证码后的显示
    private var time = Constant.Register_SendCode_MaxTime
    private var flag_but1_click=false//发送按钮是否被点击
    private var resultFromServer:String=""//服务器返回json
    private var resultFromServerhaveRegister:String=""//服务器返回能否注册字段
    private var flag_phoneisold:Boolean=true


    protected fun a(): String? {
        return null
    }

    protected fun b(): String? {
        return null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        MainApplication.getInstance().addActivity(this)
        LoginCacheActivity.addActivity(this)
        fullscreen()
        init()
        addSomeListener()
        MobSDK.init(this, this.a(), this.b())

    }

    private fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            val intent: Intent = Intent(this, LoginMain::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.alpha=0.618f
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))


            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.alpha=1.0f
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }

        sendCode!!.setOnClickListener(this)//设置发送验证监听器
        but1!!.setOnClickListener(this)//设置注册按钮监听器
    }


    fun fullscreen(){
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
    }

    private fun init() {
        phone = findViewById(R.id.reset_main_phone) as EditText
        code = findViewById(R.id.reset_main_code) as EditText
        txt_leftTime = findViewById(R.id.reset_main_txt_leftTime) as TextView
        sendCode = findViewById(R.id.reset_main_send) as TextView
        but1 = findViewById(R.id.reset_main_but1) as Button
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_center_textview!!.text="重置密码"
        title_center_textview!!.visibility=View.VISIBLE
        title_right_imageview!!.visibility=View.GONE
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.reset_main_send ->
            {
                flag_but1_click = true
                if (phonehasvalue()) {
                    val pDialog1 = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                    pDialog1!!.progressHelper.barColor = Color.parseColor("#A5DC86")
                    pDialog1!!.titleText = "Loading"
                    pDialog1!!.setCancelable(true)
                    pDialog1!!.show()
                    MyServerCheckPhoneIsOld(pDialog1)
                }
            }
            R.id.reset_main_but1 ->
                if(phonehasvalue() && codehasvalue()&&send_code_click())
                {
                    val pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                    pDialog!!.progressHelper.barColor = Color.parseColor("#A5DC86")
                    pDialog!!.titleText = "Loading"
                    pDialog!!.setCancelable(true)
                    pDialog!!.show()
                    MyServerCheck(pDialog)
                }

            else -> {}
        }
    }

    private fun send_code_click():Boolean{
        if(flag_but1_click){
            return true
        }else{
            Toasty.warning(this@ResetPassword, Constant.Register_No_SendCode).show()
            return false
        }
    }

    private fun phonehasvalue():Boolean{
        if (!TextUtils.isEmpty(phone!!.text.toString().trim { it <= ' ' })) {
            if (phone!!.text.toString().trim { it <= ' ' }.length == 11) {
                iphone = phone!!.text.toString().trim { it <= ' ' }
                code!!.requestFocus()
                return true
            } else {
                Toasty.warning(this@ResetPassword,"手机号输入有误&_&").show()
                phone!!.requestFocus()
                return false
            }
        } else {
            Toasty.warning(this@ResetPassword,"请输入您的手机号").show()
            phone!!.requestFocus()
            return false
        }
    }

    private fun codehasvalue():Boolean{
        if (!TextUtils.isEmpty(code!!.text.toString().trim { it <= ' ' })) {
            if (code!!.text.toString().trim { it <= ' ' }.length == 4) {
                icode = code!!.text.toString().trim { it <= ' ' }
                return true
            } else {
                Toasty.warning(this@ResetPassword, Constant.Register_Error_Code).show()
                code!!.requestFocus()
                return false
            }
        } else {
            Toasty.warning(this@ResetPassword, Constant.Register_No_Code).show()
            code!!.requestFocus()
            return false
        }
        return false
    }

    //验证码发送成功后提示文字
    private fun reminderText() {
        var handlerText: Handler = object : Handler() {
            override fun handleMessage(msg: Message) {
                if (msg.what == 1) {
                    if (time > 0) {
                        txt_leftTime!!.text = "验证码已发送" + time + "秒"
                        time--
                        this.sendEmptyMessageDelayed(1, 1000)
                    } else {
                        txt_leftTime!!.text = ""
                        time = 60
                        txt_leftTime!!.visibility = View.GONE
                        sendCode!!.visibility = View.VISIBLE
                    }
                } else {
                    code!!.setText("")
                    txt_leftTime!!.text = ""
                    time = 60
                    txt_leftTime!!.visibility = View.GONE
                    sendCode!!.visibility = View.VISIBLE
                }
            }
        }
        txt_leftTime!!.visibility = View.VISIBLE
        sendCode!!.visibility= View.GONE
        handlerText.sendEmptyMessageDelayed(1, 1000)
    }

    fun MyServerCheck(pDialog: SweetAlertDialog){
        if(NetUtil.checkNetWork(this)){
            val salt_phone:String
            val urlPath:String
            if(LogUtils.APP_IS_DEBUG){
                salt_phone= Constant.SALTFORTEST+iphone
                urlPath= Constant.BASEURLFORTEST + Constant.OrganizingData_Check
            }else{
                salt_phone= Constant.SALTFORRELEASE+iphone
                urlPath= Constant.BASEURLFORRELEASE + Constant.OrganizingData_Check
            }
            val sign:String= MD5Util.md5(salt_phone)

            val jsonObject=JSONObject()
            jsonObject.put("code",icode)
            jsonObject.put("sign",sign)
            jsonObject.put("phone",iphone)
            val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
            LogUtils.d_debugprint(Constant.LoginMain_Tag, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())
            val client = AsyncHttpClient(true, 80, 443)
            client.post(this@ResetPassword,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){
                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                    if(responseBody!=null){
                        if(pDialog!=null) pDialog!!.cancel()
                        val resultDate = String(responseBody!!, charset("utf-8"))
                        val response=JSONObject(resultDate)
                        resultFromServer=response.toString()
                        LogUtils.d_debugprint(Constant.LoginMain_Tag,"服务器返回verifytoken数据："+resultFromServer)
                        var getCode:String=""
                        getCode= JsonUtil.get_key_string(Constant.Server_Code,resultFromServer!!)
                        if(Constant.RIGHTCODE.equals(getCode)){
                            var result:String=""
                            result= JsonUtil.get_key_string(Constant.Server_Result,resultFromServer!!)
                            //用Bundle记录服务器返回的数据
                            var dataFromServer:Bundle=Bundle()
                            dataFromServer.putString("phone",iphone!!)
                            dataFromServer.putString("result",result!!)
                            val intent: Intent = Intent(this@ResetPassword,ResetPasswordInput::class.java)
                            intent.putExtras(dataFromServer)
                            startActivity(intent)//------------------------活动跳转处
                            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
                        }
                        else{
                            var msg:String=""
                            msg= JsonUtil.get_key_string("msg",resultFromServer!!)
                            Toasty.error(this@ResetPassword,msg).show()

                        }

                     }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                     if(pDialog!=null) pDialog!!.cancel()
                      Toasty.error(this@ResetPassword,"网络超时，请稍候重试！").show()
                }
            })

        }else{
            Toasty.info(this,Constant.NONETWORK).show()
        }
    }

    fun MyServerCheckPhoneIsOld(pDialog: SweetAlertDialog){
        if(NetUtil.checkNetWork(this)){
            val sign:String
            val urlPath:String
            val account:String=iphone!!
            if(LogUtils.APP_IS_DEBUG){
                sign= MD5Util.md5(Constant.SALTFORTEST+account)
                urlPath= Constant.BASEURLFORTEST + Constant.Register_IsAccountExist
            }else{
                sign= MD5Util.md5(Constant.SALTFORRELEASE+account)
                urlPath= Constant.BASEURLFORRELEASE + Constant.Register_IsAccountExist
            }
            val jsonObject=JSONObject()
            jsonObject.put("account",account)
            jsonObject.put("sign",sign)
            val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
            LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())
            val client = AsyncHttpClient(true, 80, 443)
            client.post(this,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){
                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                    if(responseBody!=null){
                        if(pDialog!=null) pDialog.cancel()
                        val resultDate = String(responseBody!!, charset("utf-8"))
                        val response=JSONObject(resultDate)
                        resultFromServerhaveRegister=response.toString()
                        LogUtils.d_debugprint(TAG,resultFromServerhaveRegister)
                        var getCode:String=""
                        var result:String=""
                        var isExist:Boolean
                        getCode= JsonUtil.get_key_string(Constant.Server_Code,resultFromServerhaveRegister!!)
                        result= JsonUtil.get_key_string(Constant.Server_Result,resultFromServerhaveRegister!!)
                        isExist= JsonUtil.get_key_boolean("isExist",result!!)
                        LogUtils.d_debugprint(TAG,isExist.toString())

                        if(Constant.RIGHTCODE.equals(getCode)&&isExist) {

                                SMSSDK.getVerificationCode("86", iphone)//=====发送验证码
                                reminderText()
                        }
                        else if(!isExist){

                                Toasty.error(this@ResetPassword, Constant.Register_AccountNotExist).show()
                        }

                     }
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                    if(pDialog!=null) pDialog.cancel()
                      Toasty.error(this@ResetPassword,"网络超时，请稍候重试！").show()
                }

            })

        }else{
            Toasty.info(this,Constant.NONETWORK).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    /**
     * 点击空白区域隐藏键盘.
     */
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@ResetPassword.currentFocus != null) {
                if (this@ResetPassword.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@ResetPassword.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            val intent: Intent = Intent(this, LoginMain::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }
}
