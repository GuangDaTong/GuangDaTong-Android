package com.guangdamiao.www.mew_android_debug.navigation.me.me_info

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MyData
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.Common
import com.guangdamiao.www.mew_android_debug.utils.CustomPopWindow
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import com.soundcloud.android.crop.Crop
import es.dmoral.toasty.Toasty
import java.io.File


class Me_Info : Activity() {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_textview: TextView? = null
    private var title_right_textview_btn:Button?=null
    private var me_info_nick:TextView?=null
    private var me_info_phone_txt:TextView?=null
    private var me_info_phone:TextView?=null
    private var me_info_college:TextView?=null
    private var me_info_icon:ImageView?=null
    private var me_info_college_txt:TextView?=null
    private var me_info_sex_txt:TextView?=null
    private var me_info_sex:TextView?=null
    private var me_info_grade:TextView?=null
    private var me_info_grade_txt:TextView?=null
    private var me_info_icon_rl:RelativeLayout?=null
    private var me_info_nick_rl:RelativeLayout?=null
    private var me_info_college_rl:RelativeLayout?=null
    private var me_info_sex_rl:RelativeLayout?=null
    private var me_info_grade_rl:RelativeLayout?=null

    var account = ""
    var password = ""
    var college = ""
    var school = ""
    var gender=""
    var grade=""
    var login_sign = ""
    var register_sign = ""
    var token: String? = ""
    var id = ""
    var icon = ""
    var nickname=""
    val TAG:String= Constant.Me_TAG
    val PHONEPICTURE=Constant.PHONEPICTURE
    private val MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 7

    var icon_new=""
    private var popWindow: CustomPopWindow?=null  //相册选择的弹框


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_me_info)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        MainApplication.getInstance().addActivity(this)
        getSharedFile()//=====得到全局文件，包含了个人信息
        initView()
        configImageLoader()
        addSomeListener()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_CALL_PHONE2)
        }
    }


    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Me_Info,"个人信息")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Me_Info,"个人信息")
        super.onPause()
    }

    private fun initView(){
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_right_textview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_textview = findViewById(R.id.title_right_textview) as TextView
        me_info_college_txt=findViewById(R.id.me_info_college_txt) as TextView
        me_info_college=findViewById(R.id.me_info_college) as TextView
        me_info_nick=findViewById(R.id.me_info_nick_tv) as TextView
        me_info_phone_txt=findViewById(R.id.me_info_phone_txt) as TextView
        me_info_phone=findViewById(R.id.me_info_phone) as TextView
        me_info_icon=findViewById(R.id.me_info_icon) as ImageView
        me_info_sex_txt=findViewById(R.id.me_info_sex_txt) as TextView
        me_info_sex=findViewById(R.id.me_info_sex) as TextView
        me_info_college=findViewById(R.id.me_info_college) as TextView
        me_info_grade=findViewById(R.id.me_info_grade) as TextView
        me_info_grade_txt=findViewById(R.id.me_info_grade_txt) as TextView
        me_info_icon_rl=findViewById(R.id.me_info_icon_rl) as RelativeLayout
        me_info_nick_rl=findViewById(R.id.me_info_nick_rl) as RelativeLayout
        me_info_college_rl=findViewById(R.id.me_info_college_rl) as RelativeLayout
        me_info_sex_rl=findViewById(R.id.me_info_sex_rl) as RelativeLayout
        me_info_grade_rl=findViewById(R.id.me_info_grade_rl) as RelativeLayout

        title_center_textview!!.text="个人信息"
        title_right_textview!!.text="保存"
        title_center_textview!!.visibility= View.VISIBLE
        title_right_textview!!.visibility=View.VISIBLE
        title_right_textview_btn!!.visibility=View.VISIBLE
        title_right_textview_btn!!.isEnabled=true
        me_info_nick!!.setText(nickname)
        me_info_nick!!.requestFocus()
        me_info_college!!.text=college

        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val loginType = read.getString("loginType", "")
        if(loginType.equals("phone")){
            me_info_phone_txt!!.text="手机号"
            var str_account=account.substring(3,7)
            val display=account.replace(str_account,"****")
            me_info_phone!!.text=display

        }else if(loginType.equals("QQ")){
            me_info_phone_txt!!.text="登录方式"
            me_info_phone!!.text="QQ登录"
        }


        if(gender==""||gender==null){
            me_info_sex!!.text="请选择"
        }else{
            me_info_sex!!.text=gender
        }
        if(grade==""||grade==null){
            me_info_grade!!.text="请选择"
        }else{
            me_info_grade!!.text=grade
        }

        getMyIcon()
    }

    private fun getMyIcon() {
        if (icon !== "") {
            if (LogUtils.APP_IS_DEBUG) {
                ImageLoader.getInstance().displayImage(icon , me_info_icon)
            } else {
                ImageLoader.getInstance().displayImage(icon, me_info_icon)
            }
        }
    }

    private fun addSomeListener(){
       me_info_icon_rl!!.setOnClickListener{//手机相册
           showOneDialog()
       }

       me_info_nick_rl!!.setOnClickListener{
           val intent=Intent(this, Me_ChooseName::class.java)
           val bundle=Bundle()
           val nick_=me_info_nick!!.text.toString()
           val sex_=me_info_sex!!.text.toString()
           val college_=me_info_college!!.text.toString()
           val grade_=me_info_grade!!.text.toString()
           bundle.putString("me_info_nick",me_info_nick!!.text.toString())
           bundle.putString("me_info_sex",me_info_sex!!.text.toString())
           bundle.putString("me_info_grade",me_info_grade!!.text.toString())
           bundle.putString("me_info_college",college_)

           intent.putExtras(bundle)
           startActivityForResult(intent,Constant.Me_ChooseName)
        }
       me_info_college_rl!!.setOnClickListener{
           val intent=Intent(this, Me_ChooseAcademy::class.java)
           val bundle=Bundle()
           val nick_=me_info_nick!!.text.toString()
           val sex_=me_info_sex!!.text.toString()
           val college_=me_info_college!!.text.toString()
           val grade_=me_info_grade!!.text.toString()
           if(nick_!=null){
               bundle.putString("me_info_nick",me_info_nick!!.text.toString())
           }
           if(sex_!=null){
               bundle.putString("me_info_sex",me_info_sex!!.text.toString())
           }
           if(grade_!=null){
               bundle.putString("me_info_grade",me_info_grade!!.text.toString())
           }
           if(college_!=null){
               bundle.putString("me_info_college",college_)
           }
           intent.putExtras(bundle)
           startActivityForResult(intent,Constant.Me_ChooseAcademy)
       }
       title_left_imageview_btn!!.setOnClickListener{
           finish()
           overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
       }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f
            }
            false
        }

       me_info_sex_rl!!.setOnClickListener{
           val intent=Intent(this, Me_ChooseSex::class.java)
           val bundle=Bundle()
           val nick_=me_info_nick!!.text.toString()
           val sex_=me_info_sex!!.text.toString()
           val college_=me_info_college!!.text.toString()
           val grade_=me_info_grade!!.text.toString()
           if(nick_!=null){
               bundle.putString("me_info_nick",me_info_nick!!.text.toString())
           }
           if(sex_!=null){
               bundle.putString("me_info_sex",me_info_sex!!.text.toString())
           }
           if(grade_!=null){
               bundle.putString("me_info_grade",me_info_grade!!.text.toString())
           }
           if(college_!=null){
               bundle.putString("me_info_college",college_)
           }
           intent.putExtras(bundle)
           startActivityForResult(intent,Constant.Me_ChooseSex)
       }
       me_info_grade_rl!!.setOnClickListener{
            val intent=Intent(this, Me_ChooseGrade::class.java)
            val bundle=Bundle()
            val nick_=me_info_nick!!.text.toString()
            val sex_=me_info_sex!!.text.toString()
            val college_=me_info_college!!.text.toString()
            val grade_=me_info_grade!!.text.toString()
            if(nick_!=null){
                bundle.putString("me_info_nick",me_info_nick!!.text.toString())
            }
            if(sex_!=null){
                bundle.putString("me_info_sex",me_info_sex!!.text.toString())
            }
            if(grade_!=null){
                bundle.putString("me_info_grade",me_info_grade!!.text.toString())
            }
            if(college_!=null){
                bundle.putString("me_info_college",college_)
            }
            intent.putExtras(bundle)
            startActivityForResult(intent,Constant.Me_ChooseGrade)
        }

        title_right_textview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_textview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_right_textview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_textview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_right_textview_btn!!.alpha=1.0f
            }
            false
        }

       title_right_textview_btn !!.setOnClickListener{
           val file= File(icon_new)

           if(!me_info_nick!!.text.toString().equals("")){


               val myData=MyData()
               if(file.exists()){
                   myData.icon=icon_new
               }
               myData.nickname=me_info_nick!!.text.toString()
               myData.college=me_info_college!!.text.toString()
               if(!me_info_sex!!.text.toString().equals("")&&!me_info_sex!!.text.toString().equals("请选择")){
                   myData.gender=me_info_sex!!.text.toString()
               }
               if(!me_info_grade!!.text.toString().equals("")&&!me_info_grade!!.text.toString().equals("请选择")){
                   myData.grade=me_info_grade!!.text.toString()
               }
               LogUtils.d_debugprint(Constant.Me_TAG,"这里upLoad之前得到的myData为：\n"+myData.toString())

               if(icon_new.equals("")&&nickname.equals(myData.nickname)&&college.equals(myData.college)&&gender.equals(myData.gender)&&grade.equals(myData.grade)){
                   Toasty.info(this@Me_Info,"亲，您未更改任何信息喔~").show()
               }else{

                   title_right_textview_btn!!.isEnabled=false
                   title_right_textview!!.setTextColor(R.color.bgcolor)
                   val pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                   pDialog.progressHelper.barColor = Color.parseColor("#A5DC86")
                   pDialog.titleText = "Loading"
                   pDialog.setCancelable(false)
                   pDialog.show()
                   InfoRequest.upLoadInfo(this@Me_Info,this@Me_Info,myData,pDialog,object:IDataRequestListener2String{
                       override fun loadSuccess(response_string: String?) {
                           if(response_string.equals("failed")){
                               title_right_textview_btn!!.isEnabled=true
                               title_right_textview!!.setTextColor(Color.WHITE)
                           }
                       }
                   })
               }

           }else{
             Toasty.info(this@Me_Info,"亲，昵称不能为空哦~").show()
           }

       }
    }

    private fun showOneDialog(){

            val contentView:View= LayoutInflater.from(this).inflate(R.layout.pop_one_dialog,null)
            hanleDialog(contentView)
            popWindow= CustomPopWindow.PopupWindowBuilder(this)
                    .setView(contentView)
                    .enableBackgroundDark(true)
                    .setBgDarkAlpha(0.7f)
                    .setFocusable(true)
                    .setOutsideTouchable(true)
                    .setAnimationStyle(R.style.ask_three_anim)
                    .create()
            popWindow!!.showAtLocation(me_info_icon_rl, Gravity.CENTER_HORIZONTAL,0, DensityUtil.dip2px(this,560.0f))

    }

    private fun hanleDialog(contentView:View){

        val me_one_photo=contentView.findViewById(R.id.me_one_photo) as TextView
        val me_one_cancle=contentView.findViewById(R.id.me_one_cancle) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow!=null){
                    popWindow!!.dissmiss()
                }
                when(v!!.id){

                    R.id.me_one_photo    -> getPhoto()
                    R.id.me_one_cancle   -> popWindow!!.dissmiss()
                }
            }
        }

        me_one_photo!!.setOnClickListener(listener)
        me_one_cancle!!.setOnClickListener(listener)
    }



    private fun getPhoto(){

        Crop.pickImage(this)
       /* val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent,PHONEPICTURE)*/

    }

    private fun getSharedFile(){
        val read =this.getSharedPreferences("userinfo", Context.MODE_PRIVATE)
        account = read.getString("account", "")
        password = read.getString("password", "")
        college = read.getString("college", "")
        school = read.getString("school", "")
        login_sign = read.getString("login_sign", "")
        register_sign = read.getString("register_sign", "")
        token = read.getString("token", "")
        id = read.getString("id", "")
        nickname = read.getString("nickname", "")
        icon = read.getString("icon", "")
        gender=read.getString("gender","")
        grade=read.getString("grade","")
        LogUtils.d_debugprint(TAG, "account=" + account + "\npassword=" + password + "\ncollege=" + college + "\nschool=" + school + "\nlogin_sign=" + login_sign
                + "\nregister_sign=" + register_sign + "\ntoken=" + token + "\nid=" + id + "\nnickname=" + nickname + "\nicon=" + icon+"\ngender="+gender+"\ngrade="+grade
        )
    }


  /*  public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_SELECT_PICTURE) {
                val selectedUri = data.data
                if (selectedUri != null) {
                    startCropActivity(data.data)
                } else {
                    Toast.makeText(this@ChooseIconActivity, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show()
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data)
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data)
        }
    }*/


    private fun beginCrop(source: Uri) {
        val destination = Uri.fromFile(File(cacheDir, "cropped"))
        Crop.of(source, destination).asSquare().start(this)
    }

    private fun handleCrop(resultCode: Int, result: Intent) {
        if (resultCode == Activity.RESULT_OK) {
            val uri=Crop.getOutput(result)
            me_info_icon!!.setImageDrawable(null)
            me_info_icon!!.setImageURI(uri)
            icon_new= Common.getImageAbsolutePath(this,uri)

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).message, Toast.LENGTH_SHORT).show()
        }
    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Crop.REQUEST_PICK && resultCode == Activity.RESULT_OK && data != null) {

            beginCrop(data.data)

        }else if(requestCode==Crop.REQUEST_CROP&&data!=null){
            handleCrop(resultCode,data)
            /*val selectedImage = data.data
            val filePathColumns = arrayOf(MediaStore.Images.Media.DATA)
            val c = contentResolver.query(selectedImage, filePathColumns, null, null, null)
            c!!.moveToFirst()
            val columnIndex = c.getColumnIndex(filePathColumns[0])
            val imagePath = c.getString(columnIndex)
            icon_new=imagePath
            c.close()*/
        }

        if(requestCode==Constant.Me_ChooseName){
            val bundle_from_me_choose=data!!.extras
            me_info_nick!!.text=bundle_from_me_choose!!.getString("me_info_nick")
            if(bundle_from_me_choose!!.getString("me_info_grade")!=null){
                me_info_grade!!.text=bundle_from_me_choose!!.getString("me_info_grade")
            }
            if(bundle_from_me_choose!!.getString("me_info_sex")!=null){
                me_info_sex!!.text=bundle_from_me_choose!!.getString("me_info_sex")
            }
            if(bundle_from_me_choose!!.getString("college")!=null){
                me_info_college!!.text=bundle_from_me_choose!!.getString("college")
            }
        }

        if(requestCode==Constant.Me_ChooseAcademy){
            val bundle_from_me_choose=data!!.extras
            me_info_nick!!.text=bundle_from_me_choose!!.getString("me_info_nick")
            if(bundle_from_me_choose!!.getString("me_info_grade")!=null){
                me_info_grade!!.text=bundle_from_me_choose!!.getString("me_info_grade")
            }
            if(bundle_from_me_choose!!.getString("me_info_sex")!=null){
                me_info_sex!!.text=bundle_from_me_choose!!.getString("me_info_sex")
            }
            if(bundle_from_me_choose!!.getString("college")!=null){
                me_info_college!!.text=bundle_from_me_choose!!.getString("college")
            }
        }
        if(requestCode==Constant.Me_ChooseSex){
            val bundle_from_me_choose=data!!.extras
            me_info_nick!!.text=bundle_from_me_choose!!.getString("me_info_nick")
            if(bundle_from_me_choose!!.getString("me_info_grade")!=null){
                me_info_grade!!.text=bundle_from_me_choose!!.getString("me_info_grade")
            }
            if(bundle_from_me_choose!!.getString("me_info_sex")!=null){
                me_info_sex!!.text=bundle_from_me_choose!!.getString("me_info_sex")
            }
            if(bundle_from_me_choose!!.getString("college")!=null){
                me_info_college!!.text=bundle_from_me_choose!!.getString("college")
            }
        }
        if(requestCode==Constant.Me_ChooseGrade){
            val bundle_from_me_choose=data!!.extras
            me_info_nick!!.text=bundle_from_me_choose!!.getString("me_info_nick")
            if(bundle_from_me_choose!!.getString("me_info_grade")!=null){
                me_info_grade!!.text=bundle_from_me_choose!!.getString("me_info_grade")
            }
            if(bundle_from_me_choose!!.getString("me_info_sex")!=null){
                me_info_sex!!.text=bundle_from_me_choose!!.getString("me_info_sex")
            }
            if(bundle_from_me_choose!!.getString("college")!=null){
                me_info_college!!.text=bundle_from_me_choose!!.getString("college")
            }
        }

    }

    //加载图片
    private fun showImage(imagePath: String) {
        val bm = BitmapFactory.decodeFile(imagePath)
        me_info_icon!!.setImageBitmap(bm)
        icon_new=imagePath
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    //点击空白隐藏键盘
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@Me_Info.currentFocus != null) {
                if (this@Me_Info.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@Me_Info.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }

    /**
     * 配置ImageLoder
     */
    private fun configImageLoader() {
        // 初始化ImageLoader
        val options = DisplayImageOptions.Builder().showImageOnLoading(R.mipmap.icon_empty) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.icon_empty) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.icon_error) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true) // 设置下载的图片是否缓存在SD卡中
                // .displayer(new RoundedBitmapDisplayer(20)) // 设置成圆角图片
                .build() // 创建配置过得DisplayImageOption对象

        val config = ImageLoaderConfiguration.Builder(this@Me_Info).defaultDisplayImageOptions(options)
                .threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(Md5FileNameGenerator()).tasksProcessingOrder(QueueProcessingType.LIFO).build()

        ImageLoader.getInstance().init(config)
    }

}
