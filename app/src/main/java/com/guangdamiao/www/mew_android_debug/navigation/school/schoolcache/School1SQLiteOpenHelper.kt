package com.guangdamiao.www.mew_android_debug.navigation.school.schoolcache

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2017/12/10.
 */

class School1SQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        private val name = "school1.db"
        private val version = 1
         val S1_Name="school1"
         val S1_Id="id"
         val S1_Sid="s_id"
         val S1_Title="title"
         val S1_CreateTime="createTime"
         val S1_Type="type"
         val S1_Link="link"
         val S1_ReadCount="readCount"
         val S1_Zan="zan"
         val S1_IsZan="isZan"
         val S1_IsFavorite="isFavorite"
         val S1_UrlEnd="urlEnd"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table school1(id integer primary key autoincrement," +
                "${School1SQLiteOpenHelper.S1_Sid} varchar(20)," +
                "${School1SQLiteOpenHelper.S1_Title} varchar(20),"+
                "${School1SQLiteOpenHelper.S1_CreateTime} varchar(100)," +
                "${School1SQLiteOpenHelper.S1_Type} varchar(5)," +
                "${School1SQLiteOpenHelper.S1_Link} varchar(100)," +
                "${School1SQLiteOpenHelper.S1_ReadCount} varchar(4)," +
                "${School1SQLiteOpenHelper.S1_Zan} varchar(2)," +
                "${School1SQLiteOpenHelper.S1_IsZan} varchar(2)," +
                "${School1SQLiteOpenHelper.S1_IsFavorite} varchar(2)," +
                "${School1SQLiteOpenHelper.S1_UrlEnd} varchar(50))")
/*

        db.execSQL("create table school1(id integer primary key autoincrement,s_id varchar(20),title varchar(20),type varchar(5)," +
                "link varchar(50),readCount varchar(4),zan varchar(2),isZan varchar(2),isFavorite varchar(2),urlEnd varchar(20))")
*/
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    //打开数据库时调用
    override fun onOpen(db: SQLiteDatabase) {
        //如果数据库已经存在，则再次运行不执行onCreate()方法，而是执行onOpen()打开数据库
        super.onOpen(db)
    }

}
