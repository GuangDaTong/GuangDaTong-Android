package com.guangdamiao.www.mew_android_debug.bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class MyLocation {

    var country: String? = ""
    var province:String?=""
    var city:String?=""
    var district:String?=""
    var county:String?=""
    var street:String?=""
    var siteName:String?=""

    constructor():super()
    constructor(country: String,province:String,city:String,district:String,county:String,street:String,siteName:String) : super() {
        this.country=country
        this.province=province
        this.city=city
        this.district=district
        this.county=county
        this.street=street
        this.siteName=siteName
    }


    override fun toString(): String {
        return "MyLocation[country=$country,province=$province，city=$city,district=$district,county=$county,street=$street,siteName=$siteName]"
    }
}
