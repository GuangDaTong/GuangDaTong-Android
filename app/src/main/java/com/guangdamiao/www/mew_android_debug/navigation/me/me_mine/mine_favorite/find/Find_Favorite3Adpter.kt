package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.find

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListFindFavoriteAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ImageSingleActivity
import com.guangdamiao.www.mew_android_debug.navigation.ViewHolder
import com.guangdamiao.www.mew_android_debug.navigation.find.FindWebView
import com.nostra13.universalimageloader.core.ImageLoader
import es.dmoral.toasty.Toasty
import java.util.*

/**

 */
class Find_Favorite3Adpter(protected var context: Context, list_activity: ArrayList<ListFindFavoriteAll>) : BaseAdapter() {
    private var list_activity:ArrayList<ListFindFavoriteAll>?=null
    private var icon: String? = null

    private var onShowItemClickListener: Find_Favorite3Adpter.OnShowItemClickListener?=null

    init {
        this.list_activity = list_activity

    }

    override fun getCount(): Int {
        return list_activity!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.find_list3, parent, false)
        }
        val listactivity = list_activity!!.get(position)//获取一个
        val list_icon = ViewHolder.get<ImageView>(convertView, R.id.activity_list_icon)
        val list_title = ViewHolder.get<TextView>(convertView, R.id.activity_list_title)
        val list_time = ViewHolder.get<TextView>(convertView, R.id.activity_list_time1)
        val list_price = ViewHolder.get<TextView>(convertView, R.id.activity_list_price1)
        val list_description = ViewHolder.get<TextView>(convertView, R.id.activity_list_description)
        val activity_favorite_ll=ViewHolder.get<LinearLayout>(convertView,R.id.activity_favorite_ll)
        val activity_favorite_checkBox=ViewHolder.get<CheckBox>(convertView,R.id.activity_favorite_checkBox)
        val activity_list_to=ViewHolder.get<ImageView>(convertView,R.id.activity_list_to)

        val time = listactivity.createTime!!.substring(0, 19).replace("T", " ")
        LogUtils.d_debugprint(Constant.Me_Favorite_TAG, "活动发表时间：$time")


        icon = listactivity.icon
        list_title.setText(listactivity.title)
        if(listactivity.title!!.length>18){
            list_title!!.setText(listactivity!!.title!!.substring(0,18)+"...")
        }
        list_time.setText(time)
        list_price.setText(listactivity.fee)
        list_description.text = "摘要：" + listactivity.description!!
        ImageLoader.getInstance().displayImage(icon!!, list_icon)
        list_icon!!.setOnClickListener{
            if(listactivity.icon!=null) ImageSingleActivity.startImageSingleActivity(context,listactivity.icon!!)
        }

        val link = listactivity.link
        val webView_id = listactivity.id
        val zan = listactivity.zan_
        val isZan = listactivity.isZan
        val readCount = listactivity.readCount

        activity_favorite_ll!!.setOnClickListener {
            val intent = Intent(context, FindWebView::class.java)
            val bundle = Bundle()
            bundle.putString("id", webView_id)
            if(webView_id.equals("")){
                Toasty.info(context,"亲，该内容已被删除了哦~").show()
            }else{
                bundle.putString("link", link)
                bundle.putString("zan", zan)
                bundle.putString("isZan", isZan)
                bundle.putString("readCount", readCount)
                bundle.putString("isFavorite", "1")
                bundle.putString("urlEnd", Constant.Find_Activity)
                bundle.putString("title_first", listactivity.title)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }
        }

        if(listactivity.isShow!!){
            activity_favorite_checkBox!!.visibility=View.VISIBLE
            activity_list_to!!.visibility=View.GONE
            activity_favorite_ll!!.setOnClickListener{
                if(!listactivity.isChecked!!){
                    listactivity.isChecked=true
                    activity_favorite_checkBox!!.isChecked=true
                }else{
                    activity_favorite_checkBox!!.isChecked=false
                    listactivity.isChecked=false
                }
                onShowItemClickListener!!.onShowItemClick(listactivity)
            }
        }else{
            activity_favorite_checkBox!!.visibility=View.GONE
            activity_list_to!!.visibility=View.VISIBLE
        }


        activity_favorite_checkBox!!.isChecked=listactivity.isChecked!!

        return convertView!!
    }

    interface OnShowItemClickListener {
        fun onShowItemClick(bean: ListFindFavoriteAll)
    }

    fun setOnShowItemClickListener(onShowItemClickListener: OnShowItemClickListener) {
        this.onShowItemClickListener = onShowItemClickListener
    }



}
