package com.guangdamiao.www.mew_android_debug.loginActivity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.view.ViewCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import cn.jiguang.share.android.api.AuthListener
import cn.jiguang.share.android.api.JShareInterface
import cn.jiguang.share.android.api.Platform
import cn.jiguang.share.android.model.AccessTokenInfo
import cn.jiguang.share.android.model.BaseResponseInfo
import cn.jiguang.share.android.model.UserInfo
import cn.jiguang.share.qqmodel.QQ
import cn.jpush.android.api.JPushInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MyBlackFriends
import com.guangdamiao.www.mew_android_debug.bean.MyLocation
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.FirstRequest
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.Loading_view
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONObject


class LoginMain : Activity(){

    var uuid=""
    var name=""
    var gender=""
    val TAG=Constant.LoginMain_Tag
    var resultFromServer:String=""
    var txt1:TextView?=null
    var forget_password:TextView?=null
    var but1: Button?=null
    var login_main_delete_btn: Button?=null
    var account:EditText?=null
    var password:EditText?=null
    var login_three_icon_rl:RelativeLayout?=null
    var pDialog: SweetAlertDialog?=null
    private var loading_first:Loading_view?=null
    private var handler: Handler? = object : Handler() {
        override fun handleMessage(msg: Message) {
            val toastMsg = msg.obj as ArrayList<String>
            if(loading_first!=null&&!this@LoginMain.isFinishing){
                loading_first!!.dismiss()
            }

            if(toastMsg.size==4){
                //Toasty.info(this@LoginMain,toastMsg[0]+"\n${toastMsg[1]} \n${toastMsg[2]} \n${toastMsg[3]}").show()
                uuid=toastMsg[1]
                name=toastMsg[2]
                if(toastMsg[3].equals("1")){
                    gender="男"
                }else if(toastMsg[3].equals("2")){
                    gender="女"
                }
                LoginRequest.isQQRegister(this@LoginMain,uuid,object:IDataRequestListener2String{
                    override fun loadSuccess(response_string: String?) {
                       if(response_string.equals("false")){
                           //没有注册过的，选择学院
                           val intent:Intent=Intent(this@LoginMain,ChooseAcademy::class.java)
                           startActivityForResult(intent,0x717)
                           overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)


                       }else if(response_string.equals("true")){
                           val read = this@LoginMain.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                           val college = read.getString("college", "")
                           LoginRequest.loginQQ(this@LoginMain,uuid,name,gender,college,object:IDataRequestListener2String{
                               override fun loadSuccess(response_string: String?) {
                                   val result=response_string
                                   val token: String
                                   val id: String
                                   val nickname: String
                                   val icon: String
                                   val coins: Int
                                   val level: Int
                                   val gender: String
                                   val school: String
                                   val college: String
                                   val grade: String
                                   val rewardCoins: String
                                   val rules: String
                                   var pushActivity=true
                                   var pushComment=true
                                   var pushLeaveWords=true
                                   var pushSystem=true
                                   var pushUrgentCollege=true
                                   var pushUrgentSchool=true

                                   token = if (JsonUtil.get_key_string("token", result!!) == null) "" else JsonUtil.get_key_string("token", result!!)
                                   id = if (JsonUtil.get_key_string("id", result!!) == null) "" else JsonUtil.get_key_string("id", result!!)
                                   nickname = if (JsonUtil.get_key_string("nickname", result!!) == null) "" else JsonUtil.get_key_string("nickname", result!!)
                                   icon = if (JsonUtil.get_key_string("icon", result!!) == null) "" else JsonUtil.get_key_string("icon", result!!)
                                   coins = if (JsonUtil.get_key_int("coins", result!!) == null) 0 else JsonUtil.get_key_int("coins", result!!)
                                   level = if (JsonUtil.get_key_int("level", result!!) == null) 0 else JsonUtil.get_key_int("level", result!!)
                                   gender = if (JsonUtil.get_key_string("gender", result!!) == null) "" else JsonUtil.get_key_string("gender", result!!)
                                   school = if (JsonUtil.get_key_string("school", result!!) == null) "" else JsonUtil.get_key_string("school", result!!)
                                   college = if (JsonUtil.get_key_string("college", result!!) == null) "" else JsonUtil.get_key_string("college", result!!)
                                   grade = if (JsonUtil.get_key_string("grade", result!!) == null) "" else JsonUtil.get_key_string("grade", result!!)
                                   rewardCoins = if (JsonUtil.get_key_string("rewardCoins", result!!) == null) "" else JsonUtil.get_key_string("rewardCoins", result!!)
                                   rules = if (JsonUtil.get_key_string("rules", result!!) == null) "" else JsonUtil.get_key_string("rules", result!!)
                                   pushActivity = JsonUtil.get_key_boolean("pushActivity", result!!)
                                   pushComment = JsonUtil.get_key_boolean("pushComment", result!!)
                                   pushLeaveWords = JsonUtil.get_key_boolean("pushLeaveWords", result!!)
                                   pushSystem = JsonUtil.get_key_boolean("pushSystem", result!!)
                                   pushUrgentCollege = JsonUtil.get_key_boolean("pushUrgentCollege", result!!)
                                   pushUrgentSchool = JsonUtil.get_key_boolean("pushUrgentSchool", result!!)

                                   var getData: List<Map<String, Any?>>
                                   getData = JsonUtil.getListMap("blacklist", result!!)
                                   val blacklist_array=ArrayList<String>()
                                   if (getData != null && getData.size > 0) {
                                       for (i in getData.indices) {
                                           val myFriend = MyBlackFriends()
                                           if (getData[i].getValue("createTime") != null && getData[i].getValue("icon") != null && getData[i].getValue("nickname") != null && getData[i].getValue("id") != null) {

                                               val black_id= getData[i].getValue("id").toString()
                                               blacklist_array.add(black_id)
                                           }
                                       }
                                   }

                                   //添加到黑名单全局文件
                                   val jsonArray_new= JSONArray()
                                   for(i in blacklist_array.indices){
                                       jsonArray_new.put(blacklist_array[i])
                                   }

                                   val editor2: SharedPreferences.Editor=this@LoginMain.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE).edit()
                                   editor2.putString("blacklist",jsonArray_new.toString())
                                   editor2.commit()

                                   LogUtils.d_debugprint(Constant.LoginMain_Tag,"\n黑名单登录后返回现在的json为：${jsonArray_new.toString()}")

                                   //写到全局文件中
                                   val editor: SharedPreferences.Editor = getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                                   editor.putString("loginType","QQ")
                                   editor.putString("account","")
                                   editor.putString("password","")
                                   editor.putString("token", token)
                                   editor.putString("id", id)
                                   editor.putString("nickname", nickname)
                                   editor.putString("icon", icon)
                                   editor.putInt("coins", coins)
                                   editor.putInt("level", level)
                                   editor.putString("gender", gender)
                                   editor.putString("school", school)
                                   editor.putString("college", college)
                                   editor.putString("grade", grade)
                                   editor.putString("rewardCoins", rewardCoins)
                                   editor.putString("rules", rules)
                                   editor.putBoolean("pushActivity", pushActivity)
                                   editor.putBoolean("pushComment", pushComment)
                                   editor.putBoolean("pushLeaveWords", pushLeaveWords)
                                   editor.putBoolean("pushSystem", pushSystem)
                                   editor.putBoolean("pushUrgentCollege", pushUrgentCollege)
                                   editor.putBoolean("pushUrgentSchool", pushUrgentSchool)
                                   editor.commit()

                                   val myLocation = MyLocation()
                                   val read2 = this@LoginMain.getSharedPreferences(Constant.ShareFile_Location, Context.MODE_PRIVATE)
                                   myLocation.country= read2.getString("country", "")
                                   myLocation.province=read2.getString("province","")
                                   myLocation.city=read2.getString("city","")
                                   myLocation.district=read2.getString("district","")
                                   myLocation.county=read2.getString("county","")
                                   myLocation.street=read2.getString("street","")
                                   myLocation.siteName=read2.getString("siteName","")
                                   val rid = JPushInterface.getRegistrationID(applicationContext)
                                   val read = this@LoginMain.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                   val userId = read.getString("id", "")

                                   FirstRequest.toServerLocationAndID(this@LoginMain, rid, userId, myLocation!!)
                                   LoginCacheActivity.finishActivity()
                                   overridePendingTransition(R.anim.push_top_in, R.anim.push_bottom_out)
                               }
                           })
                       }
                    }
                })
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        LoginCacheActivity.addActivity(this)
        setContentView(R.layout.activity_login_main)
        fullScreen()
        initView()
        addSomeListener()
        JShareInterface.removeAuthorize(QQ.Name, null)
    }

    fun initView(){
        but1=findViewById(R.id.login_main_but1) as Button
        txt1=findViewById(R.id.login_main_txt1) as TextView
        account=findViewById(R.id.login_main_account) as EditText
        password=findViewById(R.id.login_main_password) as EditText
        login_main_delete_btn=findViewById(R.id.login_main_delete_btn) as Button
        forget_password=findViewById(R.id.login_main_txt2) as TextView
        login_three_icon_rl=findViewById(R.id.login_three_icon_rl) as RelativeLayout

        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val account_= read.getString("account", "")
        val password_=read.getString("password","")
        account!!.setText(account_)
        password!!.setText(password_)
        if(!account_.equals("")&&!password_.equals("")){
            but1!!.isEnabled=true
            but1!!.setBackgroundResource(R.drawable.button_shape)
        }

    }

    fun addSomeListener(){
        img_backOnclick()
        registerNowListener()
        makeATextWatch()
        loginNowListerner()
        forgetPassword()

        login_three_icon_rl!!.setOnClickListener{
            loading_first= Loading_view(this@LoginMain,R.style.CustomDialog)
            loading_first!!.show()
            JShareInterface.getUserInfo(QQ.Name, mAuthListener)

        }
    }

    fun makeATextWatch() {
        class CustomTextWatcher: TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (account!!.length() ==11&&password!!.length()>=6) {
                    but1!!.isEnabled=true
                    but1!!.setBackgroundResource(R.drawable.button_shape)
                } else  {
                    but1!!.isEnabled=false
                    but1!!.setBackgroundResource(R.drawable.button_shape_unavailable)
                }
            }
        }
        account!!.addTextChangedListener(CustomTextWatcher())
        password!!.addTextChangedListener(CustomTextWatcher())
    }

    fun img_backOnclick(){
        login_main_delete_btn!!.setOnClickListener{
            LoginCacheActivity.finishActivity()
            overridePendingTransition(R.anim.push_top_in,R.anim.push_bottom_out)
        }
    }

    fun loginNowListerner(){//=====立即登录=====还没有处理好
        but1!!.setOnClickListener{
            if(NetUtil.checkNetWork(this)){
                makeALoadingDialog()
                MyServerCheck(this@LoginMain)
            }else{
                Toasty.info(this, Constant.NONETWORK).show()
            }
        }
    }

    fun forgetPassword(){
        forget_password!!.setOnClickListener{
            val intent:Intent=Intent(this, ResetPassword::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }
    }

    fun MyServerCheck(context:Context){
        val sign:String
        val urlPath:String
        val account_:String=account!!.text.toString()
        val password_:String=password!!.text.toString()
        if(LogUtils.APP_IS_DEBUG){
            sign= MD5Util.md5(Constant.SALTFORTEST+password_)
            urlPath=Constant.BASEURLFORTEST + Constant.LoginMain_Login
        }else{
            sign= MD5Util.md5(Constant.SALTFORRELEASE+password_)
            urlPath=Constant.BASEURLFORRELEASE + Constant.LoginMain_Login
        }

        val jsonObject=JSONObject()
        jsonObject.put("account",account_)
        jsonObject.put("password",password_)
        jsonObject.put("sign",sign)
        val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
        LogUtils.d_debugprint(Constant.LoginMain_Tag, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())
        val client = AsyncHttpClient(true, 80, 443)
        client.post(context, urlPath, stringEntity, Constant.ContentType, object : AsyncHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                if(responseBody!=null&&statusCode==200) {
                    if (pDialog != null) pDialog!!.cancel()
                    val resultDate = String(responseBody!!, charset("utf-8"))
                    val response = JSONObject(resultDate)
                    resultFromServer = response.toString()

                    if (JsonUtil.get_key_string(Constant.Server_Code, resultFromServer!!).equals(Constant.RIGHTCODE)) {
                        LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + resultFromServer)
                        val result: String
                        val token: String
                        val id: String
                        val nickname: String
                        val icon: String
                        val coins: Int
                        val level: Int
                        val gender: String
                        val school: String
                        val college: String
                        val grade: String
                        val rewardCoins: String
                        val rules: String
                        var pushActivity=true
                        var pushComment=true
                        var pushLeaveWords=true
                        var pushSystem=true
                        var pushUrgentCollege=true
                        var pushUrgentSchool=true


                        result = JsonUtil.get_key_string(Constant.Server_Result, resultFromServer!!)
                        token = if (JsonUtil.get_key_string("token", result!!) == null) "" else JsonUtil.get_key_string("token", result!!)
                        id = if (JsonUtil.get_key_string("id", result!!) == null) "" else JsonUtil.get_key_string("id", result!!)
                        nickname = if (JsonUtil.get_key_string("nickname", result!!) == null) "" else JsonUtil.get_key_string("nickname", result!!)
                        icon = if (JsonUtil.get_key_string("icon", result!!) == null) "" else JsonUtil.get_key_string("icon", result!!)
                        coins = if (JsonUtil.get_key_int("coins", result!!) == null) 0 else JsonUtil.get_key_int("coins", result!!)
                        level = if (JsonUtil.get_key_int("level", result!!) == null) 0 else JsonUtil.get_key_int("level", result!!)
                        gender = if (JsonUtil.get_key_string("gender", result!!) == null) "" else JsonUtil.get_key_string("gender", result!!)
                        school = if (JsonUtil.get_key_string("school", result!!) == null) "" else JsonUtil.get_key_string("school", result!!)
                        college = if (JsonUtil.get_key_string("college", result!!) == null) "" else JsonUtil.get_key_string("college", result!!)
                        grade = if (JsonUtil.get_key_string("grade", result!!) == null) "" else JsonUtil.get_key_string("grade", result!!)
                        rewardCoins = if (JsonUtil.get_key_string("rewardCoins", result!!) == null) "" else JsonUtil.get_key_string("rewardCoins", result!!)
                        rules = if (JsonUtil.get_key_string("rules", result!!) == null) "" else JsonUtil.get_key_string("rules", result!!)
                        pushActivity = JsonUtil.get_key_boolean("pushActivity", result!!)
                        pushComment = JsonUtil.get_key_boolean("pushComment", result!!)
                        pushLeaveWords = JsonUtil.get_key_boolean("pushLeaveWords", result!!)
                        pushSystem = JsonUtil.get_key_boolean("pushSystem", result!!)
                        pushUrgentCollege = JsonUtil.get_key_boolean("pushUrgentCollege", result!!)
                        pushUrgentSchool = JsonUtil.get_key_boolean("pushUrgentSchool", result!!)

                        var getData: List<Map<String, Any?>>
                        getData = JsonUtil.getListMap("blacklist", result!!)
                        val blacklist_array=ArrayList<String>()
                        if (getData != null && getData.size > 0) {
                            for (i in getData.indices) {
                                val myFriend = MyBlackFriends()
                                if (getData[i].getValue("createTime") != null && getData[i].getValue("icon") != null && getData[i].getValue("nickname") != null && getData[i].getValue("id") != null) {

                                    val black_id= getData[i].getValue("id").toString()
                                    blacklist_array.add(black_id)
                                }
                            }
                        }

                        //添加到黑名单全局文件
                         val jsonArray_new= JSONArray()
                         for(i in blacklist_array.indices){
                             jsonArray_new.put(blacklist_array[i])
                         }

                         val editor2: SharedPreferences.Editor=this@LoginMain.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE).edit()
                         editor2.putString("blacklist",jsonArray_new.toString())
                         editor2.commit()

                         LogUtils.d_debugprint(Constant.LoginMain_Tag,"\n黑名单登录后返回现在的json为：${jsonArray_new.toString()}")

                        //写到全局文件中
                        val editor: SharedPreferences.Editor = getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                        editor.putString("account", account_)
                        editor.putString("password", password_)
                        editor.putString("loginType","phone")
                        editor.putString("token", token)
                        editor.putString("id", id)
                        editor.putString("nickname", nickname)
                        editor.putString("icon", icon)
                        editor.putInt("coins", coins)
                        editor.putInt("level", level)
                        editor.putString("gender", gender)
                        editor.putString("school", school)
                        editor.putString("college", college)
                        editor.putString("grade", grade)
                        editor.putString("rewardCoins", rewardCoins)
                        editor.putString("rules", rules)
                        editor.putBoolean("pushActivity", pushActivity)
                        editor.putBoolean("pushComment", pushComment)
                        editor.putBoolean("pushLeaveWords", pushLeaveWords)
                        editor.putBoolean("pushSystem", pushSystem)
                        editor.putBoolean("pushUrgentCollege", pushUrgentCollege)
                        editor.putBoolean("pushUrgentSchool", pushUrgentSchool)
                        editor.commit()

                        val myLocation = MyLocation()
                        val read2 = context.getSharedPreferences(Constant.ShareFile_Location, Context.MODE_PRIVATE)
                        myLocation.country= read2.getString("country", "")
                        myLocation.province=read2.getString("province","")
                        myLocation.city=read2.getString("city","")
                        myLocation.district=read2.getString("district","")
                        myLocation.county=read2.getString("county","")
                        myLocation.street=read2.getString("street","")
                        myLocation.siteName=read2.getString("siteName","")
                        val rid = JPushInterface.getRegistrationID(applicationContext)
                        val read = this@LoginMain.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                        val userId = read.getString("id", "")

                        FirstRequest.toServerLocationAndID(this@LoginMain, rid, userId, myLocation!!)
                        LoginCacheActivity.finishActivity()
                        overridePendingTransition(R.anim.push_top_in, R.anim.push_bottom_out)

                    } else {

                        Toasty.error(this@LoginMain, "亲，账号或者密码不正确喔~").show()
                    }

                }
            }

            override fun onFailure(statusCode: Int, headers: Array<out cz.msebera.android.httpclient.Header>?, responseBody: ByteArray?, error: Throwable?) {
                  if(pDialog!=null) pDialog!!.cancel()
                  Toasty.error(context,"网络超时，请稍候重试！").show()
            }

        })
    }

    fun makeALoadingDialog(){
        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        pDialog!!.progressHelper.barColor = Color.parseColor("#A5DC86")
        pDialog!!.titleText = "Loading"
        pDialog!!.setCancelable(true)
        pDialog!!.show()
    }

    //马上注册监听事件
    fun registerNowListener(){
        txt1!!.isClickable=true
        txt1!!.setOnClickListener{
            registerNow(txt1)
        }
    }

    //全屏显示
    fun fullScreen() {
        val activity: Activity = this
        val statusColor: Int = Color.parseColor("#008000") as Int
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = activity.window
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = statusColor
            val mContentView: ViewGroup = activity.findViewById(Window.ID_ANDROID_CONTENT) as ViewGroup
            val mChildView: View = mContentView.getChildAt(0)
            if (mChildView != null) {
               ViewCompat.setFitsSystemWindows(mChildView, false)
            }
        }
    }

    //立即注册
    fun registerNow(v:View?){
        val intent:Intent=Intent(this, RegisterMain::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
    }

    //点击空白隐藏键盘
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@LoginMain.currentFocus != null) {
                if (this@LoginMain.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@LoginMain.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            LoginCacheActivity.finishActivity()
            overridePendingTransition(R.anim.push_top_in,R.anim.push_bottom_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    /**
     * 授权、获取个人信息回调
     * action ：Platform.ACTION_AUTHORIZING 授权
     * Platform.ACTION_USER_INFO 获取个人信息
     */
    internal var mAuthListener: AuthListener = object : AuthListener {
        override fun onComplete(platform: Platform, action: Int, data: BaseResponseInfo) {
            LogUtils.d_debugprint(TAG, "onComplete:$platform,action:$action,data:$data")
            var toastMsg: ArrayList<String>? = ArrayList<String>()
            if(platform!=null&&action!=null&&data!=null){
                when (action) {
                    Platform.ACTION_AUTHORIZING -> if (data is AccessTokenInfo) {        //授权信息
                        val token = data.token//token
                        val expiration = data.expiresIn//token有效时间，时间戳
                        val refresh_token = data.refeshToken//refresh_token
                        val openid = data.openid//openid
                        //授权原始数据，开发者可自行处理
                        val originData = data.getOriginData()
                        toastMsg!!.add("授权成功!");
                        LogUtils.d_debugprint(TAG, "openid:$openid,token:$token,expiration:$expiration,refresh_token:$refresh_token")
                        LogUtils.d_debugprint(TAG, "originData:" + originData)
                    }

                    Platform.ACTION_REMOVE_AUTHORIZING -> toastMsg!!.add("删除授权成功")

                    Platform.ACTION_USER_INFO -> if (data is UserInfo) {      //第三方个人信息
                        val openid = data.openid  //openid
                        val name = data.name  //昵称
                        val imageUrl = data.imageUrl  //头像url
                        val gender = data.gender//性别, 1表示男性；2表示女性
                        //个人信息原始数据，开发者可自行处理
                        val originData = data.getOriginData()
                        toastMsg!!.add("获取个人信息成功")
                        LogUtils.d_debugprint(TAG, "openid:$openid,name:$name,gender:$gender,imageUrl:$imageUrl")
                        LogUtils.d_debugprint(TAG, "originData:" + originData)
                        //Toasty.info(this@LoginMain,"openid:$openid,name:$name,gender:$gender,imageUrl:$imageUrl").show()
                        toastMsg!!.add(openid)
                        toastMsg!!.add(name)
                        toastMsg!!.add(gender.toString())
                    }
                }
            }

            if (handler != null) {
                val msg = handler!!.obtainMessage(1)
                msg.obj = toastMsg
                msg.sendToTarget()
            }

        }

        override fun onError(platform: Platform, action: Int, errorCode: Int, error: Throwable?) {
            LogUtils.d_debugprint(TAG, "授权失败！")
            //Toasty.info(this@LoginMain,"onError:$platform,action:$action,error:$error").show()
            var toastMsg: ArrayList<String>?= ArrayList<String>()
            if(platform!=null&&action!=null&&errorCode!=null&&error!=null){
                when (action) {
                    Platform.ACTION_AUTHORIZING -> toastMsg!!.add( "授权失败")
                    Platform.ACTION_REMOVE_AUTHORIZING -> toastMsg!!.add("删除授权失败")
                    Platform.ACTION_USER_INFO -> toastMsg!!.add("获取个人信息失败")
                }
            }
            if (handler != null) {
                val msg = handler!!.obtainMessage(1)
                msg.obj = toastMsg
                msg.sendToTarget()
            }

        }

        override fun onCancel(platform: Platform, action: Int) {
            LogUtils.d_debugprint(TAG, "onCancel:$platform,action:$action")
            //Toasty.info(this@LoginMain,"您已取消授权！").show()
            var toastMsg: ArrayList<String>? = ArrayList<String>()
            if(platform!=null&&action!=null){
                when (action) {
                    Platform.ACTION_AUTHORIZING -> toastMsg!!.add("取消授权")
                // TODO: 2017/6/23 删除授权不存在取消
                    Platform.ACTION_REMOVE_AUTHORIZING -> {
                    }
                    Platform.ACTION_USER_INFO -> toastMsg!!.add("取消获取个人信息")
                }
            }
            if (handler != null) {
                val msg = handler!!.obtainMessage(1)
                msg.obj = toastMsg
                msg.sendToTarget()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //返回标识
        if(resultCode==0x717){
            val bundle_from_choose=data!!.extras
            val college=bundle_from_choose!!.getString("college")
            LoginRequest.loginQQ(this@LoginMain,uuid,name,gender,college,object:IDataRequestListener2String{
                override fun loadSuccess(response_string: String?) {
                    val result=response_string
                    val token: String
                    val id: String
                    val nickname: String
                    val icon: String
                    val coins: Int
                    val level: Int
                    val gender: String
                    val school: String
                    val college: String
                    val grade: String
                    val rewardCoins: String
                    val rules: String
                    var pushActivity=true
                    var pushComment=true
                    var pushLeaveWords=true
                    var pushSystem=true
                    var pushUrgentCollege=true
                    var pushUrgentSchool=true

                    token = if (JsonUtil.get_key_string("token", result!!) == null) "" else JsonUtil.get_key_string("token", result!!)
                    id = if (JsonUtil.get_key_string("id", result!!) == null) "" else JsonUtil.get_key_string("id", result!!)
                    nickname = if (JsonUtil.get_key_string("nickname", result!!) == null) "" else JsonUtil.get_key_string("nickname", result!!)
                    icon = if (JsonUtil.get_key_string("icon", result!!) == null) "" else JsonUtil.get_key_string("icon", result!!)
                    coins = if (JsonUtil.get_key_int("coins", result!!) == null) 0 else JsonUtil.get_key_int("coins", result!!)
                    level = if (JsonUtil.get_key_int("level", result!!) == null) 0 else JsonUtil.get_key_int("level", result!!)
                    gender = if (JsonUtil.get_key_string("gender", result!!) == null) "" else JsonUtil.get_key_string("gender", result!!)
                    school = if (JsonUtil.get_key_string("school", result!!) == null) "" else JsonUtil.get_key_string("school", result!!)
                    college = if (JsonUtil.get_key_string("college", result!!) == null) "" else JsonUtil.get_key_string("college", result!!)
                    grade = if (JsonUtil.get_key_string("grade", result!!) == null) "" else JsonUtil.get_key_string("grade", result!!)
                    rewardCoins = if (JsonUtil.get_key_string("rewardCoins", result!!) == null) "" else JsonUtil.get_key_string("rewardCoins", result!!)
                    rules = if (JsonUtil.get_key_string("rules", result!!) == null) "" else JsonUtil.get_key_string("rules", result!!)
                    pushActivity = JsonUtil.get_key_boolean("pushActivity", result!!)
                    pushComment = JsonUtil.get_key_boolean("pushComment", result!!)
                    pushLeaveWords = JsonUtil.get_key_boolean("pushLeaveWords", result!!)
                    pushSystem = JsonUtil.get_key_boolean("pushSystem", result!!)
                    pushUrgentCollege = JsonUtil.get_key_boolean("pushUrgentCollege", result!!)
                    pushUrgentSchool = JsonUtil.get_key_boolean("pushUrgentSchool", result!!)

                    var getData: List<Map<String, Any?>>
                    getData = JsonUtil.getListMap("blacklist", result!!)
                    val blacklist_array=ArrayList<String>()
                    if (getData != null && getData.size > 0) {
                        for (i in getData.indices) {
                            val myFriend = MyBlackFriends()
                            if (getData[i].getValue("createTime") != null && getData[i].getValue("icon") != null && getData[i].getValue("nickname") != null && getData[i].getValue("id") != null) {

                                val black_id= getData[i].getValue("id").toString()
                                blacklist_array.add(black_id)
                            }
                        }
                    }

                    //添加到黑名单全局文件
                    val jsonArray_new= JSONArray()
                    for(i in blacklist_array.indices){
                        jsonArray_new.put(blacklist_array[i])
                    }

                    val editor2: SharedPreferences.Editor=this@LoginMain.getSharedPreferences(Constant.ShareFile_BlackList, Context.MODE_PRIVATE).edit()
                    editor2.putString("blacklist",jsonArray_new.toString())
                    editor2.commit()

                    LogUtils.d_debugprint(Constant.LoginMain_Tag,"\n黑名单登录后返回现在的json为：${jsonArray_new.toString()}")

                    //写到全局文件中
                    val editor: SharedPreferences.Editor = getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                    editor.putString("loginType","QQ")
                    editor.putString("account","")
                    editor.putString("password","")
                    editor.putString("token", token)
                    editor.putString("id", id)
                    editor.putString("nickname", nickname)
                    editor.putString("icon", icon)
                    editor.putInt("coins", coins)
                    editor.putInt("level", level)
                    editor.putString("gender", gender)
                    editor.putString("school", school)
                    editor.putString("college", college)
                    editor.putString("grade", grade)
                    editor.putString("rewardCoins", rewardCoins)
                    editor.putString("rules", rules)
                    editor.putBoolean("pushActivity", pushActivity)
                    editor.putBoolean("pushComment", pushComment)
                    editor.putBoolean("pushLeaveWords", pushLeaveWords)
                    editor.putBoolean("pushSystem", pushSystem)
                    editor.putBoolean("pushUrgentCollege", pushUrgentCollege)
                    editor.putBoolean("pushUrgentSchool", pushUrgentSchool)
                    editor.commit()

                    val myLocation = MyLocation()
                    val read2 = this@LoginMain.getSharedPreferences(Constant.ShareFile_Location, Context.MODE_PRIVATE)
                    myLocation.country= read2.getString("country", "")
                    myLocation.province=read2.getString("province","")
                    myLocation.city=read2.getString("city","")
                    myLocation.district=read2.getString("district","")
                    myLocation.county=read2.getString("county","")
                    myLocation.street=read2.getString("street","")
                    myLocation.siteName=read2.getString("siteName","")
                    val rid = JPushInterface.getRegistrationID(applicationContext)
                    val read = this@LoginMain.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                    val userId = read.getString("id", "")

                    FirstRequest.toServerLocationAndID(this@LoginMain, rid, userId, myLocation!!)
                    LoginCacheActivity.finishActivity()
                    overridePendingTransition(R.anim.push_top_in, R.anim.push_bottom_out)
                }

            })

        }
    }

}
