package com.guangdamiao.www.mew_android_debug.navigation.me.me_info

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.os.Environment
import android.os.Handler
import com.guangdamiao.www.mew_android_debug.bean.MyData
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.SendImage
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.*
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject
import org.xutils.common.Callback
import org.xutils.http.RequestParams
import java.io.File

/**
 * Created by Jason_Jan on 2017/12/17.
 */

class InfoRequest{
    companion object {
        fun upLoadInfo(activity: Activity, context: Context, myData: MyData, pDialog: SweetAlertDialog,listener:IDataRequestListener2String){
            val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(token.equals("")){
                val intent: Intent = Intent(context, LoginMain::class.java)
                context.startActivity(intent)
            }
            var sign=""
            var params:RequestParams?=null
            if(LogUtils.APP_IS_DEBUG){
                sign= MD5Util.md5(Constant.SALTFORTEST+token)
                params = RequestParams(Constant.BASEURLFORTEST+ Constant.Me_AlterData)
            }else{
                sign= MD5Util.md5(Constant.SALTFORRELEASE+token)
                params = RequestParams(Constant.BASEURLFORRELEASE+ Constant.Me_AlterData)
            }

            params.isMultipart=true
            params.addHeader("head","alter/data")

            LogUtils.d_debugprint(Constant.Me_TAG,"")
            val file= File(myData.icon)
            if(file.exists()){


                val last_prefix=myData.icon.substring(myData.icon.lastIndexOf("."))
                var targetPath= Environment.getExternalStorageDirectory().toString()+"/mew/"+myData.icon.substring(myData.icon.length-10,myData.icon.length).replace("/","")
                targetPath=targetPath+".jpg"
                LogUtils.d_debugprint(Constant.Me_TAG,"压缩后的图片路径为："+targetPath)

                //ImageTool.storeImage(askWrite_list[i],targetPath)
                ImageFactory.compressAndGenImage(myData.icon,targetPath,1024,false)

                val file_new= File(targetPath)
                if(last_prefix.equals(".jpg")){
                    params.addBodyParameter("icon",file_new,"image/jpg")
                }else if(last_prefix.equals(".jpeg")){
                    params.addBodyParameter("icon",file_new,"image/jpeg")
                }else if(last_prefix.equals(".png")){
                    params.addBodyParameter("icon",file_new,"image/png")
                }else if(last_prefix.equals(".gif")){
                    params.addBodyParameter("icon",file_new,"image/gif")
                }else{
                    params.addBodyParameter("icon",file_new,"image/jpg")
                }
                val newOpts= BitmapFactory.Options()
                newOpts.inJustDecodeBounds=true
                val bitmap= BitmapFactory.decodeFile(targetPath,newOpts)
                newOpts.inJustDecodeBounds=false
                val width=newOpts.outWidth
                val height=newOpts.outHeight
                val image= SendImage()
                image.width=width
                image.height=height
                LogUtils.d_debugprint(Constant.Me_TAG,"得到压缩过后的图片的长度和宽度分别为：\n"+"width="+width+"\nheight="+height)

            }

            params.addBodyParameter("nickname",myData.nickname)
            params.addBodyParameter("college",myData.college)
            params.addBodyParameter("gender",myData.gender)
            params.addBodyParameter("grade",myData.grade)
            params.addBodyParameter("sign",sign)

            LogUtils.d_debugprint(Constant.Me_TAG,"上传的参数为："+params.toString())

            org.xutils.x.http().post(params,object: Callback.CommonCallback<JSONObject>{
                override fun onSuccess(result: JSONObject?) {
                    val response=result.toString()
                    val msg= JsonUtil.get_key_string("msg",response)
                    val code= JsonUtil.get_key_string("code",response)
                    LogUtils.d_debugprint(Constant.Me_TAG,"\n服务器返回的code为："+code+"\n服务器返回的msg为："+msg)
                    pDialog.cancel()
                    if(code.equals(Constant.RIGHTCODE)){
                        JEventUtils.onCountEvent(context, Constant.Event_SendAsk)
                        /*清除图片*/

                        val file= File(myData.icon)
                        if(file.exists()){
                            val last_prefix=myData.icon.substring(myData.icon.lastIndexOf("."))
                            val targetPath=Environment.getExternalStorageDirectory().toString()+"/mew/"+myData.icon.substring(myData.icon.length-10,myData.icon.length).replace("/","")
                            LogUtils.d_debugprint(Constant.Me_TAG,"压缩后的图片路径为：正在清除"+targetPath)
                            val file_new= File(targetPath)
                            if(file_new.exists()){
                                file_new.delete()
                                LogUtils.d_debugprint(Constant.Me_TAG,"压缩后的图片路径为：清除成功 路径为：\n"+targetPath)
                            }
                        }

                        //修改全局文件
                        val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                        editor.putString("nickname",myData.nickname)
                        editor.putString("college",myData.college)
                        editor.putString("gender",myData.gender)
                        editor.putString("grade",myData.grade)
                        editor.commit()

                        val pDialog1=SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                        pDialog1.setTitleText("修改成功")
                        pDialog1.setContentText("亲，您已经成功修改个人资料！")
                        pDialog1.setConfirmClickListener {
                            activity.finish()
                        }
                        pDialog1.show()

                        val handler= Handler()
                        handler.postDelayed({
                            if(pDialog1!=null){
                                pDialog1.cancel()
                                activity.finish()
                            }
                        },1236)

                    }else{
                        pDialog.cancel()
                        /*清除图片*/
                        val file= File(myData.icon)
                        if(file.exists()){
                            val last_prefix=myData.icon.substring(myData.icon.lastIndexOf("."))
                            var targetPath=Environment.getExternalStorageDirectory().toString()+"/mew/"+myData.icon.substring(myData.icon.length-10,myData.icon.length).replace("/","")
                            targetPath=targetPath+".jpg"
                            LogUtils.d_debugprint(Constant.Me_TAG,"压缩后的图片路径为：正在清除"+targetPath)
                            val file_new= File(targetPath)
                            if(file_new.exists()){
                                file_new.delete()
                                LogUtils.d_debugprint(Constant.Me_TAG,"压缩后的图片路径为：清除成功 上传的图片路径为：\n"+targetPath)
                            }
                        }

                        val pDialog1=SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                pDialog1.setTitleText("修改失败")
                                pDialog1.setContentText("亲，"+msg)
                                pDialog1.show()

                        val handler= Handler()
                        handler.postDelayed({
                            if(pDialog1!=null) pDialog1.cancel()
                        },1236)

                        listener.loadSuccess("failed")
                    }
                }

                override fun onError(ex: Throwable?, isOnCallback: Boolean) {
                    LogUtils.d_debugprint(Constant.Me_TAG,ex.toString())
                    if(pDialog!=null) pDialog.cancel()
                    val pDialog1=SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            pDialog1.setTitleText("修改失败")
                            pDialog1.setContentText("亲，修改个人资料失败！")
                            pDialog1.show()
                    val handler= Handler()
                    handler.postDelayed({
                        if(pDialog1!=null) pDialog1.cancel()
                    },1236)

                    listener.loadSuccess("failed")
                }

                override fun onCancelled(cex: Callback.CancelledException?) {
                    if(pDialog!=null) pDialog.cancel()
                    val pDialog1=SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                            pDialog1.setTitleText("修改失败")
                            pDialog1.setContentText("亲，修改个人资料失败！")
                            pDialog1.show()

                    listener.loadSuccess("failed")
                }

                override fun onFinished() {
                    if(pDialog!=null) pDialog.cancel()
                }
            })

        }

        fun updataInfo(context:Context,token:String,listener:IDataRequestListener2String){
            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""

                if(LogUtils.APP_IS_DEBUG){
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                    urlPath=Constant.BASEURLFORTEST+Constant.URL_Data_Self
                }else{
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Data_Self
                }

                val jsonObject=JSONObject()
                jsonObject.put("sign",sign)

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Me_TAG,Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if(statusCode.toString().equals(Constant.RIGHTCODE)){
                                val response_string=response.toString()
                                //开始解析
                                LogUtils.d_debugprint(Constant.Me_TAG,"服务器返回的个人信息是："+response_string)
                                LogUtils.d_debugprint(Constant.Me_TAG, Constant.GETDATAFROMSERVER + JSONFormatUtil.formatJson(response_string)+"\n\n")

                                listener.loadSuccess(response_string)
                            }

                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }
    }
}
