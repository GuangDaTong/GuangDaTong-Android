package com.guangdamiao.www.mew_android_debug.navigation.ask.searchAsk

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.utils.JEventUtils
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils


class SearchAskActivity : Activity() {

    private var title_left_imageview_btn: Button?=null
    private var title_right_imageview_btn: Button?=null
    private var title_center_search_et: EditText?=null
    private var title_center_search_iv:ImageView?=null
    private var result_type=""
    private var result_label=""
    private var radioGroup_type: RadioGroup?=null
    private var radioGroup_label:MyRadioGroup?=null
    private var type_clear_tv:TextView?=null
    private var label_clear_tv:TextView?=null
    private var askSearch_clear_history_btn:Button?=null
    private var helper=RecordSQLiteOpenHelper(this)
    private var db:SQLiteDatabase?=null
    private var adapter:BaseAdapter?=null
    private var history_listView:SearchListView?=null
    private var searchAsk_record_rl:RelativeLayout?=null

    private var askSearch_line2_normal_btn:RadioButton?=null
    private var askSearch_line2_urgent_btn:RadioButton?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
        setContentView(R.layout.activity_search_ask)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)


        initView()
        addSomeListener()

        // 第一次进入查询所有的历史记录
        queryData("")
    }

    override fun onResume(){
        super.onResume()
        JAnalyticsInterface.onPageStart(this@SearchAskActivity,"搜索主页面")
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@SearchAskActivity,"搜索主页面")
        super.onPause()
    }

    fun initView(){
        title_left_imageview_btn=findViewById(R.id.title_left_imageview_btn) as Button
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_center_search_et=findViewById(R.id.title_center_searchAsk_et) as EditText
        title_center_search_iv=findViewById(R.id.title_center_search_iv) as ImageView
        title_center_search_iv!!.bringToFront()
        radioGroup_type=findViewById(R.id.radioGroup_type) as RadioGroup
        radioGroup_label=findViewById(R.id.radioGroup_label) as MyRadioGroup
        type_clear_tv=findViewById(R.id.askSearch_line1_clear) as TextView
        label_clear_tv=findViewById(R.id.askSearch_line3_clear) as TextView
        askSearch_clear_history_btn=findViewById(R.id.askSearch_clear_history_btn) as Button
        history_listView=findViewById(R.id.history_listView) as SearchListView
        searchAsk_record_rl=findViewById(R.id.searchAsk_record_rl) as RelativeLayout
        type_clear_tv!!.visibility=View.GONE
        label_clear_tv!!.visibility=View.GONE

        askSearch_line2_urgent_btn=findViewById(R.id.askSearch_line2_urgent_btn) as RadioButton
        askSearch_line2_normal_btn=findViewById(R.id.askSearch_line2_normal_btn) as RadioButton


    }

    fun addSomeListener() {
        title_left_imageview_btn!!.setOnClickListener {
            this.finish()
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f
            }
            false
        }

        askSearch_line2_normal_btn!!.setOnTouchListener { v, event ->

            if(event.action==MotionEvent.ACTION_DOWN){
                val animation= AnimationUtils.loadAnimation(this@SearchAskActivity,R.anim.but_scale_down)
                v.startAnimation(animation)
            }else if(event.action==MotionEvent.ACTION_UP){
                val animation=AnimationUtils.loadAnimation(this@SearchAskActivity,R.anim.but_scale_up)
                v.startAnimation(animation)
            }

            false
        }

        askSearch_line2_urgent_btn!!.setOnTouchListener { v, event ->

            if(event.action==MotionEvent.ACTION_DOWN){
                val animation=AnimationUtils.loadAnimation(this@SearchAskActivity,R.anim.but_scale_down)
                v.startAnimation(animation)
            }else if(event.action==MotionEvent.ACTION_UP){
                val animation=AnimationUtils.loadAnimation(this@SearchAskActivity,R.anim.but_scale_up)
                v.startAnimation(animation)
            }

            false
        }


        title_right_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_right_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_right_imageview_btn!!.alpha=1.0f
            }
            false
        }

        type_clear_tv!!.setOnClickListener {

            radioGroup_type!!.check(-1)
            type_clear_tv!!.visibility=View.GONE
            result_type=""

        }

        label_clear_tv!!.setOnClickListener {
            result_label=""
            label_clear_tv!!.visibility=View.GONE
            for (i in 0..radioGroup_label!!.childCount - 1) {
                val linearLayout = radioGroup_label!!.getChildAt(i) as LinearLayout
                for (j in 0..linearLayout!!.childCount - 1) {
                    val radioButton = linearLayout!!.getChildAt(j) as RadioButton

                    radioButton.isChecked = false
                }
            }
        }

        //查看选中了哪些类型
        radioGroup_type!!.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                if (checkedId != -1) {
                    val radioButton = group!!.findViewById(checkedId) as RadioButton
                    result_type = radioButton.text.toString()
                    LogUtils.d_debugprint(Constant.ASK_SEARCH_TAG, "帖子类型已选中的是：" + result_type)
                    type_clear_tv!!.visibility=View.VISIBLE
                }
            }
        })


        //查看选中了哪些标签
        radioGroup_label!!.setOnCheckedChangeListener(object : MyRadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: MyRadioGroup?, checkedId: Int) {

                if (checkedId != -1) {
                    LogUtils.d_debugprint(Constant.ASK_SEARCH_TAG, "帖子标签已选中的是id：" + checkedId)
                    val radioButton = group!!.findViewById(checkedId) as RadioButton
                    result_label = radioButton.text.toString()
                    LogUtils.d_debugprint(Constant.ASK_SEARCH_TAG, "帖子标签已选中的是：" + result_label)
                    label_clear_tv!!.visibility=View.VISIBLE
                }
            }
        })

        //清除历史
        askSearch_clear_history_btn!!.setOnClickListener {

               val pDialog=SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                pDialog.setTitleText("提示")
                pDialog.setContentText("确定删除历史记录？")
                pDialog.setCustomImage(R.mipmap.mew_icon)
                pDialog.setConfirmText("我确定")
                pDialog.setCancelText("再想想")
                pDialog .setConfirmClickListener {
                       deleteData()
                       queryData("")
                       pDialog.cancel()
                   }
                pDialog.setCancelClickListener {
                        pDialog.cancel()
                   }
                .show()
        }

        //点击了搜索框
        title_center_search_et!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode === KeyEvent.KEYCODE_ENTER && event!!.action === KeyEvent.ACTION_DOWN) {// 修改回车键功能
                    // 先隐藏键盘
                    (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                            currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

                    val hasData = hasData(title_center_search_et!!.text.toString().trim())
                    if (!hasData&&!title_center_search_et!!.text.toString().trim().equals("")) {
                        insertData(title_center_search_et!!.text.toString().trim(),result_type,result_label)
                        queryData("")
                    }
                    LogUtils.d_debugprint(Constant.ASK_SEARCH_TAG,"搜索的key是："+title_center_search_et!!.text.toString())

                    JEventUtils.onCountEvent(this@SearchAskActivity,Constant.Event_AskSearchBegin)//搜索次数统计


                    if(!hasData&&!title_center_search_et!!.text.toString().trim().equals("")){
                        insertData(title_center_search_et!!.text.toString().trim(),result_type,result_label)
                    }
                    val intent: Intent =Intent(this@SearchAskActivity,AskSearchResult::class.java)

                    val bundle=Bundle()
                    bundle.putString("key",title_center_search_et!!.text.toString())
                    bundle.putString("type",result_type)
                    bundle.putString("label",result_label)
                    intent.putExtras(bundle)

                    startActivity(intent)
                    overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)

                }
                return false
            }
        })

        //搜索框的文本变化实时监听
        title_center_search_et!!.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                val tempName=title_center_search_et!!.text.toString()
                queryData(tempName)
            }
        })

        //点击了右侧的搜索按钮
        title_right_imageview_btn!!.setOnClickListener{

                LogUtils.d_debugprint(Constant.ASK_SEARCH_TAG,"搜索的key是："+title_center_search_et!!.text.toString().trim())

                JEventUtils.onCountEvent(this@SearchAskActivity,Constant.Event_AskSearchBegin)//搜索次数统计

                if(!hasData(title_center_search_et!!.text.toString().trim())&&!title_center_search_et!!.text.toString().trim().equals("")){
                    insertData(title_center_search_et!!.text.toString().trim(),result_type,result_label)
                }
                val intent: Intent =Intent(this@SearchAskActivity,AskSearchResult::class.java)

                val bundle=Bundle()
                bundle.putString("key",title_center_search_et!!.text.toString())
                bundle.putString("type",result_type)
                bundle.putString("label",result_label)
                intent.putExtras(bundle)

                startActivity(intent)
                overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)

        }

    }


    /**
     * 插入数据
     */
    private fun insertData(tempName: String,type:String,label:String) {
        db = helper.writableDatabase
        val recordsItem= ContentValues()
        recordsItem.put("name",tempName)
        recordsItem.put("type",type)
        recordsItem.put("label",label)
        db!!.insert("records",null,recordsItem)
        db!!.close()
    }

    /**
     * 清空数据
     */
    private fun deleteData() {
        db = helper.getWritableDatabase()
        db!!.execSQL("delete from records")
        db!!.close()
    }

    /**
     * 模糊查询数据
     */
    private fun queryData(tempName: String) {
        val cursor = helper.readableDatabase.rawQuery(
                "select id as _id,name from records where name like '%$tempName%' order by id desc ", null)
        // 创建adapter适配器对象
        adapter = HistorySearchAdapter(this, R.layout.ask_search_item, cursor, arrayOf("name"),
                intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER) as BaseAdapter
        // 设置适配器
        history_listView!!.adapter=adapter
        adapter!!.notifyDataSetChanged()
    }

    /**
     * 检查数据库中是否已经有该条记录
     */
    private fun hasData(tempName: String): Boolean {
        val cursor = helper.readableDatabase.rawQuery(
                "select id as _id,name from records where name =?", arrayOf(tempName))
        //判断是否有下一个
        return cursor.moveToNext()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.push_right_in,R.anim.push_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }


    //点击空白隐藏键盘
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@SearchAskActivity.currentFocus != null) {
                if (this@SearchAskActivity.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@SearchAskActivity.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }
}
