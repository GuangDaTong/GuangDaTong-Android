package com.guangdamiao.www.mew_android_debug.overrideView;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by Jason_Jan on 2017/12/20.
 */

public class CopyTextView extends AppCompatEditText {

    public CopyTextView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public CopyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public CopyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected boolean getDefaultEditable() {//禁止EditText被编辑
        return false;
    }
}
