package com.guangdamiao.www.mew_android_debug.navigation.ask.presenter;

import android.content.Context;

import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.AskModel;
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener;
import com.guangdamiao.www.mew_android_debug.navigation.ask.view.AskMainView;

import java.util.ArrayList;


/**
 * 
* @ClassName: AskMainPresenter
* @Description: 通知model请求服务器和通知view更新
* @author JasonJan
* @date 2017-8-1
*
 */

public class AskMainPresenter {
	private AskModel mAskModel;
	private AskMainView mAskMainView;

	public AskMainPresenter(AskMainView view){
		this.mAskMainView = view;
		mAskModel = new AskModel();
	}
	
	/**
	* @Title: deleteAsk 
	* @Description: 删除动态 
	* @param
	* @return void    返回类型 
	* @throws
	 */
	public void deleteAsk(final Context context,final String token,final ArrayList<String> askIDs,final int position){
		mAskModel.deleteAsk(context,token,askIDs,new IDataRequestListener() {
			@Override
			public void loadSuccess(Object object) {
				mAskMainView.update2DeleteAsk(position);
			}
		});
	}


	/**
	 * @Title: deleteAsk
	 * @Description: 屏蔽动态
	 * @param
	 * @return void    返回类型
	 * @throws
	 */
	public void shieldingAsk(final Context context,final int position){

		mAskMainView.update2ShieldingAsk(position);
	}

	/**
	 * 
	* @Title: addZan 
	* @Description: 点赞型
	* @throws
	 */
	public void addZan(final int position,final String id,final String token,final Context context){
		mAskMainView.update2AddZan(position,new IDataRequestListener() {

			@Override
			public void loadSuccess(Object object) {
				mAskModel.addZan(id,token,context);
			}

		});
	}

}
