package com.guangdamiao.www.mew_android_debug.bean.message_bean

import java.io.Serializable


class MessageUserData : Serializable {
    var icon: String? = null
    var nickname: String? = null
    var gender:String?=null


    constructor():super()
    constructor(icon:String,nickname:String,gender:String){
       this.icon=icon
       this.nickname=nickname
       this.gender=gender
    }

    override fun toString(): String {
        return "MessageUserData[icon=$icon,\nnickname=$nickname,\ngender=$gender]"
    }
}
