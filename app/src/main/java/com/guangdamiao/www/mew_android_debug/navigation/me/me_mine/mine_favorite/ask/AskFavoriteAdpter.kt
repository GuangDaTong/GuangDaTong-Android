package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.ask

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.favorite_bean.MyFavoriteAsk
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.askDetail.AskDetail
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.MultiImageView
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconSelfInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.presenter.AskMainPresenter
import com.guangdamiao.www.mew_android_debug.utils.DateUtil
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.nostra13.universalimageloader.core.ImageLoader
import es.dmoral.toasty.Toasty
import java.util.*

/**

 */
class AskFavoriteAdpter(protected var context: Context, listAllAsk: ArrayList<MyFavoriteAsk>) : BaseAdapter() {
    private val mInflater: LayoutInflater//从别人得到 自己用
    private var mContext: Context? = null//从别人得到 自己用
    private var listAllAsk:ArrayList<MyFavoriteAsk>?=null
    private var onShowItemClickListener: OnShowItemClickListener?=null

    private var mPresenter: AskMainPresenter?=null //处理服务器交互和view更新的类

    init {
        this.mContext = context
        this.listAllAsk=listAllAsk
        this.mInflater = LayoutInflater.from(mContext)
    }

    fun setAskPresenter(presenter: AskMainPresenter){//实现了这个适配器可以处理一切的关键！！
        mPresenter=presenter
    }

    override fun getCount(): Int {
        return listAllAsk!!.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//这里只是list中单一的一行数据
        var convertView = convertView
        var holder:AskFavoriteAdpter.ViewHolder

        if (convertView == null) {
            holder=AskFavoriteAdpter.ViewHolder()
            convertView = LayoutInflater.from(context).inflate(R.layout.ask_main_list, parent, false)
            holder.ask_left_type=convertView!!.findViewById(R.id.ask_left_type) as Button
            holder.main_icon_iv=convertView!!.findViewById(R.id.ask_main_icon) as ImageView
            holder.ask_main_nick_tv=convertView!!.findViewById(R.id.ask_main_nick_tv) as TextView
            holder.ask_main_sex_tv=convertView!!.findViewById(R.id.ask_main_sex_tv) as TextView
            holder.ask_main_time=convertView!!.findViewById(R.id.ask_main_time) as TextView
            holder.ask_main_reward_tv=convertView!!.findViewById(R.id.ask_main_reward_tv) as TextView
            holder.ask_main_reward_num_tv=convertView!!.findViewById(R.id.ask_main_reward_num_tv) as TextView
            holder.ask_main_reward_tv2=convertView!!.findViewById(R.id.ask_main_reward_tv2) as TextView
            holder.ask_main_urgent_iv=convertView!!.findViewById(R.id.ask_main_urgent_iv) as ImageView
            holder.ask_main_urgent_tv=convertView!!.findViewById(R.id.ask_main_urgent_tv) as TextView
            holder.ask_content=convertView!!.findViewById(R.id.ask_content) as TextView
            holder.ask_main_deal_tv=convertView!!.findViewById(R.id.ask_main_deal_tv) as TextView
            holder.ask_main_title_tv=convertView!!.findViewById(R.id.ask_main_title_tv) as TextView
            holder.ask_main_layout=convertView!!.findViewById(R.id.ask_main_layout) as LinearLayout
            holder.ask_main_share=convertView!!.findViewById(R.id.ask_main_share) as ImageView
            holder.ask_main_zan=convertView!!.findViewById(R.id.ask_main_zan) as ImageView
            holder.ask_main_zan_num=convertView!!.findViewById(R.id.ask_main_zan_num_tv) as TextView
            holder.ask_main_comment=convertView!!.findViewById(R.id.ask_main_comment) as ImageView
            holder.ask_main_comment_num=convertView!!.findViewById(R.id.ask_main_comment_num_tv) as TextView
            holder.ask_main_more_iv=convertView!!.findViewById(R.id.ask_main_more_iv) as ImageView
            holder.ask_main_choose_cb=convertView!!.findViewById(R.id.ask_main_choose_cb) as CheckBox
            holder.multiImageView=convertView!!.findViewById(R.id.ask_main_multi_iv) as MultiImageView
            holder.ask_main_delete_tv=convertView!!.findViewById(R.id.ask_main_delete_tv) as TextView
            holder.ask_main_right_btn=convertView!!.findViewById(R.id.ask_main_right_btn) as Button
            convertView.tag=holder
        }else{
            holder=convertView.tag as AskFavoriteAdpter.ViewHolder
        }
        val listItemAsk = listAllAsk!!.get(position)//获取一个


        holder.ask_main_more_iv!!.visibility=View.GONE
        holder.ask_main_share!!.visibility=View.GONE
        holder.ask_main_zan!!.visibility=View.GONE
        holder.ask_main_zan_num!!.visibility=View.GONE
        holder.ask_main_comment!!.visibility=View.GONE
        holder.ask_main_comment_num!!.visibility=View.GONE

        //判断是否是自己的帖子
        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token=read.getString("token","")
        val current_userID = read.getString("id", "")

        holder.ask_main_deal_tv!!.visibility=View.GONE

        //帖子标题
        if(null!=listItemAsk.title&&!listItemAsk.title.equals("")){
            holder.ask_main_title_tv!!.text=listItemAsk.title
        }else{
            holder.ask_main_title_tv!!.text="无题"
        }

        //时间
        val time = listItemAsk.createTime!!.substring(0, 16).replace("T", " ")
        val display_date=DateUtil.string2Date(time,"yyyy-MM-dd HH:mm")
        val display_string=DateUtil.getTimestampString(display_date)
        holder.ask_main_time!!.setText(display_string)

        //昵称
        if(listItemAsk.nickname!!.length>12){
            val name_display=listItemAsk.nickname!!.substring(0,12)+"..."
            holder.ask_main_nick_tv!!.setText(name_display)
        }else{
            holder.ask_main_nick_tv!!.setText(listItemAsk.nickname)
        }

        //性别
        if(listItemAsk.gender.equals("男")){
            holder.ask_main_sex_tv!!.text="♂"
            holder.ask_main_sex_tv!!.setTextColor(context.resources.getColor(R.color.headline))
            holder. ask_main_sex_tv!!.visibility=View.VISIBLE
        }else if(listItemAsk.gender.equals("女")){
            holder.ask_main_sex_tv!!.text="♀"
            holder.ask_main_sex_tv!!.setTextColor(context.resources.getColor(R.color.ask_red))

            holder.ask_main_sex_tv!!.visibility=View.VISIBLE
        }else{
            holder.ask_main_sex_tv!!.visibility=View.GONE
        }

        //左侧标签
        if(!listItemAsk.label.equals("")){
            holder.ask_left_type!!.setText("#"+listItemAsk.label)
            if(listItemAsk.label.equals("寻物启事")){
                holder.ask_left_type!!.setTextColor(context.resources.getColor(R.color.ask_red))
            }else if(listItemAsk.label.equals("失物招领")){
                holder.ask_left_type!!.setTextColor(context.resources.getColor(R.color.ask_green))
            }else{
                holder.ask_left_type!!.setTextColor(context.resources.getColor(R.color.ask_yellow))
            }
        }

        //内容
        if(listItemAsk.content!!.length>200){
            val content_display=listItemAsk.content!!.substring(0,200)+"..."
            holder.ask_content!!.setText(content_display)
        }else{
            holder.ask_content!!.setText(listItemAsk.content)
        }

        //是否有悬赏
        if(!listItemAsk.reward.equals("0")){
            holder.ask_main_reward_tv!!.visibility=View.VISIBLE
            holder.ask_main_reward_num_tv!!.text=listItemAsk.reward
            holder.ask_main_reward_num_tv!!.visibility=View.VISIBLE
            holder.ask_main_reward_tv2!!.visibility=View.VISIBLE
        }else{
            holder.ask_main_reward_tv!!.visibility=View.GONE
            holder.ask_main_reward_num_tv!!.visibility=View.GONE
            holder.ask_main_reward_tv2!!.visibility=View.GONE
        }

        //是否是急问
        if(listItemAsk.type.equals("急问")){
            holder.ask_main_urgent_iv!!.visibility=View.VISIBLE
            holder.ask_main_urgent_tv!!.visibility=View.VISIBLE
        }else{
            holder.ask_main_urgent_iv!!.visibility=View.GONE
            holder.ask_main_urgent_tv!!.visibility=View.GONE
        }

        //左侧用户头像
        ImageLoader.getInstance().displayImage( listItemAsk.icon!!,  holder.main_icon_iv)

        //点击了头像
        holder.main_icon_iv!!.setOnClickListener{
            //自己的头像
            if(current_userID!!.equals(listItemAsk.publisherID)){
                val intent:Intent=Intent(context, IconSelfInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",listItemAsk.publisherID)
                bundle.putString("icon",listItemAsk.icon)
                bundle.putString("sex",listItemAsk.gender)
                bundle.putString("nickname",listItemAsk.nickname)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }else{
                val intent:Intent=Intent(context,IconInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",listItemAsk.publisherID)
                bundle.putString("icon",listItemAsk.icon)
                bundle.putString("sex",listItemAsk.gender)
                bundle.putString("nickname",listItemAsk.nickname)
                intent.putExtras(bundle)
                context.startActivity(intent)
            }
        }

        holder.multiImageView!!.visibility=View.GONE

        //空白点击事件
        holder.ask_main_layout!!.setOnClickListener{
            if(!listItemAsk.isShow!!){
                val intent:Intent=Intent(context, AskDetail::class.java)
                if(NetUtil.checkNetWork(context)){
                    val bundle=Bundle()
                    bundle.putString("id",listItemAsk.id)
                    bundle.putString("link",listItemAsk.link)
                    bundle.putString("publisherID",listItemAsk.publisherID)
                    bundle.putString("isZan",listItemAsk.zanIs)
                    bundle.putInt("position",position)
                    intent.putExtras(bundle)
                    context.startActivity(intent)
                }else{
                    Toasty.info(context,Constant.NONETWORK).show()
                }
            }
        }

        if(listItemAsk.isShow!!){
            holder.ask_main_choose_cb!!.visibility=View.VISIBLE
            holder.ask_main_layout!!.setOnClickListener{
                if(!listItemAsk.isChecked!!){
                    listItemAsk.isChecked=true
                    holder.ask_main_choose_cb!!.isChecked=true
                }else{
                    holder.ask_main_choose_cb!!.isChecked=false
                    listItemAsk.isChecked=false
                }
                onShowItemClickListener!!.onShowItemClick(listItemAsk)
            }
        }else{
            holder.ask_main_choose_cb!!.visibility=View.GONE
        }


       /* holder.ask_main_choose_cb!!.setOnCheckedChangeListener(object:CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                listItemAsk.isChecked=true
                if(!isChecked) listItemAsk.isChecked=false
                onShowItemClickListener!!.onShowItemClick(listItemAsk)
            }
        })*/

        holder.ask_main_choose_cb!!.isChecked=listItemAsk.isChecked!!

        return convertView!!
    }


   class ViewHolder{
        var main_icon_iv:ImageView?=null
        var ask_left_type:Button?=null
        var ask_main_nick_tv:TextView?=null
        var ask_main_sex_tv:TextView?=null
        var ask_main_more_iv:ImageView?=null
        var ask_main_delete_tv:TextView?=null
        var ask_main_right_btn:Button?=null
        var ask_main_time:TextView?=null
        var ask_main_reward_tv:TextView?=null
        var ask_main_reward_num_tv:TextView?=null
        var ask_main_reward_tv2:TextView?=null
        var ask_main_urgent_iv:ImageView?=null
        var ask_main_urgent_tv:TextView?=null
        var ask_content:TextView?=null
        var ask_main_share:ImageView?=null
        var ask_main_zan:ImageView?=null
        var ask_main_zan_num:TextView?=null
        var ask_main_comment_num:TextView?=null
        var ask_main_comment:ImageView?=null
        var multiImageView: MultiImageView?=null
        var ask_main_deal_tv:TextView?=null
        var ask_main_title_tv:TextView?=null
        var ask_main_layout:LinearLayout?=null
        var ask_main_choose_cb:CheckBox?=null
    }

    private fun get_token():String{
        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token= read.getString("token", "")
        if(token.equals("")){
            val intent = Intent(context, LoginMain::class.java)
            context.startActivity(intent)
        }else{
            return token
        }
        return ""
    }

    interface OnShowItemClickListener {
        fun onShowItemClick(bean: MyFavoriteAsk)
    }

    fun setOnShowItemClickListener(onShowItemClickListener: OnShowItemClickListener) {
        this.onShowItemClickListener = onShowItemClickListener
    }
}
