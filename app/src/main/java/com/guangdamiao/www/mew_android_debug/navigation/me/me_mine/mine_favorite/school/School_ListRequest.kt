package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.school

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout

import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListSchoolFavoriteAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/20.
 */

class School_ListRequest {

    companion object {


        fun getResultFromServer(activity: Activity, context: Context, TAG: String, urlEnd: String, pageIndexNow: Int, listAll: ArrayList<ListSchoolFavoriteAll>, adapter: School_ListAdpter, ptrClassicFrameLayout: PtrClassicFrameLayout, loading_first:SweetAlertDialog?,listener: IDataRequestListener2String) {

            if(NetUtil.checkNetWork(context)){
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                val orderBy=Constant.OrderBy_Default
                val order=Constant.Order_Default
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }
                val jsonObject=JSONObject()
                jsonObject.put("pageIndex",pageIndex)
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("sign",sign)
                jsonObject.put("orderBy",orderBy)
                jsonObject.put("order",order)
                val stringEntity=StringEntity(jsonObject.toString())
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){
                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (loading_first != null) loading_first.cancel()
                            if(!activity.isFinishing){
                                var getResultFromServer=""
                                getResultFromServer=response.toString()
                                LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                                val getCode: String
                                var msg: String? = null
                                var result: String
                                var getData: List<Map<String, Any?>>
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                                if (Constant.RIGHTCODE.equals(getCode)) {//=====服务器解析正常

                                    msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                    result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                    getData = JsonUtil.getListMap("data", result)//=====这里可能发生异常
                                    LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + getData.toString())
                                    if (getData != null&&getData.size>0) {
                                        if(pageIndexNow==0){
                                            listAll.clear()
                                        }
                                        for (i in getData.indices) {
                                            val favorID=getData[i].getValue("favorID").toString()
                                            val title_=getData[i].getValue("title").toString()
                                            val id=getData[i].getValue("id").toString()
                                            val createTime_=getData[i].getValue("createTime").toString()
                                            val link_=getData[i].getValue("link").toString()
                                            val readCount_=getData[i].getValue("readCount").toString()
                                            val zan_=getData[i].getValue("zan").toString()
                                            val isZan_=getData[i].getValue("isZan").toString()

                                            if(title_!=null&&id!=null&&createTime_!=null&&link_!=null&&readCount_!=null&&zan_!=null&&isZan_!=null){
                                                val listItem = ListSchoolFavoriteAll()
                                                listItem.favorID=favorID
                                                listItem.title = title_
                                                listItem.id =id
                                                listItem.createTime = createTime_
                                                listItem.link=link_
                                                listItem.readCount=readCount_
                                                listItem.zan_=zan_
                                                listItem.isZan=isZan_
                                                if(urlEnd.equals(Constant.School_List_FGUIdance)){
                                                    listItem.urlEnd=Constant.School_ListGuidance
                                                }else if(urlEnd.equals(Constant.School_List_FSummary)){
                                                    listItem.urlEnd=Constant.School_ListSummary
                                                }else if(urlEnd.equals(Constant.School_List_FClub)){
                                                    listItem.urlEnd=Constant.School_ListClub
                                                }else if(urlEnd.equals(Constant.School_List_FLook)){
                                                    listItem.urlEnd=Constant.School_ListLook
                                                }
                                                listAll.add(listItem)
                                            }else{
                                                Toasty.error(context, Constant.SERVERBROKEN).show()
                                            }
                                        }//=====所有的对象已经存放到一个List了
                                        adapter!!.notifyDataSetChanged()
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.loadMoreComplete(true)
                                        listener.loadSuccess(listAll.size.toString())
                                    } else if(pageIndexNow==0){
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                        if(urlEnd.equals(Constant.School_List_FGUIdance)){
                                            listener.loadSuccess("noSchool1Favorite")
                                        }else if(urlEnd.equals(Constant.School_List_FSummary)){
                                            listener.loadSuccess("noSchool2Favorite")
                                        }else if(urlEnd.equals(Constant.School_List_FClub)){
                                            listener.loadSuccess("noSchool3Favorite")
                                        }else if(urlEnd.equals(Constant.School_List_FLook)){
                                            listener.loadSuccess("noSchool4Favorite")
                                        }

                                    }else{
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                        listener.loadSuccess(listAll.size.toString())
                                    }
                                } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val account=read.getString("account","")
                                    val password=read.getString("password","")
                                    val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.SERVERBROKEN, Context.MODE_APPEND).edit()
                                    editor2.putString("account",account)
                                    editor2.putString("password",password)
                                    editor2.commit()
                                    editor.clear()
                                    editor.commit()
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(context, LoginMain::class.java)
                                                context.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }else{
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    Toasty.error(context, Constant.SERVERBROKEN).show()
                                }
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                        ptrClassicFrameLayout!!.refreshComplete()
                        Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                if(!activity.isFinishing){
                    Toasty.info(context,Constant.NONETWORK).show()
                }

            }

        }

        fun deleteFavoriteSchool(context: Context,urlEnd: String,IDs:ArrayList<String>,token:String,listener: IDataRequestListener2String){
            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""

                if(LogUtils.APP_IS_DEBUG){
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                    urlPath=Constant.BASEURLFORTEST+urlEnd
                }else{
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                    urlPath=Constant.BASEURLFORRELEASE+urlEnd
                }

                val jsonObject=JSONObject()
                val jsonArray= JSONArray()
                for(i in IDs.indices){
                    jsonArray.put(i,IDs[i])
                }
                jsonObject.put("IDs",jsonArray)
                jsonObject.put("sign",sign)

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Me_Favorite_TAG,Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client=AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if(statusCode.toString().equals(Constant.RIGHTCODE)){
                                val response_string=response.toString()
                                //开始解析
                                LogUtils.d_debugprint(Constant.Me_TAG,"服务器返回删除收藏帖子是："+response_string)
                                listener.loadSuccess(response_string)
                            }else{
                                Toasty.info(context,Constant.NONETWORK).show()
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                            Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

    }
}
