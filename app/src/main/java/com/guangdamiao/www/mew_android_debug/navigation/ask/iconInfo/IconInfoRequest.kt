package com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo

import android.content.Context
import android.os.Handler
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.*
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/8.
 */

class IconInfoRequest{
    companion object {
        fun requestForInfo(context: Context,id:String,listener: IDataRequestListener2String){
            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token = read.getString("token", "")
                if(LogUtils.APP_IS_DEBUG){
                    sign=MD5Util.md5(Constant.SALTFORTEST+id)
                    urlPath=Constant.BASEURLFORTEST+Constant.URL_Data_Person
                }else{
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+id)
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Data_Person
                }

                val jsonObject=JSONObject()
                jsonObject.put("id",id)
                jsonObject.put("token",token)
                jsonObject.put("sign",sign)

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.ASK_WRITE_TAG,Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client=AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if(statusCode.toString().equals(Constant.RIGHTCODE)){
                                val response_string=response.toString()
                                //开始解析
                                //LogUtils.d_debugprint(Constant.Icon_Info_TAG,"服务器返回的他人资料页是："+response_string)
                                LogUtils.d_debugprint(Constant.Icon_Info_TAG, Constant.GETDATAFROMSERVER + JSONFormatUtil.formatJson(response_string)+"\n\n")

                                listener.loadSuccess(response_string)
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun requestAddZan(context:Context,id: String, token: String){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.URL_Zan_Users
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Zan_Users
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("id",id)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Icon_Info_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                       if(responseBody!=null){
                           val resultDate = String(responseBody!!, charset("utf-8"))
                           val response=JSONObject(resultDate)
                           if(statusCode.equals(Constant.RIGHTCODE)){
                               if(JsonUtil.get_key_string("code",response.toString()).equals(Constant.RIGHTCODE))
                                   LogUtils.d_debugprint(Constant.Icon_Info_TAG,"个人信息中：点赞成功！！！\n")
                           }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                //Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun requestAddFriend(context:Context,id: String, token: String,loading_view: Loading_view){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.URL_Add_Friend
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Add_Friend
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("friendID",id)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Icon_Info_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if(statusCode.toString().equals(Constant.RIGHTCODE)){
                                if(loading_view!=null) loading_view!!.cancel()
                                val response_string=response.toString()
                                if(JsonUtil.get_key_string("code",response_string).equals("200")){
                                    LogUtils.d_debugprint(Constant.Icon_Info_TAG,"个人信息中：添加好友成功！！！\n")
                                    val pDialog=SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                    pDialog.setTitleText("提示")
                                    pDialog.setContentText("亲，添加好友成功")
                                    pDialog.show()
                                    val handler= Handler()
                                    handler.postDelayed({
                                        pDialog.cancel()
                                    },1236)
                                }else{
                                    val response_string=response.toString()
                                    if(JsonUtil.get_key_string("msg",response_string)!=null){
                                        val pDialog=SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        pDialog.setCustomImage(R.mipmap.mew_icon)
                                        pDialog.setTitleText("提示")
                                        pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                                        pDialog.show()
                                        val handler= Handler()
                                        handler.postDelayed({
                                            pDialog.cancel()
                                        },1236)
                                    }
                                }
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                         if(loading_view!=null) loading_view!!.cancel()
                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                if(loading_view!=null) loading_view!!.cancel()
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun requestGiveCoins(context:Context,receiverID: String,amount:String,words:String,token: String,listener: IDataRequestListener2String){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.URL_Give_Coins
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Give_Coins
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("receiverID",receiverID)
                jsonObject.put("amount",amount)
                jsonObject.put("words",words)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Icon_Info_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                listener.loadSuccess(response.toString())
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun requestWantCoins(context:Context,giverID: String,amount:String,words:String,token: String,listener: IDataRequestListener2String){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.URL_Want_Coins
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Want_Coins
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("giverID",giverID)
                jsonObject.put("amount",amount)
                jsonObject.put("words",words)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Icon_Info_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                listener.loadSuccess(response.toString())
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun requestLeaveWords(context:Context,receiverID: String,words:String,token: String,listener: IDataRequestListener2String){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.URL_Leave_Words
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Leave_Words
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("receiverID", receiverID)
                jsonObject.put("words",words)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Icon_Info_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                listener.loadSuccess(response.toString())
                            }else{
                                LogUtils.d_debugprint(Constant.Icon_Info_TAG,"网络请求出错："+response.toString())
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun requestInformUsers(context:Context,ID: String,reason:String,token: String,listener: IDataRequestListener2String){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.URL_Inform_Users
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Inform_Users
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("id", ID)
                jsonObject.put("reason",reason)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Icon_Info_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                listener.loadSuccess(response.toString())
                            }else{
                                LogUtils.d_debugprint(Constant.Icon_Info_TAG,"网络请求出错："+response.toString())
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                        Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

        fun requestBlackUsers(context:Context,userID: String,token: String,listener: IDataRequestListener2String){

            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.URL_Blacklist
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Blacklist
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("userID",userID)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Icon_Info_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                listener.loadSuccess(response.toString())
                            }else{
                                LogUtils.d_debugprint(Constant.Icon_Info_TAG,"网络请求出错："+response.toString())
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                        Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

    }
}
