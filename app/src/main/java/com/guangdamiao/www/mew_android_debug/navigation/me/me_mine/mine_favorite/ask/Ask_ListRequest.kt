package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.ask

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.widget.BaseAdapter
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.favorite_bean.MyFavoriteAsk
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.JSONFormatUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.JsonHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/20.
 */

class Ask_ListRequest {

    companion object {//帖子的收藏还没写，所以这里全部都要改

        fun getMyFavoriteFromServer(activity: Activity, context: Context, TAG: String, urlEnd: String, pageIndexNow: Int, listAskAll: ArrayList<MyFavoriteAsk>, adapter: BaseAdapter, ptrClassicFrameLayout: PtrClassicFrameLayout, loading_first:SweetAlertDialog?,listener:IDataRequestListener2String) {
            if (NetUtil.checkNetWork(context)) {
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject= JSONObject()
                jsonObject.put("pageIndex",pageIndex)
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("sign",sign)

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity, Constant.ContentType+ Constant.Chartset,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (loading_first != null) loading_first.cancel()
                            if(!activity.isFinishing){
                                val getResultFromServer=response.toString()
                                LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + JSONFormatUtil.formatJson(getResultFromServer))
                                var getCode: String=""
                                var msg: String? = null
                                var result: String
                                var getData: List<Map<String, Any?>>

                                if(JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)!=null){
                                    getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                                }
                                if (Constant.RIGHTCODE.equals(getCode)) {
                                    msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                    result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                    var askAll:ArrayList<MyFavoriteAsk>?=null
                                    askAll= JsonUtil.get_ask_favorite("data", result)
                                    LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + askAll.toString()+"\n")
                                    if(pageIndexNow==0){
                                        listAskAll.clear()
                                    }
                                    if (askAll != null&&askAll.size>0) {

                                        for(item in askAll){
                                            listAskAll.add(item)
                                        }
                                        listener.loadSuccess(listAskAll.size.toString())
                                        adapter!!.notifyDataSetChanged()
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    } else if(pageIndexNow==0){
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                        listener.loadSuccess("noAskFavorite")
                                    }else{
                                        listener.loadSuccess(listAskAll.size.toString())
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                        //Toasty.info(context, "没有更多数据了").show()
                                    }
                                } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val account=read.getString("account","")
                                    val password=read.getString("password","")
                                    val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.SERVERBROKEN, Context.MODE_APPEND).edit()
                                    editor2.putString("account",account)
                                    editor2.putString("password",password)
                                    editor2.commit()
                                    editor.clear()
                                    editor.commit()
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(context, LoginMain::class.java)
                                                context.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }else{
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    Toasty.info(context, Constant.SERVERBROKEN).show()
                                }
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                        ptrClassicFrameLayout!!.refreshComplete()
                        Toasty.error(context,Constant.REQUESTFAIL).show()
                    }
                })
            } else {
                if(!activity.isFinishing){
                    if(loading_first!=null) loading_first.cancel()
                    Toasty.info(context, Constant.NONETWORK).show()
                }
            }
        }

        fun deleteFavoriteAsk(context: Context,IDs:ArrayList<String>,token:String,listener: IDataRequestListener2String){
            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""

                if(LogUtils.APP_IS_DEBUG){
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                    urlPath=Constant.BASEURLFORTEST+Constant.URL_Delete_Favorite_Ask
                }else{
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                    urlPath=Constant.BASEURLFORRELEASE+Constant.URL_Delete_Favorite_Ask
                }

                val jsonObject=JSONObject()
                val jsonArray= JSONArray()
                for(i in IDs.indices){
                    jsonArray.put(i,IDs[i])
                }
                jsonObject.put("IDs",jsonArray)
                jsonObject.put("sign",sign)

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Me_Favorite_TAG,Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client=AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object:JsonHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                        //这里进行自己的业务逻辑
                        if(statusCode.toString().equals(Constant.RIGHTCODE)){
                            val response_string=response.toString()
                            //开始解析
                            LogUtils.d_debugprint(Constant.Me_TAG,"服务器返回删除收藏帖子是："+response_string)
                            listener.loadSuccess(response_string)
                        }else{
                            Toasty.info(context,Constant.NONETWORK).show()
                        }

                        super.onSuccess(statusCode, headers, response)
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONArray?) {
                        super.onFailure(statusCode, headers, throwable, errorResponse)
                        Toasty.error(context,Constant.NONETWORK).show()
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                        super.onFailure(statusCode, headers, throwable, errorResponse)
                        Toasty.error(context,Constant.NONETWORK).show()
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
                        super.onFailure(statusCode, headers, responseString, throwable)
                        Toasty.error(context,Constant.NONETWORK).show()
                    }
                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }


    }
}
