package com.guangdamiao.www.mew_android_debug.bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class MyFriends {

    var icon: String? =""
    var id:String?=""
    var nickname:String?=""
    var createTime:String?=""

    var isShow:Boolean?=false
    var isChecked:Boolean?=false

    constructor():super()
    constructor(icon:String,id:String,nickname:String){
        this.icon=icon
        this.id=id
        this.nickname=nickname
    }

    constructor(icon:String,id:String,nickname:String,createTime:String,isShow:Boolean,isChecked:Boolean){
        this.icon=icon
        this.id=id
        this.nickname=nickname
        this.createTime=createTime
        this.isShow=isShow
        this.isChecked=isChecked
    }

    override fun toString(): String {
        return "MyFriends[icon=$icon,id=$id，nickname=$nickname,createTime=$createTime]"
    }
}
