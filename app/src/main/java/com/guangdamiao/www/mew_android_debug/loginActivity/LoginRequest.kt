package com.guangdamiao.www.mew_android_debug.loginActivity

import android.content.Context
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/6.
 */

class LoginRequest{
    companion object {
        fun isQQRegister(context: Context,uuid:String,listener:IDataRequestListener2String) {

            if (NetUtil.checkNetWork(context)) {
                var sign = ""
                var urlPath = ""
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + Constant.URL_IsQQRegister
                    sign = MD5Util.md5(Constant.SALTFORTEST + uuid)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + Constant.URL_IsQQRegister
                    sign = MD5Util.md5(Constant.SALTFORRELEASE + uuid)
                }

                val jsonObject = JSONObject()
                jsonObject.put("uuid",uuid)
                jsonObject.put("sign", sign)
                val stringEntity = StringEntity(jsonObject.toString(), "UTF-8")
                LogUtils.d_debugprint(Constant.LoginMain_Tag, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())

                val client = AsyncHttpClient(true, 80, 443)
                client.post(context, urlPath, stringEntity, Constant.ContentType, object : AsyncHttpResponseHandler() {

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response= JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                val response_string = response.toString()
                                if (JsonUtil.get_key_string("code", response_string).equals(Constant.RIGHTCODE)) {
                                    val result=JsonUtil.get_key_string("result", response_string)
                                    val isRegister=JsonUtil.get_key_boolean("isRegister",result)
                                    listener.loadSuccess(isRegister.toString())
                                }
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                          Toasty.error(context,"网络超时，请稍候重试！").show()
                    }

                })
            } else {
                Toasty.info(context, Constant.NONETWORK).show()
            }
        }

        fun loginQQ(context: Context,uuid:String,nickname:String,gender:String,college:String,listener:IDataRequestListener2String) {

            if (NetUtil.checkNetWork(context)) {
                var sign = ""
                var urlPath = ""
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + Constant.URL_Login_QQ
                    sign = MD5Util.md5(Constant.SALTFORTEST + uuid)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + Constant.URL_Login_QQ
                    sign = MD5Util.md5(Constant.SALTFORRELEASE + uuid)
                }

                val jsonObject = JSONObject()
                jsonObject.put("uuid",uuid)
                jsonObject.put("nickname",nickname)
                jsonObject.put("college",college)
                jsonObject.put("gender",gender)
                jsonObject.put("school",Constant.SCHOOLDEFULT)
                jsonObject.put("sign", sign)
                val stringEntity = StringEntity(jsonObject.toString(), "UTF-8")
                LogUtils.d_debugprint(Constant.LoginMain_Tag, Constant.TOSERVER + urlPath + "\n提交的参数为：" + jsonObject.toString())

                val client = AsyncHttpClient(true, 80, 443)
                client.post(context, urlPath, stringEntity, Constant.ContentType, object : AsyncHttpResponseHandler() {

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))

                            val response= JSONObject(resultDate)
                            if (statusCode.toString().equals(Constant.RIGHTCODE)) {
                                val response_string = response.toString()
                                if (JsonUtil.get_key_string("code", response_string).equals(Constant.RIGHTCODE)) {
                                    val result=JsonUtil.get_key_string("result",response_string)
                                    listener.loadSuccess(result)
                                }
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                        Toasty.error(context,"网络超时，请稍候重试！").show()
                    }

                })
            } else {
                Toasty.info(context, Constant.NONETWORK).show()
            }
        }

    }
}
