package com.guangdamiao.www.mew_android_debug.navigation.find.findcache

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2037/8/30.
 */

class Find3SQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        private val name = "find3.db"
        private val version = 1
         val F3_Name="find3"
         val F3_Id="id"
         val F3_Sid="s_id"
         val F3_Icon="icon"
         val F3_Title="title"
         val F3_CreateTime="createTime"
         val F3_Position="position"
         val F3_Fee="fee"
         val F3_Description="description"
         val F3_Link="link"
         val F3_ReadCount="readCount"
         val F3_Zan="zan"
         val F3_IsZan="isZan"
         val F3_IsFavorite="isFavorite"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table find3(id integer primary key autoincrement," +
                "${Find3SQLiteOpenHelper.F3_Sid} varchar(20)," +
                "${Find3SQLiteOpenHelper.F3_Icon} varchar(100)," +
                "${Find3SQLiteOpenHelper.F3_Title} varchar(20),"+
                "${Find3SQLiteOpenHelper.F3_CreateTime} varchar(50)," +
                "${Find3SQLiteOpenHelper.F3_Position} varchar(20)," +
                "${Find3SQLiteOpenHelper.F3_Fee} varchar(20)," +
                "${Find3SQLiteOpenHelper.F3_Description} varchar(382)," +
                "${Find3SQLiteOpenHelper.F3_Link} varchar(100)," +
                "${Find3SQLiteOpenHelper.F3_ReadCount} varchar(5)," +
                "${Find3SQLiteOpenHelper.F3_Zan} varchar(5)," +
                "${Find3SQLiteOpenHelper.F3_IsZan} varchar(2)," +
                "${Find3SQLiteOpenHelper.F3_IsFavorite} varchar(2))")

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    //打开数据库时调用
    override fun onOpen(db: SQLiteDatabase) {
        //如果数据库已经存在，则再次运行不执行onCreate()方法，而是执行onOpen()打开数据库
        super.onOpen(db)
    }

}
