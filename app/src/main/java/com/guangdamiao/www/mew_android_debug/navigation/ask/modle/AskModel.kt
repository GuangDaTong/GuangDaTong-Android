package com.guangdamiao.www.mew_android_debug.navigation.ask.modle

import android.content.Context
import android.os.AsyncTask
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.CommentConfig
import com.guangdamiao.www.mew_android_debug.bean.ask_bean.CommentItem
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.SyncHttpClient
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONObject


/**

 * @ClassName: AskModel
 * *
 * @Description: 帖子model处理与服务器交互的请求
 * *
 * @author JasonJan
 * *
 * @date 2017-8-1
 */

class AskModel {

    fun deleteAsk(context:Context,token:String,askIDs:ArrayList<String>,listener: IDataRequestListener) {
        requestDeleteAskServer(context,token,askIDs,listener)
    }

    fun addZan( id: String, token: String,context: Context) {
        addZan_requestServer( id, token,context)
    }

    fun addComment(context: Context,comment:CommentConfig,listener: IDataRequestListener) {
        requestAddCommentServer(context,comment,listener)
    }

    fun deleteComment(context:Context,comment:CommentItem,listener: IDataRequestListener) {
        requestDeleteCommentServer(context,comment,listener)
    }

    fun adoptComment(context:Context,comment:CommentItem,listener: IDataRequestListener) {
        requestAdoptCommentServer(context,comment,listener)
    }

    /**

     * @Title: requestServer
     * *
     * @Description: 与后台交互
     * *
     * @param  listener    设定文件
     * *
     * @return void    返回类型
     * *
     * @throws
     */
    private fun requestServer(listener: IDataRequestListener) {
        object : AsyncTask<Any, Int, Boolean>() {
            override fun doInBackground(vararg params: Any): Boolean? {
                var flag=false

                if(flag){
                    return true
                }

                return flag
            }

            override fun onPostExecute(result: Boolean) {
                if (result!!) {
                    //服务器执行成功
                    listener.loadSuccess(result)
                    LogUtils.d_debugprint(Constant.Ask_TAG,"服务器执行成功！！！\n\n\n")
                    //这里就不提示用户了
                }
            }

        }.execute()
    }

    private fun requestDeleteAskServer(context:Context,token:String, askIDs: ArrayList<String>, listener: IDataRequestListener) {
        object : AsyncTask<Any, Int, Boolean>() {
            override fun doInBackground(vararg params: Any): Boolean? {
                var flag=false
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath=Constant.BASEURLFORTEST+Constant.Ask_Delete
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.Ask_Delete
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                var jsonArray= JSONArray()
                for(i in askIDs.indices){
                    jsonArray.put(i,askIDs[i])
                }
                val jsonObject=JSONObject()
                jsonObject.put("IDs",jsonArray)
                jsonObject.put("sign",sign)

                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Ask_TAG,Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client= SyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (JsonUtil.get_key_string("code",response.toString()).equals(Constant.RIGHTCODE)){
                                flag=true
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                    }

                })

                if(flag){
                    return true
                }

                return flag
            }

            override fun onPostExecute(result: Boolean) {
                if (result!!) {
                    //服务器执行成功
                    listener.loadSuccess(result)
                    LogUtils.d_debugprint(Constant.Ask_TAG,"服务器执行成功！！！\n\n\n")
                    //这里就不提示用户了
                }
            }

        }.execute()
    }

    private fun requestDeleteCommentServer(context: Context,comment: CommentItem,listener: IDataRequestListener) {
        object : AsyncTask<Any, Int, Boolean>() {
            override fun doInBackground(vararg params: Any): Boolean? {
                var flag=false

                var sign=""
                var urlPath=""

                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")

                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.Delete_Comment
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.Delete_Comment
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("id",comment.id)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Ask_TAG,Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client= SyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (JsonUtil.get_key_string("code",response.toString()).equals(Constant.RIGHTCODE)){
                                flag=true
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }


                })

                if(flag){
                    return true
                }
                return flag
            }

            override fun onPostExecute(result: Boolean) {
                if (result!!) {
                    //服务器执行成功
                    listener.loadSuccess(result)
                    LogUtils.d_debugprint(Constant.Ask_TAG,"服务器删除评论成功！！！\n\n\n")
                    //这里就不提示用户了
                }else{
                    LogUtils.d_debugprint(Constant.Ask_TAG,"服务器删除评论失败！！！\n\n\n")
                }
            }

        }.execute()
    }

    //添加评论，请求服务器
    private fun requestAddCommentServer(context: Context,comment: CommentConfig,listener: IDataRequestListener) {
        object : AsyncTask<Any, Int, Boolean>() {
            override fun doInBackground(vararg params: Any): Boolean {
                var flag=false
                var sign=""
                var urlPath=""

                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.Comment_Ask
                    sign=MD5Util.md5(Constant.SALTFORTEST+comment.token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.Comment_Ask
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+comment.token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("id",comment.askID)
                jsonObject.put("content",comment.content)
                jsonObject.put("commentReceiverID",comment.commentReceiverID)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Ask_TAG,Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client= SyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (JsonUtil.get_key_string("code",response.toString()).equals(Constant.RIGHTCODE)){
                                flag=true
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })

                if(flag){
                    return true
                }
                return false
            }

            override fun onPostExecute(result: Boolean) {
                if(result!!)
                LogUtils.d_debugprint(Constant.Ask_TAG,"服务器成功评论！！！\n\n\n")
                listener.loadSuccess(result)
            }

        }.execute()
    }

    //点赞请求服务器
    private fun addZan_requestServer( id: String, token: String,context:Context) {

        object : AsyncTask<Any, Void, Boolean>() {
            override fun doInBackground(vararg params: Any): Boolean? {
                //和后台交互
                var flag=false
                var sign=""
                var urlPath=""
                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.Zan_Ask
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.Zan_Ask
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("id",id)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Ask_TAG,Constant.TOSERVER+urlPath+"\n提交的参数为："+jsonObject.toString())

                val client= SyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (JsonUtil.get_key_string("code",response.toString()).equals(Constant.RIGHTCODE)){
                                flag=true
                            }
                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                  })

                if(flag){
                    return true
                }
                return false
            }

            override fun onPostExecute(result: Boolean?) {
                if (result!!) {
                    //服务器成功点赞
                    LogUtils.d_debugprint(Constant.Ask_TAG,"服务器成功点赞！！！\n\n\n")
                    //这里就不提示用户了
                }
            }

        }.execute()

    }

    private fun requestAdoptCommentServer(context: Context,comment: CommentItem,listener: IDataRequestListener) {
        object : AsyncTask<Any, Int, Boolean>() {
            override fun doInBackground(vararg params: Any): Boolean? {
                var flag=false

                var sign=""
                var urlPath=""

                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")

                if(LogUtils.APP_IS_DEBUG){
                    urlPath= Constant.BASEURLFORTEST+Constant.Adopt_Comment
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                }else{
                    urlPath=Constant.BASEURLFORRELEASE+Constant.Adopt_Comment
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                }

                val jsonObject=JSONObject()
                jsonObject.put("askID",comment.askID)
                jsonObject.put("commentID",comment.id)
                jsonObject.put("sign",sign)
                val stringEntity=StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Ask_TAG,Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client= SyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (JsonUtil.get_key_string("code",response.toString()).equals(Constant.RIGHTCODE)){
                                flag=true
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })

                if(flag){
                    return true
                }
                return flag
            }

            override fun onPostExecute(result: Boolean) {
                if (result!!) {
                    //服务器执行成功
                    listener.loadSuccess(result)
                    LogUtils.d_debugprint(Constant.Ask_TAG,"服务器采纳评论成功！！！\n\n\n")
                    //这里就不提示用户了
                }
            }

        }.execute()
    }


}
