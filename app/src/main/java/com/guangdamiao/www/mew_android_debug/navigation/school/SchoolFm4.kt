package com.guangdamiao.www.mew_android_debug.navigation.school


import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.chanven.lib.cptr.loadmore.OnLoadMoreListener
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListSchoolAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.school.schoolcache.School4SQLiteOpenHelper

/**
 * A simple [Fragment] subclass.
 */
class SchoolFm4 : Fragment() {

    private var name: String? = null
    private val listItems = ArrayList<ListSchoolAll>()
    private var mlistView: ListView? = null
    private var adapter: MyListViewAdpter? = null
    private val TAG = Constant.School_TAG
    private var pageNow = 0
    private var ptrClassicFrameLayout: PtrClassicFrameLayout? = null
    var handler = Handler()
    var flag=0

    private var helper4: School4SQLiteOpenHelper?=null

    override fun setArguments(args: Bundle) {
        name = args.getString("name")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view_ = inflater!!.inflate(R.layout.fragment_school_all, container, false)
        helper4= School4SQLiteOpenHelper(context)
        mlistView = view_!!.findViewById(R.id.list_find_new) as ListView
        ptrClassicFrameLayout = view_!!.findViewById(R.id.list_find_frame) as PtrClassicFrameLayout


        adapter = MyListViewAdpter(context, listItems)
        mlistView!!.setAdapter(adapter!!)//关键代码

        initData()

        return view_
    }

    override fun onStart() {

        if(flag==0){
            flag++
            queryNowAll(School4SQLiteOpenHelper.S4_Name,listItems)
            adapter!!.notifyDataSetChanged()
            ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(false) }, 150)
        }

        val read = context.getSharedPreferences("update_school4", Context.MODE_PRIVATE)
        val update_position = read.getInt("position", 0)
        val update_id=read.getString("id","")
        val update_isZan=read.getString("isZan","")
        val update_readCount=read.getString("readCount","")
        val update_zan=read.getString("zan","")
        if(listItems.size>0&&listItems.size-1>=update_position){
            if(listItems[update_position]!=null&&listItems[update_position].id!!.equals(update_id)){
                listItems[update_position].zan_=update_zan
                listItems[update_position].isZan=update_isZan
                listItems[update_position].readCount=update_readCount

                adapter!!.notifyDataSetChanged()
            }
        }

        super.onStart()
    }

    private fun initData() {

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    pageNow = 0
                    RequestServerList(pageNow,adapter!!,ptrClassicFrameLayout!!)
                    adapter!!.notifyDataSetChanged()
                     if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                         ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                     }
                }, 200)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener(OnLoadMoreListener {
            handler.postDelayed({
                //请求更多页
                RequestServerList(++pageNow,adapter!!,ptrClassicFrameLayout!!)
                //adapter!!.notifyDataSetChanged()
                ptrClassicFrameLayout!!.setLoadMoreEnable(true);

            }, 200)
        })
    }

    private fun RequestServerList(pageNow: Int,adapter:MyListViewAdpter,ptrClassicFrameLayout: PtrClassicFrameLayout) {
        SameListRequest.getResultFromServer2(context, TAG, Constant.School_ListLook, pageNow, listItems,adapter,ptrClassicFrameLayout)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser){

            JAnalyticsInterface.onPageStart(context,"看看")
        }else{
            JAnalyticsInterface.onPageEnd(context,"看看")
        }
    }

    //数据库查询
    private fun queryNowAll(table:String,listItems:ArrayList<ListSchoolAll>){


        val sql="select * from "+ School4SQLiteOpenHelper.S4_Name+" order by id asc"
        listItems.clear()
        val cursor= helper4!!.readableDatabase.rawQuery(sql,null)

        while(cursor!!.moveToNext()){
            val school= ListSchoolAll()
            school.id=cursor.getString(cursor.getColumnIndex(School4SQLiteOpenHelper.S4_Sid))
            school.title=cursor.getString(cursor.getColumnIndex(School4SQLiteOpenHelper.S4_Title))
            school.createTime=cursor.getString(cursor.getColumnIndex(School4SQLiteOpenHelper.S4_CreateTime))
            school.type=cursor.getString(cursor.getColumnIndex(School4SQLiteOpenHelper.S4_Type))
            school.link=cursor.getString(cursor.getColumnIndex(School4SQLiteOpenHelper.S4_Link))
            school.readCount=cursor.getString(cursor.getColumnIndex(School4SQLiteOpenHelper.S4_ReadCount))
            school.zan_=cursor.getString(cursor.getColumnIndex(School4SQLiteOpenHelper.S4_Zan))
            school.isZan=cursor.getString(cursor.getColumnIndex(School4SQLiteOpenHelper.S4_IsZan))
            school.urlEnd=cursor.getString(cursor.getColumnIndex(School4SQLiteOpenHelper.S4_UrlEnd))
            school.isFavorite=cursor.getString(cursor.getColumnIndex(School4SQLiteOpenHelper.S4_IsFavorite))

            listItems.add(school)
        }
        LogUtils.d_debugprint(Constant.School_TAG,"本地数据库查找到的$table ：\n\n"+listItems.toString())
    }
}
