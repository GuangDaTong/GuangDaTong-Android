package com.guangdamiao.www.mew_android_debug.bean.ask_bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class Comment {

    var id: String? = null
    var publisherID:String?=null
    var createTime:String?=null
    var icon:String?=null
    var nickname:String?=null
    var content:String?=null
    var gender:String?=null

    constructor():super()
    constructor(id:String,publisherID:String,createTime:String,icon:String,nickname:String,content:String) : super() {
        this.id=id
        this.publisherID=publisherID
        this.createTime=createTime
        this.icon=icon
        this.nickname=nickname
        this.content=content
    }

    constructor(id:String,publisherID:String,createTime:String,icon:String,nickname:String,content:String,gender:String) : super() {
        this.id=id
        this.publisherID=publisherID
        this.createTime=createTime
        this.icon=icon
        this.nickname=nickname
        this.content=content
        this.gender=gender
    }

    override fun toString(): String {
        return "Comment[id=$id,publisherID=$publisherID,createTime=$createTime,icon=$icon,nickname=$nickname,content=$content]"
    }
}
