package com.guangdamiao.www.mew_android_debug.navigation.ask.askMain

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Jason_Jan on 2017/12/10.
 */

class Ask1SQLiteOpenHelper(context: Context) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        private val name = "ask1.db"
        private val version = 1
         val A_Name="ask1"
         val A_Id="id"
         val A_Sid="s_id"
         val A_CreateTime="createTime"
         val A_PublisherID="publisherID"
         val A_NickName="nickname"
         val A_Icon="icon"
         val A_Gender="gender"
         val A_Reward="reward"
         val A_Zan="zan"
         val A_CommentID="commentID"
         val A_Type="type"
         val A_Label="label"
         val A_Title="title"
         val A_Content="content"
         val A_Link="link"
         val A_Image="image"
         val A_ThumbnailImage="thumbnailImage"
         val A_CommentCount="commentCount"
         val A_IsZan="isZan"
         val A_IsFavorite="isFavorite"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table ask1(id integer primary key autoincrement," +
                "${Ask1SQLiteOpenHelper.A_Sid} varchar(20)," +
                "${Ask1SQLiteOpenHelper.A_CreateTime} varchar(50)," +
                "${Ask1SQLiteOpenHelper.A_PublisherID} varchar(20),"+
                "${Ask1SQLiteOpenHelper.A_NickName} varchar(20)," +
                "${Ask1SQLiteOpenHelper.A_Icon} varchar(50)," +
                "${Ask1SQLiteOpenHelper.A_Gender} varchar(5)," +
                "${Ask1SQLiteOpenHelper.A_Reward} varchar(10)," +
                "${Ask1SQLiteOpenHelper.A_Zan} varchar(2)," +
                "${Ask1SQLiteOpenHelper.A_CommentID} varchar(20)," +
                "${Ask1SQLiteOpenHelper.A_Type} varchar(10)," +
                "${Ask1SQLiteOpenHelper.A_Label} varchar(10)," +
                "${Ask1SQLiteOpenHelper.A_Title} varchar(20)," +
                "${Ask1SQLiteOpenHelper.A_Content} varchar(4096)," +
                "${Ask1SQLiteOpenHelper.A_Link} varchar(100)," +
                "${Ask1SQLiteOpenHelper.A_Image} varchar(900)," +
                "${Ask1SQLiteOpenHelper.A_ThumbnailImage} varchar(900)," +
                "${Ask1SQLiteOpenHelper.A_CommentCount} varchar(10)," +
                "${Ask1SQLiteOpenHelper.A_IsZan} varchar(4)," +
                "${Ask1SQLiteOpenHelper.A_IsFavorite} varchar(2))")

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }

    //打开数据库时调用
    override fun onOpen(db: SQLiteDatabase) {
        //如果数据库已经存在，则再次运行不执行onCreate()方法，而是执行onOpen()打开数据库
        super.onOpen(db)
    }

}
