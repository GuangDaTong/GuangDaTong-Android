package com.guangdamiao.www.mew_android_debug.bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class ListFindAll {

    var id: String? = null
    var icon:String?=null
    var title:String?=null
    var createTime:String?=null
    var position:String?=null
    var fee:String?=null
    var description:String?=null
    var link:String?=null
    var readCount:String?=""
    var zan_:String?=""
    var isZan:String?=""
    var isFavorite:String?=""

    constructor():super()
    constructor(id:String,icon:String,title:String,createTime:String,position:String,fee:String,description:String,link:String,
                readCount:String,zan_:String,isZan:String,isFavorite:String):super(){
        this.id=id
        this.icon=icon
        this.title=title
        this.createTime=createTime
        this.position=position
        this.fee=fee
        this.description=description
        this.link=link
        this.readCount=readCount
        this.zan_=zan_
        this.isZan=isZan
        this.isFavorite=isFavorite
    }

    override fun toString(): String {
        return "MyListLookAll[id=$id,icon=$icon，title=$title,createTime=$createTime,position=$position,fee=$fee,description=$description,link=$link,readCount=$readCount,zan_=$zan_,isZan=$isZan,isFavorite=$isFavorite]"
    }
}
