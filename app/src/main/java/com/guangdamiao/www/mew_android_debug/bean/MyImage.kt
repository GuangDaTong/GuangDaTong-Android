package com.guangdamiao.www.mew_android_debug.bean

/**
 * Created by Jason_Jan on 2017/12/5.
 */

class MyImage {

    var link: String? = null
    var image:String?=null
    var title:String?=null

    constructor():super()
    constructor(image: String) : super() {
        this.image = image
    }
    constructor(image:String,link:String,title:String){
        this.image=image
        this.link=link
        this.title=title
    }

    override fun toString(): String {
        return "MyImage[link=$link,image=$image，title=$title]"
    }
}
