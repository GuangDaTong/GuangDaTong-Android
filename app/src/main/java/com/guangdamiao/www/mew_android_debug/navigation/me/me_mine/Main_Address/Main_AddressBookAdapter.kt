package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Address

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MyFriends
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconInfo
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil
import com.nostra13.universalimageloader.core.ImageLoader
import java.util.*

class Main_AddressBookAdapter(context:Context,myFriends: ArrayList<MyFriends>) : BaseAdapter(){
    private val mInflater: LayoutInflater//从别人得到 自己用
    private var mContext: Context? = null//从别人得到 自己用
    private var myFriends:ArrayList<MyFriends>?=null
    private var onShowItemClickListener:OnShowItemClickListener?=null

    init {
        this.mContext = context
        this.myFriends=myFriends
        this.mInflater = LayoutInflater.from(mContext)
    }
    override fun getItem(arg0: Int): Any {

        return myFriends!![arg0]
    }
    override fun getItemId(position: Int): Long {

        return position.toLong()
    }
    override fun getCount(): Int {

        return myFriends!!.size
    }
    override fun getView(position: Int, convertView: View?, parent: android.view.ViewGroup): View {
        var convertView = convertView
        var holder:ViewHolder


        if (convertView == null) {
            holder= ViewHolder()
            convertView = mInflater.inflate(R.layout.mine_item_friends, null)
            holder.indexText = convertView!!.findViewById(R.id.mine_item_friend) as TextView
            holder.mine_friend_icon=convertView!!.findViewById(R.id.mine_friend_icon) as ImageView
            holder.mine_friend_next=convertView!!.findViewById(R.id.mine_friend_next) as ImageView
            holder.checkBox=convertView!!.findViewById(R.id.mine_item_choose_cb) as CheckBox
            holder.mine_addressBook_rl=convertView!!.findViewById(R.id.mine_addressBook_rl) as RelativeLayout
            convertView.tag=holder
        }else{
            holder=convertView.tag as ViewHolder
        }

        val bean=myFriends!!.get(position)
        if(bean.isShow!!){
            holder.checkBox!!.visibility=View.VISIBLE
            holder.mine_friend_next!!.setImageResource(R.mipmap.next)
            holder.mine_friend_next!!.visibility=View.GONE
        }else{
            holder.checkBox!!.visibility=View.GONE
            holder.mine_friend_next!!.setImageResource(R.mipmap.next)
            holder.mine_friend_next!!.visibility=View.VISIBLE
        }

        holder.indexText!!.text = myFriends!![position].nickname
        holder.indexText!!.setTextColor(mContext!!.resources.getColor(R.color.black))
        getMyIcon(myFriends!![position].icon!!,holder.mine_friend_icon!!)
        
        
        //昵称
        holder!!.indexText!!.text=myFriends!![position]!!.nickname
        var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
        holder!!.indexText!!.measure(spec,spec)
        var nickwidth= holder!!.indexText!!.measuredWidth
        //LogUtils.d_debugprint(Constant.Message_TAG,"\n\n现在nickname为：${myFriends!![position]!!.nickname} \n\n 长度为：${nickwidth} \n\n转换后的167.0f为：${DensityUtil.dip2px(context,187.0f)} ")
        var nicklength=myFriends!![position]!!.nickname!!.length
        while(nickwidth> DensityUtil.dip2px(mContext,250.0f)){
            nicklength-=2
            val name_display=myFriends!![position]!!.nickname!!.substring(0,nicklength)+"..."
            holder!!.indexText!!.text=name_display
            var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
            holder!!.indexText!!.measure(spec,spec)
            nickwidth= holder!!.indexText!!.measuredWidth
            // LogUtils.d_debugprint(Constant.Message_TAG,"\n\n现在nicklength为：$nicklength 宽度为：$nickwidth\n\n")
        }
        
        holder.mine_addressBook_rl!!.setOnClickListener{
            if(!bean.isChecked!!){
                val intent: Intent = Intent(mContext, IconInfo::class.java)
                val bundle= Bundle()
                bundle.putString("id",bean.id)
                bundle.putString("icon",bean.icon)
                bundle.putString("sex","")
                bundle.putString("nickname",bean.nickname)
                intent.putExtras(bundle)
                mContext!!.startActivity(intent)
            }
        }

        if(bean.isShow!!){
            holder.checkBox!!.visibility=View.VISIBLE
            holder.mine_addressBook_rl!!.setOnClickListener{
                if(!bean.isChecked!!){
                    bean.isChecked=true
                    holder.checkBox!!.isChecked=true
                }else{
                    holder.checkBox!!.isChecked=false
                    bean.isChecked=false
                }
                onShowItemClickListener!!.onShowItemClick(bean)
            }
        }else{
            holder.checkBox!!.visibility=View.GONE
        }


        holder.checkBox!!.isChecked=bean.isChecked!!

        return convertView
    }

    private fun getMyIcon(icon_url:String,icon_object:ImageView) {
        if (icon_url !== "") {
            if (LogUtils.APP_IS_DEBUG) {
                ImageLoader.getInstance().displayImage(icon_url, icon_object)
            } else {
                ImageLoader.getInstance().displayImage(icon_url, icon_object)
            }
        }
    }

   class ViewHolder{
       var indexText:TextView?=null
       var mine_friend_icon:ImageView?=null
       var mine_friend_next:ImageView?=null
       var checkBox: CheckBox?=null
       var mine_addressBook_rl:RelativeLayout?=null
   }

    interface OnShowItemClickListener {
        fun onShowItemClick(bean: MyFriends)
    }

    fun setOnShowItemClickListener(onShowItemClickListener: OnShowItemClickListener) {
        this.onShowItemClickListener = onShowItemClickListener
    }
}
