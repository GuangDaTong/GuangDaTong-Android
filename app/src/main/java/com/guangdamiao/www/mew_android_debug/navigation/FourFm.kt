package com.guangdamiao.www.mew_android_debug.navigation


import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import cn.jiguang.share.android.api.JShareInterface
import cn.jiguang.share.android.api.PlatActionListener
import cn.jiguang.share.android.api.Platform
import cn.jiguang.share.android.api.ShareParams
import cn.jiguang.share.qqmodel.QQ
import cn.jiguang.share.qqmodel.QZone
import cn.jiguang.share.wechat.Wechat
import cn.jiguang.share.wechat.WechatMoments
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.askMain.AskMainRequest
import com.guangdamiao.www.mew_android_debug.navigation.ask.askWrite.AskWriteRequest
import com.guangdamiao.www.mew_android_debug.navigation.ask.iconInfo.IconSelfInfo
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.navigation.me.me_about.MeAbout
import com.guangdamiao.www.mew_android_debug.navigation.me.me_cooperation.Me_Cooperation
import com.guangdamiao.www.mew_android_debug.navigation.me.me_help.Me_Help
import com.guangdamiao.www.mew_android_debug.navigation.me.me_info.InfoRequest
import com.guangdamiao.www.mew_android_debug.navigation.me.me_info.Me_Level
import com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Me_Mine
import com.guangdamiao.www.mew_android_debug.navigation.me.me_setting.Me_Setting
import com.guangdamiao.www.mew_android_debug.navigation.me.me_suggestion.Me_Suggestion
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.utils.CustomPopWindow
import com.guangdamiao.www.mew_android_debug.utils.DensityUtil
import com.guangdamiao.www.mew_android_debug.utils.JEventUtils
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import es.dmoral.toasty.Toasty
import java.io.File
import java.util.*


class FourFm : Fragment() {

    private var view_: View? = null
    private var caption: TextView? = null
    private var main_setting: ImageView? = null
    private var main_right_btn:Button?=null
    private var main_right_msg_btn:Button?=null
    private var caption_msg_point: TextView? = null
    private var caption_msg: ImageView? = null
    private var caption_ask_Iv:ImageView?=null
    private var main_ask:TextView?=null
    private var me_head: TextView? = null
    private var me_icon: ImageView? = null
    private var me_grade: Button? = null
    private var me_money: TextView? = null
    private var me_nickname: TextView? = null
    private val TAG = Constant.FourFm_TAG
    private var me_about_share_rl:RelativeLayout?=null
    private var me_mine:TextView?=null
    private var me_cooperation:TextView?=null
    private var me_help:TextView?=null
    private var me_suggestion:TextView?=null
    private var me_about:TextView?=null
    private var account = ""
    internal var password = ""
    internal var college = ""
    internal var school = ""
    internal var login_sign = ""
    internal var register_sign = ""
    internal var token: String? = ""
    internal var id = ""
    internal var nickname = ""
    internal var icon = ""
    internal var gender = ""
    internal var grade = ""
    internal var coins = 0
    internal var level = 0
    var share_id=""
    var share_shareWay=""
    var share_token=""
    private var popWindow_share:CustomPopWindow?=null
    private var popWindow:CustomPopWindow?=null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        view_ = inflater!!.inflate(R.layout.fm_four, container, false)
        initView()
        initImageLoader()

        initFragment(view_!!)
        return view_
    }


    override fun onActivityCreated(savedInstanceStat: Bundle?) {
        super.onActivityCreated(savedInstanceStat)

        getSharedFile()
        getMyInfo()
        fourFm_txt_onClick()
        addSomeListener()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(context,"我主页面")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(context,"我主页面")
        super.onPause()
    }

    override fun onStart() {
        caption_msg_point!!.visibility = View.GONE
        super.onStart()
        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        token = read.getString("token", "")
        if(!token.equals("")){
            //这里处理非静态的View如头像获取，金币余额，等级
            AskWriteRequest.requestForRule(context,object: IDataRequestListener2String {
                override fun loadSuccess(response_string: String) {
                    if(JsonUtil.get_key_string("code",response_string).equals(Constant.RIGHTCODE)){
                        val result= JsonUtil.get_key_string("result",response_string)
                        val coins= JsonUtil.get_key_int("coins",result)
                        val rules_string= JsonUtil.get_key_string("rules",result)
                        val jsonString_urgent= JsonUtil.get_key_string("urgent",rules_string)
                        val jsonString_normal= JsonUtil.get_key_string("normal",rules_string)
                        val college_urgent_int= JsonUtil.get_key_int("college",jsonString_urgent)
                        val school_urgent_int= JsonUtil.get_key_int("school",jsonString_urgent)
                        val college_normal_int= JsonUtil.get_key_int("college",jsonString_normal)
                        val school_normal_int= JsonUtil.get_key_int("school",jsonString_normal)
                        LogUtils.d_debugprint(Constant.Me_TAG,"\n金币最新余额："+coins+"\n从服务器拿到的规则是：\n"+"\n急问=====同学院: "+college_urgent_int+
                                "\n急问=====全校: "+school_urgent_int+"\n普问=====同学院："+college_normal_int+"\n普问=====全校所有人："+school_normal_int
                        )
                        val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                        editor.putInt("coins",coins)
                        editor.commit()

                        getSharedFile()
                        getMyInfo()
                        view_!!.postInvalidate()
                    }
                }
            })//点进来就首先请求服务器，获得当前金币数量，以及规则

            InfoRequest.updataInfo(context,token!!,object:IDataRequestListener2String{
                override fun loadSuccess(response_string: String?) {
                    if(!response_string.equals("")&&JsonUtil.get_key_string("code",response_string!!).equals(Constant.RIGHTCODE)){

                        val result=JsonUtil.get_key_string("result",response_string)
                        val account=JsonUtil.get_key_string("account",result)
                        val nickname=JsonUtil.get_key_string("nickname",result)
                        val icon_=JsonUtil.get_key_string("icon",result)
                        val school=JsonUtil.get_key_string("school",result)
                        val college=JsonUtil.get_key_string("college",result)
                        val grade=JsonUtil.get_key_string("grade",result)

                        val editor: SharedPreferences.Editor=context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
                        editor.putString("account",account)
                        editor.putString("nickname",nickname)
                        editor.putString("icon",icon_)
                        editor.putString("school",school)
                        editor.putString("college",college)
                        editor.putString("grade",grade)

                        editor.commit()

                        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                        val gender_ = read.getString("gender", "")
                        gender=gender_
                        icon=icon_
                        //iconSelfInfo_icon_iv!!.setImageResource(R.mipmap.user_default_icon)
                        getSharedFile()
                        getMyInfo()
                        view_!!.postInvalidate()
                    }
                }
            })

        }

        getSharedFile()
        getMyInfo()
        view_!!.postInvalidate()
    }

    /** 初始化imageLoader  */
    private fun initImageLoader() {
        val options = DisplayImageOptions.Builder().showImageForEmptyUri(R.mipmap.icon_empty)
                .showImageOnFail(R.mipmap.icon_empty).showImageOnLoading(R.mipmap.icon_empty).cacheInMemory(true)
                .cacheOnDisc(true).build()

        val cacheDir = File(Constant.DEFAULT_SAVE_IMAGE_PATH)
        val imageconfig = ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(200)
                .discCache(UnlimitedDiscCache(cacheDir))
                .discCacheFileNameGenerator(Md5FileNameGenerator())
                .defaultDisplayImageOptions(options).build()

        ImageLoader.getInstance().init(imageconfig)

    }

    private fun getSharedFile() {
        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        account = read.getString("account", "")
        password = read.getString("password", "")
        college = read.getString("college", "")
        school = read.getString("school", "")
        login_sign = read.getString("login_sign", "")
        register_sign = read.getString("register_sign", "")
        token = read.getString("token", "")
        id = read.getString("id", "")
        nickname = read.getString("nickname", "")
        gender = read.getString("gender", "")
        grade = read.getString("grade", "")
        icon = read.getString("icon", "")
        coins = read.getInt("coins", 0)
        level = read.getInt("level", 0)
        val loginType=read.getString("loginType","")
        val voice_bool= read.getBoolean("voice_bool",true)
        val vibrate_bool=read.getBoolean("vibrate_bool",true)
        val urgentSchool_bool = read.getBoolean("urgentSchool_bool",true)
        val urgentCollege_bool=read.getBoolean("urgentCollege_bool",true)
        val comment_bool=read.getBoolean("comment_bool",true)
        val leaveWords_bool=read.getBoolean("leaveWords_bool",true)
        val activity_bool=read.getBoolean("activity_bool",true)
        val system_bool=read.getBoolean("system_bool",true)

        LogUtils.d_debugprint(Constant.Me_TAG, "getSharedFile中  \naccount=" + account + "\npassword=" + password + "\ncollege=" + college + "\nschool=" + school + "\nlogin_sign=" + login_sign + "\ngender=" + gender
                + "\nregister_sign=" + register_sign + "\ntoken=" + token + "\nid=" + id + "\nnickname=" + nickname + "\nicon=" + icon + "\ncoins=" + coins + "\nlevel=" + level+"\nloginType=$loginType"
                +"\nvoice_bool="+voice_bool+"\nvibrate_bool="+vibrate_bool+"\nurgentSchool_bool="+urgentSchool_bool+"\nurgentCollege_bool="+urgentCollege_bool+"\ncomment_bool="+comment_bool
                +"\nleaveWords_bool="+leaveWords_bool+"\nactivity_bool="+activity_bool+"\nsystem_bool="+system_bool)
    }

    private fun getMyInfo() {
        getMyIcon()
        if(nickname.equals("")){
            me_nickname!!.text = "未登录"
        }else{
            //昵称
            me_nickname!!.text= nickname
            var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
            me_nickname!!.measure(spec,spec)
            var nickwidth= me_nickname!!.measuredWidth
           // LogUtils.d_debugprint(Constant. Me_TAG,"\n\n现在nickname为：${ nickname} \n\n 长度为：${nickwidth} \n\n转换后的167.0f为：${DensityUtil.dip2px(context,187.0f)} ")
            var nicklength= nickname!!.length
            while(nickwidth> DensityUtil.dip2px(context,187.0f)){
                nicklength-=2
                val name_display= nickname!!.substring(0,nicklength)+"..."
                me_nickname!!.text=name_display
                var spec=View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED)
                me_nickname!!.measure(spec,spec)
                nickwidth= me_nickname!!.measuredWidth
                //LogUtils.d_debugprint(Constant. Me_TAG,"\n\n现在nicklength为：$nicklength 宽度为：$nickwidth\n\n")
            }
        }
        me_money!!.text = Integer.toString(coins)
        me_grade!!.text = "LV" + Integer.toString(level)
        LogUtils.d_debugprint(TAG, "\n我的昵称=$nickname\n我的金币=$coins\n我的等级=$level")
    }

    private fun getMyIcon() {
        if (icon !== "") {
            if (LogUtils.APP_IS_DEBUG) {
                ImageLoader.getInstance().displayImage(icon, me_icon)
            } else {
                ImageLoader.getInstance().displayImage(icon, me_icon)
            }
        }else{
            me_icon!!.setImageResource(R.mipmap.user_default_icon)
        }
    }


    private fun initView() {
        caption = activity.findViewById(R.id.main_txt1) as TextView
        main_setting = activity.findViewById(R.id.main_setting) as ImageView
        main_right_btn=activity.findViewById(R.id.main_right_btn) as Button
        main_right_msg_btn=activity.findViewById(R.id.main_right_msg_btn) as Button
        caption_msg_point = activity.findViewById(R.id.main_unread_msg) as TextView

        caption_ask_Iv=activity.findViewById(R.id.ask_down_Iv) as ImageView
        caption_ask_Iv!!.visibility=View.GONE
        caption_msg = activity.findViewById(R.id.main_msg) as ImageView
        caption_msg!!.visibility = View.GONE
        main_setting!!.visibility = View.VISIBLE
        main_right_msg_btn!!.visibility=View.GONE
        main_right_btn!!.visibility=View.VISIBLE
        main_setting!!.setImageResource(R.mipmap.me_setting)
        main_ask=activity.findViewById(R.id.main_ask) as TextView
        main_ask!!.visibility=View.GONE
        caption_msg_point!!.visibility = View.GONE
        caption!!.text = "我"
    }

    private fun initFragment(view: View) {
        me_head = view.findViewById(R.id.fourFm_txt1) as TextView
        me_icon = view.findViewById(R.id.fourFm_icon) as ImageView
        me_about_share_rl=view.findViewById(R.id.me_about_share_rl) as RelativeLayout
        me_grade = view.findViewById(R.id.fourFm_grade) as Button
        me_money = view.findViewById(R.id.fourFm_moneynum) as TextView
        me_nickname = view.findViewById(R.id.fourFm_nick) as TextView
        me_mine=view.findViewById(R.id.me_mine) as TextView
        me_cooperation=view.findViewById(R.id.me_cooperation) as TextView
        me_help=view.findViewById(R.id.me_help) as TextView
        me_suggestion=view.findViewById(R.id.me_suggestion) as TextView
        me_about=view.findViewById(R.id.me_about) as TextView
    }

    private fun fourFm_txt_onClick() {
        me_head!!.setOnClickListener {
            if (token === "" || token == null) {//=====token过期，或者未登录
                //Common.showLongToast(getContext(),"这里要提示登录的提示框，确定即登录，取消即返回");
                SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText("提示")
                        .setContentText("您还没有登录，点击进入登录页面")
                        .setCustomImage(R.mipmap.mew_icon)
                        .setConfirmText("登录")
                        .setConfirmClickListener { sDialog ->
                            sDialog.dismissWithAnimation()
                            //跳转到登录，不用传东西过去
                            val intent = Intent(context, LoginMain::class.java)
                            context.startActivity(intent)
                            activity.overridePendingTransition(R.anim.push_bottom_in, R.anim.push_top_out)
                        }
                        .setCancelText("取消")
                        .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                        .show()
            } else {
                //跳转到个人详细资料的页面
                val intent:Intent=Intent(context, IconSelfInfo::class.java)
                val bundle=Bundle()
                bundle.putString("id",id)
                bundle.putString("icon",icon)
                bundle.putString("sex",gender)
                bundle.putString("nickname",nickname)
                intent.putExtras(bundle)
                context.startActivity(intent)
                activity.overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out)
            }
        }
    }

    private fun addSomeListener() {
        me_grade!!.setOnClickListener {
            val intent = Intent(context, Me_Level::class.java)
            context.startActivity(intent)
            activity.overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out)
        }

        main_right_btn!!.setOnClickListener {
            val intent = Intent(context, Me_Setting::class.java)
            context.startActivity(intent)
            activity.overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out)
        }

        main_right_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                main_setting!!.alpha=0.618f
                main_right_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))

            }else{
                main_setting!!.alpha=1.0f
                main_right_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }

        me_mine!!.setOnClickListener{
            val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
            val token = read.getString("token", "")
            if(!token.equals("")){
                val intent:Intent=Intent(context, Me_Mine::class.java)
                startActivity(intent)
                activity.overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
            }else{
                val intent = Intent(context, LoginMain::class.java)
                context.startActivity(intent)
                activity.overridePendingTransition(R.anim.push_bottom_in, R.anim.push_top_out)
            }
        }
        me_cooperation!!.setOnClickListener{
            val intent:Intent=Intent(context, Me_Cooperation::class.java)
            startActivity(intent)
            activity.overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }
        me_help!!.setOnClickListener{
           showhelpDialog()
        }
        me_suggestion!!.setOnClickListener{
            val intent:Intent=Intent(context, Me_Suggestion::class.java)
            startActivity(intent)
            activity.overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }

        me_about_share_rl!!.setOnClickListener{
            make_share()
        }

        me_about!!.setOnClickListener{

            val intent:Intent=Intent(context, MeAbout::class.java)
            startActivity(intent)
            activity.overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }


    }

    private fun showhelpDialog(){

        val contentView:View= LayoutInflater.from(context).inflate(R.layout.pop_help_dialog,null)
        hanleDialog(contentView)
        popWindow= CustomPopWindow.PopupWindowBuilder(context)
                .setView(contentView)
                .enableBackgroundDark(true)
                .setBgDarkAlpha(0.7f)
                .setFocusable(true)
                .setOutsideTouchable(true)
                .setAnimationStyle(R.style.ask_three_anim)
                .create()
        popWindow!!.showAtLocation(me_help, Gravity.CENTER_HORIZONTAL,0, DensityUtil.dip2px(context,560.0f))

    }

    private fun hanleDialog(contentView:View){

        val me_help_guide=contentView.findViewById(R.id.me_help_guide) as TextView
        val me_help_webview=contentView.findViewById(R.id.me_help_webview) as TextView
        val me_help_cancle=contentView.findViewById(R.id.me_help_cancle) as TextView
        val listener:View.OnClickListener=object:View.OnClickListener{
            override fun onClick(v: View?) {
                if(popWindow!=null){
                    popWindow!!.dissmiss()
                }
                when(v!!.id){
                    R.id.me_help_guide   -> toGuideActivity()
                    R.id.me_help_webview    -> toHelpWebView()
                    R.id.me_help_cancle   -> popWindow!!.dissmiss()
                }
            }
        }

        me_help_guide!!.setOnClickListener(listener)
        me_help_webview!!.setOnClickListener(listener)
        me_help_cancle!!.setOnClickListener(listener)
    }

    private fun toGuideActivity(){
        val intent:Intent=Intent(context, GuideActivity::class.java)
        startActivity(intent)
        activity.overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
    }

    private fun toHelpWebView(){
        val intent:Intent=Intent(context, Me_Help::class.java)
        startActivity(intent)
        activity.overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser){
            caption_msg_point!!.visibility = View.GONE
        }else{
            caption_msg_point!!.visibility = View.GONE
        }
    }

    fun make_share(){
        val contentView:View= LayoutInflater.from(context).inflate(R.layout.ask_share,null)
        hanleShareDialog(contentView)
        popWindow_share= CustomPopWindow.PopupWindowBuilder(context)
                .setView(contentView)
                .enableBackgroundDark(true)
                .setBgDarkAlpha(0.7f)
                .setFocusable(true)
                .setOutsideTouchable(true)
                .setAnimationStyle(R.style.ask_share_anim)
                .create()
        popWindow_share!!.showAtLocation(me_about_share_rl, Gravity.CENTER,0,0)
    }

    private fun hanleShareDialog(contentView: View){
        val ask_share_qq=contentView.findViewById(R.id.ask_share_qq) as RelativeLayout
        val ask_share_wechat=contentView.findViewById(R.id.ask_share_wechat) as RelativeLayout
        val ask_share_qq_zone=contentView.findViewById(R.id.ask_share_qq_zone) as RelativeLayout
        val ask_share_wechat_friends=contentView.findViewById(R.id.ask_share_wechat_friends) as RelativeLayout
        val ask_share_copy=contentView.findViewById(R.id.ask_share_copy) as RelativeLayout


        ask_share_qq!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk( QQ.Name,"QQFriend")
        }

        ask_share_qq_zone!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk( QZone.Name,"QQZone")
        }

        ask_share_wechat!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk( Wechat.Name,"weChatFriend")
        }

        ask_share_wechat_friends!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            shareAsk(WechatMoments.Name,"weChatCircle")
        }

        ask_share_copy!!.setOnClickListener{
            popWindow_share!!.dissmiss()
            val cm=context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            var mClipData: ClipData?=null
            if(LogUtils.APP_IS_DEBUG){
                mClipData= ClipData.newPlainText("Label",Constant.BASEURLFORTEST)
            }else{
                mClipData= ClipData.newPlainText("Label",Constant.BASEURLFORRELEASE)
            }
            cm.primaryClip=mClipData
            Toasty.info(context,"亲，复制成功，赶快分享一下吧~").show()
        }

    }

    private fun shareAsk(shareType:String,shareWay:String){
        JEventUtils.onCountEvent(context, Constant.Event_AskShare)//分享帖子内容统计

        val shareParams= ShareParams()
        shareParams.shareType= Platform.SHARE_WEBPAGE
        shareParams.text="免费、有料、能救急的首页生活小助理"
        shareParams.title="广大通"
        if(shareType.equals(WechatMoments.Name)){
            shareParams.title="免费、有料、能救急的首页生活小助理"
        }
        if(LogUtils.APP_IS_DEBUG){
            shareParams.url=Constant.BASEURLFORTEST
        }else{
            shareParams.url=Constant.BASEURLFORRELEASE
        }
        shareParams.imagePath= MainApplication.ImagePath

        val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        share_token = read.getString("token", "")
        share_id=""
        share_shareWay=shareWay

        JShareInterface.share(shareType, shareParams, mPlatActionListener)
    }


    private val mPlatActionListener = object : PlatActionListener {
        override fun onComplete(platform: Platform, action: Int, data: HashMap<String, Any>) {
            LogUtils.d_debugprint(Constant.Me_TAG,"APP分享成功！！！即将向服务器发送数据~~")
            Toasty.info(context,"亲，分享成功！").show()
            AskMainRequest.makeShareToServer(Constant.Me_TAG,Constant.URL_Share_APP,context,share_id,share_shareWay,share_token)

        }

        override fun onError(platform: Platform, action: Int, errorCode: Int, error: Throwable) {
            LogUtils.d_debugprint(Constant.Me_TAG,"APP分享失败！！！")
            Toasty.info(context,"亲，分享失败！").show()
        }

        override fun onCancel(platform: Platform, action: Int) {
            LogUtils.d_debugprint(Constant.Me_TAG,"APP分享取消！！！")
            Toasty.info(context,"亲，您已取消分享！").show()
        }
    }

  
}
