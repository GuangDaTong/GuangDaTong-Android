package com.guangdamiao.www.mew_android_debug.navigation.school.message

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter

/**
 * Created by Jason_Jan on 2017/12/9.
 */

class MessageAdapter(private val fm: FragmentManager, private val fmList: List<Fragment>) : FragmentPagerAdapter(fm) {

    override fun getItem(arg0: Int): Fragment {
        return fmList[arg0 % fmList.size]
    }

    override fun getCount(): Int {
        return fmList.size
    }

    override fun getItemPosition(`object`: Any?): Int {
        //没有找到child  要求重新加载
        return PagerAdapter.POSITION_NONE
    }
}
