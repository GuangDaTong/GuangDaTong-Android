package com.guangdamiao.www.mew_android_debug.navigation.find

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.askWrite.ReboundScrollView
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.JEventUtils
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import es.dmoral.toasty.Toasty

class SignUp : BaseActivity() {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_textview: TextView? = null
    private var title_right_imageview: ImageView?=null
    private var title_right_imageview_btn: Button?=null
    private var signUp_root_rs :ReboundScrollView?=null
    private var signUp_activity_rl:RelativeLayout?=null
    private var signUp_activity_view:View?=null
    private var signUp_clause_tv:TextView?=null
    private var signUp_activity_num_et:EditText?=null
    private var signUp_name_et:EditText?=null
    private var signUp_qq_et:EditText?=null
    private var signUp_wechat_et:EditText?=null
    private var signUp_phone_et:EditText?=null

    private var getBundle:Bundle?=null
    private var id=""
    private var token=""
    private var phone=""
    private var urlEnd=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        MainApplication.getInstance().addActivity(this)
        getBundle=intent.extras
        initView()
        addSomeListener()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@SignUp,"我要报名")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@SignUp,"我要报名")
        super.onPause()
    }

    fun initView(){
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_textview = findViewById(R.id.title_right_textview) as TextView
        title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
        signUp_root_rs=findViewById(R.id.signUp_root_rs) as ReboundScrollView
        signUp_activity_rl=findViewById(R.id.signUp_activity_rl) as  RelativeLayout
        signUp_activity_view=findViewById(R.id.signUp_activity_view) as View
        signUp_clause_tv=findViewById(R.id.signUp_clause_tv) as TextView
        signUp_activity_num_et=findViewById(R.id.signUp_activity_num_et) as EditText
        signUp_name_et=findViewById(R.id.signUp_name_et) as EditText
        signUp_qq_et=findViewById(R.id.signUp_qq_et) as EditText
        signUp_phone_et=findViewById(R.id.signUP_phone_et) as EditText
        signUp_wechat_et=findViewById(R.id.signUp_wechat_et) as EditText
        title_center_textview!!.text="我要报名"
        title_right_textview!!.text="完成"
        title_right_imageview!!.visibility=View.GONE
        title_right_textview!!.visibility= View.VISIBLE
        title_center_textview!!.visibility=View.VISIBLE
        title_right_imageview_btn!!.visibility=View.VISIBLE

        if(getBundle!=null){
            id=getBundle!!.getString("id")
            token=getBundle!!.getString("token")
            phone=getBundle!!.getString("account")
            urlEnd=getBundle!!.getString("urlEnd")
        }

        if(urlEnd.equals(Constant.Find_Activity)){
            signUp_activity_rl!!.visibility=View.VISIBLE
            signUp_activity_view!!.visibility=View.VISIBLE
        }else{
            signUp_activity_rl!!.visibility=View.GONE
            signUp_activity_view!!.visibility=View.GONE
        }

        if(!phone.equals("")) signUp_phone_et!!.setText(phone)

    }

    fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.alpha=0.618f
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))


            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.alpha=1.0f
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
            }
            false
        }

        signUp_clause_tv!!.setOnClickListener{
            val intent:Intent=Intent(this@SignUp,ServiceActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }

        title_right_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_textview!!.alpha=0.618f
                title_right_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))


            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_textview!!.alpha=1.0f
                title_right_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)

            }
            false
        }

        title_right_imageview_btn!!.setOnClickListener{
            if(!token.equals("")&&(!id.equals("")&&!phone.equals(""))){
                if(!signUp_name_et!!.text.toString().trim().equals("")){
                    JEventUtils.onCountEvent(this@SignUp,Constant.Event_DiscovetyApply)//报名次数统计
                    val pDialog1 =SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog1.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog1.setTitleText("Loading")
                    pDialog1.setCancelable(true)
                    pDialog1.show()
                    if(urlEnd.equals(Constant.Find_PartTimeJob)){
                       SameListRequest.makeApply(this@SignUp,id,signUp_name_et!!.text.toString(),signUp_phone_et!!.text.toString(),1.toString(),signUp_qq_et!!.text.toString(),signUp_wechat_et!!.text.toString(),token,Constant.URL_SignUp_PartTimeJob,object:IDataRequestListener2String{
                           override fun loadSuccess(response_string: String?) {
                                LogUtils.d_debugprint(Constant.Find_TAG,"我要报名中，服务器返回的数据为："+response_string)
                                if(JsonUtil.get_key_string(Constant.Server_Code,response_string!!).equals(Constant.RIGHTCODE)){
                                    //报名成功
                                    pDialog1.cancel()
                                    LogUtils.d_debugprint(Constant.Find_TAG,"我要报名中"+" 兼职模块中 "+" 报名成功！！！\n")
                                    val pDialog= SweetAlertDialog(this@SignUp, SweetAlertDialog.SUCCESS_TYPE)
                                    pDialog.setTitleText("提示")
                                    pDialog.setContentText("亲，您已经成功报名，请等待客服联系喔~")
                                    pDialog.show()
                                    val handler= Handler()
                                    handler.postDelayed({
                                        pDialog.cancel()
                                        finish()
                                        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
                                    },1236)

                                }else if(JsonUtil.get_key_string(Constant.Server_Code,response_string!!).equals(Constant.LOGINFAILURECODE)){
                                    val read = this@SignUp.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val token = read.getString("token", "")
                                    val account=read.getString("account","")
                                    val password=read.getString("password","")
                                    val editor: SharedPreferences.Editor=this@SignUp.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    editor.clear()
                                    editor.commit()
                                    val editor2: SharedPreferences.Editor=this@SignUp.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                    editor2.putString("account",account)
                                    editor2.putString("password",password)
                                    editor2.commit()

                                    SweetAlertDialog(this@SignUp, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(this@SignUp, LoginMain::class.java)
                                                this@SignUp.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }else if(response_string.equals("failed")) {
                                    pDialog1.cancel()
                                    Toasty.error(this@SignUp,Constant.REQUESTFAIL).show()
                                }else{
                                    if(JsonUtil.get_key_string("msg",response_string)!=null){
                                        pDialog1.cancel()
                                        val pDialog= SweetAlertDialog(this@SignUp, SweetAlertDialog.WARNING_TYPE)
                                        pDialog.setTitleText("提示")
                                        pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                                        pDialog.show()
                                        val handler= Handler()
                                        handler.postDelayed({
                                            pDialog.cancel()
                                        },1236)
                                    }
                                }
                           }
                       })

                    }else if(urlEnd.equals(Constant.Find_Training)){
                        SameListRequest.makeApply(this@SignUp,id,signUp_name_et!!.text.toString(),phone,1.toString(),signUp_qq_et!!.text.toString(),signUp_wechat_et!!.text.toString(),token,Constant.URL_SignUp_Training,object:IDataRequestListener2String{
                            override fun loadSuccess(response_string: String?) {
                                LogUtils.d_debugprint(Constant.Find_TAG,"我要报名中，服务器返回的数据为："+response_string)
                                if(JsonUtil.get_key_string(Constant.Server_Code,response_string!!).equals(Constant.RIGHTCODE)){
                                    //报名成功
                                    pDialog1.cancel()
                                    LogUtils.d_debugprint(Constant.Find_TAG,"我要报名中"+" 培训模块中 "+" 报名成功！！！\n")
                                    val pDialog= SweetAlertDialog(this@SignUp, SweetAlertDialog.SUCCESS_TYPE)
                                    pDialog.setTitleText("提示")
                                    pDialog.setContentText("亲，您已经成功报名，请等待客服联系喔~")
                                    pDialog.show()
                                    val handler= Handler()
                                    handler.postDelayed({
                                        pDialog.cancel()
                                        finish()
                                        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
                                    },1236)

                                }else if(JsonUtil.get_key_string(Constant.Server_Code,response_string!!).equals(Constant.LOGINFAILURECODE)){
                                    val read = this@SignUp.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val token = read.getString("token", "")
                                    val account=read.getString("account","")
                                    val password=read.getString("password","")
                                    val editor: SharedPreferences.Editor=this@SignUp.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    editor.clear()
                                    editor.commit()
                                    val editor2: SharedPreferences.Editor=this@SignUp.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                    editor2.putString("account",account)
                                    editor2.putString("password",password)
                                    editor2.commit()

                                    SweetAlertDialog(this@SignUp, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(this@SignUp, LoginMain::class.java)
                                                this@SignUp.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }else if(response_string.equals("failed")){
                                    pDialog1.cancel()
                                    Toasty.error(this@SignUp,Constant.REQUESTFAIL).show()
                                }else{
                                    if(JsonUtil.get_key_string("msg",response_string)!=null){
                                        pDialog1.cancel()
                                        val pDialog= SweetAlertDialog(this@SignUp, SweetAlertDialog.WARNING_TYPE)
                                        pDialog.setTitleText("提示")
                                        pDialog.setContentText(JsonUtil.get_key_string("msg",response_string))
                                        pDialog.show()
                                        val handler= Handler()
                                        handler.postDelayed({
                                            pDialog.cancel()
                                        },1236)
                                    }
                                }
                            }
                        })

                    }else if(urlEnd.equals(Constant.Find_Activity)){
                        if(signUp_activity_num_et!!.text.toString().toInt()==0){
                            if(pDialog1!=null) pDialog1.cancel()
                            Toasty.info(this@SignUp,"亲，参与人数不能为0喔~").show()
                        }else{
                            SameListRequest.makeApply(this@SignUp, id, signUp_name_et!!.text.toString(), phone, signUp_activity_num_et!!.text.toString(), signUp_qq_et!!.text.toString(), signUp_wechat_et!!.text.toString(), token, Constant.URL_SignUp_Activity, object : IDataRequestListener2String {
                                override fun loadSuccess(response_string: String?) {
                                    LogUtils.d_debugprint(Constant.Find_TAG, "我要报名中，服务器返回的数据为：" + response_string)
                                    if (JsonUtil.get_key_string(Constant.Server_Code, response_string!!).equals(Constant.RIGHTCODE)) {
                                        //报名成功
                                        pDialog1.cancel()
                                        LogUtils.d_debugprint(Constant.Find_TAG, "我要报名中" + " 活动模块中 " + "报名成功！！！\n")
                                        val pDialog = SweetAlertDialog(this@SignUp, SweetAlertDialog.SUCCESS_TYPE)
                                        pDialog.setTitleText("提示")
                                        pDialog.setContentText("亲，您已经成功报名，请等待客服联系喔~")
                                        pDialog.show()
                                        val handler = Handler()
                                        handler.postDelayed({
                                            pDialog.cancel()
                                            finish()
                                            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
                                        }, 1236)

                                    } else if (JsonUtil.get_key_string(Constant.Server_Code, response_string!!).equals(Constant.LOGINFAILURECODE)) {
                                        val read = this@SignUp.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                        val token = read.getString("token", "")
                                        val account = read.getString("account", "")
                                        val password = read.getString("password", "")
                                        val editor: SharedPreferences.Editor = this@SignUp.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                        editor.clear()
                                        editor.commit()
                                        val editor2: SharedPreferences.Editor = this@SignUp.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit()
                                        editor2.putString("account", account)
                                        editor2.putString("password", password)
                                        editor2.commit()

                                        SweetAlertDialog(this@SignUp, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                                .setTitleText(Constant.LOGINFAILURETITLE)
                                                .setContentText(Constant.LOGINFAILURE)
                                                .setCustomImage(R.mipmap.mew_icon)
                                                .setConfirmText("确定")
                                                .setConfirmClickListener { sDialog ->
                                                    sDialog.dismissWithAnimation()
                                                    //跳转到登录，不用传东西过去
                                                    val intent = Intent(this@SignUp, LoginMain::class.java)
                                                    this@SignUp.startActivity(intent)
                                                }
                                                .setCancelText("取消")
                                                .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                                .show()
                                    } else if(response_string.equals("failed")){
                                        pDialog1.cancel()
                                        Toasty.error(this@SignUp,Constant.REQUESTFAIL).show()
                                    }else{
                                        if (JsonUtil.get_key_string("msg", response_string) != null) {
                                            pDialog1.cancel()
                                            val pDialog = SweetAlertDialog(this@SignUp, SweetAlertDialog.WARNING_TYPE)
                                            pDialog.setTitleText("提示")
                                            pDialog.setContentText(JsonUtil.get_key_string("msg", response_string))
                                            pDialog.show()
                                            val handler = Handler()
                                            handler.postDelayed({
                                                pDialog.cancel()
                                            }, 1236)
                                        }
                                    }
                                }
                            })
                        }
                    }else {
                        if(pDialog1!=null) pDialog1.cancel()
                        Toasty.info(this@SignUp,Constant.SERVERBROKEN).show()
                    }
                }else{
                    Toasty.info(this@SignUp,"亲，您还没有填写姓名喔~").show()
                }

            }
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish()
            overridePendingTransition(0, R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    //点击空白隐藏键盘
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (this@SignUp.currentFocus != null) {
                if (this@SignUp.currentFocus!!.windowToken != null) {
                    imm.hideSoftInputFromWindow(this@SignUp.currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }
            }
        }
        return super.onTouchEvent(event)
    }
}
