package com.guangdamiao.www.mew_android_debug.navigation


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.HTab
import com.guangdamiao.www.mew_android_debug.bean.HTabDb
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.navigation.find.FindAdapter
import com.guangdamiao.www.mew_android_debug.navigation.find.FindFm1
import com.guangdamiao.www.mew_android_debug.navigation.find.FindFm2
import com.guangdamiao.www.mew_android_debug.navigation.find.FindFm3
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import java.io.File

class TwoFm : Fragment(), ViewPager.OnPageChangeListener {

    private var caption: TextView? = null
    private var main_setting:ImageView?=null
    private var main_right_btn:Button?=null
    private var main_right_msg_btn:Button?=null
    private var caption_msg: ImageView? = null
    private var caption_msg_point: TextView? = null
    private var caption_ask_Iv:ImageView?=null
    private var main_ask:TextView?=null
    private var view_: View? = null
    private var rg_: RadioGroup? = null
    private var vp_: ViewPager? = null
    private var hv_: HorizontalScrollView? = null
    private val newsList = ArrayList<Fragment>()
    private var adapter: FindAdapter? = null
    private var hTabs: List<HTab>? = null//=====处理4个小分页
    var parent:ViewGroup?=null
    private val TAG = Constant.Find_TAG

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        initCaption()
        initImageLoader()
        if(view_==null){
            view_ = inflater!!.inflate(R.layout.fm_two, container, false)//初始化view
            rg_ = view_!!.findViewById(R.id.two_rg) as RadioGroup//分页顶部的单选按钮
            vp_ = view_!!.findViewById(R.id.two_view) as ViewPager//每一个小分页
            hv_ = view_!!.findViewById(R.id.two_hv) as HorizontalScrollView//分页顶部的那条线
            rg_!!.setOnCheckedChangeListener { group, id -> vp_!!.currentItem = id }

            initTab(inflater)
            initView()//初始化viewpager

        }
        if(view_!!.parent!=null){
            parent=view_!!.parent as ViewGroup
            parent!!.removeView(view_)
        }
        return view_
    }

    override fun onStart() {
        caption_msg_point!!.visibility = View.GONE
        super.onStart()
    }

    override fun onResume(){
        super.onResume()
        JAnalyticsInterface.onPageStart(context,"栏目主页面")
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(context,"栏目主页面")
        super.onPause()
    }

    private fun initCaption() {
        caption = activity.findViewById(R.id.main_txt1) as TextView
        caption_msg = activity.findViewById(R.id.main_msg) as ImageView
        main_setting=activity.findViewById(R.id.main_setting) as ImageView
        caption_msg_point = activity.findViewById(R.id.main_unread_msg) as TextView
        caption_ask_Iv=activity.findViewById(R.id.ask_down_Iv) as ImageView
        main_ask=activity.findViewById(R.id.main_ask) as TextView
        main_right_btn=activity.findViewById(R.id.main_right_btn) as Button
        main_right_msg_btn=activity.findViewById(R.id.main_right_msg_btn) as Button
        main_right_btn!!.visibility=View.GONE
        main_right_msg_btn!!.visibility=View.GONE
        main_ask!!.visibility=View.GONE
        caption_ask_Iv!!.visibility=View.GONE
        caption_msg!!.visibility = View.GONE
        main_setting!!.visibility=View.GONE
        caption_msg_point!!.visibility = View.GONE
        caption!!.text = "栏目"
    }

    private fun initTab(inflater:LayoutInflater){
        val hTabs = HTabDb.getFindSelected()
        for (i in hTabs.indices) {
            val rbButton = inflater.inflate(R.layout.tab_top1, null) as RadioButton
            rbButton.id = i
            rbButton.text = hTabs[i].name
            val dm = DisplayMetrics()
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            wm.defaultDisplay.getMetrics(dm)
            val screenWidth = dm.widthPixels
            rbButton.width = screenWidth / 3
            val params = RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT,
                    RadioGroup.LayoutParams.WRAP_CONTENT)
            rg_!!.addView(rbButton, params)
        }
        rg_!!.check(1)
    }

    private fun initView(){
        hTabs = HTabDb.getFindSelected()
        Find_FirstTab()
        Find_SecondTab()
        Find_ThirdTab()
        adapter = FindAdapter(activity.supportFragmentManager, newsList)
        vp_!!.adapter = adapter
        vp_!!.offscreenPageLimit = 2
        vp_!!.currentItem = 1
        vp_!!.setOnPageChangeListener(this)
    }

    private fun Find_FirstTab() {
        val fm1 = FindFm1()
        val bundle1 = Bundle()
        bundle1.putString("name", hTabs!![0].name)
        fm1.arguments = bundle1
        newsList.add(fm1)
    }

    private fun Find_SecondTab() {
        val fm1 = FindFm2()
        val bundle1 = Bundle()
        bundle1.putString("name", hTabs!![1].name)
        fm1.arguments = bundle1
        newsList.add(fm1)//4个小碎片的list
    }

    private fun Find_ThirdTab() {
        val fm1 = FindFm3()
        val bundle1 = Bundle()
        bundle1.putString("name", hTabs!![2].name)
        fm1.arguments = bundle1
        newsList.add(fm1)
    }

    /** 初始化imageLoader  */
    private fun initImageLoader() {
        val options = DisplayImageOptions.Builder().showImageForEmptyUri(R.mipmap.icon_empty)
                .showImageOnFail(R.mipmap.icon_empty).showImageOnLoading(R.mipmap.icon_empty).cacheInMemory(true)
                .cacheOnDisc(true).build()

        val cacheDir = File(Constant.DEFAULT_SAVE_IMAGE_PATH)
        val imageconfig = ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(200)
                .discCache(UnlimitedDiscCache(cacheDir))
                .discCacheFileNameGenerator(Md5FileNameGenerator())
                .defaultDisplayImageOptions(options).build()

        ImageLoader.getInstance().init(imageconfig)

    }


    private fun setTab(id: Int) {
        val rbButton = rg_!!.getChildAt(id) as RadioButton
        rbButton.isChecked = true
        val left = rbButton.left
        val width = rbButton.measuredWidth
        val metrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(metrics)
        val screenWidth = metrics.widthPixels
        val len = left + width / 2 - screenWidth / 2
        hv_!!.smoothScrollTo(len, 0)
    }

    override fun onPageScrollStateChanged(arg0: Int) {}

    override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

    override fun onPageSelected(id: Int) {
        setTab(id)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser){
            caption_msg_point!!.visibility = View.GONE
        }else{
            caption_msg_point!!.visibility = View.GONE
        }
    }
}
