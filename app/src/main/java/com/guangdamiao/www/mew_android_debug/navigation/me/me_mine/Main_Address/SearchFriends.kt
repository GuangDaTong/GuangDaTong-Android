package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Address

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MySearchFriends
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import es.dmoral.toasty.Toasty

class SearchFriends : Activity() {

    private var title_left_imageview_btn: Button?=null
    private var title_right_imageview_btn: Button?=null
    private var title_center_search_et: EditText?=null
    private var title_center_search_iv: ImageView?=null

    var mListView: ListView?=null
    var adapter:Main_SearchFriendsAdapter?=null
    private var dataList=ArrayList<MySearchFriends>()
    private var no_data_rl: RelativeLayout?=null
    var pageNow = 0
    var handler = Handler()
    var ptrClassicFrameLayout: PtrClassicFrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.getInstance().addActivity(this)
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
        setContentView(R.layout.activity_search_friends)
        initView()

        adapter= Main_SearchFriendsAdapter(this@SearchFriends,dataList)
        mListView!!.setAdapter(adapter!!)//关键代码
        initData2()
        addSomeListener()

    }

    override fun onResume(){
        super.onResume()
        JAnalyticsInterface.onPageStart(this@SearchFriends,"添加好友")
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@SearchFriends,"添加好友")
        super.onPause()
    }

    fun initView(){
        title_left_imageview_btn=findViewById(R.id.title_left_imageview_btn) as Button
        title_right_imageview_btn=findViewById(R.id.title_right_imageview_btn) as Button
        title_center_search_et=findViewById(R.id.title_center_searchAsk_et) as EditText
        title_center_search_iv=findViewById(R.id.title_center_search_iv) as ImageView
        title_center_search_iv!!.bringToFront()
        no_data_rl=findViewById(R.id.no_data_rl) as RelativeLayout
        mListView=findViewById(R.id.mine_search_friends) as ListView
        ptrClassicFrameLayout=findViewById(R.id.list_searchFriends_frame) as PtrClassicFrameLayout
        no_data_rl!!.visibility=View.GONE

    }

    fun addSomeListener() {
        title_left_imageview_btn!!.setOnClickListener {
            this.finish()
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f
            }
            false
        }

        title_right_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_right_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_right_imageview_btn!!.alpha=1.0f
            }
            false
        }


        //点击了搜索框
        title_center_search_et!!.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode === KeyEvent.KEYCODE_ENTER && event!!.action === KeyEvent.ACTION_DOWN) {// 修改回车键功能
                    // 先隐藏键盘
                    (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                            currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

                    LogUtils.d_debugprint(Constant.SearchFriends_TAG,"搜索的key是："+title_center_search_et!!.text.toString())

                    if(!title_center_search_et!!.text.toString().trim().equals("")){
                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                        initData()
                    }else{
                        Toasty.info(this@SearchFriends,"您还没有输入要搜索的好友哦").show()
                    }

                }
                return false
            }
        })


        //点击了右侧的搜索按钮
        title_right_imageview_btn!!.setOnClickListener{

            LogUtils.d_debugprint(Constant.SearchFriends_TAG,"搜索的key是："+title_center_search_et!!.text.toString().trim())
            if(!title_center_search_et!!.text.toString().trim().equals("")){
                ptrClassicFrameLayout!!.isLoadMoreEnable=true
                initData()
            }else{
                Toasty.info(this@SearchFriends,"您还没有输入要搜索的好友哦").show()
            }

        }

    }

    private fun initData() {

        ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(true) }, 150)

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    if(!title_center_search_et!!.text.toString().trim().equals("")){
                        pageNow = 0
                        RequestServerList(pageNow,adapter!!,ptrClassicFrameLayout!!)
                        if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                            ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                        }
                    }else{
                        Toasty.info(this@SearchFriends,"您还没有输入要搜索的好友哦").show()
                        ptrClassicFrameLayout!!.refreshComplete()
                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                    }
                }, 200)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener{
            handler.postDelayed({
                RequestServerList(++pageNow,adapter!!,ptrClassicFrameLayout!!)
            }, 200)
        }
    }

    private fun initData2() {

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    if(!title_center_search_et!!.text.toString().trim().equals("")){
                        pageNow = 0
                        RequestServerList(pageNow,adapter!!,ptrClassicFrameLayout!!)
                        if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                            ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                        }
                    }else{
                        Toasty.info(this@SearchFriends,"您还没有输入要搜索的好友哦").show()
                        ptrClassicFrameLayout!!.refreshComplete()
                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                    }
                }, 200)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener{
            handler.postDelayed({
                RequestServerList(++pageNow,adapter!!,ptrClassicFrameLayout!!)
            }, 200)
        }
    }

    private fun RequestServerList(pageNow: Int, adapter: Main_SearchFriendsAdapter, ptrClassicFrameLayout:PtrClassicFrameLayout) {

        LogUtils.d_debugprint(Constant.SearchFriends_TAG,"搜索的key是："+title_center_search_et!!.text.toString().trim())

            AddressRequest.searchFriendsFromServer(this@SearchFriends,title_center_search_et!!.text.toString().trim(),pageNow,dataList,adapter,ptrClassicFrameLayout,object: IDataRequestListener2String {
                override fun loadSuccess(response_string: String?) {
                    if("noSearchFriends".equals(response_string)){
                        no_data_rl!!.visibility=View.VISIBLE
                    }else{
                        no_data_rl!!.visibility=View.GONE
                    }
                }
            })

    }

}
