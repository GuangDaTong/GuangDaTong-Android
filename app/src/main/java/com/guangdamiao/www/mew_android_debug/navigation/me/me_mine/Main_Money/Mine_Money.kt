package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.Main_Money

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.widget.*
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.chanven.lib.cptr.PtrDefaultHandler
import com.chanven.lib.cptr.PtrFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.MyMoney
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils

class Mine_Money:BaseActivity() {

     private var title_left_imageview_btn: Button? = null
     private var title_center_textview: TextView? = null
     private var title_right_imageview: ImageView? = null
     private var mListView: ListView? = null
     private var myMoneyList=ArrayList<MyMoney>()

    private var pageNow = 0
    private var ptrClassicFrameLayout: PtrClassicFrameLayout? = null
    var handler = Handler()
    private var adapter: MoneyAdapter?=null
    private var no_data_rl:RelativeLayout?=null


    override fun onCreate(savedInstanceState: Bundle?) {
          super.onCreate(savedInstanceState)

          setContentView(R.layout.activity_mine_money)
          StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
          MainApplication.getInstance().addActivity(this)

          initView()
          adapter = MoneyAdapter(this@Mine_Money,myMoneyList)
          mListView!!.setAdapter(adapter!!)//关键代码
          addSomeListener()
          initData()

    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Mine_Money,"金币收益记录")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Mine_Money,"金币收益记录")
        super.onPause()
    }

    private fun initData() {

        ptrClassicFrameLayout!!.postDelayed( { ptrClassicFrameLayout!!.autoRefresh(true) }, 150)

        ptrClassicFrameLayout!!.setPtrHandler(object : PtrDefaultHandler() {

            override fun onRefreshBegin(frame: PtrFrameLayout) {
                handler.postDelayed({
                    pageNow = 0
                    RequestServerList(pageNow,adapter!!,ptrClassicFrameLayout!!)
                    if (!ptrClassicFrameLayout!!.isLoadMoreEnable()) {
                        ptrClassicFrameLayout!!.setLoadMoreEnable(true)
                    }
                }, 200)
            }
        })

        ptrClassicFrameLayout!!.setOnLoadMoreListener{
            handler.postDelayed(
                    {
                        //请求更多页
                        RequestServerList(++pageNow, adapter!!, ptrClassicFrameLayout!!)
                    }, 200)
        }
    }


    private fun RequestServerList(pageNow: Int,adapter:MoneyAdapter,ptrClassicFrameLayout:PtrClassicFrameLayout) {
        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token = read.getString("token", "")
        if(!token.equals("")){
            MoneyRequest.requestFromServer(this@Mine_Money, pageNow,myMoneyList,adapter,ptrClassicFrameLayout,object:IDataRequestListener2String{
                override fun loadSuccess(response_string: String?) {
                        if(response_string.equals("noMoney")){
                            no_data_rl!!.visibility=View.VISIBLE
                        }else{
                            no_data_rl!!.visibility=View.GONE
                        }
                }
            })
        }else{
            val intent = Intent(this@Mine_Money, LoginMain::class.java)
            this@Mine_Money.startActivity(intent)
        }

    }

    fun initView(){

      title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
      title_center_textview = findViewById(R.id.title_center_textview) as TextView
      title_right_imageview=findViewById(R.id.title_right_imageview) as ImageView
      title_center_textview!!.text="金币收益记录"
      no_data_rl=findViewById(R.id.no_data_rl) as RelativeLayout
      title_center_textview!!.visibility= View.VISIBLE
      title_right_imageview!!.setImageResource(R.mipmap.help)
      title_right_imageview!!.visibility= View.VISIBLE
      mListView =findViewById(R.id.mine_money_lv) as ListView
      ptrClassicFrameLayout =findViewById(R.id.list_money_frame) as PtrClassicFrameLayout
     }

    fun addSomeListener(){
         title_right_imageview!!.setOnClickListener{
             val intent: Intent = Intent(this@Mine_Money, Mine_Money_Help::class.java)
             startActivity(intent)
             overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out)

         }
         title_left_imageview_btn!!.setOnClickListener{
             finish()
             overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
         }

     }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

}



