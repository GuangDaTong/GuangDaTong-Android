package com.guangdamiao.www.mew_android_debug.navigation.ask.askWrite

import android.app.Activity
import android.content.res.Resources
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.navigation.me.me_info.ListSimpleAdapter
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils
import java.util.*

class AskWrite_WhoCanSee : Activity() {


    private var mListView: ListView? = null//从别人得到暂用
    private var items: ArrayList<String> = ArrayList<String>()//从别人得到暂用
    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var bundle:Bundle?=null
    private var old_see:String=""
    private val TAG= Constant.ASK_WRITE_TAG



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ask_write_who_can_see)
        MainApplication.getInstance().addActivity(this)
        fullscreen()
        bundle=intent.extras//接收别人传过来的bundle
        if(bundle!!.getString("ask_see")!=null){
            old_see=bundle!!.getString("ask_see")
        }
        initView()
        initValue()
        addSomeListener()
    }

    fun addSomeListener(){
        val itemClickListener= AdapterView.OnItemClickListener { parent, view, position, id ->

            LogUtils.d_debugprint(TAG,"帖子谁可以看："+items[position])
            bundle!!.putString("ask_see",items[position])
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.AskWrite_ChooseSee,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        mListView!!.choiceMode= ListView.CHOICE_MODE_SINGLE
        mListView!!.setOnItemClickListener(itemClickListener)
        title_left_imageview_btn!!.setOnClickListener{
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.AskWrite_ChooseSee,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f

            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f
            }
            false
        }
    }

    fun fullscreen(){
        StatusBarUtils.setWindowStatusBarColor(this,R.color.headline_black)
    }

    fun initView(){
        title_left_imageview_btn=findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview=findViewById(R.id.title_center_textview) as TextView
        mListView=findViewById(R.id.choose_listview) as ListView
    }

    fun initValue(){
        title_center_textview!!.text="谁可以看"
        title_center_textview!!.visibility= View.VISIBLE
        val resources: Resources =resources
        var item=resources.getStringArray(R.array.askWrite_see)
        for(i in item){
            items.add(i)
        }
        mListView!!.adapter= ListSimpleAdapter(applicationContext, items, old_see)//=====适配器植入点
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            intent.putExtras(bundle)//添加到自己的intent
            setResult(Constant.AskWrite_ChooseSee,intent)
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }
}
