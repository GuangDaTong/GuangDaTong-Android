package com.guangdamiao.www.mew_android_debug.navigation.me.me_mine.mine_favorite.find

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.widget.BaseAdapter
import com.guangdamiao.www.mew_android_debug.sweetAlert.SweetAlertDialog
import com.chanven.lib.cptr.PtrClassicFrameLayout
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.bean.ListFindFavoriteAll
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.ask.modle.IDataRequestListener2String
import com.guangdamiao.www.mew_android_debug.utils.JSONFormatUtil
import com.guangdamiao.www.mew_android_debug.utils.JsonUtil
import com.guangdamiao.www.mew_android_debug.utils.MD5Util
import com.guangdamiao.www.mew_android_debug.utils.NetUtil
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Jason_Jan on 2017/12/20.
 */

class Find_ListRequest {

    companion object {


        fun getResultFromServer(activity: Activity, context: Context, TAG: String, urlEnd: String, pageIndexNow: Int, listAll: ArrayList<ListFindFavoriteAll>, adapter: BaseAdapter, ptrClassicFrameLayout: PtrClassicFrameLayout, loading_first:SweetAlertDialog?,listener: IDataRequestListener2String) {

            if(NetUtil.checkNetWork(context)){
                val pageIndex = pageIndexNow
                val pageSize = Constant.PAGESIZE
                var sign = ""
                var urlPath = ""
                val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                val token= read.getString("token", "")
                if (LogUtils.APP_IS_DEBUG) {
                    urlPath = Constant.BASEURLFORTEST + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORTEST+token)
                } else {
                    urlPath = Constant.BASEURLFORRELEASE + urlEnd
                    sign = MD5Util.md5(Constant.SALTFORRELEASE+token)
                }
                val jsonObject= JSONObject()
                jsonObject.put("pageIndex",pageIndex)
                jsonObject.put("pageSize",pageSize)
                jsonObject.put("sign",sign)

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(TAG, Constant.TOSERVER + urlPath + "\n提交的params=" + jsonObject.toString())
                val client= AsyncHttpClient(true,80,443)

                client.post(context,urlPath,stringEntity,Constant.ContentType,object: AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if (loading_first != null) loading_first.cancel()
                            if(!activity.isFinishing){
                                var getResultFromServer =""
                                getResultFromServer=response.toString()
                                LogUtils.d_debugprint(TAG, Constant.GETDATAFROMSERVER + getResultFromServer)
                                val getCode: String
                                var msg: String? = null
                                var result: String
                                var getData: List<Map<String, Any?>>
                                getCode = JsonUtil.get_key_string(Constant.Server_Code, getResultFromServer!!)
                                if (Constant.RIGHTCODE.equals(getCode)) {
                                    msg = JsonUtil.get_key_string(Constant.Server_Msg, getResultFromServer!!)
                                    result = JsonUtil.get_key_string(Constant.Server_Result, getResultFromServer!!)
                                    getData = JsonUtil.getListMap("data", result)//=====这里可能发生异常
                                    LogUtils.d_debugprint(TAG, "json解析出来的对象是=" + JSONFormatUtil.formatJson(getData.toString())+"\n")
                                    if(pageIndexNow==0){
                                        listAll.clear()
                                    }
                                    if (getData != null&&getData.size>0) {
                                        for (i in getData.indices) {
                                            val title_=getData[i].getValue("title").toString()
                                            val id_=getData[i].getValue("id").toString()
                                            val createTime_=getData[i].getValue("createTime").toString()
                                            val link_=getData[i].getValue("link").toString()
                                            val readCount_=getData[i].getValue("readCount").toString()
                                            val zan_=getData[i].getValue("zan").toString()
                                            val isZan_=getData[i].getValue("isZan").toString()
                                            val icon_=getData[i].getValue("icon").toString()
                                            val fee_=getData[i].getValue("fee").toString()
                                            val position_=getData[i].getValue("position").toString()
                                            val description_=getData[i].getValue("description").toString()
                                            val favorID_=getData[i].getValue("favorID").toString()


                                            val listItem = ListFindFavoriteAll()
                                            listItem.title = if (title_==null) "" else title_
                                            listItem.id = if(id_==null) "" else id_
                                            listItem.createTime = if(createTime_==null) "" else createTime_
                                            listItem.fee =if(fee_==null) "" else fee_
                                            listItem.link=if(link_==null) "" else link_
                                            listItem.position=if(position_==null) "" else position_
                                            listItem.description=if(description_==null) "" else description_
                                            listItem.icon=if(icon_==null) "" else icon_
                                            listItem.readCount=if(readCount_==null) "" else readCount_
                                            listItem.zan_=if(zan_==null) "" else zan_
                                            listItem.isZan=if(isZan_==null) "" else isZan_
                                            listItem.favorID=if(favorID_==null) "" else favorID_
                                            listItem.isShow=false
                                            listItem.isChecked=false

                                            listAll.add(listItem)

                                        }//=====所有的对象已经存放到一个List了
                                        listener.loadSuccess(listAll.size.toString())
                                        adapter!!.notifyDataSetChanged()
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    } else if(pageIndexNow==0){
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                        if(urlEnd.equals(Constant.Find_List_FPartTimeJob)){
                                            listener.loadSuccess("noFind1Favorite")
                                        }else if(urlEnd.equals(Constant.Find_List_FTraining)){
                                            listener.loadSuccess("noFind2Favorite")
                                        }else if(urlEnd.equals(Constant.Find_List_FActivity)){
                                            listener.loadSuccess("noFind3Favorite")
                                        }
                                    }else{
                                        listener.loadSuccess(listAll.size.toString())
                                        ptrClassicFrameLayout!!.refreshComplete()
                                        ptrClassicFrameLayout!!.isLoadMoreEnable=true
                                        ptrClassicFrameLayout!!.loadMoreComplete(false)
                                        //Toasty.info(context, "没有更多数据了").show()
                                    }
                                } else if(Constant.LOGINFAILURECODE.equals(getCode)){
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    val read = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
                                    val account=read.getString("account","")
                                    val password=read.getString("password","")
                                    val editor: SharedPreferences.Editor = context.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE).edit()
                                    val editor2: SharedPreferences.Editor=context.getSharedPreferences(Constant.SERVERBROKEN, Context.MODE_APPEND).edit()
                                    editor2.putString("account",account)
                                    editor2.putString("password",password)
                                    editor2.commit()
                                    editor.clear()
                                    editor.commit()
                                    SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                            .setTitleText(Constant.LOGINFAILURETITLE)
                                            .setContentText(Constant.LOGINFAILURE)
                                            .setCustomImage(R.mipmap.mew_icon)
                                            .setConfirmText("确定")
                                            .setConfirmClickListener { sDialog ->
                                                sDialog.dismissWithAnimation()
                                                //跳转到登录，不用传东西过去
                                                val intent = Intent(context, LoginMain::class.java)
                                                context.startActivity(intent)
                                            }
                                            .setCancelText("取消")
                                            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
                                            .show()
                                }else{
                                    adapter!!.notifyDataSetChanged()
                                    ptrClassicFrameLayout!!.refreshComplete()
                                    ptrClassicFrameLayout!!.loadMoreComplete(true)
                                    Toasty.info(context, Constant.SERVERBROKEN).show()
                                }
                            }

                        }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                         ptrClassicFrameLayout!!.refreshComplete()
                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }
                })
            }else{
                if(!activity.isFinishing){
                    if(loading_first!=null) loading_first.cancel()
                    Toasty.info(context,Constant.NONETWORK).show()
                }
            }
        }

        fun deleteFavoriteFind(context: Context,urlEnd: String,IDs:ArrayList<String>,token:String,listener: IDataRequestListener2String){
            if(NetUtil.checkNetWork(context)){
                var sign=""
                var urlPath=""

                if(LogUtils.APP_IS_DEBUG){
                    sign=MD5Util.md5(Constant.SALTFORTEST+token)
                    urlPath=Constant.BASEURLFORTEST+urlEnd
                }else{
                    sign=MD5Util.md5(Constant.SALTFORRELEASE+token)
                    urlPath=Constant.BASEURLFORRELEASE+urlEnd
                }

                val jsonObject=JSONObject()
                val jsonArray= JSONArray()
                for(i in IDs.indices){
                    jsonArray.put(i,IDs[i])
                }
                jsonObject.put("IDs",jsonArray)
                jsonObject.put("sign",sign)

                val stringEntity= StringEntity(jsonObject.toString(),"UTF-8")
                LogUtils.d_debugprint(Constant.Me_Favorite_TAG,Constant.TOSERVER+urlPath+"\n\n提交的参数为："+jsonObject.toString())

                val client=AsyncHttpClient(true,80,443)
                client.post(context,urlPath,stringEntity,Constant.ContentType,object:AsyncHttpResponseHandler(){

                    override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                        if(responseBody!=null){
                            val resultDate = String(responseBody!!, charset("utf-8"))
                            val response=JSONObject(resultDate)
                            if(statusCode.toString().equals(Constant.RIGHTCODE)){
                                val response_string=response.toString()
                                //开始解析
                                LogUtils.d_debugprint(Constant.Me_TAG,"服务器返回删除收藏栏目模块是："+JSONFormatUtil.formatJson(response_string))
                                listener.loadSuccess(response_string)
                            }else{
                                Toasty.info(context,Constant.NONETWORK).show()
                            }
                         }
                    }

                    override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {

                          Toasty.error(context,Constant.REQUESTFAIL).show()
                    }

                })
            }else{
                Toasty.info(context,Constant.NONETWORK).show()
            }
        }

    }
}
