package com.guangdamiao.www.mew_android_debug.navigation.me.me_setting

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import cn.jiguang.analytics.android.api.JAnalyticsInterface
import com.guangdamiao.www.mew_android_debug.R
import com.guangdamiao.www.mew_android_debug.global.Constant
import com.guangdamiao.www.mew_android_debug.global.LogUtils
import com.guangdamiao.www.mew_android_debug.global.MainApplication
import com.guangdamiao.www.mew_android_debug.loginActivity.LoginMain
import com.guangdamiao.www.mew_android_debug.navigation.BaseActivity
import com.guangdamiao.www.mew_android_debug.utils.StatusBarUtils

class Me_Push_Setting : BaseActivity() {

    private var title_left_imageview_btn: Button? = null
    private var title_center_textview: TextView? = null
    private var title_right_imageview: ImageView? = null
    private var push_urgency1_college_btn:Button?=null
    private var push_urgency2_school_btn:Button?=null
    private var setting_push_urgency1:ImageView?=null
    private var setting_push_urgency2:ImageView?=null
    private var push_urgency1_college:TextView?=null
    private var push_urgency2_school:TextView?=null
    private var setting_comment_txt:TextView?=null
    private var setting_message_txt:TextView?=null
    private var setting_activity_txt:TextView?=null
    private var setting_system_txt:TextView?=null
    private var setting_push_comment:ImageView?=null
    private var setting_push_message:ImageView?=null
    private var setting_push_activity:ImageView?=null
    private var setting_push_system:ImageView?=null
    private var flag_college=1
    private var flag_school=1
    private var flag_comment=0
    private var flag_message=0
    private var flag_activity=0
    private var flag_system=0
    private var urgentSchool_bool=true
    private var urgentCollege_bool=true
    private var comment_bool=true
    private var leaveWords_bool=true
    private var activity_bool=true
    private var system_bool=true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_me_push_setting)
        StatusBarUtils.setWindowStatusBarColor(this, R.color.headline_black)
        MainApplication.getInstance().addActivity(this)
        initView()
        addSomeListener()
    }

    override fun onResume(){
        JAnalyticsInterface.onPageStart(this@Me_Push_Setting,"设置-推送设置")
        super.onResume()
    }

    override fun onPause(){
        JAnalyticsInterface.onPageEnd(this@Me_Push_Setting,"设置-推送设置")
        super.onPause()
    }

    private fun initView(){
        title_left_imageview_btn = findViewById(R.id.title_left_imageview_btn) as Button
        title_center_textview = findViewById(R.id.title_center_textview) as TextView
        title_right_imageview = findViewById(R.id.title_right_imageview) as ImageView
        title_center_textview!!.text="推送设置"
        title_right_imageview!!.setImageResource(R.mipmap.help)
        title_center_textview!!.visibility= View.VISIBLE
        title_right_imageview!!.visibility=View.VISIBLE
        setting_push_urgency1=findViewById(R.id.setting_push_urgency1) as ImageView
        setting_push_urgency2=findViewById(R.id.setting_push_urgency2) as ImageView
        push_urgency1_college=findViewById(R.id.push_urgency1_college) as TextView
        push_urgency2_school=findViewById(R.id.push_urgency2_school) as TextView
        push_urgency1_college_btn=findViewById(R.id.setting_push_urgency_btn1) as Button
        push_urgency2_school_btn=findViewById(R.id.setting_push_urgency_btn2) as Button
        setting_comment_txt=findViewById(R.id.setting_comment_txt) as TextView
        setting_message_txt=findViewById(R.id.setting_message_txt) as TextView
        setting_activity_txt=findViewById(R.id.setting_activity_txt) as TextView
        setting_system_txt=findViewById(R.id.setting_system_txt) as TextView
        setting_push_comment=findViewById(R.id.setting_push_comment) as ImageView
        setting_push_message=findViewById(R.id.setting_push_message) as ImageView
        setting_push_activity=findViewById(R.id.setting_push_activity) as ImageView
        setting_push_system=findViewById(R.id.setting_push_system) as ImageView

        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        urgentSchool_bool = read.getBoolean("urgentSchool_bool",true)
        urgentCollege_bool=read.getBoolean("urgentCollege_bool",true)
        comment_bool=read.getBoolean("comment_bool",true)
        leaveWords_bool=read.getBoolean("leaveWords_bool",true)
        activity_bool=read.getBoolean("activity_bool",true)
        system_bool=read.getBoolean("system_bool",true)

        if(urgentSchool_bool){
            flag_school=1
            push_urgency2_school!!.setTextColor(resources.getColor(R.color.black))
            setting_push_urgency2!!.setImageResource(R.mipmap.multiple_choose_green)
        }else{
            flag_school=0
            push_urgency2_school!!.setTextColor(resources.getColor(R.color.huise))
            setting_push_urgency2!!.setImageResource(R.mipmap.multiple_choose_gray)
        }

        if(urgentCollege_bool){
            flag_college=1
            push_urgency1_college!!.setTextColor(resources.getColor(R.color.black))
            setting_push_urgency1!!.setImageResource(R.mipmap.multiple_choose_green)
        }else{
            flag_college=0
            push_urgency1_college!!.setTextColor(resources.getColor(R.color.huise))
            setting_push_urgency1!!.setImageResource(R.mipmap.multiple_choose_gray)
        }

        if(comment_bool){
            flag_comment=0
            setting_push_comment!!.visibility=View.VISIBLE
        }else{
            flag_comment=1
            setting_push_comment!!.visibility=View.GONE
        }

        if(leaveWords_bool){
            flag_message=0
            setting_push_message!!.visibility=View.VISIBLE
        }else{
            flag_message=1
            setting_push_message!!.visibility=View.GONE
        }

        if(activity_bool){
            flag_activity=0
            setting_push_activity!!.visibility=View.VISIBLE
        }else{
            flag_activity=1
            setting_push_activity!!.visibility=View.GONE
        }

        if(system_bool){
            flag_system=0
            setting_push_system!!.visibility=View.VISIBLE
        }else{
            flag_system=1
            setting_push_system!!.visibility=View.GONE
        }
     }

    private fun addSomeListener(){
        title_left_imageview_btn!!.setOnClickListener{
            finish()
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out)
        }

        title_left_imageview_btn!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_left_imageview_btn!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_left_imageview_btn!!.alpha=0.618f


            }else if(event.action== MotionEvent.ACTION_UP){
                title_left_imageview_btn!!.setBackgroundColor(Color.TRANSPARENT)
                title_left_imageview_btn!!.alpha=1.0f

            }
            false
        }

        title_right_imageview!!.setOnTouchListener { v, event ->
            if(event.action== MotionEvent.ACTION_DOWN){
                title_right_imageview!!.setBackgroundColor(resources.getColor(R.color.refreshText))
                title_right_imageview!!.alpha=0.618f


            }else if(event.action== MotionEvent.ACTION_UP){
                title_right_imageview!!.setBackgroundColor(Color.TRANSPARENT)
                title_right_imageview!!.alpha=1.0f

            }
            false
        }

        title_right_imageview!!.setOnClickListener{
           val intent:Intent=Intent(this@Me_Push_Setting, Me_Push_Help::class.java)
           startActivity(intent)
           overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }
        push_urgency1_college_btn!!.setOnClickListener{
            if(flag_college++%2==1){//按钮便成绿色
                urgentCollege_bool=false
                push_urgency1_college!!.setTextColor(resources.getColor(R.color.huise))
                setting_push_urgency1!!.setImageResource(R.mipmap.multiple_choose_gray)
            }else{//按钮变成灰色
                urgentCollege_bool=true
                push_urgency1_college!!.setTextColor(resources.getColor(R.color.black))
                setting_push_urgency1!!.setImageResource(R.mipmap.multiple_choose_green)
            }
        }

        push_urgency2_school_btn!!.setOnClickListener{
            if(flag_school++%2==1){
                urgentSchool_bool=false
                push_urgency2_school!!.setTextColor(resources.getColor(R.color.huise))
                setting_push_urgency2!!.setImageResource(R.mipmap.multiple_choose_gray)
            }else{
                urgentSchool_bool=true
                push_urgency2_school!!.setTextColor(resources.getColor(R.color.black))
                setting_push_urgency2!!.setImageResource(R.mipmap.multiple_choose_green)
            }
        }

        setting_comment_txt!!.setOnClickListener{
            if(flag_comment++%2==1){
                comment_bool=true
                setting_push_comment!!.visibility=View.VISIBLE
            }else{
                comment_bool=false
                setting_push_comment!!.visibility=View.GONE
            }
        }
        setting_message_txt!!.setOnClickListener{
            if(flag_message++%2==1){
                leaveWords_bool=true
                setting_push_message!!.visibility=View.VISIBLE
            }else{
                leaveWords_bool=false
                setting_push_message!!.visibility=View.GONE
            }
        }
        setting_activity_txt!!.setOnClickListener{
            if(flag_activity++%2==1){
                activity_bool=true
                setting_push_activity!!.visibility=View.VISIBLE
            }else{
                activity_bool=false
                setting_push_activity!!.visibility=View.GONE
            }
        }
        setting_system_txt!!.setOnClickListener{
            if(flag_system++%2==1){
                system_bool=true
                setting_push_system!!.visibility=View.VISIBLE
            }else{
                system_bool=false
                setting_push_system!!.visibility=View.GONE
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode== KeyEvent.KEYCODE_BACK){
            this.finish()
            overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out)
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onDestroy(){
        LogUtils.d_debugprint(Constant.Me_TAG,"\n在我的推送设置中\n 设置的急问通知选择的urgentSchool是"+urgentSchool_bool+"\n urgentCollege是 "+urgentCollege_bool+"\ncomment是 "+comment_bool+"\nleaveWords是 "+leaveWords_bool+"\nactivity_bool是 "+activity_bool+"\nsystem_bool是 "+system_bool)
        val read = this.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_PRIVATE)
        val token = read.getString("token", "")
        if(!token.equals("")){
            val params=HashMap<String,Boolean>()
            params.put("urgentSchool",urgentSchool_bool)
            params.put("urgentCollege",urgentCollege_bool)
            params.put("comment",comment_bool)
            params.put("leaveWords",leaveWords_bool)
            params.put("activity",activity_bool)
            params.put("system",system_bool)

            MeSettingRequest.RequestSetPush(this@Me_Push_Setting,token,params)

            val editor: SharedPreferences.Editor=this@Me_Push_Setting.getSharedPreferences(Constant.ShareFile_UserInfo, Context.MODE_APPEND).edit();
            editor.putBoolean("urgentSchool_bool",params.get("urgentSchool")!!)
            editor.putBoolean("urgentCollege_bool",params.get("urgentCollege")!!)
            editor.putBoolean("comment_bool",params.get("comment")!!)
            editor.putBoolean("leaveWords_bool",params.get("leaveWords")!!)
            editor.putBoolean("activity_bool",params.get("activity")!!)
            editor.putBoolean("system_bool",params.get("system")!!)
            editor.commit()

        }else{
            val intent:Intent=Intent(this@Me_Push_Setting,LoginMain::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out)
        }
        super.onDestroy()
    }
}
