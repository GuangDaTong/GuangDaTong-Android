

1.compile files('libs/universal-image-loader-1.9.2-SNAPSHOT-with-sources.jar')
  介绍：加载网络图片，非常好用，一行代码搞定
  github网站：https://github.com/nostra13/Android-Universal-Image-Loader
  参考博客 http://blog.csdn.net/xiaanming/article/details/26810303


3. compile 'com.loopj.android:android-async-http:1.4.9'
  介绍：网络请求 第三方库(非常棒，10000的stars)
  github网站：https://github.com/loopj/android-async-http

4.遗弃！

5. compile 'cn.pedant.sweetalert:library:1.3'
   介绍：弹出对话框，可以自定义图标，警告，加载等，很好看。
   github网站：https://github.com/pedant/sweet-alert-dialog
   参考博客：http://blog.csdn.net/zhangli_/article/details/50131883


6.compile 'com.chanven.lib:cptr:1.1.0'
   介绍：下拉刷新，上拉加载
   github网站：https://github.com/Chanven/CommonPullToRefresh


7.compile 'com.github.pinguo-zhouwei:CustomPopwindow:2.1.1'
   介绍：弹出pop框=====很简单，只有一个类
   github网站：https://github.com/pinguo-zhouwei/CustomPopwindow


8. compile 'com.commit451:PhotoView:1.2.4'
   介绍：图片缩放处理，10000多的starts,极其好用
   github网站：https://github.com/chrisbanes/PhotoView
   参考博客：http://blog.csdn.net/true100/article/details/50605692


9. compile 'com.liaoinstan.springview:library:1.2.6'
   介绍：ScrollView的上拉加载，下拉刷新
   github网站：https://github.com/liaoinstan/SpringView
   参考博客：http://blog.csdn.net/ss1168805219/article/details/53559611

10.帖子图片发表=====参考博客=====http://blog.csdn.net/klxh2009/article/details/51254771

11. compile 'com.makeramen:roundedimageview:2.3.0'
    介绍：圆角图片，比较出名，有5000+stars
    github网站：https://github.com/JasonToJan/RoundedImageView

12.compile 'com.github.traex.rippleeffect:library:1.3'
    介绍：android点击动画，如button,imageview,效果如同水波一样
    github网站：https://github.com/traex/RippleEffect/network
    舍弃了，因为不好用，条件比较苛刻。

13.compile 'com.github.lovetuzitong:MultiImageSelector:1.2'
    介绍：图片选择，用来发表帖子的图片选择
    github网站：https://github.com/lovetuzitong/MultiImageSelector

14.compile 'com.soundcloud.android:android-crop:1.0.1@aar'
    介绍：android图片裁剪第三方库-简单好用！！！
    github网站：https://github.com/jdamcd/android-crop





